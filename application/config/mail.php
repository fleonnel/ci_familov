<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$config['smtpHost'] = 'send.one.com';
$config['smtpPort'] = '465';
$config['smtpSecurity'] = 'ssl';
$config['smtpUsername'] = SMTP_MAIL_ID;
$config['smtpPassword'] = SMTP_MAIL_PASSWORD;
?>