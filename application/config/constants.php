<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
//if($_SERVER['HTTP_HOST'] == "localhost") {
$main_url = (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['HTTP_HOST'];
	$main_url .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);

//}
//else {
	//$main_url = "";
//}


/* NEW Data Setup */
/**
 * FOR common Setup
 **/
if (!defined('ASSETS')) define('ASSETS', 'assets/');
if (!defined('ASSETS_BUILD')) define('ASSETS_BUILD', ASSETS.'build/');
if (!defined('ASSETS_BUILD_CSS')) define('ASSETS_BUILD_CSS', ASSETS.'build/CSS/');
if (!defined('ASSETS_BUILD_JS')) define('ASSETS_BUILD_JS', ASSETS.'build/js/');
if (!defined('EXTRA_PLUGIN')) define('EXTRA_PLUGIN', $main_url . ASSETS.'ex_plugins/');
if (!defined('A_TAG_DISABLE')) define('A_TAG_DISABLE','javascript:void(0)');
if (!defined('ASSETS_COMMON')) define('ASSETS_COMMON', ASSETS . 'common/');
/**
* FOR ADMIN Panel
**/
if (!defined('SESSION_NAME')) define('SESSION_NAME', 'BB_ADMIN');
if (!defined('SESSION_SITE')) define('SESSION_SITE', 'FAMILOV');
if (!defined('ASSETS_ADMIN')) define('ASSETS_ADMIN', ASSETS.'admin/');


/* OLD Data Setup */
define('MAIN_URL', $main_url);

//seller path
define('ASSETS_URL', MAIN_URL.'assets/');
define('JS_URL', ASSETS_URL.'js/');
define('JS_PLUGIN_URL', ASSETS_URL.'plugins/');
define('CSS_URL', ASSETS_URL.'css/');
define('CSS_BUILD_URL',ASSETS_URL.'build/css/');
define('CSS_BUILD_IMAGE',ASSETS_URL.'build/images/');
define('CSS_BUILD_JS',ASSETS_URL.'build/js/');

//Admin path
define('HTTP_ASSETS_PATH_ADMIN', MAIN_URL.'assets/');
define('HTTP_CSS_PATH_ADMIN', MAIN_URL.'assets/admin/css/');
define('HTTP_IMAGES_PATH_ADMIN', MAIN_URL.'assets/admin/images/');
define('HTTP_JS_PATH_ADMIN', MAIN_URL.'assets/js/');
#Front
if (!defined('ASSETS_FRONT')) define('ASSETS_FRONT', ASSETS . 'front/');
define('ASSETS_URL_FRONT', MAIN_URL . ASSETS_FRONT);

# External Plugins
define('EX_PLUGINS', ASSETS_URL . 'ex_plugins/');


define('DIR_UPLOAD_PATH', FCPATH . "uploads/");
define('DIR_UPLOAD_URL', $main_url . "uploads/");
define('DIR_CATEGORY_PATH', DIR_UPLOAD_PATH . "category/");
define('DIR_CATEGORY_URL', DIR_UPLOAD_URL . "category/");
define('DIR_ATTACHMENT_PATH', DIR_UPLOAD_PATH . "attachment/");
define('DIR_ATTACHMENT_URL', DIR_UPLOAD_URL . "attachment/");
define('DIR_PRODUCT_PATH', DIR_UPLOAD_PATH . "products/");
define('DIR_PRODUCT_URL', DIR_UPLOAD_URL . "products/");
define('DIR_SHOP_PATH', DIR_UPLOAD_PATH . "shop/");
define('DIR_SHOP_URL', DIR_UPLOAD_URL . "shop/");
define('DIR_BANNER_PATH', DIR_UPLOAD_PATH . "banner/");
define('DIR_BANNER_URL', DIR_UPLOAD_URL . "banner/");
define('DIR_ORDER_BILL_PATH', DIR_UPLOAD_PATH . "order_bill/");
define('DIR_ORDER_BILL_URL', DIR_UPLOAD_URL . "order_bill/");
define('PREFIX_ATTACHMENT', "FAMILOV_");
define('ATTACHMENT_MAX_FILE_SIZE', 10485760);// 104857600 = 100 MB (1 MB = 1048576 Bytes, 10 MB = 10485760)


define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


define('ASCENDING', 'asc');
define('DESCENDING', 'desc');
define('CURRENCY_SIGN_DOLLAR', '$');
define('SUPER_ADMIN_ROLE_ID', 1);
define('ADMIN_ROLE_ID', 2);


define('NO_IMG', "_no_img.jpg");
define('NO_IMG_COVER', "cover_no_img.jpg");
define('DEFAULT_PHOTO', DIR_UPLOAD_URL . "default/");
define('DEFAULT_PHOTO_COVER', DIR_UPLOAD_URL . "default/");

define('MAP_API_KEY', '');
define('RECORD_LIMIT', '2');

define('ACCEPT_IMG', 'image/gif, image/jpeg, image/png');

/* Admin Setting Section */
if (!defined('SETTING_TYPE_GENERAL')) define('SETTING_TYPE_GENERAL', 'General');
if (!defined('SETTING_TYPE_Email')) define('SETTING_TYPE_Email', 'Email');
if (!defined('SETTING_TYPE_SMS')) define('SETTING_TYPE_SMS', 'SMS');

if (!defined('SITE_NAME_COM')) define('SITE_NAME_COM', 'familov.com');
if (!defined('MAIL_SITE_NAME')) define('MAIL_SITE_NAME', 'familov.com');
if (!defined('MAIL_CONTACT')) define('MAIL_CONTACT', 'familovmarket@gmail.com');
if (!defined('MAIL_HELLO')) define('MAIL_HELLO', 'hello@familov.com');


if (!defined('STATUS_CUSTOMER_ACTIVE')) define('STATUS_CUSTOMER_ACTIVE', 'Active');
if (!defined('SERVICE_FEES')) define('SERVICE_FEES', 2.50);

if (!defined('PAYMENT_STRIPE')) define('PAYMENT_STRIPE', 'Stripe');
if (!defined('PAYMENT_PAYPAL')) define('PAYMENT_PAYPAL', 'Paypal');
if (!defined('PAYMENT_BANK_TRANSFER')) define('PAYMENT_BANK_TRANSFER', 'Bank Transfer');
if (!defined('PAYMENT_SEPA_DD')) define('PAYMENT_SEPA_DD', 'SEPA Direct Debit');
if (!defined('PAYMENT_SOFORT')) define('PAYMENT_SOFORT', 'SOFORT');
if (!defined('PAYMENT_BANCONTACT')) define('PAYMENT_BANCONTACT', 'BANCONTACT');

if (!defined('ORDER_TEXT')) define('ORDER_TEXT', 'F-');

if (!defined('ORDER_PAYMENT_STATUS_SUCCESS')) define('ORDER_PAYMENT_STATUS_SUCCESS', 'SUCCESS');
if (!defined('ORDER_PAYMENT_STATUS_FAILED')) define('ORDER_PAYMENT_STATUS_FAILED', 'FAILED');

/** SMTP **/

if (!defined('SMTP_MAIL_ID')) define('SMTP_MAIL_ID', 'replysmtp@familov.com');
if (!defined('SMTP_MAIL_PASSWORD')) define('SMTP_MAIL_PASSWORD', 'devsmtp@2017');


/** Strip **/
/*if (!defined('STRIPE_MODE')) define('STRIPE_MODE', 'Test'); // Live OR Test
if (!defined('STRIPE_SITE_NAME')) define('STRIPE_SITE_NAME', 'familov.com');*/

/*if (!defined('STRIPE_SECRET_KEY_TEST')) define('STRIPE_SECRET_KEY_TEST', 'sk_test_8kR1cxsuz9ZylxLGEdHIOe1v');
if (!defined('STRIPE_PUBLIC_KEY_TEST')) define('STRIPE_PUBLIC_KEY_TEST', 'pk_test_zJeMJhXgQCvHy1h6PevnfQZV');*/

/*if (!defined('STRIPE_SECRET_KEY_TEST')) define('STRIPE_SECRET_KEY_TEST', 'sk_test_PjXblK5gvDtXKbN0LAm9c5kS'); #NEW STRIPE LIB
if (!defined('STRIPE_PUBLIC_KEY_TEST')) define('STRIPE_PUBLIC_KEY_TEST', 'pk_test_6KirUNVpAYNCSQj0o6TXY9Mz');

if (!defined('STRIPE_SECRET_KEY_LIVE')) define('STRIPE_SECRET_KEY_LIVE', 'sk_live_3PcyvtyFpQyJ2nwEei2tO5gM');
if (!defined('STRIPE_PUBLIC_KEY_LIVE')) define('STRIPE_PUBLIC_KEY_LIVE', 'pk_live_AkmrND1lKDrPAYzmdmWrnjiG');*/
/*if (!defined('STRIPE_MODE')) define('STRIPE_MODE', 'Test'); // Live OR Test
if (!defined('STRIPE_SITE_NAME')) define('STRIPE_SITE_NAME', 'familov.com');
if (STRIPE_MODE == 'Test') {
	if (!defined('STRIPE_SECRET_KEY_TEST')) define('STRIPE_SECRET_KEY_TEST', 'sk_test_8kR1cxsuz9ZylxLGEdHIOe1v');
	if (!defined('STRIPE_PUBLIC_KEY_TEST')) define('STRIPE_PUBLIC_KEY_TEST', 'pk_test_zJeMJhXgQCvHy1h6PevnfQZV');

} else {
	if (!defined('STRIPE_SECRET_KEY_LIVE')) define('STRIPE_SECRET_KEY_LIVE', 'sk_test_8kR1cxsuz9ZylxLGEdHIOe1v');
	if (!defined('STRIPE_PUBLIC_KEY_LIVE')) define('STRIPE_PUBLIC_KEY_LIVE', 'pk_test_zJeMJhXgQCvHy1h6PevnfQZV');

}*/
/*** Order Mode */
if (!defined('ORDER_STATUS_WAIT')) define('ORDER_STATUS_WAIT', 'Wait'); #Awaiting for payment
if (!defined('ORDER_STATUS_BUY')) define('ORDER_STATUS_BUY', 'Buy'); #Payment accepted
if (!defined('ORDER_STATUS_IN_PROCESS')) define('ORDER_STATUS_IN_PROCESS', 'In Process'); #In Process
if (!defined('ORDER_STATUS_DELIVERED')) define('ORDER_STATUS_DELIVERED', 'Delivered'); #Delivered
if (!defined('ORDER_STATUS_CANCEL')) define('ORDER_STATUS_CANCEL', 'Cancel'); #Cancel
if (!defined('PRODUCT_STATUS_PUBLISHED')) define('PRODUCT_STATUS_PUBLISHED', 'PUBLISHED');
if (!defined('PRODUCT_STATUS_UNPUBLISHED')) define('PRODUCT_STATUS_UNPUBLISHED', 'UNPUBLISHED');
if (!defined('PRODUCT_STATUS_AVAILABLE')) define('PRODUCT_STATUS_AVAILABLE', 'AVAILABLE');
if (!defined('PRODUCT_STATUS_NOT_AVAILABLE')) define('PRODUCT_STATUS_NOT_AVAILABLE', 'NOT AVAILABLE');


if (!defined('FAMILOV_ADDRESS')) define('FAMILOV_ADDRESS', '<strong>Familov, Inc.</strong><br>

Phone: ( +49) 15259948834<br>
Email: hello@familov.com');
if (!defined('FAMILOV_ADDRESS_MANAGER')) define('FAMILOV_ADDRESS_MANAGER', 'Familov LIMT <br>
');
/* End of file constants.php */
/* Location: ./application/config/constants.php */
