<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'front/home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
foreach (['admin', 'cron', 'front'] as $value) {
	$route[$value] = $value.'/home';
}
/**
 *  Cron Section
 **/
$route['cron/home'] = 'cron/home';

/**
 * Super Admin Section
 */

$route['admin/login'] = 'admin/home/login';
$route['admin/reset-password'] = 'admin/home/resetPasswordLink';
$route['admin/forgot-password'] = 'admin/home/forgot_password';
$route['admin/test-mail'] = 'admin/home/test_mail';

#$route['admin/email-template'] = 'admin/email_template/lists';

/* Front Side */
$route['home'] = 'front/home';
$route['faq'] = 'front/cms/faq';
$route['how-it-works'] = 'front/cms/how_it_works';
$route['terms'] = 'front/cms/terms';
$route['privacy'] = 'front/cms/privacy';
$route['about-us'] = 'front/cms/about_us';
$route['contact-us'] = 'front/cms/contact_us';
$route['press'] = 'front/cms/press';
$route['jobs'] = 'front/cms/jobs';

$route['language-change/(:any)'] = 'front/LanguageSwitcher/switchLang/$1';
$route['currency-change/(:any)'] = 'front/LanguageSwitcher/switch_currency/$1';
$route['sign-up'] = 'front/home/sign_up';
$route['sign-up/(:any)'] = 'front/home/sign_up/$1';
$route['log-in'] = 'front/home/log_in';
$route['log-out'] = 'front/home/log_out';
$route['forgot-password'] = 'front/home/forgot_password';
$route['forgot-password-send'] = 'front/home/forgot_password_send';
$route['my-account'] = 'front/home/my_account';
$route['my-account-update'] = 'front/home/my_account_update';
$route['change-password'] = 'front/home/change_password';
$route['change-password-update'] = 'front/home/change_password_update';
$route['invite-friends'] = 'front/home/invite_friends';
$route['reset-password'] = 'front/home/reset_password';
$route['reset-password/(:any)'] = 'front/home/reset_password/$1';
$route['reset-password/(:any)/(:any)'] = 'front/home/reset_password/$1/$2';
$route['reset-password-update'] = 'front/home/reset_password_update';

$route['get-city-list'] = 'front/home/get_city_list';
$route['get-shop-list'] = 'front/home/get_shop_list';

$route['product-listing'] = 'front/product/listing';
$route['product-list'] = 'front/product/product_list';
$route['product-list/(:num)'] = 'front/product/product_list/$1';
$route['product-list/(:num)/(:num)'] = 'front/product/product_list/$1/$2';
$route['product-list/(:num)/(:num)/(:num)'] = 'front/product/product_list/$1/$2/$3';
$route['product-list/(:num)/(:num)/(:num)/(:any)'] = 'front/product/product_list/$1/$2/$3/$4';
$route['product-list/(:num)/(:num)/(:num)/(:any)/(:any)'] = 'front/product/product_list/$1/$2/$3/$4/$5';

$route['product-search'] = 'front/product/product_search';

$route['product-detail'] = 'front/product/product_detail';
$route['product-detail/(:any)'] = 'front/product/product_detail/$1';
$route['product-detail/(:any)/(:num)'] = 'front/product/product_detail/$1/$2';

$route['view-order'] = 'front/product/order_list';
$route['order-detail-modal'] = 'front/product/order_detail_pop_up';

$route['shop-change'] = 'front/product/shop_change';
$route['shop-change/(:num)/(:num)/(:num)'] = 'front/product/shop_change/$1/$2/$3';
$route['shop-change-page'] = 'front/product/shop_change_page';
$route['shop-change/(:any)'] = 'front/product/shop_change/$1';
$route['add-to-cart'] = 'front/product/add_to_cart';
$route['view-cart'] = 'front/product/view_cart';
$route['update-cart'] = 'front/product/update_cart';
$route['delete-cart-item'] = 'front/product/delete_cart_item';
$route['delete-cart-item/(:num)'] = 'front/product/delete_cart_item/$1';
$route['checkout'] = 'front/product/checkout';
$route['order-delivery'] = 'front/product/order_delivery';
$route['promo-code'] = 'front/product/order_promo_code';
$route['checkout-proceed'] = 'front/product/checkout_proceed';
$route['order-confirmation'] = 'front/product/order_confirmation';
$route['paypal-return'] = 'front/product/paypal_return';
$route['paypal-return/(:any)'] = 'front/product/paypal_return/$1';
$route['sofort-return'] = 'front/product/sofort_return';
$route['sofort-return/(:any)'] = 'front/product/sofort_return/$1';
$route['bancontact-return'] = 'front/product/bancontact_return';
$route['bancontact-return/(:any)'] = 'front/product/bancontact_return/$1';

$route['client/feedback'] = 'front/home/feedback';
$route['client/feedback/(:num)'] = 'front/home/feedback/$1';
$route['client/feedback/(:num)/(:any)'] = 'front/home/feedback/$1/$2';
$route['client/feedback/(:num)/(:any)/(:any)'] = 'front/home/feedback/$1/$2/$3';





