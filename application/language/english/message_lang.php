<?php
/***
 * Created by PhpStorm.
 * User: Vikrant Parmar
 * Date: 10/26/2017
 * Time: 6:06 PM
 **/

/** Menu **/
$lang['LBL_MENU_01'] = 'Help';
$lang['LBL_MENU_02'] = 'Sign Up';
$lang['LBL_MENU_03'] = 'Log In';
$lang['LBL_MENU_04'] = 'My Account';
$lang['LBL_MENU_05'] = 'Change Password';
$lang['LBL_MENU_06'] = 'Logout';
$lang['LBL_MENU_07'] = 'My Orders';
$lang['LBL_MENU_08'] = 'Invite Friends';
$lang['LBL_MENU_09'] = 'How it works';
$lang['LBL_MENU_10'] = 'Support';
$lang['LBL_MENU_11'] = 'General terms and conditions';
$lang['LBL_MENU_12'] = 'Privacy';
$lang['LBL_MENU_13'] = 'About Us';
$lang['LBL_MENU_14'] = 'Contact';
$lang['LBL_MENU_15'] = 'Press';
$lang['LBL_MENU_16'] = 'Jobs';

/** Social **/
$lang['LBL_SOCIAL_FACEBOOK'] = 'Facebook';
$lang['LBL_SOCIAL_TWITTER'] = 'Twitter';

/** Footer **/
$lang['LBL_FOOTER_01'] = date('Y') . ' © ' . @SITE_NAME_COM;
$lang['LBL_FOOTER_02'] = 'Made with  <i class="fa fa-heart wow pulse" data-wow-iteration="30" style="color: rgb(236, 30, 115); visibility: visible; animation-iteration-count: 30; animation-name: pulse;"></i> by Familover';


/** Home Page First Section **/
$lang['welcome_message'] = 'Welcome to Familov';
$lang['LBL_HERO_ONE'] = 'Hello,';
$lang['LBL_HERO_TWO'] = 'How do you want to support<br>your loved ones today ? ';
$lang['LBL_HERO_THREE'] = 'Purchase from abroad, foodstuffs,hygiene product<br>
                           school, health services... we deliver your loved ones<br>
                           home in less than 36 hours.';

$lang['LBL_RECEIVER_COUNTRY'] = 'Receiver Country';
$lang['LBL_RECEIVER_CITY'] = 'Receiver City';
$lang['LBL_CHOOSE_SHOP'] = 'Choose a shop';
$lang['LBL_RECEIVER_GO'] = 'Go !';
$lang['LBL_SEARCH_VALIDATION_COUNTRY'] = 'Please select country';
$lang['LBL_SEARCH_VALIDATION_CITY'] = 'Please select city';
$lang['LBL_SEARCH_VALIDATION_SHOP'] = 'Please select shop';
$lang['LBL_SEARCH_CITY_LIST'] = 'City not found';
$lang['LBL_SEARCH_SHOP_LIST'] = 'Shop not found';

$lang['LBL_HOME_01'] = 'Unbeatable Benefits';
$lang['LBL_HOME_02'] = 'Send more , buy less';
$lang['LBL_HOME_03'] = 'Save up to 95% vs. traditional money transfer provider and local price Garantie !';
$lang['LBL_HOME_04'] = 'Money well used';
$lang['LBL_HOME_05'] = 'guarantees that the money will be used for the right purpose';
$lang['LBL_HOME_06'] = 'Safe & secure';
$lang['LBL_HOME_07'] = 'We use an SSL certificate encryption.Your money is in safe hands';
$lang['LBL_HOME_08'] = 'Easy to use';
$lang['LBL_HOME_09'] = 'Whether on desktop or mobile you’ll always know what your money do';
$lang['LBL_HOME_10'] = 'Positive impact';
$lang['LBL_HOME_11'] = 'You do something positive for people to improve local economic';
$lang['LBL_HOME_12'] = 'Help people';
$lang['LBL_HOME_13'] = 'Easy send food and basic needs to orphan, school other hospital';
$lang['LBL_HOME_14'] = 'Create an account, it\'s free!';


$lang['LBL_HOME_15'] = '" I have been living in Europe for 4 years and I work so hard every day to support my family but for many reasons it is not always easy and obvious for me to send money home. Familov offers me a new way to take care of my loved ones and now I take pleasure in helping them because I can do more for them every day. "';
$lang['LBL_HOME_16'] = '#BeAFamilover';

/** Home Page How It Work Section **/
$lang['LBL_HOW_IT_WORKS_01'] = 'How it works ?';
$lang['LBL_HOW_IT_WORKS_02'] = 'Are you familiar with the heavy costs of money transfer services and Frustrated by improper';
$lang['LBL_HOW_IT_WORKS_03'] = 'utilization of remittances?';
$lang['LBL_HOW_IT_WORKS_04'] = 'Choose products';
$lang['LBL_HOW_IT_WORKS_05'] = 'Choose what you wish to buy and provide details of the recipient. Full name and Phone will be required.';
$lang['LBL_HOW_IT_WORKS_06'] = 'Pay securely';
$lang['LBL_HOW_IT_WORKS_07'] = 'Select the payment method suitable for you. You can pay via Visa or Master Card or use other online money wallet like PayPal.';
$lang['LBL_HOW_IT_WORKS_08'] = 'SMS Removal Code';
$lang['LBL_HOW_IT_WORKS_09'] = 'Our system send detailed information about the package to the local store and send the withdrawal code to the package recipient.';
$lang['LBL_HOW_IT_WORKS_10'] = 'Delivery';
$lang['LBL_HOW_IT_WORKS_11'] = 'The package is prepared and the recipient is contacted within 36 hours * for delivery and you receive a confirmation of a successful delivery.';


/** Home Page Testimonial Section **/
$lang['LBL_TESTIMONIAL_01'] = 'Join thousands of happy families';
$lang['LBL_TESTIMONIAL_02'] = 'Familov is the new way families are staying connected. We\'re quickly growing across the country, and we always offer what the really need. Just ask!';

$lang['LBL_TESTIMONIAL_PERSON_1_01'] = 'Mary';
$lang['LBL_TESTIMONIAL_PERSON_1_02'] = 'Student, USA';
$lang['LBL_TESTIMONIAL_PERSON_1_03'] = 'Familov has helped a great deal. The cost of sending money to my family in Africa was almost draining me, but since I started using Familov, my family gets food at their doorstep and the cost is now manageable.';

$lang['LBL_TESTIMONIAL_PERSON_2_01'] = 'Michael Francis';
$lang['LBL_TESTIMONIAL_PERSON_2_02'] = 'Ingenieur, Germany';
$lang['LBL_TESTIMONIAL_PERSON_2_03'] = 'It is now cheap and convenient to feed my family back home thanks to Familov. I can buy whatever I want for my family from just a click in my computer.';


$lang['LBL_TESTIMONIAL_PERSON_3_01'] = 'Annita';
$lang['LBL_TESTIMONIAL_PERSON_3_02'] = 'Assistant, France';
$lang['LBL_TESTIMONIAL_PERSON_3_03'] = 'I like to give a helping hand to my family in the country. The notifications that I receive in relation to the delivery reassure me in addition, I can propose other products. Thank you.';

/** FAQ Page **/
$lang['PAGE_TITLE_FAQ'] = 'FAQ : ' . @SITE_NAME_COM;
$lang['LBL_FAQ_01'] = 'Frequently Answered Questions';
$lang['LBL_FAQ_02'] = 'Orders';
$lang['LBL_FAQ_03'] = 'Payment';


/** How it works Page **/
$lang['PAGE_TITLE_HIW'] = 'How it works : ' . @SITE_NAME_COM;
$lang['LBL_HIW_01'] = 'How it works?';
$lang['LBL_HIW_02'] = 'If you are in a foreign country with an aim of helping your family back at home, then you are familiar with the heavy costs of money transfer services and Frustrated by improper utilization of remittances.<br>
Be more efficient and save up to 95%
vs. traditional money transfer provider';
$lang['LBL_HIW_03'] = 'Choose products';
$lang['LBL_HIW_04'] = 'Choose what you wish to buy and provide details of the recipient. Full name and Phone will be required.';
$lang['LBL_HIW_05'] = 'Pay securely';
$lang['LBL_HIW_06'] = 'Select the payment method suitable for you. You can pay via Visa or Master Card or use other online money wallet like PayPal.';
$lang['LBL_HIW_07'] = 'SMS Removal Code';
$lang['LBL_HIW_08'] = 'Our system send detailed information about the package to the local store and send the withdrawal code to the package recipient.';
$lang['LBL_HIW_09'] = 'Delivery';
$lang['LBL_HIW_10'] = 'The package is prepared and the recipient is contacted within 36 hours * for delivery and you receive a confirmation of a successful delivery.';
$lang['LBL_HIW_11'] = 'IMPORTANT';
$lang['LBL_HIW_12'] = '*36 hours subject to the availability of the recipient !
Lorsque une commande est effectuée,nous disposons de maximum 36 heures pour la preparer et contacter le bénéficiaire, ';
$lang['LBL_HIW_13'] = 'c´est pourquoi il est IMPORTANT que vos proches attendent d´être appelés avant de se rendre au magasin munis d´une pièce d´identité pour le retrait.';
$lang['LBL_HIW_14'] = 'Merci';
$lang['LBL_HIW_15'] = 'Create an account, it\'s free!';
$lang['LBL_HIW_16'] = 'Why you’re making the right choice?';
$lang['LBL_HIW_17'] = 'Send more , buy less';
$lang['LBL_HIW_18'] = 'Save up to 95% vs. traditional money transfer provider and local price Garantie !';
$lang['LBL_HIW_19'] = 'Money well used';
$lang['LBL_HIW_20'] = 'guarantees that the money will be used for the right purpose';
$lang['LBL_HIW_21'] = 'Safe & secure';
$lang['LBL_HIW_22'] = 'We use an SSL certificate encryption.Your money is in safe hands';
$lang['LBL_HIW_23'] = 'Easy to use';
$lang['LBL_HIW_24'] = 'Whether on desktop or mobile you’ll always know what your money do .';
$lang['LBL_HIW_25'] = 'Positive impact';
$lang['LBL_HIW_26'] = 'You do something positive for people to improve local economic.';
$lang['LBL_HIW_27'] = 'Help people';
$lang['LBL_HIW_28'] = 'Easy send food and basic needs to orphan, school other hospital';


/** Terms Page **/
$lang['PAGE_TITLE_TERMS'] = 'Terms and Conditions : ' . @SITE_NAME_COM;
$lang['LBL_TERMS_01'] = 'Terms & Conditions';

/** Privacy Page **/
$lang['PAGE_TITLE_PRIVACY'] = 'Privacy Policy : ' . @SITE_NAME_COM;
$lang['LBL_PRIVACY_POLICY_01'] = 'Privacy Policy';

/** About Us Page **/
$lang['PAGE_TITLE_ABOUT_US'] = 'About Us : ' . @SITE_NAME_COM;
$lang['LBL_ABOUT_US_01'] = 'About Us';

/** Contact Us Page **/
$lang['PAGE_TITLE_CONTACT_US'] = 'Contact Us : ' . @SITE_NAME_COM;
$lang['LBL_CONTACT_US_01'] = 'Contact Us';


/** Press Page **/
$lang['PAGE_TITLE_PRESS'] = 'Press : ' . @SITE_NAME_COM;
$lang['LBL_PRESS_01'] = 'Familov is featured by';
$lang['LBL_PRESS_02'] = 'Continue Reading ...';
$lang['LBL_PRESS_03'] = '" Currently, many students send money home. This is expensive, and the money is often not used to buy food ... "';


/** Jobs Page **/
$lang['PAGE_TITLE_JOBS'] = 'Jobs : ' . @SITE_NAME_COM;
$lang['LBL_JOBS_01'] = 'Why join our team?';
$lang['LBL_JOBS_02'] = 'Be part of a young, ambitious, dynamic team. <br><br>

Participate in the development of an innovative project with strong international potential.<br><br>

Take part in an entrepreneurial adventure with a real game changing mission "Geld tranfert alternative !
<br><br>

Take part in an social and entrepreneurial adventure with a real changing mission : Giving more than money 1';


/** Sign Up  Page **/
$lang['PAGE_TITLE_SIGN_UP'] = 'Sign Up: ' . @SITE_NAME_COM;
$lang['LBL_SIGN_UP_01'] = 'Create your account. It\'s free.';
$lang['LBL_SIGN_UP_02'] = 'First name';
$lang['LBL_SIGN_UP_03'] = 'Last name';
$lang['LBL_SIGN_UP_04'] = 'E-mail';
$lang['LBL_SIGN_UP_05'] = 'Your mobile number';
$lang['LBL_SIGN_UP_06'] = 'Password';
$lang['LBL_SIGN_UP_07'] = 'Confirm Password';
$lang['LBL_SIGN_UP_08'] = 'Create Familov Account';
$lang['LBL_SIGN_UP_09'] = 'By clicking <a href="' . @MENU_TERMS . '" class="f-w-700" style="color:#36cd75">Terms</a> and <a href="' . @MENU_PRIVACY . '" class="f-w-700" style="color:#36cd75">Privacy Policy</a>.';
$lang['LBL_SIGN_UP_10'] = 'Already have a Familov account? <a href="' . @MENU_LOG_IN . '" class="f-w-700" style="color:#36cd75">Sign in</a>.';

$lang['LBL_SIGN_UP_VALIDATION_01'] = 'Please Enter First Name!';
$lang['LBL_SIGN_UP_VALIDATION_02'] = 'Please Enter Last Name!';
$lang['LBL_SIGN_UP_VALIDATION_03'] = 'Please Enter Email Id!';
$lang['LBL_SIGN_UP_VALIDATION_04'] = 'Please Enter Valid Email Id!';
$lang['LBL_SIGN_UP_VALIDATION_05'] = 'Please Enter Mobile Number!';
$lang['LBL_SIGN_UP_VALIDATION_06'] = 'Please Enter Password!';
$lang['LBL_SIGN_UP_VALIDATION_07'] = 'Please Enter Confirm Password!';
$lang['LBL_SIGN_UP_VALIDATION_08'] = 'Confirm Password is do not match!';
$lang['LBL_SIGN_UP_VALIDATION_09'] = 'Password must be at least 8 characters!';
$lang['LBL_SIGN_UP_VALIDATION_10'] = 'Only allow numbers!';

$lang['MSG_SIGN_UP_EMAIL_EXIST'] = 'Email id is already existing with us. Please try with another one.';
$lang['MSG_SIGN_UP_SUCCESS'] = '<strong>Congratulation!!</strong> <br>You have been successfully registered with us. Thank You!';
$lang['MSG_SIGN_UP_FAILED'] = 'Sorry something went wrong with signup process. Please try after some time.';
$lang['MSG_DATA_NOT_FOUND'] = 'Data Not Available.';

/** Login Page **/
$lang['PAGE_TITLE_LOG_IN'] = 'Log In: ' . @SITE_NAME_COM;
$lang['LBL_LOG_IN_01'] = 'Hi. Great to find you <i class="icon-smile"></i> !';
$lang['LBL_LOG_IN_02'] = ' No account yet? <a href="' . @MENU_SIGN_UP . '" style="color:#36cd75">Register</a>';
$lang['LBL_LOG_IN_03'] = 'Simple and fast';
$lang['LBL_LOG_IN_04'] = 'Manage your orders';
$lang['LBL_LOG_IN_05'] = 'Customized notifications';
$lang['LBL_LOG_IN_06'] = 'Secure payment';
$lang['LBL_LOG_IN_07'] = 'Exclusive Offers';
$lang['LBL_LOG_IN_08'] = 'Forgot your password ?';
$lang['LBL_LOG_IN_09'] = 'Login';
$lang['LBL_LOG_IN_10'] = 'Email';
$lang['LBL_LOG_IN_11'] = 'Password';
$lang['LBL_LOG_IN_12'] = 'Please enter your email address. You will receive a link to create a new password via email.';

$lang['MSG_LOGIN_INVALID'] = '<strong>Access Denied !</strong><br> Invalid Username/Password';
$lang['MSG_LOGIN_STATUS_INACTIVE'] = '<strong>Access Denied !</strong><br> You cannot use this service now. Please contact us for more info ' . @MAIL_HELLO;
$lang['MSG_FORGOT_PASSWORD_INVALID'] = '<strong>Access Denied !</strong><br> #EMAIL# Invalid Email Id. Please Enter Correct Email Id.';
$lang['MSG_FORGOT_PASSWORD_SUCCESS'] = 'We received your forgot password request.<br> We will send you mail on #EMAIL# for reset password.';
$lang['MSG_FORGOT_PASSWORD_INACTIVE'] = '<strong>Access Denied !</strong><br> You cannot use this service now. Please contact us for more info ' . @MAIL_HELLO;
$lang['MSG_RESET_PASSWORD_MISSING'] = '<strong>Access Denied !</strong><br> You cannot use this service now. Please contact us for more info ' . @MAIL_HELLO;
$lang['MSG_RESET_PASSWORD_INVALID'] = '<strong>Access Denied !</strong><br> Your authentication token has expired.';
$lang['MSG_RESET_PASSWORD_SUCCESS'] = '<strong>Congratulations!</strong><br> Your password has been reset successfully.';
$lang['MSG_RESET_PASSWORD_INACTIVE'] = '<strong>Access Denied !</strong><br> You cannot use this service now. Please contact us for more info ' . @MAIL_HELLO;
$lang['MSG_LOGIN_SUCCESS'] = 'Login Successfully!';
$lang['MSG_LOGOUT_SUCCESS'] = 'Logout Successfully!';

/** Mail Section **/
$lang['MSG_MAIL_HEADER_TITLE'] = 'Familov';
$lang['MSG_MAIL_COPYRIGHT_TEXT'] = date('Y') . '@Familov.com';
$lang['MSG_MAIL_FOOTER_TEAM'] = 'The Familov Team';


/** Product Page Section **/
$lang['MSG_PRODUCT_TITLE'] = 'Product Listing : Familov';
$lang['MSG_PRODUCT_01'] = 'All products';
$lang['MSG_PRODUCT_02'] = 'Add to cart';
$lang['MSG_PRODUCT_03'] = 'Products';
$lang['MSG_PRODUCT_02_NOT_AVAILABLE'] = 'Not Available';
$lang['MSG_PRODUCT_04'] = 'Products are not available!';
$lang['MSG_PRODUCT_05'] = 'Hello :) There are currently <strong>#COUNT# products</strong> selection in this store add in your package.';
$lang['MSG_PRODUCT_06'] = 'No products were found matching your selection.';
$lang['MSG_PRODUCT_07'] = 'Loading More Product';
$lang['MSG_PRODUCT_08'] = 'Categories';
$lang['MSG_PRODUCT_09'] = 'SEE MORE PRODUCTS.';
$lang['MSG_PRODUCT_CART_ADD_SUCCESS'] = '#ITEM# has been added to your cart.';
$lang['MSG_PRODUCT_CART_ADD_FAILED'] = 'Item not added into your cart. Something went wrong. Please contact to administrator.';


/** Product Detail Page Section **/
$lang['MSG_PRODUCT_DETAIL_TITLE'] = 'Product Detail : ' . @SITE_NAME_COM;
$lang['MSG_PRODUCT_DETAIL_01'] = 'Product Detail';
$lang['MSG_PRODUCT_DETAIL_02'] = 'Product Detail and shop detail not available!';
$lang['MSG_PRODUCT_DETAIL_03'] = 'Product detail is not available!';
$lang['MSG_PRODUCT_DETAIL_04'] = 'Home>';
$lang['MSG_PRODUCT_DETAIL_05'] = 'Price';


/** Product Cart Page Section **/
$lang['MSG_PRODUCT_CART_TITLE'] = 'Product Cart : ' . @SITE_NAME_COM;
$lang['MSG_PRODUCT_CART_01'] = 'Product Cart';
$lang['MSG_PRODUCT_CART_02'] = 'Your cart is currently empty!';
$lang['MSG_PRODUCT_CART_03'] = 'Verifiez votre paquet puis cliquez sur "Validez le paquet" pour continuer...';
$lang['MSG_PRODUCT_CART_04'] = 'Your Cart';
$lang['MSG_PRODUCT_CART_05'] = 'Pic';
$lang['MSG_PRODUCT_CART_06'] = 'Name';
$lang['MSG_PRODUCT_CART_07'] = 'price';
$lang['MSG_PRODUCT_CART_08'] = 'Qty';
$lang['MSG_PRODUCT_CART_09'] = 'Total';
$lang['MSG_PRODUCT_CART_10'] = 'Sub total';
$lang['MSG_PRODUCT_CART_11'] = 'Service charge';
$lang['MSG_PRODUCT_CART_12'] = 'Total';
$lang['MSG_PRODUCT_CART_13'] = 'Continue Shopping';
$lang['MSG_PRODUCT_CART_14'] = 'Validate the package';
$lang['MSG_PRODUCT_CART_15'] = '(* #PERCENTAGE#% offered)';
$lang['MSG_PRODUCT_CART_16'] = 'Total TTC';
$lang['MSG_PRODUCT_CART_17'] = 'Your cart has been updated.';
$lang['MSG_PRODUCT_CART_18'] = 'Are you sure?';
$lang['MSG_PRODUCT_CART_19'] = 'You won\'t be able to revert this!';
$lang['MSG_PRODUCT_CART_20'] = 'Something went wrong. Item is not deleted from your cart.';
$lang['MSG_PRODUCT_CART_21'] = 'Product item has been removed from your cart.';


/** Product Checkout Page Section **/
$lang['MSG_PRODUCT_CHECKOUT_TITLE'] = 'Product Checkout : ' . @SITE_NAME_COM;
$lang['MSG_PRODUCT_CHECKOUT_01'] = 'Checkout';
$lang['MSG_PRODUCT_CHECKOUT_02'] = 'Oops! Your cart is empty!';
$lang['MSG_PRODUCT_CHECKOUT_03'] = 'Pick up from the store';
$lang['MSG_PRODUCT_CHECKOUT_04'] = 'Select delivery mode';
$lang['MSG_PRODUCT_CHECKOUT_05'] = 'Pick up from the shop ';
$lang['MSG_PRODUCT_CHECKOUT_06'] = 'Home delivery ';
$lang['MSG_PRODUCT_CHECKOUT_07'] = 'Do you have any coupon code?';
$lang['MSG_PRODUCT_CHECKOUT_08'] = 'Please enter here an validate, if not ignore this field!';
$lang['MSG_PRODUCT_CHECKOUT_09'] = 'Order delivery option has been updated successfully!';
$lang['MSG_PRODUCT_CHECKOUT_10'] = 'Promo code';
$lang['MSG_PRODUCT_CHECKOUT_11'] = 'Promo code is invalid!';
$lang['MSG_PRODUCT_CHECKOUT_12'] = 'Promo code applied successfully!';
$lang['MSG_PRODUCT_CHECKOUT_13'] = 'Promo code is not applied!<br>Cart amount is lower then discount amount!';
$lang['MSG_PRODUCT_CHECKOUT_14'] = 'Step 1:Check the package to send';
$lang['MSG_PRODUCT_CHECKOUT_15'] = 'Bravo! Votre paquet est prêt!Verifiez que tous les produits de votre choix sont biens presents, Validez le code Promo passez à l´étape 2.';
$lang['MSG_PRODUCT_CHECKOUT_16'] = 'Step 2: Information from the beneficiary';
$lang['MSG_PRODUCT_CHECKOUT_17'] = 'Great ! Just tell us the full name and phone number of the beneficiary by carefully checking the information. This will allow us to contact him for withdrawal and authenticate by his Identity Card (CNI).';
$lang['MSG_PRODUCT_CHECKOUT_18'] = 'Recipient´s information.';
$lang['MSG_PRODUCT_CHECKOUT_19'] = 'Receiver full name Ex : Nowen Merline ';
$lang['MSG_PRODUCT_CHECKOUT_20'] = 'Tel of recipient Ex : 6987XXXXX';
$lang['MSG_PRODUCT_CHECKOUT_21'] = 'Greeting Message';
$lang['MSG_PRODUCT_CHECKOUT_22'] = 'If you want to leave a short message feel you free...?';
$lang['MSG_PRODUCT_CHECKOUT_23'] = 'e.g.  God bless you  / I miss you...';
$lang['MSG_PRODUCT_CHECKOUT_24'] = 'Step 3: Choosing a secure payment method';
$lang['MSG_PRODUCT_CHECKOUT_25'] = 'Easy is not it? Choose the payment method that suits you accept the terms of sale and pay any credit sécurité.Carte (VISA, Master Card, American Express ...) and Paypal.';
$lang['MSG_PRODUCT_CHECKOUT_26'] = 'Payment Method';
$lang['MSG_PRODUCT_CHECKOUT_27'] = 'Credit Card';
$lang['MSG_PRODUCT_CHECKOUT_28'] = 'Paypal';
$lang['MSG_PRODUCT_CHECKOUT_29'] = 'Bank Transfer(1 or 2 days)';
$lang['MSG_PRODUCT_CHECKOUT_30'] = 'General terms and conditions';
$lang['MSG_PRODUCT_CHECKOUT_31'] = 'I accept the terms of sale';
$lang['MSG_PRODUCT_CHECKOUT_32'] = 'Confirm the payment';
$lang['MSG_PRODUCT_CHECKOUT_33'] = 'Please Fill up beneficiary information!';
$lang['MSG_PRODUCT_CHECKOUT_34'] = 'Please indicate that you have read and agree to the Terms and Conditions!';
$lang['MSG_PRODUCT_CHECKOUT_35'] = 'Invalid Payment Method!';
$lang['MSG_PRODUCT_CHECKOUT_36'] = 'Something went wrong! Please try with another payment method.';
$lang['MSG_PRODUCT_CHECKOUT_37'] = 'Stripe server is down. Please try with another payment method.';
$lang['MSG_PRODUCT_CHECKOUT_38'] = 'Please login first.';
$lang['MSG_PRODUCT_CHECKOUT_39'] = 'Please contact to administrator.<br>';
$lang['MSG_PRODUCT_CHECKOUT_40'] = 'Please wait, your request is being processed and you will be redirect to the paypal gateway.';
$lang['MSG_PRODUCT_CHECKOUT_41'] = 'Hang in there ! We are processing your request please wait...';
$lang['MSG_PRODUCT_CHECKOUT_42'] = "Very cool! It's already the last stage!";
$lang['MSG_PRODUCT_CHECKOUT_43'] = "Logon you to accept the package, otherwise register for free to enjoy the service.";
$lang['MSG_PRODUCT_CHECKOUT_44'] = 'SEPA Direct Debit 2 or 3 days';
$lang['MSG_PRODUCT_CHECKOUT_45'] = 'IBAN';
$lang['MSG_PRODUCT_CHECKOUT_46'] = 'Enter IBAN number without space.';
$lang['MSG_PRODUCT_CHECKOUT_47'] = 'SOFORT';
$lang['MSG_PRODUCT_CHECKOUT_48'] = 'Select Country of your bank';
$lang['MSG_PRODUCT_CHECKOUT_49'] = 'BANCONTACT';

/** Order confirmation Page Section **/
$lang['MSG_OC_TITLE'] = 'Order Confirmation: ' . @SITE_NAME_COM;
$lang['MSG_OC_01'] = 'Order Confirmation';
$lang['MSG_OC_02'] = 'Youpiii!!! #USER_NAME#,';
$lang['MSG_OC_03'] = 'Your package for <span style="color:#ee3682;text-transform: uppercase;">#RECEIVER_NAME#</span> is successfully sent. You have nothing more to do.';
$lang['MSG_OC_04'] = 'The withdrawal code is: <span style="color:#ee3682;">#CODE#</span>. On receipt of the payment <span style="color:#ee3682;">#RECEIVER_NAME#</span> will contact for the delivery.<br>Thank you!';
$lang['MSG_OC_05'] = 'Quickly send another package? Or return to the homepage';
$lang['MSG_OC_06'] = 'Bank Details';
$lang['MSG_OC_07'] = 'Please transfer [#AMOUNT#] in below bank account.';
$lang['MSG_OC_08'] = 'Holder Name';
$lang['MSG_OC_09'] = 'Bank Name';
$lang['MSG_OC_10'] = 'IBAN';
$lang['MSG_OC_11'] = 'BIC';
$lang['MSG_OC_12'] = 'Ref/VW';
$lang['MSG_OC_13'] = 'Oops! #USER_NAME#,';
$lang['MSG_OC_14'] = 'There is something wrong. Please contact to administrator.';


/** View Order Page Section **/
$lang['MSG_VO_TITLE'] = 'My Orders: ' . @SITE_NAME_COM;
$lang['MSG_VO_01'] = 'My Orders';
$lang['MSG_VO_02'] = 'There is no any order available in your account!';
$lang['MSG_VO_03'] = 'View Detail';
$lang['MSG_VO_04'] = 'Total orders ';
$lang['MSG_VO_05'] = 'Order detail found!';
$lang['MSG_VO_06'] = 'Order detail detail not available!';
$lang['MSG_VO_07'] = 'Order Code';
$lang['MSG_VO_08'] = 'Receiver Name';
$lang['MSG_VO_09'] = 'Receiver Contact No.';
$lang['MSG_VO_10'] = 'Greeting Message';
$lang['MSG_VO_11'] = 'Order Date';
$lang['MSG_VO_12'] = 'Payment Method';
$lang['MSG_VO_13'] = 'Order Status';
$lang['MSG_VO_14'] = 'Order Detail';
$lang['MSG_VO_15'] = 'Close';


/** Change Password Page Section **/
$lang['MSG_CP_TITLE'] = 'Change Password: ' . @SITE_NAME_COM;
$lang['MSG_CP_01'] = 'Change Password';
$lang['MSG_CP_02'] = 'Current Password';
$lang['MSG_CP_03'] = 'Your Current Password';
$lang['MSG_CP_04'] = 'New Password';
$lang['MSG_CP_05'] = 'Enter Your New Password';
$lang['MSG_CP_06'] = 'Repeat New Password';
$lang['MSG_CP_07'] = 'Enter Repeat New Password';
$lang['MSG_CP_08'] = 'Update Password';
$lang['LBL_CP_VALIDATION_01'] = 'Please Enter Your Current Password!';
$lang['LBL_CP_VALIDATION_02'] = 'Please Enter Your New Password!';
$lang['LBL_CP_VALIDATION_03'] = 'Please Enter Your Repeat Password!';
$lang['LBL_CP_VALIDATION_04'] = 'Repeat Password is do not match!';
$lang['LBL_CP_VALIDATION_05'] = 'Password must be at least 8 characters!';
$lang['MSG_CP_INVALID'] = 'Your current password does not match. Please enter correct password.';
$lang['MSG_CP_SUCCESS'] = 'Your password has been changed successfully.';
$lang['MSG_CP_FAILED'] = 'Your change password request failed. Please try again.';


/** My Account Page Section **/
$lang['MSG_MA_TITLE'] = 'My Account: ' . @SITE_NAME_COM;
$lang['MSG_MA_01'] = 'My Account';
$lang['MSG_MA_02'] = 'Editez votre compte';
$lang['MSG_MA_03'] = 'Pour davantage sécuriser vos transferts vous devez mettre à jour votre profil.';
$lang['MSG_MA_04'] = 'Personal Details';
$lang['MSG_MA_05'] = 'First Name';
$lang['MSG_MA_06'] = 'Last Name';
$lang['MSG_MA_07'] = '* denotes required form fields ';
$lang['MSG_MA_08'] = 'Contact Details';
$lang['MSG_MA_09'] = 'Email';
$lang['MSG_MA_10'] = 'Enter your email address';
$lang['MSG_MA_11'] = 'Phone Number';
$lang['MSG_MA_12'] = 'Address Details';
$lang['MSG_MA_13'] = 'Country';
$lang['MSG_MA_14'] = 'City';
$lang['MSG_MA_15'] = 'Address';
$lang['MSG_MA_16'] = 'Postal Code';
$lang['MSG_MA_17'] = 'Notification Preferences';
$lang['MSG_MA_18'] = 'Choose how you\'d like to be kept in the loop by familov';
$lang['MSG_MA_19'] = 'Let me know about news and offers by email';
$lang['MSG_MA_20'] = 'Let me know about news and offers by text';
$lang['MSG_MA_21'] = 'Update';
$lang['MSG_MA_22'] = 'Your profile has been updated successfully!';
$lang['MSG_MA_23'] = 'Your profile update request failed.Please try again.';
$lang['LBL_MA_VALIDATION_01'] = 'Please Enter First Name!';
$lang['LBL_MA_VALIDATION_02'] = 'Please Enter Last Name!';
$lang['LBL_MA_VALIDATION_03'] = 'Please Select Country!';
$lang['LBL_MA_VALIDATION_04'] = 'Please Enter City!';
$lang['LBL_MA_VALIDATION_05'] = 'Please Enter Phone Number!';
$lang['LBL_MA_VALIDATION_06'] = 'Please Enter Address!';
$lang['LBL_MA_VALIDATION_07'] = 'Please Enter Postal Code!';

$lang['MSG_WAIT'] = 'Please Wait...';
$lang['MSG_COPY'] = 'Copy URL to clipboard successfully.';


/** Invite Friend Page Section **/
$lang['MSG_IF_TITLE'] = 'Invite Friend: ' . @SITE_NAME_COM;
$lang['MSG_IF_01'] = 'Earn money with your friends :)';
$lang['MSG_IF_02'] = 'Invite Your Friends';
$lang['MSG_IF_03'] = 'Copy the link';
$lang['MSG_IF_04'] = 'Or click to invite friends on Facebook';
$lang['MSG_IF_05'] = 'Facebook';
$lang['MSG_IF_06'] = 'Your win';
$lang['MSG_IF_07'] = 'You have used always';
$lang['MSG_IF_08'] = 'Available for checkout';


/** Forgot Password Page Section **/
$lang['MSG_FP_TITLE'] = 'Forgot Password: ' . @SITE_NAME_COM;
$lang['MSG_FP_01'] = 'Forgot Password';
$lang['MSG_FP_02'] = 'Forgot Password';


/** Reset Password Page Section **/
$lang['MSG_RP_TITLE'] = 'Reset Password: ' . @SITE_NAME_COM;
$lang['MSG_RP_01'] = 'Reset Password';
$lang['MSG_RP_02'] = 'Reset Password';

$lang['MSG_BAD_REQUEST'] = 'Something data are missing. Please check your request.';

/** Shop Change **/
$lang['SHOP_CHANGE_01'] = 'Shop Change';
$lang['SHOP_CHANGE_02'] = 'Are You Sure Want To Change Shop';
$lang['SHOP_CHANGE_03'] = 'Yes';
$lang['SHOP_CHANGE_04'] = 'No';
$lang['SHOP_CHANGE_05'] = 'If you change the shop you will loose your cart';
$lang['SHOP_CHANGE_06'] = 'Are You Sure?';


$lang['MSG_USER_NOT_FOUND'] = 'Invalid User!';
$lang['MSG_ORDER_NOT_FOUND'] = 'Order(s) Not Found!';
$lang['MSG_ORDER_DETAIL_NOT_FOUND'] = 'Order Detail Not Found!';