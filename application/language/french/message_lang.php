<?php
/***
 * Created by PhpStorm.
 * User: Vikrant Parmar
 * Date: 10/26/2017
 * Time: 6:06 PM
 **/


/* Menu */
$lang['LBL_MENU_01'] = 'Des questions?';
$lang['LBL_MENU_02'] = 'S´inscrire';
$lang['LBL_MENU_03'] = 'Se connecter ';
$lang['LBL_MENU_04'] = 'Mon compte';
$lang['LBL_MENU_05'] = 'Changer le mot de passe';
$lang['LBL_MENU_06'] = 'Se deconnecter';
$lang['LBL_MENU_07'] = 'Mes commandes';
$lang['LBL_MENU_08'] = 'Gagner de l´argent';
$lang['LBL_MENU_09'] = 'Comment ça marche';
$lang['LBL_MENU_10'] = 'Besoin d´aide?';
$lang['LBL_MENU_11'] = 'Conditions d´utilisation';
$lang['LBL_MENU_12'] = 'Privacy';
$lang['LBL_MENU_13'] = 'A propos';
$lang['LBL_MENU_14'] = 'Contact';
$lang['LBL_MENU_15'] = 'presse';
$lang['LBL_MENU_16'] = 'Emplois';

/** Social **/
$lang['LBL_SOCIAL_FACEBOOK'] = 'Facebook';
$lang['LBL_SOCIAL_TWITTER'] = 'Twitter';

/** Footer **/
$lang['LBL_FOOTER_01'] = date('Y') . ' © ' . @SITE_NAME_COM;
$lang['LBL_FOOTER_02'] = 'Made with  <i class="fa fa-heart wow pulse" data-wow-iteration="30" style="color: rgb(236, 30, 115); visibility: visible; animation-iteration-count: 30; animation-name: pulse;"></i> im Saarland';

/** Home Page First Section **/
$lang['welcome_message'] = 'Familov.om - Delivering happiness';
$lang['LBL_HERO_ONE'] = 'Bonjour,';
$lang['LBL_HERO_TWO'] = 'Comment voulez-vous soutenir<br>votre famille aujourd´hui ?';
$lang['LBL_HERO_THREE'] = 'Commandez depuis l\'étranger des produits alimentaires,<br>
                           d\'hygiène, scolaires, service de santé... et nous livrons vos<br>
                           proches au pays en moins de 36 heures.';

$lang['LBL_RECEIVER_COUNTRY'] = 'Pays du bénéficiaire';
$lang['LBL_RECEIVER_CITY'] = 'Ville du bénéficiaire ';
$lang['LBL_CHOOSE_SHOP'] = 'Choisissez une boutique';
$lang['LBL_RECEIVER_GO'] = 'ENTREZ !';

$lang['LBL_SEARCH_VALIDATION_COUNTRY'] = 'Choisissez un pays';
$lang['LBL_SEARCH_VALIDATION_CITY'] = 'Choisissez une ville';
$lang['LBL_SEARCH_VALIDATION_SHOP'] = 'Choisissez une boutique';
$lang['LBL_SEARCH_CITY_LIST'] = 'Oops!Aucune ville trouvée';
$lang['LBL_SEARCH_SHOP_LIST'] = 'Oops!Aucun magasin trouvé';


$lang['LBL_HOME_01'] = 'Benéfices imbattables';
$lang['LBL_HOME_02'] = 'Envoyez plus , Payez moins';
$lang['LBL_HOME_03'] = 'Save up to 95% vs. traditional money transfer provider and local price Garantie !';
$lang['LBL_HOME_04'] = 'Money well used';
$lang['LBL_HOME_05'] = 'guarantees that the money will be used for the right purpose';
$lang['LBL_HOME_06'] = 'Safe & secure';
$lang['LBL_HOME_07'] = 'We use an SSL certificate encryption.Your money is in safe hands';
$lang['LBL_HOME_08'] = 'Easy to use';
$lang['LBL_HOME_09'] = 'Whether on desktop or mobile you’ll always know what your money do';
$lang['LBL_HOME_10'] = 'Positive impact';
$lang['LBL_HOME_11'] = 'You do something positive for people to improve local economic';
$lang['LBL_HOME_12'] = 'Help people';
$lang['LBL_HOME_13'] = 'Easy send food and basic needs to orphan, school other hospital';
$lang['LBL_HOME_14'] = 'Créez un compte, c´est gratuit !';


$lang['LBL_HOME_15'] = '" Je vis en Europe depuis 4 ans et je travaille si dur tous les jours pour soutenir ma famille, mais pour de nombreuses raisons,il n\'est pas toujours facile et évident pour moi d\'envoyer de l\'argent à la maison. familov m\'offre une nouvelle façon de prendre soin de mes proches et maintenant je prends plaisir à les aider parce que je peux faire plus pour eux chaque jour. "';
$lang['LBL_HOME_16'] = '#BeAFamilover';


/* Home Page How It Work Section */
$lang['LBL_HOW_IT_WORKS_01'] = 'Comment ça marche ?';
$lang['LBL_HOW_IT_WORKS_02'] = 'Are you familiar with the heavy costs of money transfer services and Frustrated by improper';
/*$lang['LBL_HOW_IT_WORKS_03'] = 'utilization of remittances?';*/
$lang['LBL_HOW_IT_WORKS_04'] = 'Choisir les produits';
$lang['LBL_HOW_IT_WORKS_05'] = 'Choose what you wish to buy and provide details of the recipient. Full name and Phone will be required.';
$lang['LBL_HOW_IT_WORKS_06'] = 'Pay securely';
$lang['LBL_HOW_IT_WORKS_07'] = 'Select the payment method suitable for you. You can pay via Visa or Master Card or use other online money wallet like PayPal.';
$lang['LBL_HOW_IT_WORKS_08'] = 'SMS Removal Code';
$lang['LBL_HOW_IT_WORKS_09'] = 'Our system send detailed information about the package to the local store and send the withdrawal code to the package recipient.';
$lang['LBL_HOW_IT_WORKS_10'] = 'Delivery';
$lang['LBL_HOW_IT_WORKS_11'] = 'The package is prepared and the recipient is contacted within 36 hours * for delivery and you receive a confirmation of a successful delivery.';


/* Home Page Testimonial Section */
$lang['LBL_TESTIMONIAL_01'] = 'Rejoignez des milliers de familles heureuses';
$lang['LBL_TESTIMONIAL_02'] = 'Familov est le nouveau moyen par lequel les familles partout dans le monde restent connectées. Nous nous déployons rapidement partout dans le pays en proposant un service adapté au réalité!';

$lang['LBL_TESTIMONIAL_PERSON_1_01'] = 'Marie Laure';
$lang['LBL_TESTIMONIAL_PERSON_1_02'] = 'Etudiante, USA';
$lang['LBL_TESTIMONIAL_PERSON_1_03'] = 'Toutes les semaines je fais un panier comme au marché pour ma mère et mes frères. Ma famille est ravie et moi encore plus . C\'est pas cher, rapide flexible et surtout très simple. I like it !';

$lang['LBL_TESTIMONIAL_PERSON_2_01'] = 'Michael Francis';
$lang['LBL_TESTIMONIAL_PERSON_2_02'] = 'Ingenieur, Allemagne';
$lang['LBL_TESTIMONIAL_PERSON_2_03'] = 'C\'est une bonne idée et ça marche très bien.Fini les tracasseries et les appels en tout genre. Au moins comme ca je suis tranquille car je sais que mon argent les aide efficacement.';


$lang['LBL_TESTIMONIAL_PERSON_3_01'] = 'Annita';
$lang['LBL_TESTIMONIAL_PERSON_3_02'] = 'Assistant, France';
$lang['LBL_TESTIMONIAL_PERSON_3_03'] = 'J\'aime bien donner un coup de main ma famille au pays.Les notifications que je recois par rapport à la livraison me rassurent en plus, je peux proposer d´autres produits. Merci.';

/* FAQ Page */
$lang['PAGE_TITLE_FAQ'] = 'FAQ : . @SITE_NAME_COM';
$lang['LBL_FAQ_01'] = 'Foire aux questions';
$lang['LBL_FAQ_02'] = 'Questions fréquentes';
$lang['LBL_FAQ_03'] = 'Paiement';


/** How it works Page **/
$lang['PAGE_TITLE_HIW'] = 'How it works : ' . @SITE_NAME_COM;
$lang['LBL_HIW_01'] = 'Comment ça marche?';
$lang['LBL_HIW_02'] = 'Fatigué de l´argent mal utilisé et des frais de transfert d´argent élévés?
Soyez plus efficace et économisez
jusqu´à 95% de frais en moins!';
$lang['LBL_HIW_03'] = 'Faites le choix en ligne';
$lang['LBL_HIW_04'] = 'Choisissez les produits ou service que vous souhaitez.';
$lang['LBL_HIW_05'] = 'Payez en toute sécurité';
$lang['LBL_HIW_06'] = 'Utilisez au choix un moyen de paiement sécurisé. ';
$lang['LBL_HIW_07'] = 'Code de retrait par SMS';
$lang['LBL_HIW_08'] = 'Le bénéficiaire recoit le code unique par SMS.';
$lang['LBL_HIW_09'] = 'Livraison et confirmation';
$lang['LBL_HIW_10'] = 'Le paquet est préparé et livré en moins de 36 H*.';
$lang['LBL_HIW_11'] = 'IMPORTANT';
$lang['LBL_HIW_12'] = '*36 heure sous reserve de la disponibilité du bénéficiaire !
Lorsque une commande est effectuée,nous disposons de maximum 36 heures pour la preparer et contacter le bénéficiaire, ';
$lang['LBL_HIW_13'] = 'c´est pourquoi il est IMPORTANT que vos proches attendent d´être appelés avant de se rendre au magasin munis d´une pièce d´identité pour le retrait.';
$lang['LBL_HIW_14'] = 'Merci';
$lang['LBL_HIW_15'] = 'Create an account, it\'s free!';
$lang['LBL_HIW_16'] = 'Why you’re making the right choice?';
$lang['LBL_HIW_17'] = 'Send more , buy less';
$lang['LBL_HIW_18'] = 'Save up to 95% vs. traditional money transfer provider and local price Garantie !';
$lang['LBL_HIW_19'] = 'Money well used';
$lang['LBL_HIW_20'] = 'guarantees that the money will be used for the right purpose';
$lang['LBL_HIW_21'] = 'Safe & secure';
$lang['LBL_HIW_22'] = 'We use an SSL certificate encryption.Your money is in safe hands';
$lang['LBL_HIW_23'] = 'Easy to use';
$lang['LBL_HIW_24'] = 'Whether on desktop or mobile you’ll always know what your money do .';
$lang['LBL_HIW_25'] = 'Positive impact';
$lang['LBL_HIW_26'] = 'You do something positive for people to improve local economic.';
$lang['LBL_HIW_27'] = 'Help people';
$lang['LBL_HIW_28'] = 'Easy send food and basic needs to orphan, school other hospital';


/* Terms Page */
$lang['PAGE_TITLE_TERMS'] = 'CONDITIONS GENERALES D´UTILISATION : Familov';
$lang['LBL_TERMS_01'] = 'CONDITIONS GENERALES D´UTILISATION';


/** Privacy Page **/
$lang['PAGE_TITLE_PRIVACY'] = 'Privacy Policy : ' . @SITE_NAME_COM;
$lang['LBL_PRIVACY_POLICY_01'] = 'Privacy Policy';

/** About Us Page **/
$lang['PAGE_TITLE_ABOUT_US'] = 'About Us : ' . @SITE_NAME_COM;
$lang['LBL_ABOUT_US_01'] = 'À propos de nous';


/** Contact Us Page **/
$lang['PAGE_TITLE_CONTACT_US'] = 'Contact Us : ' . @SITE_NAME_COM;
$lang['LBL_CONTACT_US_01'] = 'Contact Us';


/** Press Page **/
$lang['PAGE_TITLE_PRESS'] = 'Press : ' . @SITE_NAME_COM;
$lang['LBL_PRESS_01'] = 'Familov est présenté par';
$lang['LBL_PRESS_02'] = 'Continuer la lecture ...';
$lang['LBL_PRESS_03'] = '" Aktuell schicken viele Studenten Geld nach Hause. Das ist teuer, und das Geld wird oft auch nicht zum Kauf von Lebensmitteln genutzt... "';

/** Jobs Page **/
$lang['PAGE_TITLE_JOBS'] = 'Jobs : ' . @SITE_NAME_COM;
$lang['LBL_JOBS_01'] = 'Pourquoi rejoindre notre équipe?';
$lang['LBL_JOBS_02'] = 'Faire partie d\'une équipe jeune, ambitieuse et dynamique. <br><br>

    Participer au développement d\'un projet innovant à fort potentiel international.<br><br>
    
Participez à une aventure entrepreneuriale avec une vraie mission de changement de jeu "Geld tranfert alternative!
<br><br>

Prendre part à une incroyable aventure entrepreneuriale et sociale avec une vraie mission : Envoyer plus que de l´argent à nos proches';


/* Sign Up  Page */
$lang['PAGE_TITLE_SIGN_UP'] = 'Sign Up: ' . @SITE_NAME_COM;
$lang['LBL_SIGN_UP_01'] = 'Créer votre compte. C´est gratuit.';
$lang['LBL_SIGN_UP_02'] = ' Prénom';
$lang['LBL_SIGN_UP_03'] = 'Nom';
$lang['LBL_SIGN_UP_04'] = 'E-mail';
$lang['LBL_SIGN_UP_05'] = 'Votre numéro mobile';
$lang['LBL_SIGN_UP_06'] = 'Mot de passe';
$lang['LBL_SIGN_UP_07'] = 'Confirmer le mot de passe';
$lang['LBL_SIGN_UP_08'] = 'Créer mon compte Familov';
$lang['LBL_SIGN_UP_09'] = 'By clicking Signup you agree to the <a href="' . @MENU_TERMS . '" class="f-w-700" style="color:#36cd75">termes</a> et <a href="' . @MENU_PRIVACY . '" class="f-w-700" style="color:#36cd75">Politique de confidentialité</a>.';
$lang['LBL_SIGN_UP_10'] = 'Vous avez déjà un compte Familov? <a href="' . @MENU_LOG_IN . '" class="f-w-700" style="color:#36cd75">se connecter</a>.';

$lang['LBL_SIGN_UP_VALIDATION_01'] = 'Indiquez votre prénom!';
$lang['LBL_SIGN_UP_VALIDATION_02'] = 'Indiquez votre nom!';
$lang['LBL_SIGN_UP_VALIDATION_03'] = 'Indiquez votre Email!';
$lang['LBL_SIGN_UP_VALIDATION_04'] = 'Email adresse non valide!';
$lang['LBL_SIGN_UP_VALIDATION_05'] = 'Please Enter Mobile Number!';
$lang['LBL_SIGN_UP_VALIDATION_06'] = 'Indiquez un mot de passe!';
$lang['LBL_SIGN_UP_VALIDATION_07'] = 'confirmez le mot de passe !';
$lang['LBL_SIGN_UP_VALIDATION_08'] = 'Les mots de passe ne correspondent pas!';
$lang['LBL_SIGN_UP_VALIDATION_09'] = 'Le mot de passe doit contenir min. 8 charactères!';
$lang['LBL_SIGN_UP_VALIDATION_10'] = 'Only allow numbers!';

$lang['MSG_SIGN_UP_EMAIL_EXIST'] = 'Email id is already existing with us. Please try with another one.';
$lang['MSG_SIGN_UP_SUCCESS'] = '<strong>Congratulation!!</strong> <br>You have been successfully registered with us. Thank You!';
$lang['MSG_SIGN_UP_FAILED'] = 'Sorry something went wrong with signup process. Please try after some time.';
$lang['MSG_DATA_NOT_FOUND'] = 'Data Not Available.';

/** Login Page **/
$lang['PAGE_TITLE_LOG_IN'] = 'Log In: ' . @SITE_NAME_COM;
$lang['LBL_LOG_IN_01'] = 'Hi. Super content de te retrouver <i class="icon-smile"></i> !';
$lang['LBL_LOG_IN_02'] = 'Pas encore de compte? <a href="' . @MENU_SIGN_UP . '" style="color:#36cd75">S´incrire</a>';
$lang['LBL_LOG_IN_03'] = 'Simple et rapide';
$lang['LBL_LOG_IN_04'] = 'Gérer ses commandes';
$lang['LBL_LOG_IN_05'] = 'Notifications personalisées';
$lang['LBL_LOG_IN_06'] = 'Paiement sécurisé';
$lang['LBL_LOG_IN_07'] = 'Offres exclusives';
$lang['LBL_LOG_IN_08'] = 'Mot de passe oublié ?';
$lang['LBL_LOG_IN_09'] = 'S\'IDENTIFIER';
$lang['LBL_LOG_IN_10'] = 'Email';
$lang['LBL_LOG_IN_11'] = 'Mot de passe';

$lang['LBL_LOG_IN_12'] = 'Veillez entrer votre adresse email. Vous recevrez &agrave; cette adresse un lien pour créer un nouveau mot de passe...';

$lang['MSG_LOGIN_INVALID'] = '<strong>Access Denied !</strong><br> Invalid Username/Password';
$lang['MSG_LOGIN_STATUS_INACTIVE'] = '<strong>Access Denied !</strong><br> You cannot use this service now. Please contact us for more info ' . @MAIL_HELLO;
$lang['MSG_FORGOT_PASSWORD_INVALID'] = '<strong>Access Denied !</strong><br> #EMAIL# Invalid Email Id. Please Enter Correct Email Id.';
$lang['MSG_FORGOT_PASSWORD_SUCCESS'] = 'Nous avons recu votre demande de restauration de mot de passe.<br>Un Email a �t� envoy� � votre adresse :  #EMAIL# pour cr�er un nouveau mot de passe.';
$lang['MSG_FORGOT_PASSWORD_INACTIVE'] = '<strong>Access Denied !</strong><br> You cannot use this service now. Please contact us for more info ' . @MAIL_HELLO;
$lang['MSG_RESET_PASSWORD_MISSING'] = '<strong>Access Denied !</strong><br> You cannot use this service now. Please contact us for more info ' . @MAIL_HELLO;
$lang['MSG_RESET_PASSWORD_INVALID'] = '<strong>Access Denied !</strong><br> Your authentication token has expired.';
$lang['MSG_RESET_PASSWORD_SUCCESS'] = '<strong>Congratulations!</strong><br> Your password has been reset successfully.';
$lang['MSG_RESET_PASSWORD_INACTIVE'] = '<strong>Access Denied !</strong><br> You cannot use this service now. Please contact us for more info ' . @MAIL_HELLO;
$lang['MSG_LOGIN_SUCCESS'] = 'Login Successfully!';
$lang['MSG_LOGOUT_SUCCESS'] = 'Logout Successfully!';


/* Product Page Section */
$lang['MSG_PRODUCT_TITLE'] = 'Product Listing : Familov';
$lang['MSG_PRODUCT_01'] = 'Tous les produits';
$lang['MSG_PRODUCT_02'] = 'Ajouter';
$lang['MSG_PRODUCT_02_NOT_AVAILABLE'] = 'Rupture de Stock';
$lang['MSG_PRODUCT_03'] = 'Products';
$lang['MSG_PRODUCT_04'] = 'Produits non disponibles';
$lang['MSG_PRODUCT_05'] = 'Hello :) Il ya actuellement <strong>#COUNT# produits</strong> de selection dans ce magasin a ajouter dans ton paquet.';
/** Mail Section **/
$lang['MSG_MAIL_HEADER_TITLE'] = 'Familov';
$lang['MSG_MAIL_COPYRIGHT_TEXT'] = date('Y') . '@Familov.com';
$lang['MSG_MAIL_FOOTER_TEAM'] = 'The Familov Team';


$lang['MSG_PRODUCT_06'] = 'No products were found matching your selection.';
$lang['MSG_PRODUCT_07'] = 'VOIR PLUS DE PRODUITS';
$lang['MSG_PRODUCT_08'] = 'Categories';
$lang['MSG_PRODUCT_09'] = 'Voir plus de produits.';
$lang['MSG_PRODUCT_CART_ADD_SUCCESS'] = '#ITEM# a été ajouté avec succès au paquet.';
$lang['MSG_PRODUCT_CART_ADD_FAILED'] = 'Item not added into your cart. Something went wrong. Please contact administrator.';


/* Product Detail Page Section */
$lang['MSG_PRODUCT_DETAIL_TITLE'] = 'Product Detail : ' . @MAIL_SITE_NAME;
$lang['MSG_PRODUCT_DETAIL_01'] = 'Product Detail';
$lang['MSG_PRODUCT_DETAIL_02'] = 'Product Detail and shop detail not available!';
$lang['MSG_PRODUCT_DETAIL_03'] = 'Product detail is not available!';
$lang['MSG_PRODUCT_DETAIL_04'] = 'Home>';
$lang['MSG_PRODUCT_DETAIL_05'] = 'Price';


/* Product Cart Page Section */
$lang['MSG_PRODUCT_CART_TITLE'] = 'Product Cart : ' . @MAIL_SITE_NAME;
$lang['MSG_PRODUCT_CART_01'] = 'Votre paquet';
$lang['MSG_PRODUCT_CART_02'] = 'Oups ! Votre panier est actuellement vide!';
$lang['MSG_PRODUCT_CART_03'] = 'Verifiez votre paquet puis cliquez sur "Validez le paquet" pour continuer...';
$lang['MSG_PRODUCT_CART_04'] = 'Mon panier';
$lang['MSG_PRODUCT_CART_05'] = 'Pic';
$lang['MSG_PRODUCT_CART_06'] = 'Produits';
$lang['MSG_PRODUCT_CART_07'] = 'Prix ';
$lang['MSG_PRODUCT_CART_08'] = 'Qté';
$lang['MSG_PRODUCT_CART_09'] = 'total';
$lang['MSG_PRODUCT_CART_10'] = 'Sous total:';
$lang['MSG_PRODUCT_CART_11'] = 'Frais de service ';
$lang['MSG_PRODUCT_CART_12'] = 'Prix total';
$lang['MSG_PRODUCT_CART_13'] = 'Ajoutez des produits ';
$lang['MSG_PRODUCT_CART_14'] = 'Validez le paquet ';
$lang['MSG_PRODUCT_CART_15'] = '(* #PERCENTAGE#% offert)';
$lang['MSG_PRODUCT_CART_16'] = 'Total TTC';
$lang['MSG_PRODUCT_CART_17'] = 'Votre panier a bien été modifié.';
$lang['MSG_PRODUCT_CART_18'] = 'êtes vous sure?';
$lang['MSG_PRODUCT_CART_19'] = 'You won\'t be able to revert this!';
$lang['MSG_PRODUCT_CART_20'] = 'Something went wrong. Item is not deleted from your cart.';
$lang['MSG_PRODUCT_CART_21'] = 'Le produit a bien été supprimé de votre panier.';



/* Product Checkout Page Section */
$lang['MSG_PRODUCT_CHECKOUT_TITLE'] = 'Product Checkout : ' . @MAIL_SITE_NAME;
$lang['MSG_PRODUCT_CHECKOUT_01'] = 'Checkout';
$lang['MSG_PRODUCT_CHECKOUT_02'] = 'Oops! Votre panier est vide!';
$lang['MSG_PRODUCT_CHECKOUT_03'] = 'Retrait en magasin';
$lang['MSG_PRODUCT_CHECKOUT_04'] = 'Sélectionner le mode de livraison';
$lang['MSG_PRODUCT_CHECKOUT_05'] = ' Retrait en magasin';
$lang['MSG_PRODUCT_CHECKOUT_06'] = 'Livraison à domicile ';
$lang['MSG_PRODUCT_CHECKOUT_07'] = 'Avez-vous un code promo ?';
$lang['MSG_PRODUCT_CHECKOUT_08'] = 'Si oui,entrez le ici et appliquez, sinon ignorez et passez à l´étape 2 !';
$lang['MSG_PRODUCT_CHECKOUT_09'] = 'Order delivery option has been updated successfully!';
$lang['MSG_PRODUCT_CHECKOUT_10'] = 'Code Promo ';
$lang['MSG_PRODUCT_CHECKOUT_11'] = 'Oops !Code Promo non valide!';
$lang['MSG_PRODUCT_CHECKOUT_12'] = 'Super!Code promo validé avec succès!';
$lang['MSG_PRODUCT_CHECKOUT_13'] = 'Code promo pas validé !<br>Le montant de votre panier est inferieur au montant de la promo !';
$lang['MSG_PRODUCT_CHECKOUT_14'] = 'Etape 1 : Verifiez le paquet';
$lang['MSG_PRODUCT_CHECKOUT_15'] = 'Bravo! Votre paquet est prêt! Verifiez que tous les produits de votre choix sont biens presents et passez à l´étape 2.';
$lang['MSG_PRODUCT_CHECKOUT_16'] = 'Etape 2 : les informations du béneficiaire';
$lang['MSG_PRODUCT_CHECKOUT_17'] = 'Super ! Indiquez-nous juste le nom complet et le numéro de télephone du bénéficiaire en revérifiant soigneusement les informations. Ceci nous permettra de le contacter pour le retrait et l´authentifier par sa carte d´identité (CNI)..';
$lang['MSG_PRODUCT_CHECKOUT_18'] = 'Svp,Indiquez les informations du bénéficiaire';
$lang['MSG_PRODUCT_CHECKOUT_19'] = ' Nom complet du béneficiaire Ex : Balima David  ';
$lang['MSG_PRODUCT_CHECKOUT_20'] = 'N° de Tel. du béneficiaire Ex : 6987XXXXX';
$lang['MSG_PRODUCT_CHECKOUT_21'] = 'Message personnalisé';
$lang['MSG_PRODUCT_CHECKOUT_22'] = 'Voulez-vous laisser un petit message au bénéficiaire?';
$lang['MSG_PRODUCT_CHECKOUT_23'] = 'e.g.  God bless you  / I miss you...';
$lang['MSG_PRODUCT_CHECKOUT_24'] = 'Etape 3 : Choisir sa methode de paiement sécurisé';
$lang['MSG_PRODUCT_CHECKOUT_25'] = 'Facile n´est ce pas ? Choisissez le mode de paiement qui vous convient , acceptez les conditions de vente et payez en toute sécurité.';
$lang['MSG_PRODUCT_CHECKOUT_26'] = 'Payment Method';
$lang['MSG_PRODUCT_CHECKOUT_27'] = 'Credit Card - VISA, MASTER, AMEX etc.';
$lang['MSG_PRODUCT_CHECKOUT_28'] = 'Paypal';
$lang['MSG_PRODUCT_CHECKOUT_29'] = 'Virement bancaire (1 &agrave; 2 jours)';
$lang['MSG_PRODUCT_CHECKOUT_30'] = 'Conditions génerales de ventes';
$lang['MSG_PRODUCT_CHECKOUT_31'] = 'J´accepte les conditions de vente';
$lang['MSG_PRODUCT_CHECKOUT_32'] = 'Confirmez le payement ';
$lang['MSG_PRODUCT_CHECKOUT_33'] = 'Svp remplissez les informations du bénéficiaire!';
$lang['MSG_PRODUCT_CHECKOUT_34'] = 'Vous devez valider les conditions d´utilisation !';
$lang['MSG_PRODUCT_CHECKOUT_35'] = 'Methode de payement invalide!';
$lang['MSG_PRODUCT_CHECKOUT_36'] = 'Il ya surement un soucis ! Svp veillez choisir une autre méthode de payement.';
$lang['MSG_PRODUCT_CHECKOUT_'] = '';
$lang['MSG_PRODUCT_CHECKOUT_37'] = 'Stripe server is down. Please try with another payment method.';
$lang['MSG_PRODUCT_CHECKOUT_38'] = 'veillez vous connecter pour valider le payement.';
$lang['MSG_PRODUCT_CHECKOUT_39'] = 'Please contact to administrator.<br>';
$lang['MSG_PRODUCT_CHECKOUT_40'] = 'Please wait, your request is being processed and you will be redirect to the paypal gateway.';
$lang['MSG_PRODUCT_CHECKOUT_41'] = 'Hang in there ! We are processing your request please wait...';
$lang['MSG_PRODUCT_CHECKOUT_42'] = "Très cool! C´est déja la derniere étape!";
$lang['MSG_PRODUCT_CHECKOUT_43'] = "Logon you to accept the package, otherwise register for free to enjoy the service.";
$lang['MSG_PRODUCT_CHECKOUT_44'] = 'SEPA Direct Debit (1 &agrave; 2 jours)';
$lang['MSG_PRODUCT_CHECKOUT_45'] = 'IBAN';
$lang['MSG_PRODUCT_CHECKOUT_46'] = 'Enter IBAN number without space.';
$lang['MSG_PRODUCT_CHECKOUT_47'] = 'SOFORT';
$lang['MSG_PRODUCT_CHECKOUT_48'] = 'S�lectionnez le pays de votre banque';
$lang['MSG_PRODUCT_CHECKOUT_49'] = 'BANCONTACT';

/** Order confirmation Page Section **/
$lang['MSG_OC_TITLE'] = 'Order Confirmation: ' . @SITE_NAME_COM;
$lang['MSG_OC_01'] = 'Order Confirmation';
$lang['MSG_OC_02'] = 'Youpiii!!! #USER_NAME#,';
$lang['MSG_OC_03'] = 'Votre paquet pour <span style="color:#ee3682;text-transform: uppercase;">#RECEIVER_NAME#</span> a bien été expédié.
Vous n´avez plus rien &agrave; faire.On se charge de tout !';
$lang['MSG_OC_04'] = 'Le code de retrait est : <span style="color:#ee3682;">#CODE#</span>.Dès reception du paiement <span style="color:#ee3682;">#RECEIVER_NAME#</span> recevra le code par SMS et sera recontacté dans les 36 heures pour le retrait.<br><h5>Merci!</h5>';
$lang['MSG_OC_05'] = 'Envoyez rapidement un autre paquet ? ou retournez &agrave; l´acceuil';
$lang['MSG_OC_06'] = 'Bank Details';
$lang['MSG_OC_07'] = 'Please transfer [#AMOUNT#] in below bank account.';
$lang['MSG_OC_08'] = 'Holder Name';
$lang['MSG_OC_09'] = 'Banque';
$lang['MSG_OC_10'] = 'IBAN';
$lang['MSG_OC_11'] = 'BIC';
$lang['MSG_OC_12'] = 'Ref/VW';
$lang['MSG_OC_13'] = 'Oops! #USER_NAME#,';
$lang['MSG_OC_14'] = 'There is something wrong. Please contact to administrator.';


/** View Order Page Section **/
$lang['MSG_VO_TITLE'] = 'Mes commande: ' . @SITE_NAME_COM;
$lang['MSG_VO_01'] = 'Mes commandes';
$lang['MSG_VO_02'] = 'There is no any order available in your account!';
$lang['MSG_VO_03'] = 'Voir les détails';
$lang['MSG_VO_04'] = 'Total commande';
$lang['MSG_VO_05'] = 'Order detail found!';
$lang['MSG_VO_06'] = 'Order detail detail not available!';
$lang['MSG_VO_07'] = 'Order Code';
$lang['MSG_VO_08'] = 'Receiver Name';
$lang['MSG_VO_09'] = 'Receiver Contact No.';
$lang['MSG_VO_10'] = 'Greeting Message';
$lang['MSG_VO_11'] = 'Order Date';
$lang['MSG_VO_12'] = 'Payment Method';
$lang['MSG_VO_13'] = 'Order Status';
$lang['MSG_VO_14'] = 'Order Detail';
$lang['MSG_VO_15'] = 'Close';

/** Change Password Page Section **/
$lang['MSG_CP_TITLE'] = 'Changer le mot de passe: ' . @SITE_NAME_COM;
$lang['MSG_CP_01'] = 'Changer le mot de passe';
$lang['MSG_CP_02'] = 'Mot de passe actuel';
$lang['MSG_CP_03'] = 'Your Current Password';
$lang['MSG_CP_04'] = 'nouveau mot de passe';
$lang['MSG_CP_05'] = 'Enter Your New Password';
$lang['MSG_CP_06'] = 'Répété le nouveau mot de passe';
$lang['MSG_CP_07'] = 'Enter Repeat New Password';
$lang['MSG_CP_08'] = 'Changer le mot de passe';
$lang['LBL_CP_VALIDATION_01'] = 'Please Enter Your Current Password!';
$lang['LBL_CP_VALIDATION_02'] = 'Please Enter Your New Password!';
$lang['LBL_CP_VALIDATION_03'] = 'Please Enter Your Repeat Password!';
$lang['LBL_CP_VALIDATION_04'] = 'Repeat Password is do not match!';
$lang['LBL_CP_VALIDATION_05'] = 'Password must be at least 8 characters!';
$lang['MSG_CP_INVALID'] = 'Your current password does not match. Please enter correct password.';
$lang['MSG_CP_SUCCESS'] = 'Your password has been changed successfully.';
$lang['MSG_CP_FAILED'] = 'Your change password request failed. Please try again.';


/** My Account Page Section **/
$lang['MSG_MA_TITLE'] = 'My Account: ' . @SITE_NAME_COM;
$lang['MSG_MA_01'] = 'My Account';
$lang['MSG_MA_02'] = 'Editez votre compte';
$lang['MSG_MA_03'] = 'Pour davantage sécuriser vos transferts vous devez mettre &agrave; jour votre profil.';
$lang['MSG_MA_04'] = 'Prénom';
$lang['MSG_MA_05'] = 'First Name';
$lang['MSG_MA_06'] = 'Nom de famille';
$lang['MSG_MA_07'] = '* denotes required form fields ';
$lang['MSG_MA_08'] = 'Contact Details';
$lang['MSG_MA_09'] = 'Email';
$lang['MSG_MA_10'] = 'Enter your email address';
$lang['MSG_MA_11'] = 'Phone Number';
$lang['MSG_MA_12'] = 'Address Details';
$lang['MSG_MA_13'] = 'Country';
$lang['MSG_MA_14'] = 'City';
$lang['MSG_MA_15'] = 'Address';
$lang['MSG_MA_16'] = 'Postal Code';
$lang['MSG_MA_17'] = 'Notification Preferences';
$lang['MSG_MA_18'] = 'Choose how you\'d like to be kept in the loop by familov';
$lang['MSG_MA_19'] = 'Let me know about news and offers by email';
$lang['MSG_MA_20'] = 'Let me know about news and offers by text';
$lang['MSG_MA_21'] = 'METTRE À JOUR';
$lang['MSG_MA_22'] = 'Your profile has been updated successfully!';
$lang['MSG_MA_23'] = 'Your profile update request failed.Please try again.';
$lang['LBL_MA_VALIDATION_01'] = 'Please Enter First Name!';
$lang['LBL_MA_VALIDATION_02'] = 'Please Enter Last Name!';
$lang['LBL_MA_VALIDATION_03'] = 'Please Select Country!';
$lang['LBL_MA_VALIDATION_04'] = 'Please Enter City!';
$lang['LBL_MA_VALIDATION_05'] = 'Please Enter Phone Number!';
$lang['LBL_MA_VALIDATION_06'] = 'Please Enter Address!';
$lang['LBL_MA_VALIDATION_07'] = 'Please Enter Postal Code!';

$lang['MSG_WAIT'] = 'Please Wait...';
$lang['MSG_COPY'] = 'Copy URL to clipboard successfully.';


/** Invite Friend Page Section **/
$lang['MSG_IF_TITLE'] = 'Invite Friend: ' . @SITE_NAME_COM;
$lang['MSG_IF_01'] = 'Gagnez de l´argent avec vos amis :)';
$lang['MSG_IF_02'] = 'Invite Your Friends';
$lang['MSG_IF_03'] = 'Copy the link';
$lang['MSG_IF_04'] = 'Or click to invite friends on Facebook';
$lang['MSG_IF_05'] = 'Facebook';
$lang['MSG_IF_06'] = 'Your win';
$lang['MSG_IF_07'] = 'You have used always';
$lang['MSG_IF_08'] = 'Available for checkout';


/** Forgot Password Page Section **/
$lang['MSG_FP_TITLE'] = 'Mot de passe oublié?: ' . @SITE_NAME_COM;
$lang['MSG_FP_01'] = 'Mot de passe oublié?';
$lang['MSG_FP_02'] = 'Envoyez le lien';


/** Reset Password Page Section **/
$lang['MSG_RP_TITLE'] = 'Reset Password: ' . @SITE_NAME_COM;
$lang['MSG_RP_01'] = 'Reset Password';
$lang['MSG_RP_02'] = 'Reset Password';

$lang['MSG_BAD_REQUEST'] = 'Something data are missing. Please check your request.';
/** Shop Change **/
$lang['SHOP_CHANGE_01'] = 'Changement de magasin';
$lang['SHOP_CHANGE_02'] = '<^êtes vous sûre de vouloir changer de magasin ?';
$lang['SHOP_CHANGE_03'] = 'Yes';
$lang['SHOP_CHANGE_04'] = 'No';
$lang['SHOP_CHANGE_05'] = 'Si vous changez de magasin, vous perdrez votre panier';
$lang['SHOP_CHANGE_06'] = 'Are You Sure?';


$lang['MSG_USER_NOT_FOUND'] = 'Utilisateur invalide!';
$lang['MSG_ORDER_NOT_FOUND'] = 'Order(s) Not Found!';
$lang['MSG_ORDER_DETAIL_NOT_FOUND'] = 'Order Detail Not Found!';