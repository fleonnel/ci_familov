<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CController extends CI_Controller
{
    protected $data = array();
    protected $meta = array();

    function __construct()
    {
        parent::__construct();
        $this->data['setting_page_title'] = 'CI App';
    }

    protected function render($the_view = NULL, $template = 'admin')
    {
        if($template == 'json' || $this->input->is_ajax_request())
        {
              header('Content-Type: application/json');
              echo json_encode($this->data);
        }
        else
        {
              $this->data['the_view_content'] = (is_null($the_view)) ? '' : $this->load->view($the_view,$this->data, TRUE);
              $this->load->view('template/'.$template.'_view', $this->data);
        }
    }
}