<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cron_Controller extends CController
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('constants_admin_helper');
        $this->data['page_title'] = 'Cron Job';
    }

    protected function render($the_view = NULL, $template = 'cron')
    {
        $the_view = "cron/" . $the_view;
        parent::render($the_view, $template);
    }
}