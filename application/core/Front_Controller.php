<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Front_Controller extends CController
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('constants_front_helper');
        $this->load->helper('url');
        $class = $this->router->fetch_class();
        $method = $this->router->fetch_method();
        $this->class = $class;
        $this->method = $method;
        $this->current_language = $this->session->userdata('site_lang');
        $this->data['page_title'] = 'Welcome to Familov';
        $currency_list = set_currency();
        $this->data['currency_list'] = $currency_list;
        $language_list = set_language();
        $this->data['language_list'] = $language_list;
        #_pre($this->session->userdata());
        #$this->session->unset_userdata('cart');
        $cart_total_item = count($this->session->userdata('cart'));
        $this->cart_total_item = $cart_total_item;
        #_pre($cart_total_item);
        $this->currency_symbol = $this->session->userdata('currency')->vSymbol;
        $this->currency_code = $this->session->userdata('currency')->vCode;
        if (@$this->session->userdata(SESSION_SITE)->is_login) {
            #_pre($this->session->userdata(SESSION_SITE));
            $this->data['header_full_name'] = $this->session->userdata(SESSION_SITE)->username . " " . $this->session->userdata(SESSION_SITE)->lastname;
            $this->data['customer_id'] = $this->session->userdata(SESSION_SITE)->customer_id;
            $this->customer_id = $this->session->userdata(SESSION_SITE)->customer_id;
            $this->email = $this->session->userdata(SESSION_SITE)->email_address;
            $this->is_login = true;
            $this->user_name = $this->data['header_full_name'];
        } else {
            $this->customer_id = 0;
        }
    }
    protected function render($the_view = NULL, $template = 'front')
    {
        $the_view = "front/" . $the_view;
        parent::render($the_view, $template);
    }

}