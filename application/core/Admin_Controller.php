<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_Controller extends CController
{
    public $admin_id = null;
    public $roles_id = null;
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin_model');
        $this->load->model('admin/acl_model');
        $this->load->helper('constants_admin_helper');
        $this->load->library('admin/nav');
        
        // Breadcrumbs Setting
        $this->load->config('breadcrumbs',true);
        $this->config->set_item('crumb_divider', '');
        $this->config->set_item('tag_open', '<ol class="breadcrumb">');
        $this->config->set_item('tag_close', '</ol>');
        $this->config->set_item('crumb_open', '<li>');
        $this->config->set_item('crumb_last_open', '<li class="active">');
        $this->config->set_item('crumb_close', '</li>');
        $this->load->library('breadcrumbs');
        $this->breadcrumbs->push('<i class="fa fa-dashboard"></i> Home','admin/dashboard/');
        //End Breadcrumbs
        $this->data['setting_page_title'] = 'Admin Panel';
        $this->data['page_title'] = 'Admin Panel';
        $class = $this->router->fetch_class();
        $method = $this->router->fetch_method();
        $this->data['class'] = $class;
        $this->data['method'] = $method;
        $this->class = $class;
        $this->method = $method;
        if ($class != 'home') {
            if (!$this->session->userdata(SESSION_NAME)->is_master_admin_login) {
                redirect(base_url(ROOT_DIR_ADMIN.'login'));
            } else {
                $this->data['session_master_admin'] = $this->session->userdata(SESSION_NAME);
                $this->data['header_full_name'] = $this->session->userdata(SESSION_NAME)->first_name." ".$this->session->userdata(SESSION_NAME)->last_name;
                $this->admin_id = $this->session->userdata(SESSION_NAME)->admin_id;
                $this->roles_id = $this->session->userdata(SESSION_NAME)->roles_id;
                $this->acl_model->getAccess($this->roles_id);
                if(!$this->acl_model->isAllowed($class,$method)) {
                    _set_flashdata(FLASH_STATUS_ERROR, MESSAGE_ACCESS_DENIED, FLASH_HTML);
                    redirect(admin_url(DASHBOARD_C . 'access_denied'));
                }
            }
        } else {
            if ($method != 'logout') {
                if (isset($this->session->userdata(SESSION_NAME)->is_master_admin_login)) {
                    redirect(base_url(ROOT_DIR_ADMIN.DASHBOARD_C));
                }
            }
            if ($method == 'logout') {
                $this->admin_id = $this->session->userdata(SESSION_NAME)->admin_id;
                $this->roles_id = $this->session->userdata(SESSION_NAME)->roles_id;
            }
        }
    }

    protected function render($the_view = NULL, $template = 'admin')
    {
    	#$the_view="admin/".$the_view;
    	$the_view = ADMIN_D.$the_view;
        parent::render($the_view, $template);
    }
}