<?php

/* FOR STATUS OPERATION MESSAGE */
define('DATA_TABLE_ERROR_STATUS_UPDATE', 'Status Update Failed !');
define('DATA_TABLE_SUCCESS_STATUS_UPDATE_ACTIVE', 'Status Activated Successfully.');
define('DATA_TABLE_SUCCESS_STATUS_UPDATE_INACTIVE', 'Status Inactivated Successfully.');
define('MESSAGE_LAST_OPERATION_DISCARD', 'Discard Last Operation.');
define('MESSAGE_INFORMATION_NOT_AVAILABLE', 'Information Not Available.');

/* FOR STATUS OPERATION MESSAGE END */


/***
 * ROUTE
 *  D : For Directory
 *  C : For Controller
 *  M : For Model
 *  V : For View (Do not add slash)
 **/

/* Define Directory*/
if (!defined('ROOT_DIR_FRONT')) define('ROOT_DIR_FRONT', 'front/');
if (!defined('HOME_D')) define('HOME_D', 'home/');
if (!defined('CMS_D')) define('CMS_D', 'cms/');
if (!defined('USER_D')) define('USER_D', 'user/');
if (!defined('PRODUCT_D')) define('PRODUCT_D', 'product/');

/* Define Controller */
if (!defined('HOME_C')) define('HOME_C', 'home/');
if (!defined('CMS_C')) define('CMS_C', 'cms/');
if (!defined('USER_C')) define('USER_C', 'user/');
if (!defined('PRODUCT_C')) define('PRODUCT_C', 'product/');

/* Home Directory Views */
if (!defined('HOME_V')) define('HOME_V', 'home');
if (!defined('LOGIN_V')) define('LOGIN_V', 'login_view');
if (!defined('FORGOT_PASSWORD_V')) define('FORGOT_PASSWORD_V', 'forgot_password_view');
if (!defined('RESET_PASSWORD_V')) define('RESET_PASSWORD_V', 'reset_password_view');
if (!defined('CHANGE_PASSWORD_V')) define('CHANGE_PASSWORD_V', 'change_password_view');
if (!defined('DASHBOARD_V')) define('DASHBOARD_V', 'dashboard');
if (!defined('MY_ACCOUNT_V')) define('MY_ACCOUNT_V', 'my_account_view');
if (!defined('INVITE_FRIENDS_V')) define('INVITE_FRIENDS_V', 'invite_friends_view');
if (!defined('ACCESS_DENIED_V')) define('ACCESS_DENIED_V', 'view_access_denied');

/* User Directory Views */
if (!defined('SIGN_UP_V')) define('SIGN_UP_V', 'sign_up_view');
if (!defined('LOG_IN_V')) define('LOG_IN_V', 'log_in_view');

/* CMS Directory Views */
if (!defined('FAQ_V')) define('FAQ_V', 'faq_view');
if (!defined('HOW_IT_WORKS_V')) define('HOW_IT_WORKS_V', 'how_it_works_view');
if (!defined('TERMS_V')) define('TERMS_V', 'terms_view');
if (!defined('PRIVACY_V')) define('PRIVACY_V', 'privacy_view');
if (!defined('ABOUT_US_V')) define('ABOUT_US_V', 'about_us_view');
if (!defined('CONTACT_US_V')) define('CONTACT_US_V', 'contact_us_view');
if (!defined('PRESS_V')) define('PRESS_V', 'press_view');
if (!defined('JOBS_V')) define('JOBS_V', 'jobs_view');

/* User Directory Views */
if (!defined('PRODUCT_LISTING_V')) define('PRODUCT_LISTING_V', 'product_list_view');
if (!defined('PRODUCT_DETAIL_V')) define('PRODUCT_DETAIL_V', 'product_detail_view');
if (!defined('PRODUCT_CART_V')) define('PRODUCT_CART_V', 'cart_view');
if (!defined('PRODUCT_CHECKOUT_V')) define('PRODUCT_CHECKOUT_V', 'checkout_view');
if (!defined('ORDER_SUCCESS_V')) define('ORDER_SUCCESS_V', 'order_success_view');
if (!defined('ORDER_PROCEED_V')) define('ORDER_PROCEED_V', 'order_proceed_view');
if (!defined('ORDER_LIST_V')) define('ORDER_LIST_V', 'order_list_view');

/**
 * Templating Section
 **/
if (!defined('THEME_HEADER_SECTION')) define('THEME_HEADER_SECTION', 'template/front/_header');
if (!defined('THEME_BREADCRUMB_SECTION')) define('THEME_BREADCRUMB_SECTION', 'template/front/_breadcrumb');
if (!defined('THEME_LEFT_MENU_SECTION')) define('THEME_LEFT_MENU_SECTION', 'template/front/_leftmenu');
if (!defined('THEME_TOP_MENU_SECTION')) define('THEME_TOP_MENU_SECTION', 'template/front/_topmenu');
if (!defined('THEME_RIGHT_MENU_SECTION')) define('THEME_RIGHT_MENU_SECTION', 'template/front/_rightmenu');
if (!defined('THEME_FOOTER_SECTION')) define('THEME_FOOTER_SECTION', 'template/front/_footer');
if (!defined('THEME_CSS_SECTION')) define('THEME_CSS_SECTION', 'template/front/_css');
if (!defined('THEME_JS_SECTION')) define('THEME_JS_SECTION', 'template/front/_js');


/**
 * mENU
 **/
if (!defined('MENU_FAQ')) define('MENU_FAQ', 'faq');
if (!defined('MENU_LANGUAGE_CHANGE')) define('MENU_LANGUAGE_CHANGE', 'language-change/');
if (!defined('MENU_CURRENCY_CHANGE')) define('MENU_CURRENCY_CHANGE', 'currency-change/');
if (!defined('MENU_SIGN_UP')) define('MENU_SIGN_UP', 'sign-up');
if (!defined('MENU_LOG_IN')) define('MENU_LOG_IN', 'log-in');
if (!defined('MENU_LOG_OUT')) define('MENU_LOG_OUT', 'log-out');
if (!defined('MENU_MY_ACCOUNT')) define('MENU_MY_ACCOUNT', 'my-account');
if (!defined('MENU_MY_ACCOUNT_UPDATE')) define('MENU_MY_ACCOUNT_UPDATE', 'my-account-update');
if (!defined('MENU_FORGOT_PASSWORD')) define('MENU_FORGOT_PASSWORD', 'forgot-password');
if (!defined('MENU_FORGOT_PASSWORD_SEND')) define('MENU_FORGOT_PASSWORD_SEND', 'forgot-password-send');
if (!defined('MENU_CHANGE_PASSWORD')) define('MENU_CHANGE_PASSWORD', 'change-password');
if (!defined('MENU_RESET_PASSWORD')) define('MENU_RESET_PASSWORD', 'reset-password');
if (!defined('MENU_RESET_PASSWORD_UPDATE')) define('MENU_RESET_PASSWORD_UPDATE', 'reset-password-update');
if (!defined('MENU_TERMS')) define('MENU_TERMS', 'terms');
if (!defined('MENU_PRIVACY')) define('MENU_PRIVACY', 'privacy');
if (!defined('MENU_ABOUT_US')) define('MENU_ABOUT_US', 'about-us');
if (!defined('MENU_CONTACT_US')) define('MENU_CONTACT_US', 'contact-us');
if (!defined('MENU_PRESS')) define('MENU_PRESS', 'press');
if (!defined('MENU_JOBS')) define('MENU_JOBS', 'jobs');
if (!defined('MENU_HOW_IT_WORKS')) define('MENU_HOW_IT_WORKS', 'how-it-works');
if (!defined('MENU_PRODUCT_LIST')) define('MENU_PRODUCT_LIST', 'product-list');
if (!defined('MENU_PRODUCT_SEARCH')) define('MENU_PRODUCT_SEARCH', 'product-search');
if (!defined('MENU_PRODUCT_DETAIL')) define('MENU_PRODUCT_DETAIL', 'product-detail');
if (!defined('MENU_PRODUCT_LISTING')) define('MENU_PRODUCT_LISTING', 'product-listing');
if (!defined('MENU_CHANGE_SHOP')) define('MENU_CHANGE_SHOP', 'shop-change');
if (!defined('MENU_ADD_TO_CART')) define('MENU_ADD_TO_CART', 'add-to-cart');
if (!defined('MENU_VIEW_CART')) define('MENU_VIEW_CART', 'view-cart');
if (!defined('MENU_UPDATE_CART')) define('MENU_UPDATE_CART', 'update-cart');
if (!defined('MENU_DELETE_CART_ITEM')) define('MENU_DELETE_CART_ITEM', 'delete-cart-item');
if (!defined('MENU_CHECKOUT')) define('MENU_CHECKOUT', 'checkout');
if (!defined('MENU_ORDER_DELIVERY')) define('MENU_ORDER_DELIVERY', 'order-delivery');
if (!defined('MENU_PROMO_CODE')) define('MENU_PROMO_CODE', 'promo-code');
if (!defined('MENU_CHECKOUT_PROCEED')) define('MENU_CHECKOUT_PROCEED', 'checkout-proceed');
if (!defined('MENU_ORDER_CONFIRMATION')) define('MENU_ORDER_CONFIRMATION', 'order-confirmation');
if (!defined('MENU_MY_ORDER')) define('MENU_MY_ORDER', 'view-order');
if (!defined('MENU_PAYPAL_RETURN')) define('MENU_PAYPAL_RETURN', 'paypal-return');
if (!defined('MENU_ORDER_DETAIL_MODAL')) define('MENU_ORDER_DETAIL_MODAL', 'order-detail-modal');
if (!defined('MENU_INVITE_FRIENDS')) define('MENU_INVITE_FRIENDS', 'invite-friends');
if (!defined('MENU_WARNING_VIEW')) define('MENU_WARNING_VIEW', 'shop_change');
if (!defined('CHANGE_SHOP')) define('CHANGE_SHOP', 'shop-change-page');
if (!defined('MENU_SOFORT_RETURN')) define('MENU_SOFORT_RETURN', 'sofort-return');
if (!defined('MENU_BANCONTACT_RETURN')) define('MENU_BANCONTACT_RETURN', 'bancontact-return');

/*** Footer ***/
if (!defined('SOCIAL_FACEBOOK_URL')) define('SOCIAL_FACEBOOK_URL', 'https://www.facebook.com/Familover/');
if (!defined('SOCIAL_TWITTER_URL')) define('SOCIAL_TWITTER_URL', 'https://twitter.com/familovly');


# Login Section
if (!defined('MSG_LOGIN_INVALID')) define('MSG_LOGIN_INVALID', '<strong>Access Denied !</strong><br> Invalid Username/Password');
if (!defined('MSG_FORGOT_PASSWORD_INVALID')) define('MSG_FORGOT_PASSWORD_INVALID', '<strong>Access Denied !</strong> Username/Email not exist.');
if (!defined('MSG_FORGOT_PASSWORD_SUCCESS')) define('MSG_FORGOT_PASSWORD_SUCCESS', 'We received your forgot password request.<br> We will send you mail on {EMAIL} with new password.');

if (!defined('MSG_SITE_LOGIN_SUCCESS')) define('MSG_SITE_LOGIN_SUCCESS', 'Login Successfully !');
if (!defined('MSG_SITE_LOGOUT_SUCCESS')) define('MSG_SITE_LOGOUT_SUCCESS', 'Logout Successfully !');
if (!defined('ALL')) define('ALL', 'all/');