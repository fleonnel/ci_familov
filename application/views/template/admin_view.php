<?=doctype('html5');?>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!--<link rel="icon" href="<?php /*echo base_url(ASSETS_ADMIN . 'dist/img/favicon.ico') */ ?>" type="image/gif"
          sizes="16x16">-->
    <title><?php echo $page_title; ?> </title>
    <link rel="shortcut icon" href="<?php echo base_url(ASSETS . 'common/favicon.png'); ?>" type="image/png">
    <link rel="icon" href="<?php echo base_url(ASSETS . 'common/favicon.png'); ?>" type="image/png">
    <?= $this->load->view(THEME_CSS_SECTION); ?>
    <?= $this->load->view(THEME_JS_SECTION); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <!-- Header Section -->
    <?= $this->load->view(THEME_HEADER_SECTION); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?= $this->load->view(THEME_LEFT_MENU_SECTION); ?>
    <div class="content-wrapper" style="background-color:#ffffff">
        <!-- Content Header (Page header) -->
        <?= $this->load->view(THEME_BREADCRUMB_SECTION); ?>
        <!-- Content Wrapper. Contains page content -->
        <?php echo $the_view_content; ?>
        <!-- /.content-wrapper -->
    </div>
    <?= $this->load->view(THEME_FOOTER_SECTION); ?>
    <!-- Control Sidebar -->
    <?= $this->load->view(THEME_RIGHT_MENU_SECTION); ?>
</div>
</body>
</html>