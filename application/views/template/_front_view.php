<!DOCTYPE html>
<!-- define angular app -->
<!-- ng-app="myApp" -->
<html ng-app="rsApp">

<head>
    <!-- SCROLLS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.css"/>

    <!-- SPELLS -->
    <script>
        var SiteURL = '<?php echo base_url(); ?>';
    </script>
    <script src="<?php echo base_url(); ?>assets/js/jquery-1.10.2.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <!--<script src="<?php /*echo base_url(); */ ?>assets/angular/angular.min.js"></script>-->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/angular/app/config.js"></script>
    <script src="<?php echo base_url(); ?>assets/angular/controller/site_route.js"></script>
    <script src="<?php echo base_url(); ?>assets/angular/controller/commonController.js"></script>
    <!--<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.25/angular-route.js"></script>-->
    <!--<script src="<?php /*echo base_url(); */ ?>assets/js/script.js"></script>-->

</head>
<!-- define angular controller -->
<!--ng-controller="mainController"-->
<body>
<?= $this->load->view('template/front/_top_menu'); ?>
<div id="main">
    <!-- angular templating -->
    <!-- this is where content will be injected -->
    <!--<div ng-view></div>-->
    <?php echo $the_view_content; ?>
</div>
<toaster-container toaster-options="{'time-out': 3000}"></toaster-container>
<?= $this->load->view('template/front/_footer'); ?>
</body>
</html>
