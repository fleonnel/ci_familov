<!-- THEME JS Section -->
<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url(ASSETS_ADMIN.'plugins/jQuery/jquery-2.2.3.min.js');?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url(ASSETS_ADMIN.'bootstrap/js/bootstrap.min.js');?>"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url(ASSETS_ADMIN.'plugins/morris/morris.min.js');?>"></script>
<!-- Sparkline -->
<script src="<?php echo base_url(ASSETS_ADMIN.'plugins/sparkline/jquery.sparkline.min.js');?>"></script>

<!-- jvectormap -->
<script src="<?php echo base_url(ASSETS_ADMIN.'plugins/jvectormap/jquery-jvectormap-1.2.2.min.js');?>"></script>
<script src="<?php echo base_url(ASSETS_ADMIN.'plugins/jvectormap/jquery-jvectormap-world-mill-en.js');?>"></script>

<!-- jQuery Knob Chart -->
<script src="<?php echo base_url(ASSETS_ADMIN.'plugins/knob/jquery.knob.js');?>"></script>
<script src="<?php echo base_url(ASSETS_ADMIN.'plugins/chartjs/Chart.js');?>"></script>

<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo base_url(ASSETS_ADMIN.'plugins/daterangepicker/daterangepicker.js');?>"></script>

<!-- datepicker -->
<script src="<?php echo base_url(ASSETS_ADMIN.'plugins/datepicker/bootstrap-datepicker.js');?>"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url(ASSETS_ADMIN.'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js');?>"></script>

<!-- Slimscroll -->
<script src="<?php echo base_url(ASSETS_ADMIN.'plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url(ASSETS_ADMIN.'plugins/fastclick/fastclick.js');?>"></script>

<!-- iCheck -->
<script src="<?php echo base_url(ASSETS_ADMIN . 'plugins/iCheck/icheck.min.js'); ?>"></script>
<script src="<?php echo base_url(ASSETS_ADMIN.'plugins/froala_editor/js/froala_editor.pkgd.min.js');?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(ASSETS_ADMIN.'dist/js/app.min.js');?>"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!--<script src="<?php /*echo base_url(ASSETS_ADMIN.'dist/js/pages/dashboard.js');*/?>"></script>-->

<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(ASSETS_ADMIN.'dist/js/demo.js');?>"></script>
<!-- ckeditor-->
<script src="<?php echo base_url(ASSETS_ADMIN.'plugins/ckeditor/ckeditor.js');?>"></script>
<!-- EXTRA JS SECTION -->
<script src="<?php echo base_url(ASSETS_BUILD_JS.'bootstrapValidator.js');?>"></script>
<script src="<?php echo base_url(ASSETS.'js/validate.js');?>"></script>
<script src="<?= EXTRA_PLUGIN ?>validate/jquery.validate.min.js"></script>

<script src="<?= EX_PLUGINS ?>toastr/toastr.min.js"></script>
<!-- Datatable -->
<?php
echo link_tag(EXTRA_PLUGIN . 'datatables/css/jquery.dataTables.min.css');
?>
<script src="<?php echo EXTRA_PLUGIN . 'datatables/js/datatables.min.js' ?>"></script>