<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars" aria-hidden="true"></i></a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <!--<li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-cog" aria-hidden="true"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <li><a href="javascript:;"> XML Upload</a></li>
                        <li><a href="javascript:;">Manage User</a></li>
                    </ul>
                </li>-->
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                       aria-expanded="false">
                        <?= $this->session->userdata(SESSION_NAME)['first_name'] . " " . $this->session->userdata(SESSION_NAME)['last_name']; ?>
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <li><a href="<?php echo base_url('admin/dashboard/newPass'); ?>"> Change Password</a></li>
                        <li><a href="<?php echo base_url('admin/home/logout'); ?>"> Sign Out</a></li>
                        <li><a href="javascript:;">Help</a></li>
                    </ul>
                </li>
                <!--<li role="presentation" class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                        Subscription <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                        <li>Some examples Some examplesSome examplesSome examples </li>
                    </ul>
                </li>-->
            </ul>
        </nav>
    </div>
</div>
<!-- /top navigation -->
