<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="<?php echo base_url(ASSETS_ADMIN.'bootstrap/css/bootstrap.min.css');?>">
<!-- Font Awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!--<link rel="stylesheet" href="<?php /*echo base_url(ASSETS_ADMIN . 'bootstrap/css/font-awesome.min.css'); */ ?>">-->
<!-- Ionicons -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!--<link rel="stylesheet" href="<?php /*echo base_url(ASSETS_ADMIN . 'bootstrap/css/ionicons.min.css'); */ ?>">-->
<link rel="stylesheet" href="<?php echo base_url(ASSETS_ADMIN . 'plugins/select2/select2.min.css'); ?>">
<!-- Theme style -->
<link rel="stylesheet" href="<?php echo base_url(ASSETS_ADMIN.'dist/css/AdminLTE.min.css');?>">
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="<?php echo base_url(ASSETS_ADMIN.'dist/css/skins/_all-skins.min.css');?>">
<!-- iCheck -->
<link rel="stylesheet" href="<?php echo base_url(ASSETS_ADMIN.'plugins/iCheck/flat/blue.css');?>">
<!-- Morris chart -->
<link rel="stylesheet" href="<?php echo base_url(ASSETS_ADMIN.'plugins/morris/morris.css');?>">

<!-- jvectormap -->
<link rel="stylesheet" href="<?php echo base_url(ASSETS_ADMIN.'plugins/jvectormap/jquery-jvectormap-1.2.2.css');?>">
<!-- Date Picker -->
<link rel="stylesheet" href="<?php echo base_url(ASSETS_ADMIN.'plugins/datepicker/datepicker3.css');?>">
<!-- Daterange picker -->
<link rel="stylesheet" href="<?php echo base_url(ASSETS_ADMIN.'plugins/daterangepicker/daterangepicker.css');?>">
<link rel="stylesheet" href="<?php echo base_url(ASSETS_ADMIN.'plugins/froala_editor/css/froala_editor.pkgd.min.css');?>">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="<?php echo base_url(ASSETS_ADMIN.'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css');?>">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Extra CSS SECTION -->
<!-- Notification-->
<link href="<?= EXTRA_PLUGIN ?>toastr/toastr.min.css" rel="stylesheet">
<!-- SweetAlert-->
<link href="<?= EXTRA_PLUGIN ?>sweetalert/sweetalert.css" rel="stylesheet">

<!-- NProgress -->
<link rel="stylesheet" href="<?php echo base_url(ASSETS.'vendors/nprogress/nprogress.css');?>">
<!-- bootstrap-wysiwyg -->
<link rel="stylesheet" href="<?php echo base_url(ASSETS.'vendors/google-code-prettify/bin/prettify.min.css');?>">

<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url(ASSETS_ADMIN.'custom/custom.css');?>">
<style>
    .btn-success{
        background-color: #2cfb93;
        border-color: #2cfb93;
        color: #222222;
     
    
    }
    .btn-warning{
        background-color: #eaff00;
        border-color: #eaff00;
        color: #222222;
       
    
    }
    
    .btn-danger{
        background-color: #ef4036;
        border-color: #ef4036;
        color: #222222;
     
    
    }
    
     .btn-primary{
        background-color: #2ddff3;
        border-color: #2ddff3;
        color: #222222;
       
    
    }
     .btn-info{
        background-color: #ff3fd7;
        border-color: #ff3fd7;
        color: #222222;
     
    
    }
    .label {
        
        padding: .2em .6em .3em;
        font-size: 100%;
    }
    
</style>