<header class="main-header"  >
    <!-- Logo -->
    <a href="#" class="logo" style="background-color:#fff!important">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <!--<span class="logo-mini"><b><? /*=COMPANY_NAME_SHORT_NAME*/ ?></b></span>-->
        <span class="logo-mini"><b><img src="<?php echo(ASSETS_URL . 'images/familov-logo.png'); ?>" class="img-circle"
                                        alt="<?= _get_config('admin_dashboard_title_short') ?>" width="80%"></b></span>
        <!-- logo for regular state and mobile devices -->
       
        <!--<span class="logo-lg"><b><?= _get_config('admin_dashboard_title_full') ?></b></span>-->
        <span class="logo-lg"> <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/familov-logo.png'); ?>" width="150px"></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" style="background-color:#2cfb93 !important">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" style="color:#222e32"data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <!--<li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-warning">10</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have 10 notifications</li>
                        <li>
                            <ul class="menu">
                                <li>
                                    <a href="#">
                                        <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                                        page and may cause design problems
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-users text-red"></i> 5 new members joined
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-user text-red"></i> You changed your username
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="footer"><a href="#">View all</a></li>
                    </ul>
                </li>-->
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?php echo base_url(ASSETS_ADMIN . 'dist/img/avatar5.png'); ?>" class="user-image"
                             alt="User Image">
                        <span class="hidden-xs" style="color:#222e32;"><?= @$header_full_name ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?php echo base_url(ASSETS_ADMIN . 'dist/img/avatar5.png'); ?>" class="img-circle"
                                 alt="User Image">
                            <p>
                                <?= @$header_full_name ?>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <li class="user-body">
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <a href="<?= @admin_url(ADMIN_C."update/".$this->admin_id)?>" ><button type="button" class="btn btn-success">Profile</button></a>
                                </div>
                            </div>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <!--<div class="pull-left">
                                <a href="<?= base_url(ADMIN_C.'change_password') ?>" class="btn btn-primary btn-flat">Change Password</a>
                            </div>-->
                            <div class="pull-right">
                                <a href="<?= base_url(ADMIN_D.HOME_C.'logout') ?>" class="btn btn-danger btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
                <li>
                    <!--<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>-->
                </li>
            </ul>
        </div>
    </nav>
</header>