<!-- =========================
     HEADER
============================== -->
<style>
   input[type=text],.form-control {   
        /* Remove First */
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
    
        /* Then Style */
       
        outline: none; 
        padding-left:15px;
    }
    select{
        
         background-image: url('../assets/front/img/images/down-arrow.png')  !important;
         background-size:15px 15px !important;
         background-repeat:no-repeat!important;
         background-position:94% center !important;
    }
    .navbar{
        
        padding-left:20px;
        padding-right:30px;
        /*Background:Transparent;*/
        box-shadow: none;
    }
    .navbar-nav>li>a {
        text-shadow: none;
       
        font-weight:bold;
    }
    .navbar-brand > img {
        display: block;
        
    }
    
    .top-nav-collapse {
        Background:#ffffff;
       box-shadow: 0 1px 3px rgba(0, 0, 0, .05);;
    }
    .top-nav-collapse li a{
        color:#566985 !important;
    }
    
    @media (max-width: 960px){
       
        .navbar {
            height: 40px;
            Background:#ffffff;
        }
        
        .navbar-brand > img {
            display: block;
            width:120px;
         }
         
         .form-bg {
        
            padding: 15px 2px 0px 15px !important;
            width:93%;
        }
        
        .safari .vertical-center-rel {
             margin-top: 170px !important;
        }
                
    }

</style>
<header id="nav1-3">
    <nav class="navbar navbar-fixed-top" id="main-navbar">
        <div class="container-full">
            <div class="navbar-header">
                <!-- Menu Button for Mobile Devices -->
                <button type="button" style="float:left" class="navbar-toggle" data-toggle="collapse"
                        data-target="#navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Image Logo -->
                <!--
                recommended sizes
                    width: 150px;
                    height: 35px;
                -->
                <a href="<?php echo base_url(); ?>" class="navbar-brand smooth-scroll"><img
                        src="<?php echo(ASSETS_URL_FRONT . 'img/images/familov-logo.png'); ?>" width="165px"
                        alt="logo familov"></a>

                <div class="navbar-toggle" style="float:right; margin-top: -5px;">
                    <a href="<?php echo base_url(MENU_VIEW_CART); ?>" class="smooth-scroll">
                        <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/familov-cart.svg'); ?>"
                             style="margin: 0px -34px 15px 7px;width:40px;">
                        <span id="no_of_selected_item_in_cart1" class="cart cart_total_item"><?= @$this->cart_total_item ?></span></a>
                </div>
               <!--  <div class="navbar-toggle" style=" display: inline-block;float:right;margin-left: 10px;margin-top: 5px;"><a
                                href="<?php echo base_url(MENU_VIEW_CART); ?>" class="smooth-scroll">
                                <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/familov-cart.svg'); ?> "
                                     style="margin: 0px -15px 22px 7px;" width="165px">
                                <span id="no_of_selected_item_in_cart1"
                                      class="cart cart_total_item"><?= @$this->cart_total_item ?></span></a></div> -->
                <!-- Image Logo For Background Transparent -->
                <!--
                    <a href="#" class="navbar-brand logo-black smooth-scroll"><img src="images/logo-black.png" alt="logo" /></a>
                    <a href="#" class="navbar-brand logo-white smooth-scroll"><img src="images/logo-white.png" alt="logo" /></a>
                -->
            </div>
            <!-- /End Navbar Header -->
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-left">
                </ul>
                <!-- /End Menu Links -->
                <!-- Menu Links -->
                <ul class="nav navbar-nav navbar-right" style="margin-top:-16px">
                    <!--  <li><a href="why.php" class="smooth-scroll">Why us?</a></li>-->
                    <li class="dropdown">
                        <a id="dLabel" data-toggle="dropdown" data-target="#" href="#"
                           style="text-transform: capitalize;"> <img
                                src="<?php echo(ASSETS_URL_FRONT . 'img/images/flag/' . @$this->session->userdata('language')->vLangCode . '.svg'); ?>"
                                style="width:20px;"> </a>
                        <ul class="dropdown-menu multi-level" role="menu">
                            <?php foreach (@$language_list as $k => $v) { ?>
                                <li><a href="<?php echo base_url(MENU_LANGUAGE_CHANGE . strtolower(@$v->vLanguage)); ?>"
                                       id=""
                                       class="curr_change1"><img
                                            src="<?php echo(ASSETS_URL_FRONT . 'img/images/flag/' . strtolower(@$v->vLangCode) . '.svg'); ?>"
                                            style="width:20px;"> <?= @$v->vLanguage ?></a></li>
                            <?php } ?>
                            <!--<li><a href="#" id="" class="currency_change"><img
                                        src="<?php /*echo(ASSETS_URL_FRONT . 'img/images/flag/en.svg'); */ ?>"
                                        style="width:20px;"> English</a></li>-->
                        </ul>
                    </li>
                    <!--<li>
                        <select
                            onchange="javascript:window.location.href='<?php /*echo base_url(MENU_LANGUAGE_CHANGE); */ ?>'+this.value;">
                            <option
                                value="english" <?php /*if ($this->session->userdata('site_lang') == 'english') echo 'selected="selected"'; */ ?>>
                                English
                            </option>
                            <option
                                value="french" <?php /*if ($this->session->userdata('site_lang') == 'french') echo 'selected="selected"'; */ ?>>
                                French
                            </option>
                        </select>
                    </li>-->
                    <li><a href="<?php echo base_url('faq'); ?>"
                           class="smooth-scroll"><?php echo $this->lang->line('LBL_MENU_01'); ?></a>
                    </li>
                    <?php if (!@$this->is_login) { ?>
                        <li><a href="<?php echo base_url(MENU_SIGN_UP); ?>"
                               class="btn-nav btn-green smooth-scroll"><?php echo $this->lang->line('LBL_MENU_02'); ?></a>
                        </li>
                        <li><a href="<?php echo base_url(MENU_LOG_IN); ?>"
                               class="btn-nav btn-green smooth-scroll"><?php echo $this->lang->line('LBL_MENU_03'); ?></a>
                        </li>
                    <?php } else { ?>
                        <li class="dropdown">
                            <a id="dLabel" data-toggle="dropdown" data-target="#" href="#"
                               style="text-transform: capitalize;"> <img
                                    src="<?php echo(ASSETS_URL_FRONT . 'img/images/user.png"'); ?>"
                                    style="width:25px;height:25px;"
                                    > <?php echo @$this->user_name; ?>
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu multi-level" role="menu">
                                <li>
                                    <a href="<?php echo base_url(MENU_MY_ORDER); ?>"><?= $this->lang->line('LBL_MENU_07') ?></a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(MENU_MY_ACCOUNT); ?>"><?= $this->lang->line('LBL_MENU_04') ?></a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(MENU_CHANGE_PASSWORD); ?>"><?= $this->lang->line('LBL_MENU_05') ?></a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(MENU_INVITE_FRIENDS); ?>"><?= $this->lang->line('LBL_MENU_08') ?></a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(MENU_LOG_OUT); ?>"><?= $this->lang->line('LBL_MENU_06') ?></a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                    <li class="dropdown">
                        <a id="dLabel" data-toggle="dropdown" data-target="#" href="#"
                           style="text-transform: capitalize;"> <?php echo @$this->session->userdata('currency')->vSymbol; ?>
                            <span
                                class="caret"></span></a>
                        <ul class="dropdown-menu multi-level" role="menu">
                            <?php foreach (@$currency_list as $k => $v) { ?>
                                <li><a href="<?php echo base_url(MENU_CURRENCY_CHANGE . @$v->vCode); ?>" id=""
                                       class="curr_change1"><?= @$v->vName . ' ' . @$v->vSymbol ?></a></li>
                            <?php } ?>
                            <!--<li><a href="<?php /*echo base_url(MENU_CURRENCY_CHANGE . 'USD'); */ ?>" id=""
                                   class="curr_change1">$</a></li>
                            <li><a href="<?php /*echo base_url(MENU_CURRENCY_CHANGE . 'EUR'); */ ?>" id=""
                                   class="curr_change1">EUR</a></li>-->
                        </ul>
                        <!--<select
                            onchange="javascript:window.location.href='<?php /*echo base_url(MENU_CURRENCY_CHANGE); */ ?>'+this.value;">
                            <option
                                value="USD" <?php /*if ($this->session->userdata('site_curr') == '$') echo 'selected="selected"'; */ ?>>
                                $
                            </option>
                            <option
                                value="EUR" <?php /*if ($this->session->userdata('site_curr') == 'EUR') echo 'selected="selected"'; */ ?>>
                                EUR
                            </option>
                        </select>-->
                    </li>
                    <li class="display-status">
                        <div style=" display: inline-block;margin-left: 10px;margin-top: 5px;"><a
                                href="<?php echo base_url(MENU_VIEW_CART); ?>" class="smooth-scroll">
                                <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/familov-cart.svg'); ?>"
                                     style="margin: 0px -17px 12px 7px; width:35px">
                                <span id="no_of_selected_item_in_cart"
                                      class="cart cart_total_item"><?= @$this->cart_total_item ?></span></a></div>
                    </li>
                    <li class="display-status">
                        <div style=" display: inline-block;margin-left: 10px;margin-top: 5px;">
                            <a href="<?php echo base_url(MENU_VIEW_CART); ?>" class="smooth-scroll"> </a>
                        </div>
                    </li>
                </ul>
                <!-- /End Menu Links -->
            </div>
            <!-- /End Navbar Collapse -->

        </div>
    </nav>
    <!-- /End Navbar -->
</header>
<!-- /End Header Section -->