<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">

            </div>
            <div class="pull-left info">
                <p></p>

                <button class="btn btn-xs dropdown-toggle btn-rounded" type="button" data-toggle="dropdown"
                        aria-expanded="true" style="font-size: 15px;background-color:#302e64;">
                    <i class="fa fa-circle status-icon available"></i> Available <i class="fa fa-angle-down"></i>
                </button>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1"
                    style="font-size: 10px;border:none;">
                    <li role="presentation"><a role="menuitem" href="/admin/home/logout"><i
                                class="fa fa-circle status-icon signout"></i> Sign out</a></li>
                </ul>
            </div>
        </div>
        <!-- search form -->
        <!--<form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>-->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <!--  <li class="header">MAIN NAVIGATION</li> -->
            <?= $this->nav->getAccountMenu(); ?>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>