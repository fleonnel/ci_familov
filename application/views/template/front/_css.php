<!-- BOOTSTRAP CSS -->
<link rel="stylesheet" href="<?php echo base_url(ASSETS_FRONT . 'css/bootstrap.min.css'); ?>">

<!-- FONT ICONS -->
<link rel="stylesheet" href="<?php echo base_url(ASSETS_FRONT . 'css/icons/iconfont.css'); ?>">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<!-- GOOGLE FONTS -->
<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic'
      rel='stylesheet' type='text/css'>

<!-- PLUGINS STYLESHEET -->
<link rel="stylesheet" href="<?= base_url(ASSETS_FRONT . 'css/magnific-popup.css'); ?>">
<link rel="stylesheet" href="<?= base_url(ASSETS_FRONT . 'css/owl.carousel.css'); ?>">
<link rel="stylesheet" href="<?= base_url(ASSETS_FRONT . 'css/loaders.css'); ?>">
<link rel="stylesheet" href="<?= base_url(ASSETS_FRONT . 'css/animate.css'); ?>">
<link rel="stylesheet" href="<?= base_url(ASSETS_FRONT . 'css/pickadate-default.css'); ?>">
<link rel="stylesheet" href="<?= base_url(ASSETS_FRONT . 'css/pickadate-default.date.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?= base_url(ASSETS_FRONT . 'css/example.css'); ?>">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="<?php echo base_url(ASSETS_FRONT . 'css/intlTelInput.css'); ?>">
<!-- SWEET ALERT CSS -->
<link rel="stylesheet" href="<?php echo base_url(ASSETS_FRONT . 'css/sweetalert2.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url(ASSETS_FRONT . 'css/sweetalert2.min.css'); ?>">
<!-- CUSTOM STYLESHEET -->
<link rel="stylesheet" href="<?php echo base_url(ASSETS_FRONT . 'css/style.css'); ?>">
<!--<script src="<?php /*echo base_url(ASSETS_FRONT. 'js/jquery1.11.2.min.js'); */ ?>"></script>-->
<!-- RESPONSIVE FIXES -->
<link rel="stylesheet" href="<?php echo base_url(ASSETS_FRONT . 'css/responsive.css'); ?>">
<style type="text/css">

    @media screen and (max-width: 991px) {

        .display-status {
            display: none !important;
        }
        .widget .w-comments li {
    font-size: 15px;
    padding-top: 10px;
    padding-bottom: 5px;
    width: 50%;
    float: left;

}  
    }

    .safari .form-bg {
        /*margin-top:150px;*/
    }
</style>

<!-- Font Awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

<!-- Extra CSS SECTION -->
<!-- Notification-->
<link href="<?= EXTRA_PLUGIN ?>toastr/toastr.min.css" rel="stylesheet">
<!-- SweetAlert-->
<link href="<?= EXTRA_PLUGIN ?>sweetalert/sweetalert.css" rel="stylesheet">

<!-- Custom CSS -->
<!--<link rel="stylesheet" href="<?php /*echo base_url(ASSETS_ADMIN . 'custom/custom.css'); */ ?>">-->