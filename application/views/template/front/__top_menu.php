<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">Demo Site</a>
        </div>

        <!-- <ul class="nav navbar-nav navbar-right">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="#contact"><i class="fa fa-comment"></i> Login</a></li>
            <li><a href="signup"><i class="fa fa-comment"></i> Signup</a></li>
        </ul> -->

        <ul class="nav navbar-nav navbar-right">
            <li><a href="../home"><i class="fa fa-home"></i> Home</a></li>
            <?php
            $userData = $this->session->userdata('UserInformation');
            $UserInformation = (!empty($userData) ? $this->session->userdata('UserInformation') : array());
            if (count($UserInformation) > 0 && $UserInformation['is_login'] > 0) {
                // redirect('/front/home/index');
                echo '<li><a href="../front/home/change_password"><i class="fa fa-key"></i> Change Password</a></li>
                <li><a href="#"><i class="fa fa-User"></i> <b>Hello ' . $UserInformation['first_name'] . ' ' . $UserInformation['last_name'] . ' </b></a></li>';
            } else {
                echo '<li><a href="login"><i class="fa fa-comment"></i> Login</a></li>
                <li><a href="signup"><i class="fa fa-comment"></i> Signup</a></li>';
            }
            ?>

        </ul>

    </div>
</nav>