<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Shop_model extends CI_Model
{
    var $table = 'shops';
    var $table_country = 'country';
    var $table_city = 'city';
    var $order = array('shop_id' => DESCENDING);
    var $primary_key = 'shop_id';
    var $column_order = array(null, 'shop_name', 'country_name', 'city_name', 'shop_address', 'pickup_from_shop_price', 'home_delivery_price', 'home_delivery_status', null); //set column field database for datatable orderable
    var $column_search = array('shop_name', 'country_name', 'city_name', 'shop_address', 'pickup_from_shop_price', 'home_delivery_price', 'home_delivery_status');
    public function __construct()
    {
        parent::__construct();
    }
    public function get_info_by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where($this->primary_key, $id);
        $response_data = $this->db->get()->row();
        return $response_data;
    }
    public function get_list()
    {
        $this->get_data();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    private function get_data()
    {
        $this->fetch_data();
        $i = 0;
        foreach ($this->column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {
                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    private function fetch_data()
    {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->join($this->table_country, 'shops.country_id =  country.country_id', 'left');
        $this->db->join($this->table_city, 'shops.city_id =  city.city_id', 'left');
       #$this->db->order_by('shop_name', ASCENDING);
    }

    public function count_filtered()
    {
        $this->get_data();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->fetch_data();
        return $this->db->count_all_results();
    }
    public function update_files($file, $FileId)
    {
        $this->db->where('shop_id', $FileId);
        $this->db->set('shop_logo',$file);
        $this->db->update('shops');
        return TRUE;
    }

    public function update_files_banner($file, $FileId)
    {
        $this->db->where('shop_id', $FileId);
        $this->db->set('shop_banner',$file);
        $this->db->update('shops');
        return TRUE;
    }

    public function get($id)
    {
       $this->db->select('*');
       $this->db->from($this->table);
       $this->db->where('shop_id', $id);
       $query = $this->db->get();
        return $query->result();
    }
    public function change_status($id,$status)
    {
        $this->db->where('shop_id', $id);
        $this->db->set('shop_status',$status);
        $this->db->update($this->table);
        return TRUE;
    }
    public function getimagedata($id)
    {
       $this->db->select('*');
       $this->db->from($this->table);
       $this->db->where('shop_id', $id);
       $response_data = $this->db->get()->row();
        return $response_data;
    }

    public function getdata($id)
    {
       $this->db->select('*');
       $this->db->from($this->table);
       $this->db->where('shop_id', $id);
       $response_data = $this->db->get()->row();
        return $response_data;
    }

    public function update_logo($id)
    {
        $this->db->where('shop_id', $id);
        $this->db->set('shop_logo','');
        $this->db->update('shops');
        return TRUE;
    }

    public function update_banner($id)
    {
        $this->db->where('shop_id', $id);
        $this->db->set('shop_banner','');
        $this->db->update('shops');
        return TRUE;
    }
    public function get_shop_list()
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->order_by('shop_name', ASCENDING);
        $response_data = $this->db->get()->result();
        return $response_data;
    }

    public function delete($id)
    {
        $this->db->where('shop_id', $id);
        $this->db->delete($this->table);

        return TRUE;
    }

    public function get_shop_list_by_city($city_id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('city_id', $city_id);
        $this->db->where('shop_status', PRODUCT_STATUS_PUBLISHED);
        $this->db->order_by('shop_name', ASCENDING);
        $response_data = $this->db->get()->result();
        return $response_data;
    }
    public function get_shop_name($id)
    {
        $this->db->select('shop_name');
        $this->db->from($this->table);
        $this->db->where('shop_id', $id);
        $response_data = $this->db->get()->row();
        return $response_data;
    }
}

?>