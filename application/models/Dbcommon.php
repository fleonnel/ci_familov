<?php
class Dbcommon extends CI_Model
{
		
	function __construct()
    {
        // Call the Model constructor
       parent::__construct();
	   $this->load->database();
    }
	
	function insert_batch($tablename,$array)
	{
		$this->db->insert_batch($tablename,$array);
	}
	function insert($table_name,$data,$lastid='')
	{
		if($this->db->insert($table_name, $data))
		{
			//return true;
			if($lastid!='')
			{
				return $this->db->insert_id();
			}else
			{
				return true;
			}
		}
		else
		{
			return false;
		}
	}
	function update($table_name,$array,$data)
	{
		foreach($array as $key=>$value)
		{
			$this->db->where($key,$value);
		}
		if($this->db->update($table_name, $data))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	function num_rows($sql)
	{
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	function delete($table_name,$array)
	{
		foreach($array as $key=>$value)
		{
			$this->db->where($key,$value);
		}
		$this->db->delete($table_name); 
		return true;	
	}

	function getnumofdetails($table_name,$where)
	{
		$sql="select count(*) as cnt from $table_name ".$where;
		$query = $this->db->query($sql);
		$result=$query->row();
		return $result->cnt;
	}
	function getnumofdetails_($query)
	{
		$sql="select ".$query."";
		$query = $this->db->query($sql);
		return $query->num_rows(); 
	}
	
	function getdetailsinfo($table_name,$array,$multiple='')
	{
		$sql="select * from $table_name where ";
		foreach($array as $key=>$value)
		{
			$sql.=" $key=$value and";
		}
		$sql=trim($sql,' and');
		//echo $sql;
		$query = $this->db->query($sql);
		if($multiple=='')
		{
			return $query->row();
		}else
		{
			return $query->result();
		}
	}
	
	function getNextid($value,$primary_key,$table_name)
	{
		$sql="select $primary_key as max from $table_name where $primary_key>$value order by $primary_key desc limit 0,1";
		$query = $this->db->query($sql);
		$result=$query->row();	
		return $result->max;
	}
	function getPrevid($value,$primary_key,$table_name)
	{
		$sql="select $primary_key as min from $table_name where $primary_key<$value order by $primary_key desc limit 0,1";
		$query = $this->db->query($sql);
		$result=$query->row();	
		return $result->min;
	}
	
	function getdetails_($query,$offset='0',$limit='1')
	{
		$sql="select ".$query." LIMIT $offset,$limit";
		
	
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function getInfo_($query,$return='')
	{
		$query=$this->db->query($query);
		if($return=='')
		{
			return $query->row();
		}
		else
		{
			return $query->result();
		}

	}
	
	function imageAdd($image_module_name,$image_configuration_name,$id='',$field_name,$table_name,$table_id,$name)
	{
			$info=$this->dbcommon->getInfo_("select * from image_module where name='$image_module_name'");
			
			$image_module_id=$info->image_module_id;
			
			$path=rtrim($info->path,"/");
			$path.="/";
			
			$query="select * from image_configuration where image_module_id=$image_module_id";
			$image_info=$this->dbcommon->getInfo_($query,'1');
	//		var_dump($image_info);
		
		//	$category_banner="select * from image_configuration where name='$image_configuration_name'";
		
		//	$category_banner_info=$this->dbcommon->getInfo_($category_banner,'1');
		//	var_dump($category_banner_info);
			//category benner//
			foreach($image_info as $cat_info)
			{
				
				$imagetotalimage[$cat_info->folder_name]=$cat_info->width.",".$cat_info->height;	
				
				
			}
			
		//	var_dump($imagetotalimage);
			if($id=='')
			{
				
				
				$image=image_resize($field_name,$path,$imagetotalimage,'','true',$original="full",seo_url($name));
				
			}else
			{

				$array=array($table_id=>$id);
				$info=$this->dbcommon->getdetailsinfo($table_name,$array);

				$image=image_resize($field_name,$path,$imagetotalimage,$info->$field_name,'true',$original="full",seo_url($name));
				

			}

			return $image;
	}
}
?>