<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');


class Feedback_model extends CI_Model
{
    var $table = 'feedback';
    var $primary_key = 'feedback_token';

    var $table_ticket = 'tickets';
    var $primary_key_ticket = 'ticket_id';

    var $column_order = array(null, null); //set column field database for datatable orderable
    var $column_search = array(null); //set column field database for datatable searchable
    var $order = array(); // default order

    public function __construct()
    {
        parent::__construct();
    }

    public function check_customer_token($token, $ticket_id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where($this->primary_key, $token);
        $this->db->where('feedback_ticket_unique_id', $ticket_id);
        $response_data = $this->db->get()->row();
        #_e($this->db->last_query());
        return $response_data;
    }

    public function get_system_report()
    {
        $this->db->select('feedback_rank, count(feedback_rank) AS TOTAL , sum(feedback_rank) AS TOT_SUM ');
        $this->db->from($this->table);
        $this->db->where('feedback_rank !=', null);
        $this->db->group_by('feedback_rank');
        $this->db->order_by('feedback_rank');
        $response_data = $this->db->get()->result();
        #_e($this->db->last_query());
        return $response_data;
    }

    public function get_service_report($service_id = '')
    {
        $this->db->select('F.feedback_rank, count(F.feedback_rank) AS TOTAL, sum(F.feedback_rank) AS TOT_SUM, T.ticket_service_id');
        $this->db->from($this->table . ' AS F');
        $this->db->join($this->table_ticket . ' as T ', ' T.ticket_unique_id = F.feedback_ticket_unique_id', 'left');
        $this->db->where('F.feedback_rank !=', null);
        if ($service_id != '') {
            $this->db->where('T.ticket_service_id', $service_id);
        }

        $this->db->group_by('F.feedback_rank,  T.ticket_service_id ');
        $this->db->order_by('F.feedback_rank');
        $response_data = $this->db->get()->result();
        #_e($this->db->last_query());
        return $response_data;
    }

    public function get_customer_report($customer_id = '')
    {
        $this->db->select('F.feedback_rank, count(F.feedback_rank) AS TOTAL, sum(F.feedback_rank) AS TOT_SUM, T.ticket_customer_id');
        $this->db->from($this->table . ' AS F');
        $this->db->join($this->table_ticket . ' as T ', ' T.ticket_unique_id = F.feedback_ticket_unique_id', 'left');
        $this->db->where('F.feedback_rank !=', null);
        if ($customer_id != '') {
            $this->db->where('F.feedback_customer_id', $customer_id);
        }
        $this->db->group_by('F.feedback_rank,  T.ticket_customer_id ');
        $this->db->order_by('F.feedback_rank');
        $response_data = $this->db->get()->result();
        #_e($this->db->last_query());
        return $response_data;
    }

    public function get_employee_report($employee_id = '')
    {
        $this->db->select('F.feedback_rank, count(F.feedback_rank) AS TOTAL, sum(F.feedback_rank) AS TOT_SUM,');
        $this->db->from($this->table . ' AS F');
        $this->db->join($this->table_ticket . ' as T ', ' T.ticket_unique_id = F.feedback_ticket_unique_id', 'left');
        $this->db->where('F.feedback_rank !=', null);
        $this->db->where('F.feedback_employee_id', $employee_id);
        $this->db->group_by('F.feedback_rank,  F.feedback_employee_id');
        $this->db->order_by('F.feedback_rank');
        $response_data = $this->db->get()->result();
        # _e($this->db->last_query());
        return $response_data;
    }

}

?>