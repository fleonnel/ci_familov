<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Promotion_model extends CI_Model
{
    var $table = 'promocode';
    var $order = array('promo_id' => ASCENDING);
    var $primary_key = 'promo_id';
    var $column_order = array(null, 'vCode','vDescription','iAmount','start_date','end_date',null); //set column field database for datatable orderable
    var $column_search = array('vCode','vDescription','iAmount','start_date','end_date');
    public function __construct()
    {
        parent::__construct();
    }
    public function get_info_by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where($this->primary_key, $id);
        $response_data = $this->db->get()->row();
        return $response_data;
    }
    public function get_list()
    {
        $this->get_data();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    private function get_data()
    {
        $this->fetch_data();
        $i = 0;
        foreach ($this->column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {
                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    private function fetch_data()
    {
        $this->db->select("*");
        $this->db->from($this->table);
       #$this->db->order_by('shop_name', ASCENDING);
    }

    public function count_filtered()
    {
        $this->get_data();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->fetch_data();
        return $this->db->count_all_results();
    }
     public function change_status($id,$status)
    {
        $this->db->where('promo_id', $id);
        $this->db->set('eStatus',$status);
        $this->db->update('promocode');
        return TRUE;
    }

    public function get_info_by_code($code)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('vCode', $code);
        $this->db->where('eStatus', 'Active');
        $response_data = $this->db->get()->row();
        return $response_data;
    }
}

?>