<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Customers_model extends CI_Model
{
    var $table = 'customers';
    var $table_country = 'country';
    var $table_city = 'city';
    var $primary_key = 'customer_id';
    var $column_order = array(null, 'username', 'email_address', 'home_address', 'phone_number', 'country.country_name', 'city.city_name', null); //set column field database for datatable orderable
    var $column_search = array('username', 'email_address', 'home_address', 'phone_number', 'country_id', 'city_id'); //set column field database for datatable searchable

    var $order = array('customer_id' => DESCENDING); // default order

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * For datatable process start
     * This function is used for get list
     * @return object
     */
    public function get_list()
    {
        $this->get_data();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        # _e($this->db->last_query(),0);
        return $query->result();
    }

    private function get_data()
    {
        $this->fetch_data();
        $i = 0;
        foreach ($this->column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {
                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    private function fetch_data()
    {
        $this->db->select("*");
        $this->db->from($this->table);
        //$this->db->join($this->table_country, 'customers.country_id =  country.country_id', 'left');
        //$this->db->join($this->table_city, 'customers.city_id =  city.city_id', 'left');
        #  $this->db->order_by($this->primary_key, ASCENDING);
    }

    public function count_filtered()
    {
        $this->get_data();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->fetch_data();
        return $this->db->count_all_results();
    }
    /** Datatable Process End **/


    /**
     * This function is use for update status
     * @return object
     */
    public function update_column($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    /**
     * This function is use for getting information by id
     * @return object
     */

    public function get_info_by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where($this->primary_key, $id);
        $response_data = $this->db->get()->row();
        return $response_data;
    }

    /**
     * This function is use for getting information by id
     * @return object
     */

    public function get_customer_list($id = '')
    {
        $this->db->select('*');
        $this->db->from($this->table);
        if ($id != '') {
            $this->db->where($this->primary_key, $id);
        }
        $response_data = $this->db->get()->result();
        return $response_data;
    }

    public function get_customer_list_service($ids = '')
    {
        $this->db->select('*');
        $this->db->from($this->table);
        if ($ids != '') {
            $this->db->where_not_in($this->primary_key, $ids);
        }
        $response_data = $this->db->get()->result();
        return $response_data;
    }
    public function get_customer()
    {
        $this->fetch_data();
        $this->db->order_by('username', ASCENDING);
        $query = $this->db->get();
        #_e($this->db->last_query());
        return $query->result();
    }
    public function change_status($id,$status)
    {
        $this->db->where('customer_id', $id);
        $this->db->set('status',$status);
        $this->db->update($this->table);
        return TRUE;
    }
}

?>