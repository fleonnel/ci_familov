<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Permission_model extends CI_Model
{
    var $table = 'access';
    var $table_roles = 'roles';
    var $primary_key = 'access_id';
    var $primary_key_roles = 'roles_id';
    #var $column_order = array(null, 'service_name', 'dep_service_name', 'service_periodicity', null); //set column field database for datatable orderable
    #var $column_search = array('service_name', 'dep_service_name', 'service_periodicity'); //set column field database for datatable searchable
    #var $order = array('service_name' => ASCENDING); // default order

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * For datatable process start
     * This function is used for get list
     * @return object
     */
    public function get_list()
    {
        $this->get_data();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    private function get_data()
    {
        $this->fetch_data();
        $i = 0;
        foreach ($this->column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {
                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    private function fetch_data()
    {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->join($this->table_department . ' as DEPART ', ' dep_service_id =  department_id', 'left');
        $this->db->order_by($this->primary_key, ASCENDING);
    }

    public function count_filtered()
    {
        $this->get_data();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->fetch_data();
        return $this->db->count_all_results();
    }
    /** Datatable Process End **/


    /**
     * This function is use for update status
     * @return object
     */
    public function update_column($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    /**
     * This function is use for getting information by id
     * @return object
     */

    public function get_info_by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where($this->primary_key, $id);
        $response_data = $this->db->get()->row();
        return $response_data;
    }

    /**
     * This function is use for getting roles list except super admin
     * @return object
     */
    public function get_roles_list()
    {
        $this->db->select('*');
        $this->db->from($this->table_roles);
        $this->db->where_not_in($this->primary_key_roles, SUPER_ADMIN_ROLE_ID);
        $response_data = $this->db->get()->result();
        return $response_data;
    }

    /**
     * This function is use for check role exist or not by id and get role detail
     * @return object
     */
    public function get_role_detail_by_id($roles_id)
    {
        $this->db->select('*');
        $this->db->from($this->table_roles);
        $this->db->where($this->primary_key_roles, $roles_id);
        $this->db->where_not_in($this->primary_key_roles, SUPER_ADMIN_ROLE_ID);
        $response_data = $this->db->get()->row();
        return $response_data;
    }

    /**
     * This function is use for check permission
     * @return object
     */
    public function check_permission($roles_id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where($this->primary_key_roles, $roles_id);
        return $this->db->get()->num_rows();

    }

    /**
     * This function is use for get permission list role id wise
     * @return object
     */
    public function get_permission_list($roles_id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where($this->primary_key_roles, $roles_id);
        $response_data = $this->db->get()->row();
        return $response_data;
    }
}

?>