<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class City_model extends CI_Model
{
    var $table = 'city';
    var $table_country = 'country';
    var $order = array('city_id' => ASCENDING);
    var $primary_key = 'city_id';
    var $column_order = array(null, 'city_name', 'C.country_name', null); //set column field database for datatable orderable
    var $column_search = array('city_name', 'C.country_name');
    public function __construct()
    {
        parent::__construct();
    }
    public function get_info_by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where($this->primary_key, $id);
        $response_data = $this->db->get()->row();
        return $response_data;
    }
    public function get_list()
    {
        $this->get_data();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    private function get_data()
    {
        $this->fetch_data();
        $i = 0;
        foreach ($this->column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {
                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    private function fetch_data()
    {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->join($this->table_country . ' as C', $this->table . '.country_id = C.country_id', 'left');
        # $this->db->order_by('city_name', ASCENDING);
    }

    public function count_filtered()
    {
        $this->get_data();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->fetch_data();
        return $this->db->count_all_results();
    }
    public function get_city_list()
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->order_by('city_name', ASCENDING);
        $response_data = $this->db->get()->result();
        return $response_data;
    }
    public function get_city_list_by_country($country_id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('country_id', $country_id);
        $this->db->order_by('city_name', ASCENDING);
        $response_data = $this->db->get()->result();
        return $response_data;
    }

    public function delete($id)
    {
        $this->db->where('city_id', $id);
        $this->db->delete($this->table);

        return TRUE;
    }
    public function get_city_name($id)
    {
        $this->db->select('city_name');
        $this->db->from($this->table);
        $this->db->where('city_id', $id);
        $response_data = $this->db->get()->row();
        return $response_data;
    }
}

?>