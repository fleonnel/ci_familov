<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Order_model extends CI_Model
{
    var $table = 'order';
    var $table_order_items = 'order_items';
    var $table_shop = 'shops';
    var $table_products = 'products';
    var $table_customer = 'customers';
    var $primary_key = 'order_id';
    var $column_order = array(null, 'generate_code', 'shops.shop_name', 'receiver_name', 'date_payment', null, null); //set column field database for datatable orderable
    var $column_search = array('generate_code','shops.shop_name','customers.username','customers.lastname','receiver_name','receiver_phone','grand_total','payment_method','date_payment','delivery_option');
    var $order = array('order.order_id' => DESCENDING);
    public function __construct()
    {
        parent::__construct();
    }
    public function get_info_by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where($this->primary_key, $id);
        $response_data = $this->db->get()->row();
        return $response_data;
    }
    public function get_list($shop_id = '', $roles_id='')
    {
        $this->get_data($shop_id, $roles_id);
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
#         _e($this->db->last_query());
        return $query->result();
    }

    private function get_data($shop_id = '', $roles_id='')
    {
        $this->fetch_data($shop_id, $roles_id);
         if ($this->input->post('order_status')) {
            $this->db->where('order.order_status', $this->db->escape_str($this->input->post('order_status')));
        }
        $i = 0;
        foreach ($this->column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {
                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    private function fetch_data($shop_id = '', $roles_id='')
    {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->join($this->table_customer, 'order.customer_id =  customers.customer_id', 'left');
        $this->db->join($this->table_shop, 'order.shop_id =  shops.shop_id');
        
        if ($shop_id >= 0 && $shop_id != '' && $shop_id != NULL) {
            $this->db->where_in('order.shop_id', explode(",", $shop_id));
        }

        if($roles_id!='')
        {
            if ($roles_id == SUPER_ADMIN_ROLE_ID || $roles_id == ADMIN_ROLE_ID) {
            } else {
                $this->db->where_in('order.shop_id', explode(",", $shop_id));
                $this->db->where_in('order.order_status', array('Buy', 'In Process'));
            }
        }

    }
    public function update_bill($id)
    {
        $this->db->where('order_id', $id);
        $this->db->set('upload_billing','');
        $this->db->update('order');
        return TRUE;
    }
    public function count_filtered($shop_id = '', $roles_id='')
    {
        $this->get_data($shop_id, $roles_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all($shop_id = '', $roles_id='')
    {
        
        $this->fetch_data($shop_id, $roles_id);
        #$this->db->count_all_results();
        #_e($this->db->last_query());
        return $this->db->count_all_results();
    }
    public function delete($id) {
        $this->db->where('shop_id', $id);
        $this->db->delete($this->table);
        return TRUE;
    }
    public function get_order_details($id)
    {
        $this->db->select('*');
        $this->db->from($this->table_order_items);
        $this->db->join($this->table_products, 'order_items.product_id =  products.product_id', 'left');
        $this->db->where('order_id', $id);
        $response_data = $this->db->get()->result();
        return $response_data;
    }
    public function count_order($status,$shop_id = '')
    {
        $this->db->select('*');
        $this->db->from($this->table);
        if($shop_id != ''){
            $this->db->where_in('shop_id', explode(",", $shop_id));
        }
        $this->db->where('order_status', $status);
        return $this->db->count_all_results();
    }
    public function order_total($payment,$shop_id = '')
    {

        $this->db->select_sum('total_prize');
        $this->db->from($this->table);
        $this->db->join($this->table_order_items, 'order_items.order_id =  order.order_id', 'left');
        if($shop_id != ''){
            $this->db->where_in('order.shop_id', explode(",", $shop_id));
        }
        $this->db->where('payment_curr', $payment);
        $response_data = $this->db->get()->result();
        return $response_data;
    }
    public function order_total_billing($payment)
    {

        $this->db->select_sum('order_billing_price');
        $this->db->from($this->table);
        $this->db->where('payment_curr', $payment);
        $response_data = $this->db->get()->result();
        return $response_data;
    }
    public function tax_billing($payment)
    {

        $this->db->select_sum('service_tax');
        $this->db->from($this->table);
        $this->db->where('payment_curr', $payment);
        $response_data = $this->db->get()->result();
        return $response_data;
    }
    public function order_total_currency($payment,$shop_id = '')
    {

        $this->db->select('order_id');
        $this->db->from($this->table);
        //$this->db->join($this->table_order_items, 'order_items.order_id =  order.order_id', 'left');
        if($shop_id != ''){
            $this->db->where_in('order.shop_id', explode(",", $shop_id));
        }
        $this->db->where('payment_curr', $payment);
       //$response_data = $this->db->get()->result();
         return $this->db->count_all_results();
    }

    public function order_monthly_total($shop_id = '')
    {
            
        $this->db->select("SUM(CASE MONTH(date_time) WHEN 1 THEN 1 ELSE 0 END) AS 'January', SUM(CASE MONTH(date_time) WHEN 2 THEN 1 ELSE 0 END) AS 'February', SUM(CASE MONTH(date_time) WHEN 3 THEN 1 ELSE 0 END) AS 'March', SUM(CASE MONTH(date_time) WHEN 4 THEN 1 ELSE 0 END) AS 'April', SUM(CASE MONTH(date_time) WHEN 5 THEN 1 ELSE 0 END) AS 'May', SUM(CASE MONTH(date_time) WHEN 6 THEN 1 ELSE 0 END) AS 'June', SUM(CASE MONTH(date_time) WHEN 7 THEN 1 ELSE 0 END) AS 'July', SUM(CASE MONTH(date_time) WHEN 8 THEN 1 ELSE 0 END) AS 'August', SUM(CASE MONTH(date_time) WHEN 9 THEN 1 ELSE 0 END) AS 'September', SUM(CASE MONTH(date_time) WHEN 10 THEN 1 ELSE 0 END) AS 'October', SUM(CASE MONTH(date_time) WHEN 11 THEN 1 ELSE 0 END) AS 'November', SUM(CASE MONTH(date_time) WHEN 12 THEN 1 ELSE 0 END) AS 'December'");
        $this->db->from($this->table);
        $this->db->where("date_time BETWEEN CONCAT(YEAR(CURDATE()),'-01-01') AND CONCAT(YEAR(CURDATE())+1,'-12-31')");
        if($shop_id != ''){
            $this->db->where_in('shop_id', explode(",", $shop_id));
        }
        $response_data = $this->db->get()->result();
        return $response_data;
    }
    public function change_status($id,$status)
    {
        $this->db->where('order_id', $id);
        $this->db->set('order_status',$status);
        $this->db->update($this->table);
        return TRUE;
    }

    public function get_order_list($customer_id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('customer_id', $customer_id);
        $this->db->order_by($this->primary_key, DESCENDING);
        $response_data = $this->db->get()->result();
        return $response_data;
    }

    public function get_order_detail_by_id($customer_id, $order_id)
    {
        $this->db->select('OT.*, P.product_name,P.product_image');
        $this->db->from($this->table_order_items . " AS OT");
        $this->db->join($this->table_products . " AS P", 'OT.product_id =  P.product_id', 'left');
        $this->db->join($this->table . " AS O", 'OT.order_id =  O.order_id', 'left');
        $this->db->where("OT." . $this->primary_key, $order_id);
        if ($customer_id != '') {
            $this->db->where('O.customer_id', $customer_id);
        }
        $response_data = $this->db->get()->result();
        # _e($this->db->last_query());
        return $response_data;
    }

    public function get_order_by_id($customer_id, $order_id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join($this->table_shop, 'order.shop_id =  shops.shop_id', 'left');
        $this->db->where($this->primary_key, $order_id);
        if ($customer_id != '') {
            $this->db->where('customer_id', $customer_id);
        }
        $response_data = $this->db->get()->row();
        #_e($this->db->last_query());
        return $response_data;
    }
    public function order_data($shop_id = '', $roles_id = '')
    {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->join($this->table_customer, 'order.customer_id =  customers.customer_id', 'left');
        $this->db->join($this->table_shop, 'order.shop_id =  shops.shop_id', 'left');
        if($shop_id != ''){
            $this->db->where_in('order.shop_id', explode(",", $shop_id));
        }

        if($roles_id!='')
        {
            if ($roles_id == SUPER_ADMIN_ROLE_ID || $roles_id == ADMIN_ROLE_ID) {
            } else {
                $this->db->where_in('order.order_status', array('Buy', 'In Process'));
            }
        }

        $this->db->order_by('order.order_id', DESCENDING);        
        $this->db->limit(10);
        $response_data = $this->db->get()->result();
        return $response_data;
        
    }


    public function get_order_by_shop($shop_id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('shop_id', $shop_id);
        $response_data = $this->db->get()->result();
        return $response_data;
    }
    public function get_order_by_product($product_id)
    {
        $this->db->select('*');
        $this->db->from($this->table_order_items);
        $this->db->where('product_id', $product_id);
        $response_data = $this->db->get()->result();
        return $response_data;
    }
}

?>