<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * encryption Library
 *
 * @author    Vishal Bhalani
 */
class Encryptionlib {

	private $RA;

	public function __construct()
    {
    	$this->RA =& get_instance();
        $this->RA->load->library('encryption');
       /* $this->RA->encryption->initialize(
        array(
                'cipher' => 'aes-128',
                'mode' => 'ctr ',
                //'key' => 'sdfg#43FG45RFGR$#$12'
        	)
		);*/
    }
    public function encode($string) {
    	$ciphertext = $this->RA->encryption->encrypt($string);
    	$ciphertext = strtr(
                    $ciphertext,
                    array(
                        '+' => '.',
                        '/' => '~'
                    )
                );
    	return $ciphertext;
    }

    public function decode($ciphertext) {
    	$ciphertext = strtr(
                $ciphertext,
                array(
                    '.' => '+',
                    '~' => '/'
                )
        );
    	$string = $this->RA->encryption->decrypt($ciphertext);
    	return $string;
    }
    /*public function key() {
    	$key = bin2hex($this->RA->encryption->create_key(16));

    	return $key;
    }*/
}
?>