<?php
/**
 * Library for mail.
 * @author    Vishal Bhalani
 */
class Mail {

    public $CI;
	private $smtpHost = '';
    private $smtpPort = 0;
    private $smtpSecurity = 'tls';
    private $smtpUsername = '';
    private $smtpPassword = '';
    private $isSmtp = TRUE;
    private $from = array();
    private $to = array();
    private $cc = array();
    private $bcc = array();
    private $replyTo = array();
    private $subject = null;
    private $body = null;
    private $attachments = [];
    private $message = null;
    private $connection = null;
    private $mailer = null;

    /**
     * Constructor.
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->config->load('mail');
    }

    /**
     * Change SMTP behaviour
     * @param bool $smtp
     */
    public function setIsSmtp($smtp)
    {
        $this->isSmtp = $smtp;
    }

    /**
     * Add Mail Attachment
     * @param string $fileName
     */
    public function setAttachment($fileName)
    {
        $this->attachments[] = $fileName;
    }

    /**
     * Sending Mail
     * @return bool
     */
    public function send()
    {
        $this->_buildMessage();
        $this->connect();
        $this->mailer = \Swift_Mailer::newInstance($this->connection);
        if (count($this->getTo()) > 0) {
            $this->mailer->send($this->message);
        }
        return true;
    }

    /**
     * Building Mail with from, to, subject, body and attachment
     */
    private function _buildMessage()
    {
        $this->message = \Swift_Message::newInstance();
        if (count($this->getFrom()) > 0) {
            $this->message->setFrom($this->getFrom());
        }
        $this->message->setTo($this->getTo());

        if (count($this->getCc()) > 0) {
            $this->message->setCc($this->getCc());
        }
        if (count($this->getBcc()) > 0) {
            $this->message->setBcc($this->getBcc());
        }
        if (count($this->getReplyTo()) > 0) {
            $this->message->setReplyTo($this->getReplyTo());
        }
        $this->message->setSubject($this->getSubject());
        $this->message->setBody($this->getBody());
        $this->message->setContentType('text/html');
        if (count($this->attachments) > 0) {
            foreach ($this->attachments as $attachFile) {
                $this->message->attach(\Swift_Attachment::fromPath($attachFile)->setDisposition('inline'));
            }
        }
    }

    /**
     * Get From Address
     * @return array
     */
    public function getFrom()
    {
        if (count($this->from) <= 0) {
            $this->from[SMTP_MAIL_ID] = MAIL_SITE_NAME; #Todo
        }
        return $this->from;
    }

    /**
     * Setting From Address
     * @param string $address
     * @param string $name optional
     */
    public function setFrom($address, $name = NULL) {
        if ($name == NULL) {
            $this->from[] = $address;
        } else {
            $this->from[$address] = $name;
        }
    }

    /**
     * Get To Address
     * @return array
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * Set To Address
     * @param string $address
     * @param string $name optional
     */
    public function setTo($address, $name = NULL)
    {
        if ($name == NULL) {
            $this->to[] = $address;
        } else {
            $this->to[$address] = $name;
        }
    }

    /**
     * Get CC address
     * @return array
     */
    public function getCc() {
        return $this->cc;
    }

    /**
     * Set CC address
     * @param string $address
     * @param string $name optional
     */
    public function setCc($address, $name = NULL)
    {
        if ($name == NULL) {
            $this->cc[] = $address;
        } else {
            $this->cc[$address] = $name;
        }
    }

    /**
     * Get BCC address
     * @return array
     */
    public function getBcc() {
        return $this->bcc;
    }

    /**
     * Set BCC address
     * @param string $address
     * @param string $name optional
     */
    public function setBcc($address, $name = NULL)
    {
        if ($name == NULL) {
            $this->bcc[] = $address;
        } else {
            $this->bcc[$address] = $name;
        }
    }

    /**
     * Getting Reply-To address
     * @return array
     */
    public function getReplyTo() {
        return $this->replyTo;
    }

    /**
     * Set Reply-To address
     * @param string $address
     * @param string $name optional
     */
    public function setReplyTo($address, $name = NULL)
    {
        if ($name == NULL) {
            $this->replyTo[] = $address;
        } else {
            $this->replyTo[$address] = $name;
        }
    }

    /**
     * Get Mail Subject
     * @return string
     */
    public function getSubject() {
        return $this->subject;
    }

    /**
     * Setting Mail Subject
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * Get Mail Body
     * @return string
     */
    public function getBody() {
        return $this->body;
    }

    /**
     * Set Mail Body
     * @param mix $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * SMTP Connection
     */
    private function connect()
    {
        if (!$this->connection) {
            if ($this->isSmtp()) {
                $this->smtpHost = $this->CI->config->item('smtpHost');
                $this->smtpPort = $this->CI->config->item('smtpPort');
                $this->smtpSecurity = $this->CI->config->item('smtpSecurity');
                $this->smtpUsername = $this->CI->config->item('smtpUsername');
                $this->smtpPassword = $this->CI->config->item('smtpPassword');
                $this->connection = \Swift_SmtpTransport::newInstance($this->smtpHost, $this->smtpPort, $this->smtpSecurity)
                    ->setUsername($this->smtpUsername)
                    ->setPassword($this->smtpPassword);
            } else {
                $this->connection = \Swift_MailTransport::newInstance();
            }
        }
    }

    /**
     * Use SMTP or not
     * @return bool
     */
    public function isSmtp()
    {
        return $this->isSmtp;
    }
}
?>