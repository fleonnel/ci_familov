<section id="hero12" class="hero hero-countdown bg-img">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 text-center">
                <h1 class="text-white"><?= $this->lang->line('MSG_IF_01') ?></h1>
            </div>
            <div class="col-md-6 col-md-offset-3 text-white text-center">
            </div>
        </div>
    </div>
</section>
<!-- =========================
   Invite Friend SECTION
============================== -->
<section id="features2-1" class="p-y-md">
    <div class="container">
        <div class="row">
            <?php _notify(); ?>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div
                    class="alert alert-default  alert-dismissable section_grayq"><?= $this->lang->line('MSG_IF_02') ?></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="pull-right1">
                    <div class="form-group">
                        <div class="col-md-9">
                            <input type="text" id="referraltext" name="referraltext"
                                   class="col-md-8 search-input form-control"
                                   autocomplete="on" style="" value="<?= @$referral_url ?>" placeholder="Some path"
                                   readonly/>
                        </div>
                        <div class="col-md-3">
                            <button class="btn btn-default copy-button copybutton col-md-2 " type="button"
                                    id="copy-button" data-toggle="tooltip" onclick="copyToClipboard('#referraltext')"
                                ><?= $this->lang->line('MSG_IF_03') ?></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-12">
                <div class="section-header text-center wow fadeIn">
                    <p><?= $this->lang->line('MSG_IF_04') ?></p>
                    <a href="https://facebook.com/sharer.php?u=<?= @$referral_url ?>" class="btn button facebook"
                       target="_blank"><i class="fa fa-facebook"></i><?= $this->lang->line('MSG_IF_05') ?></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="row text-center features-block c3">
                <div class="col-md-4">
                    <div class="gt_main_services bg_3">
                        <i class="icon-wallet"></i>
                        <h5><?= $this->lang->line('MSG_IF_06') ?></h5>

                        <h2><span
                                class="number"><?php echo convert_price(@$data["totalReferral"]) . '' . @$_SESSION["cur"]["symbol"]; ?></span>
                        </h2>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="gt_main_services bg_3">
                        <i class="icon-check"></i>
                        <h5><?= $this->lang->line('MSG_IF_07') ?></h5>

                        <h2><span class="number" style="background-color:#ec1e73"><?php
                                $total_used = @$data["totalReferral"] - @$data["myReferral"];
                                echo convert_price(@$total_used) . '' . @$_SESSION["cur"]["symbol"]; ?></span></h2>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="gt_main_services bg_3">
                        <i class="icon-wallet2"></i>
                        <h5><?= $this->lang->line('MSG_IF_08') ?></h5>

                        <h2><span
                                class="number"><?php echo convert_price(@$data["myReferral"]) . '' . @$_SESSION["cur"]["symbol"]; ?></span>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /End  Section -->
<script>
    function copyToClipboard(element) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(element).val()).select();
        document.execCommand("copy");
        $temp.remove();
        toastr.info('<?= $this->lang->line('MSG_COPY') ?>', '', {positionClass: 'toast-top-full-width'});
    }
</script>