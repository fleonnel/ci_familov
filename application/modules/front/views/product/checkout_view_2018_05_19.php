<script src="<?php echo base_url(ASSETS_URL_FRONT . 'dist/js/bootstrap-formhelpers-countries.js'); ?>"></script>

<link rel="stylesheet" href="<?php echo base_url(ASSETS_URL_FRONT . 'dist/css/bootstrap-formhelpers.css'); ?>">
<section id="hero12" class="hero hero-countdown bg-img">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 text-center">
                <h1 class="text-white"><?= $this->lang->line('MSG_PRODUCT_CHECKOUT_01') ?></h1>
            </div>
            <div class="col-md-6 col-md-offset-3 text-white text-center">
            </div>
        </div>
    </div>
</section>
<?php if (count(@$cart) == 0) { ?>
    <section id="faq3-1" class="p-y-lg faqs schedule">
        <div class="container">
            <div class="row ">
                <div class="col-md-12 text-center">
                    <div class="alert alert-info alert-dismissible">
                        <img
                            src="<?php echo(ASSETS_URL_FRONT . 'img/images/groceries.png'); ?>">   <?= $this->lang->line('MSG_PRODUCT_CART_02') ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } else { ?>
    <!-- =========================
              Product Cart SECTION
    ============================== -->
    <section id="login" class="p-y-lg1 bg-color" style="">
        <div class="container">
            <!--<div class="row">
                <div class="alert-group">
                    <div class="alert alert-default alert-dismissable">
                        <? /*= $this->lang->line('MSG_PRODUCT_CART_03') */ ?>
                    </div>
                </div>
            </div>-->
            <div class="row">
                <?= _notify(); ?>
            </div>
            <div class="row">
                <div style="background:#fff ;">
                    <h5 class="m-t-lg m-b-0 text-left center-md checkout-title">
                        <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/groceries.png'); ?>"
                             width="20px"> <?= $this->lang->line('MSG_PRODUCT_CHECKOUT_14') ?> </h5>

                    <div class="alert-group">
                        <div class="alert alert-default alert-dismissable section_gray">
                            <?= $this->lang->line('MSG_PRODUCT_CHECKOUT_15') ?>
                        </div>
                    </div>
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <!--<th><strong><? /*= $this->lang->line('MSG_PRODUCT_CART_05') */ ?></strong></th>-->
                            <th><strong><?= $this->lang->line('MSG_PRODUCT_CART_06') ?></strong></th>
                            <th class="text-right"><strong><?= $this->lang->line('MSG_PRODUCT_CART_07') ?></strong></th>
                            <th><strong></strong></th>
                            <th class="text-center"><strong><?= $this->lang->line('MSG_PRODUCT_CART_08') ?></strong>
                            </th>
                            <th class="text-right"><strong><?= $this->lang->line('MSG_PRODUCT_CART_09') ?></strong></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $ctr = 1;
                        $cart_total_item_qty = 0;
                        $cart_amount_total = 0.00;
                        $service_fees = convert_price(SERVICE_FEES);
                        foreach (@$cart as $k => $v) {
                            $cart_total_item_qty += $v['quantity'];
                            $product_price = _number_format(convert_price($v['price_temp']) * $v['quantity']);
                            $cart_amount_total = $cart_amount_total + $product_price;
                            ?>
                            <tr>
                                <td class="text-center"><?= $ctr++ ?></td>
                                <!--<td>
                                <div
                                    style="background-image:url('<?php /*echo(DIR_PRODUCT_URL . $v['product_image']); */ ?>'); background-size: cover; height:100px; width:100px;"></div>
                            </td>-->
                                <td><?= stripslashes($v['product_name']) ?></td>
                                <td class="text-right"><?= $this->currency_symbol . convert_price($v['price_temp']) ?></td>
                                <td class="text-center">X</td>
                                <td class="text-center"><?= $v['quantity'] ?></td>
                                <td class="text-right"><?= $this->currency_symbol . _number_format(convert_price($v['price_temp']) * $v['quantity']) ?></td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td colspan="4"><strong><?= $this->lang->line('MSG_PRODUCT_CART_10') ?></strong></td>
                            <td class="text-center"><b><?= @$cart_total_item_qty ?></b></td>
                            <td class="text-right"
                                style="color:#36cd75 ;font-weight:bold;"><?= $this->currency_symbol . ($cart_amount_total) ?></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <strong><?= $this->lang->line('MSG_PRODUCT_CART_11') /*. " " . str_replace("#PERCENTAGE#", 15, $this->lang->line('MSG_PRODUCT_CART_15'))*/ ?></strong>
                            </td>
                            <td class="text-center"></td>
                            <td class="text-right"><?= $this->currency_symbol . $service_fees ?></td>
                            <td></td>
                        </tr>
                        <?php if (@$delivery_info->delivery_mode == 'free_delivery') { ?>
                            <tr>
                                <td colspan="4">
                                    <strong><?= $this->lang->line('MSG_PRODUCT_CHECKOUT_05') ?></strong>
                                </td>
                                <td class="text-center"></td>
                                <td class="text-right"><?= $this->currency_symbol . convert_price(@$delivery_info->delivery_price) ?></td>
                                <td></td>
                            </tr>
                        <?php } else { ?>
                            <tr>
                                <td colspan="4">
                                    <strong><?= $this->lang->line('MSG_PRODUCT_CHECKOUT_06') ?></strong>
                                </td>
                                <td class="text-center"></td>
                                <td class="text-right"><?= $this->currency_symbol . convert_price(@$delivery_info->delivery_price) ?></td>
                                <td></td>
                            </tr>
                        <?php } ?>
                        <?php if (@$promo_code->promo_id != '' && @$promo_code->promo_code_amount > 0) { ?>
                            <tr>
                                <td colspan="4">
                                    <strong><?= $this->lang->line('MSG_PRODUCT_CHECKOUT_10') ?>
                                        (<?= @$promo_code->promo_code ?>)</strong>
                                </td>
                                <td class="text-center"></td>
                                <td class="text-right"><?= $this->currency_symbol . convert_price(@$promo_code->promo_code_amount) ?></td>
                                <td></td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td colspan="5"><strong><?= $this->lang->line('MSG_PRODUCT_CART_16') ?></strong></td>
                            <td class="text-right" style="color:#36cd75 ;font-weight:bold;">
                                <strong id="grandprice" style="color:#000;font-size:20px">
                                    <?php $grand_total = ($cart_amount_total + $service_fees + convert_price(@$delivery_info->delivery_price)) - convert_price(@$promo_code->promo_code_amount); #todo add all charges and substract promo code amount
                                    echo $this->currency_symbol . ($grand_total); #todo currency icon set in front controller.
                                    ?>
                                </strong>
                            </td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-121">
                    <?php if (@$shop_info->home_delivery_status == 'Activated') { ?>
                        <form method="post" action="<?= base_url(MENU_ORDER_DELIVERY) ?>">
                            <div class="col-md-6">
                                <div class="left_beneficiary">
                                    <div class="alert-group">
                                        <div class="alert alert-success alert-dismissable section_gray">
                                            <div class="cities">
                                                <label
                                                    for="sfEmail"><?= $this->lang->line('MSG_PRODUCT_CHECKOUT_04') ?></label>

                                                <div class="funkyradio">
                                                    <div class="form-group">
                                                        <div class="funkyradio-success">
                                                            <input type="radio" name="delivery_option" id="radio2"
                                                                   data-value="<?= convert_price(@$shop_info->pickup_from_shop_price) ?>"
                                                                   value="free_delivery"
                                                                   <?php if ((!isset($delivery_info) || @$delivery_info->delivery_mode == 'free_delivery')){ ?>checked="checked" <?php } ?>
                                                                   class="grocery delivery_option"
                                                                   onclick="this.form.submit()">
                                                            <label
                                                                for="radio2"> <?= $this->lang->line('MSG_PRODUCT_CHECKOUT_05') ?><?= $this->currency_symbol . convert_price(@$shop_info->pickup_from_shop_price) ?></label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="funkyradio-success">
                                                            <input type="radio" name="delivery_option" id="radio3"
                                                                   data-value="<?= convert_price(@$shop_info->home_delivery_price) ?>"
                                                                   value="home_delivery"
                                                                   <?php if (isset($delivery_info) && @$delivery_info->delivery_mode == 'home_delivery'){ ?>checked="checked" <?php } ?>
                                                                   class="fast-delivery delivery_option "
                                                                   onclick="this.form.submit()">
                                                            <label for="radio3"
                                                                   class="fast-delivery1"> <?= $this->lang->line('MSG_PRODUCT_CHECKOUT_06') ?><?= $this->currency_symbol . convert_price(@$shop_info->home_delivery_price) ?></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    <?php } ?>
                    <div class="col-md-6">
                        <form method="post" action="<?= base_url(MENU_PROMO_CODE) ?>">
                            <div class="left_beneficiary">
                                <div class="alert-group">
                                    <div class="alert alert-success alert-dismissable section_gray">
                                        <div class="cities">
                                            <label
                                                for="sfEmail"><?= $this->lang->line('MSG_PRODUCT_CHECKOUT_07') ?></label>

                                            <p><?= $this->lang->line('MSG_PRODUCT_CHECKOUT_08') ?></p>

                                            <div class="form-group">
                                                <input type="text" class="form-control newtxt" name="promo_code"
                                                       id="promo_code" value="<?php echo @$promo_code->promo_code; ?>">
                                            </div>
                                            <div class="form-group">
                                                <button style="margin-top: 0px; margin-bottom: -26px;"
                                                        class="btn btn-green" id="apply_promo">Apply
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <form action="<?= base_url(MENU_CHECKOUT_PROCEED) ?>" method="POST" name="checkout_form" id="checkout_form">
                <div class="row">
                    <div style="background:#fff ;">
                        <h5 class="m-t-lg m-b-0 text-left center-md checkout-title">
                            <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/groceries.png'); ?>"
                                 width="20px"> <?= $this->lang->line('MSG_PRODUCT_CHECKOUT_16') ?> </h5>

                        <div class="alert-group">
                            <div class="alert alert-default alert-dismissable section_gray">
                                <?= $this->lang->line('MSG_PRODUCT_CHECKOUT_17') ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-121">
                        <div class="col-md-6">
                            <div class="left_beneficiary">
                                <div class="alert-group">
                                    <div class="alert alert-success alert-dismissable section_gray"
                                         style="min-height: 188px  !important;">
                                        <div class="cities">
                                            <label
                                                for="sfEmail"><?= $this->lang->line('MSG_PRODUCT_CHECKOUT_18') ?></label>

                                            <div class="funkyradio">
                                                <div class="form-group">
                                                    <input type="text" class="form-control newtxt bene_cls"
                                                           id="recp_name"
                                                           name="recp_name"
                                                           value="<?php echo edit_display('recp_name', @$info); ?>"
                                                           placeholder="<?= $this->lang->line('MSG_PRODUCT_CHECKOUT_19') ?>"
                                                           required>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-group newtxt ">
                                                        <input class="form-control col-sm-6 bene_cls" type="tel"
                                                               id="phone_code" name="phone_code" placeholder="" value=""
                                                               readonly>
                                                        <input class="form-control col-sm-6 bene_cls phone_number"
                                                               type="text" id="phone_number1" name="phone_number"
                                                               value=""
                                                               onkeypress='return event.charCode >= 48 && event.charCode <= 57'

                                                               required>
                                                        <!-- <input  type="tel" id="phone_code" name="phone_code" required style="width: 339px !important;"> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="left_beneficiary">
                                <div class="alert-group">
                                    <div class="alert alert-success alert-dismissable section_gray">
                                        <div class="cities">
                                            <label
                                                for="sfEmail"><?= $this->lang->line('MSG_PRODUCT_CHECKOUT_21') ?></label>

                                            <p><?= $this->lang->line('MSG_PRODUCT_CHECKOUT_22') ?></p>

                                            <div class="form-group">
                                               <textarea rows="2" cols="" class="form-control newtxt"
                                                         id="greeting_msg" name="greeting_msg"
                                                         placeholder="<?= $this->lang->line('MSG_PRODUCT_CHECKOUT_23') ?>"
                                                         maxlength="150"><?php echo ''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div style="background:#fff ;">
                        <h5 class="m-t-lg m-b-0 text-left center-md checkout-title">
                            <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/groceries.png'); ?>"
                                 width="20px"> <?= $this->lang->line('MSG_PRODUCT_CHECKOUT_24') ?> </h5>

                        <div class="alert-group">
                            <div class="alert alert-default alert-dismissable section_gray">
                                <?= $this->lang->line('MSG_PRODUCT_CHECKOUT_25') ?>
                            </div>
                        </div>
                    </div>

                    <?php #if($this->customer_id == 0){
                    if (!@$this->is_login) {
                        ?>
                        <div class="col-md-12">
                            <div class="col-md-4  col-md-offset-4">
                                <h4 class="m-t-lg m-b-0 text-left center-md">
                                    <b><?= $this->lang->line('MSG_PRODUCT_CHECKOUT_42') ?> </b>
                                </h4>
                                <?= $this->lang->line('MSG_PRODUCT_CHECKOUT_43') ?><br>
                                <a href="<?php echo base_url(MENU_SIGN_UP); ?>"
                                   class="btn btn-green"><?= $this->lang->line('LBL_MENU_02') ?></a>&nbsp;&nbsp;<a
                                    href="<?php echo base_url(MENU_LOG_IN); ?>" class="btn btn-green"
                                    style="border-radius:0px;"><?= $this->lang->line('LBL_MENU_03') ?></a><br>
                                <br><br> <img
                                    src="<?php echo(ASSETS_URL_FRONT . 'img/images/familov-deliverman.png'); ?>"
                                    width="250px" alt="">
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="left_beneficiary">
                                    <div class="alert-group">
                                        <div class="alert alert-success alert-dismissable section_gray"
                                             style="min-height: 188px  !important;">
                                            <div class="cities">
                                                <label
                                                    for="sfEmail"><?= $this->lang->line('MSG_PRODUCT_CHECKOUT_26') ?></label>

                                                <div class="funkyradio">
                                                    <div class="form-group">
                                                        <div class="funkyradio-success">
                                                            <input type="radio" name="payment_type"
                                                                   id="payment_type" checked="checked"
                                                                   value="<?= PAYMENT_STRIPE ?>"/>
                                                            <label for="payment_type"><img
                                                                    src="<?php echo(ASSETS_URL_FRONT . 'img/images/payement/creditcard.png'); ?>"
                                                                    class="payment-method-logo lft-40"><?= $this->lang->line('MSG_PRODUCT_CHECKOUT_27') ?>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <!--<form action="checkout.php" method="POST" >-->
                                                    <script
                                                        src="https://checkout.stripe.com/checkout.js"
                                                        class="stripe-button"
                                                        data-key="<?= STRIPE_PUBLIC_KEY_TEST ?>"
                                                        data-image="<?php echo(ASSETS_URL_FRONT . 'img/images/picto.png'); ?>"
                                                        data-name="<?= STRIPE_SITE_NAME ?>"
                                                        data-description="Buy easy and secure (<?= $this->currency_symbol . convert_price($grand_total) ?>)"
                                                        data-amount="<?php echo convert_price($grand_total) * 100 ?>"
                                                        data-currency="<?php echo @$this->currency_code; ?>"
                                                        data-locale="auto"
                                                        data-label="Checkout"
                                                        >
                                                    </script>
                                                    <!--</form>-->
                                                    <div class="form-group">
                                                        <div class="funkyradio-success">
                                                            <input type="radio" name="payment_type"
                                                                   id="payment_type_2" value="<?= PAYMENT_PAYPAL ?>"/>
                                                            <label for="payment_type_2"><img
                                                                    src="<?php echo(ASSETS_URL_FRONT . 'img/images/payement/paypaladyen.png'); ?>"
                                                                    class="payment-method-logo lft-40"><?= $this->lang->line('MSG_PRODUCT_CHECKOUT_28') ?>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="funkyradio-success">
                                                            <input type="radio" name="payment_type"
                                                                   id="payment_type_9"
                                                                   value="<?= PAYMENT_BANK_TRANSFER ?>"/>
                                                            <label for="payment_type_9">
                                                                <img
                                                                    src="<?php echo(ASSETS_URL_FRONT . 'img/images/bank-48.png'); ?>"
                                                                    class="payment-method-logo lft-40"><?= $this->lang->line('MSG_PRODUCT_CHECKOUT_29') ?>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="left_beneficiary">
                                    <div class="alert-group">
                                        <div class="alert alert-success alert-dismissable section_gray">
                                            <img
                                                src="<?php echo(ASSETS_URL_FRONT . 'img/images/powered-by-stripe-648x170.jpg'); ?>"
                                                width="100%">
                                        </div>
                                    </div>
                                </div>
                                <div class="left_beneficiary">
                                    <div class="alert-group">
                                        <div class="alert alert-success alert-dismissable section_gray">
                                            <div class="cities">
                                                <label
                                                    for="sfEmail"> <a href="<?= base_url(MENU_TERMS) ?>"
                                                                      style="font-weight: normal;font-size: 12px; color: #777777;"><?= $this->lang->line('MSG_PRODUCT_CHECKOUT_30') ?></a></label>

                                                <div class="funkyradio">
                                                    <div class="form-group">
                                                        <div class="funkyradio-success">
                                                            <input type="checkbox" name="agree" id="agree"
                                                                   class=""/>
                                                            <label
                                                                for="agree"
                                                                id="agree1"> <?= $this->lang->line('MSG_PRODUCT_CHECKOUT_31') ?></label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <button id="checkout_go"
                                                            style="margin-top: 0px;margin-bottom: -26px;"
                                                            class="btn btn-green"><?= $this->lang->line('MSG_PRODUCT_CHECKOUT_32') ?>
                                                        <i class="icon-check"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </form>
            <div class="row">
                <span class=" col-md-offset-4 col-md-4 "><br/><br/> <img
                        src="<?php echo(ASSETS_URL_FRONT . 'img/images/paiementsecurise.png'); ?>"
                        width="350px"
                        alt="Secure Familov"></span>
            </div>
        </div>
    </section>
    <!-- /End Section -->
<?php } ?>
<?php
@$this->load->view('_cart');
?>
<script>
    $('#checkout_go').click(function () {
        var order_id = jQuery('#order_id').val();
        var phone_number = jQuery('#phone_number1').val();
        var greeting_msg = jQuery('#greeting_msg').val();
        var customer_id = jQuery('#customer_id').val();
        var recp_name = jQuery('#recp_name').val();
        var security = $('input[name="agree"]').prop('checked');
        var js_status = $('input[name=payment_type]:checked').val();
        var c_code = $('ul.country-list > li.active > .dial-code').html();
        if (security == true) {
            jQuery('#agree1').attr('style', 'border:1px solid green;');
        }
        if (phone_number == "" || recp_name == "") {
            jQuery('.phone_number').attr('style', 'border:1px solid red;');
            jQuery('#recp_name').attr('style', 'border:1px solid red;');
            toastr.error('<?= $this->lang->line('MSG_PRODUCT_CHECKOUT_33') ?>', '<?=ERROR?>', {positionClass: 'toast-top-full-width'});
        } else if (security != true) {
            jQuery('.phone_number').attr('style', 'border:1px solid green;');
            jQuery('#recp_name').attr('style', 'border:1px solid green;');
            jQuery('#agree1').attr('style', 'border:1px solid red;');
            toastr.error('<?= $this->lang->line('MSG_PRODUCT_CHECKOUT_34') ?>', '<?=ERROR?>', {positionClass: 'toast-top-full-width'});
        } else if (greeting_msg.length > 140) {
            jQuery('#greeting_msg').attr('style', 'border:1px solid green;');
            toastr.error('<?= $this->lang->line('MSG_PRODUCT_CHECKOUT_35') ?>', '<?=ERROR?>', {positionClass: 'toast-top-full-width'});
        } else {
            jQuery('.phone_number').attr('style', 'border:1px solid green;');
            jQuery('#recp_name').attr('style', 'border:1px solid green;');
            jQuery('#agree1').attr('style', 'border:1px solid green;');
            jQuery('#greeting_msg').attr('style', 'border:1px solid green;');

            if (js_status == '<?=PAYMENT_STRIPE?>') {
                jQuery('.stripe-button-el').click();
                //$("#checkout_form").submit();
            } else if (js_status == '<?=PAYMENT_PAYPAL?>') {
                $("#checkout_form").submit();
            } else if (js_status == '<?=PAYMENT_BANK_TRANSFER?>') {
                $("#checkout_form").submit();
            } else {
                toastr.error('<?= $this->lang->line('MSG_PRODUCT_CHECKOUT_35') ?>', '<?=ERROR?>', {positionClass: 'toast-top-full-width'});
            }
            return false;
        }
        return false;
    })
</script>

<style>
    @import(

    'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.0/css/bootstrap.min.css'
    )

    .funkyradio div {
        clear: both;
        overflow: hidden;
    }

    .funkyradio label {
        width: 100%;
        border-radius: 3px;
        border: 1px solid #D1D3D4;
        font-weight: normal;
    }

    .funkyradio input[type="radio"]:empty,
    .funkyradio input[type="checkbox"]:empty {
        display: none;
    }

    .funkyradio input[type="radio"]:empty ~ label,
    .funkyradio input[type="checkbox"]:empty ~ label {
        position: relative;
        line-height: 2.5em;
        text-indent: 3.25em;
        margin-top: 7px;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    .funkyradio input[type="radio"]:empty ~ label:before,
    .funkyradio input[type="checkbox"]:empty ~ label:before {
        position: absolute;
        display: block;
        top: 0;
        bottom: 0;
        right: 0;
        /*left: 0;*/
        /*content: '';*/
        content: '\2714';
        text-indent: 0.9em;
        color: #fff;
        width: 2.5em;
        background: #D1D3D4;
        border-radius: 3px 0 0 3px;
    }

    .funkyradio input[type="radio"]:hover:not(:checked) ~ label,
    .funkyradio input[type="checkbox"]:hover:not(:checked) ~ label {
        color: #888;
    }

    .funkyradio input[type="radio"]:hover:not(:checked) ~ label:before,
    .funkyradio input[type="checkbox"]:hover:not(:checked) ~ label:before {
        content: '\2714';
        text-indent: .9em;
        color: #C2C2C2;
    }

    .funkyradio input[type="radio"]:checked ~ label,
    .funkyradio input[type="checkbox"]:checked ~ label {
        color: #777;
    }

    .funkyradio input[type="radio"]:checked ~ label:before,
    .funkyradio input[type="checkbox"]:checked ~ label:before {
        content: '\2714';
        text-indent: .9em;
        color: #333;
        background-color: #ccc;
    }

    .funkyradio input[type="radio"]:focus ~ label:before,
    .funkyradio input[type="checkbox"]:focus ~ label:before {
        box-shadow: 0 0 0 3px #999;
    }

    .funkyradio-default input[type="radio"]:checked ~ label:before,
    .funkyradio-default input[type="checkbox"]:checked ~ label:before {
        color: #333;
        background-color: #ccc;
    }

    .funkyradio-primary input[type="radio"]:checked ~ label:before,
    .funkyradio-primary input[type="checkbox"]:checked ~ label:before {
        color: #fff;
        background-color: #337ab7;
    }

    .funkyradio-success input[type="radio"]:checked ~ label:before,
    .funkyradio-success input[type="checkbox"]:checked ~ label:before {
        color: #fff;
        background-color: #5cb85c;
    }

    .funkyradio-danger input[type="radio"]:checked ~ label:before,
    .funkyradio-danger input[type="checkbox"]:checked ~ label:before {
        color: #fff;
        background-color: #d9534f;
    }

    .funkyradio-warning input[type="radio"]:checked ~ label:before,
    .funkyradio-warning input[type="checkbox"]:checked ~ label:before {
        color: #fff;
        background-color: #f0ad4e;
    }

    .funkyradio-info input[type="radio"]:checked ~ label:before,
    .funkyradio-info input[type="checkbox"]:checked ~ label:before {
        color: #fff;
        background-color: #5bc0de;
    }

    .funkyradio input[id="radio3"]:empty ~ label:after {
        position: absolute;
        display: block;
        top: 0;
        bottom: 0;
        left: 0;
        content: '';
        text-indent: 0.9em;
        color: #fff;
        width: 3.0em;
        background: #D1D3D4;
        border-radius: 3px 0 0 3px;
        background: url('<?php echo(ASSETS_URL_FRONT . 'img/images/fast-delivery.png'); ?>') no-repeat;
        /*background: url(images/fast-delivery.png) no-repeat;*/
        background-size: 34px;
        margin-left: 7px;
    }

    .funkyradio input[id="radio2"]:empty ~ label:after {
        position: absolute;
        display: block;
        top: 0;
        bottom: 0;
        left: 0;
        content: '';
        text-indent: 0.9em;
        color: #fff;
        width: 3.0em;
        background: #D1D3D4;
        border-radius: 3px 0 0 3px;
        /*background: url(images/grocery.png) no-repeat;*/
        background: url('<?php echo(ASSETS_URL_FRONT . 'img/images/grocery.png'); ?>') no-repeat;
        background-size: 29px;
        margin-left: 7px;
        margin-top: 5px;
    }

    .lft-40 {
        margin-left: -40px;
    }

    #phone_code {
        width: 90px !important;
        padding-right: 0px !important;
    }

    .intl-tel-input {
        width: 90px !important;
        float: left !important;
        margin: 0px !important;
    }

    #sender_number {
        width: 339px !important;
    }

    .phone_number {
        /*width: 339px !important;*/
        width: 82% !important;
    }

    .stripe-button-el {
        display: none !important;
    }

    #phone_code {
        height: 45px;
        border: 2px solid rgba(76, 67, 96, 0.10);
        border-right: none;
        background: #fff;
    }

    .phone_number {
        border-left: none;
    }

    .phone_number:focus {
        border-color: rgba(76, 67, 96, 0.10)
    }

    .intl-tel-input .flag-dropdown .selected-flag {
        padding: 13px 16px 6px 6px;
    }

    .flag-dropdown {
        padding: 0px;
        margin-top: 7px;
    }

    .intl-tel-input .flag-dropdown .selected-flag .down-arrow {
        left: 25px;
    }

    .flag {
        margin-left: 8px;
    }
</style>
<style>

    @media (max-width: 960px) {
        .phone_number {
            /*width: 339px !important;*/
            width: 75% !important;
        }
    }

</style>
<link rel="stylesheet" href="<?php echo base_url(ASSETS . 'ex_plugins/dd/intlTelInput.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url(ASSETS . 'ex_plugins/dd/demo.css'); ?>">
<script src="<?php echo base_url(ASSETS . 'ex_plugins/dd/intlTelInput.js'); ?>"></script>
<script>
    //     $("#phone_code").intlTelInput();
</script>
<script>
    $("#phone_code").intlTelInput({
        autoFormat: true,
        // allowDropdown: false,
        // autoHideDialCode: false,
        // autoPlaceholder: "off",
        // dropdownContainer: "body",
        // excludeCountries: ["us"],
        formatOnDisplay: true,
        // geoIpLookup: function(callback) {
        //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
        //     var countryCode = (resp && resp.country) ? resp.country : "";
        //     callback(countryCode);
        //   });
        // },
        // hiddenInput: "full_number",
        // initialCountry: "auto",
        // nationalMode: false,
        // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
        // placeholderNumberType: "MOBILE",
        // preferredCountries: ['cn', 'jp'],
        // separateDialCode: true,
        autoHideDialCode: true,
        utilsScript: "build/js/utils.js"
    });
</script>
