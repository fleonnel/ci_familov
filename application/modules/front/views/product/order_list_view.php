<link rel="stylesheet" href="<?php echo base_url(ASSETS_URL_FRONT.'dist/css/invoice.css');?>">
<section id="hero12" class="hero hero-countdown bg-img">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 text-center">
                <h1 class="text-white"><?= $this->lang->line('MSG_VO_01') ?></h1>
            </div>
            <div class="col-md-6 col-md-offset-3 text-white text-center">
            </div>
        </div>
    </div>
</section>
<?php if (count(@$order_list) == 0) { ?>
    <section id="faq3-1" class="p-y-lg faqs schedule">
        <div class="container">
            <div class="row ">
                <div class="col-md-12 text-center">
                    <div class="alert alert-info alert-dismissible">
                        <?= $this->lang->line('MSG_VO_02') ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <center>
                        <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/familov-deliverman.png'); ?>"
                             style="margin-top:95px;"></center>
                </div>
            </div>
        </div>
    </section>
<?php } else { ?>
    <section id="login" class=" p-y-lg1 bg-color">
        <div class="container">
            <div class="row">
                <?= _notify(); ?>
            </div>
            <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    <div class="w3-container ">
                       <!-- <div class="w3-panel w3-green11 bg-black text-white">
                            <h3><?= $this->lang->line('MSG_VO_04') ?> <span
                                    class="w3-badge w3-bar-item w3-circle  w3-yellow "><?= count(@$order_list) ?></span>
                            </h3>
                        </div>-->
                        <ul class="w3-ul w3-card-4" style="margin-top:30px;">
                            <?php $ctr = 0;
                            foreach (@$order_list as $k => $v) {
                                $ctr++; ?>
                                <div class="row">
                                <li class="w3-bar " style="border-bottom:1px solid #eee;">
                                    <div class="col-md-2 ">
                                        <!--<div class="w3-bar-item w3-center">
                                            <span
                                                class="w3-badge w3-bar-item w3-circle  w3-blue-grey w3-right1-small"><?= $ctr ?></span><br>
                                        </div>-->
                                         <small><?= date_time($v->date_time, 7) ?></small>
                                    </div>
                                     <div class="col-md-2 ">
                                    
                                        <span   class="label label-default w3-blackX"><?= $v->generate_code ?></span>
                                               
                                            
                                    </div>
                                    <div class="col-md-2 ">
                                        
                                       <span><?= $v->payment_method ?></span>
                                               
                                            
                                    </div>
                                     <div class="col-md-2 ">
                                        
                                      <span class=""><?= $v->payment_curr_code . ' ' . $v->grand_total ?></span>
                                            
                                    </div>
                                     <div class="col-md-2 ">
                                        
                                     <span class=" pull-right2"><?= $v->order_status ?></span>
                                            
                                    </div>
                                     <div class="col-md-2 ">
                                        
                                    <button
                                                class="pull-left1 w3-button w3-ripple  w3-green1 pull-right1 view_detail  col-xs-12   col-md-12 col-sm-12"
                                                data-id="<?= $v->order_id ?>"><?= $this->lang->line('MSG_VO_03') ?></button>
                                            
                                    </div>
                                  
                                   
                                </li>
                                </div>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <center>
                        <img width="250px" src="<?php echo(ASSETS_URL_FRONT . 'img/images/familov-deliverman.png'); ?>"
                             style="margin-top:50px;padding:30px;"></center>
                </div>
            </div>
        </div>
    </section>
    <!-- The Modal -->
    <div class="modal fade order_detail_popup" id="myModalXX" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content"  style="background-color:#ffffff;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><?= $this->lang->line('MSG_VO_14') ?></h4>
                </div>
                <div class="modal-body od_view" id="od_view">
                    <p></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-black "
                            data-dismiss="modal"><?= $this->lang->line('MSG_VO_15') ?></button>
                </div>
            </div>
        </div>
    </div>

<?php } ?>
<!-- /End Section -->
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>
    #myModal .modal-dialog {
        margin-top: 10% !important;
    }
    
     .w3-card-4, .w3-hover-shadow:hover {
     box-shadow:none;
    }

    .view_detail {
        display: inline-block;
        color: #ffffff;
        padding: 6px;
        background-color: #36cd75;
        font-size: 12px;
        /* font-weight: 900; */
        letter-spacing: 1px;
        vertical-align: middle;
        white-space: pre-line;
        text-align: center;
        -webkit-border-radius: 40px;
        -moz-border-radius: 40px;
        border-radius: 2px;
        cursor: pointer;
        -webkit-transition: all 0.3s ease-out;
        -moz-transition: all 0.3s ease-out;
        -ms-transition: all 0.3s ease-out;
        -o-transition: all 0.3s ease-out;
        transition: all 0.3s ease-out;
    }
</style>
<?php
@$this->load->view('_cart');
?>
<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        /*document.body.innerHTML = printContents;
         window.print();
         document.body.innerHTML = originalContents;
         */
        var myWindow = window.open('', '', '');
        myWindow.document.write($('#' + divName).html());
        myWindow.document.close();
        myWindow.focus();
        myWindow.print();
        myWindow.close();

    }
</script>
