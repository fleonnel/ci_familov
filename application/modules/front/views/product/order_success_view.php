<section id="login" class=" p-y-lg bg-color">
    <div class="container">
        <?php
        #_pre($order_confirmation);
        if (@$order_confirmation->payment_status == ORDER_PAYMENT_STATUS_SUCCESS) { ?>
            <div class="row">
                <div class="col-md-12">
                    <center><img src="<?php echo(ASSETS_URL_FRONT . 'img/images/checked.svg'); ?>"
                                 style="width:10%; margin-bottom:30px;">
                    </center>
                    <h2>
                        <center> <?= stripslashes(str_replace("#USER_NAME#", @$this->user_name, $this->lang->line('MSG_OC_02'))) ?></center>
                    </h2>
                    <h5>
                        <center> <?= stripslashes(str_replace("#RECEIVER_NAME#", @$order_confirmation->order_info['receiver_name'], $this->lang->line('MSG_OC_03'))) ?></center>
                    </h5>
                    <p style="font-size: 25px;font-weight: lighter;line-height: 35px;">
                    </p>
                    <center> <?= stripslashes(str_replace("#CODE#", @$order_confirmation->order_info['generate_code'], str_replace("#RECEIVER_NAME#", @$order_confirmation->order_info['receiver_name'], $this->lang->line('MSG_OC_04')))) ?></center>
                </div>
                <?php if (@$order_confirmation->payment_status == ORDER_PAYMENT_STATUS_SUCCESS && @$order_confirmation->payment_type == PAYMENT_BANK_TRANSFER) { ?>
                 <div class="col-md-5 col-md-offset-4 ">
                            <div class="left_beneficiary">
                                <div class="alert-group">
                                    <div class="alert alert-success alert-dismissable section_gray">
                                        <h2 class="text-center"><img src="<?php echo(ASSETS_URL_FRONT . 'img/images/bank-48.png'); ?>" class="payment-method-logo lft-40"> Bank Details </h2>
                                        <span
                                            style="color:#ee3682; font-weight: bold;">Please transfert [<?= @$order_confirmation->order_info['payment_curr'] . ' ' ._number_format( @$order_confirmation->order_info['grand_total']) ?>] in below bank account.</span>
                                        <br>
                                        <br>
                                        <strong>
                                            <table>
                                                <tbody><tr>
                                                    <td> Holder Name</td>
                                                    <td> &nbsp; : &nbsp;</td>
                                                    <td> FAMILOV LIMT</td>
                                                </tr>
                                                <tr>
                                                    <td> Bank Name</td>
                                                    <td> &nbsp; : &nbsp;</td>
                                                    <td> Sparkasse Saarbruecken</td>
                                                </tr>
                                                <tr>
                                                    <td> IBAN</td>
                                                    <td> &nbsp; : &nbsp;</td>
                                                    <td> DE12590501010067111195</td>
                                                </tr>
                                                <tr>
                                                    <td> BIC</td>
                                                    <td> &nbsp; : &nbsp;</td>
                                                    <td> SAKSDE55XXX</td>
                                                </tr>
                                                <tr>
                                                    <td> Ref/VW</td>
                                                    <td> &nbsp; : &nbsp;</td>
                                                    <td> VAQn8CI5kH</td>
                                                </tr>
                                            </tbody></table>
                                        </strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <div class="col-md-12">
                    <center>
                        <div><a href="<?= base_url() ?>"
                                style="color:#000; text-decoration: underline;"><?= $this->lang->line('MSG_OC_05') ?></a>
                        </div>
                        <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/familov-deliverman.png'); ?>"
                             style="margin-top:95px;"></center>
                </div>
            </div>
             
        <?php } else { ?>
            <div class="row">
                <div class="col-md-12">
                    <center><img src="<?php echo(ASSETS_URL_FRONT . 'img/images/if_Close_Icon_Circle_1398920.svg'); ?>"
                                 style="width:10%; margin-bottom:30px;">
                    </center>
                    <h2>
                        <center> <?= str_replace("#USER_NAME#", @$this->user_name, $this->lang->line('MSG_OC_13')) ?></center>
                    </h2>
                    <h5>
                        <center> <?= str_replace("#RECEIVER_NAME#", @$this->receiver_phone, $this->lang->line('MSG_OC_14')) ?></center>
                    </h5>

                </div>
                <div class="col-md-12">
                    <center>
                        <div><a href="<?= base_url() ?>"
                                style="color:#000; text-decoration: underline;"><?= $this->lang->line('MSG_OC_05') ?></a>
                        </div>
                        <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/familov-deliverman.png'); ?>"
                             style="margin-top:95px;"></center>
                </div>
            </div>
        <?php } ?>

    </div>
</section>