<?php
// echo "<pre>";
//    print_r($posts_images);
// echo "</pre>";exit;
if(count(@$product_response)>0){ ?>
    <!--<div class="col-md-12">
    <div class="well well-sm pull-right">
        <div class="btn-group">
            <a href="#" id="list" class="btn btn-default btn-sm"><span
                    class="glyphicon glyphicon-th-list"></span>List</a>
            <a href="#" id="grid" class="btn btn-default btn-sm"><span
                    class="glyphicon glyphicon-th"></span>Grid</a>
        </div>
    </div>
    </div>-->

    <?php
    foreach (@$product_response as $post => $v) { //_pre($v);?>
        <!--<div class="item  col-xs-3 col-lg-3">
            <div class="thumbnail">
                <img class="group list-group-image" src="http://placehold.it/400x250/000/fff" alt="" />
                <div class="caption">
                    <h4 class="group inner list-group-item-heading">
                        Product title</h4>
                    <p class="group inner list-group-item-text">
                        Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                        sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <p class="lead">
                                $21.000</p>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <a class="btn btn-success" href="http://www.jquery2dotnet.com">Add to cart</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
        <div class="item col-md-3 col-sm-4 col-xs-6">
            <div class="widget inner_product_section">
                <a href="<?= base_url(MENU_PRODUCT_DETAIL) ?>/<?= @$v->product_id ?>/<?= @$this->session->userdata('order_info')->shop_id ?>">
                    <div class="product-image group list-group-image" style="background-image:url('<?=DIR_PRODUCT_URL?><?=$v->product_image?>'); "></div>
                </a>
                <?php if (@$v->special_product_prices > 0) {
                    $discount = get_percentage(convert_price(@$v->product_prices), convert_price(@$v->special_product_prices)) . "%"; ?>
                    <div class="price-box"><?= $discount ?></div>
                <?php } else { ?>
                    <div class="price-box1"></div>
                <?php } ?>
                <br>
                <div class="product_info_list">
                    <a href="<?= base_url(MENU_PRODUCT_DETAIL) ?>/<?= @$v->product_id ?>/<?= @$this->session->userdata('order_info')->shop_id ?>">
                        <?= stripslashes(@$v->product_name) ?>
                    </a></div>
                <center>
                    <?php if (@$v->special_product_prices > 0) { ?>
                        <div class="main-price"><span
                                class="main-price-left"><?= $this->currency_symbol . convert_price(@$v->product_prices); ?></span>
                            <span
                                class="main-price-right"><?= $this->currency_symbol . convert_price(@$v->special_product_prices); ?></span>
                        </div>
                    <?php } else { ?>
                        <div
                            class="main-price"><?= $this->currency_symbol . convert_price(@$v->product_prices); ?></div>
                    <?php } ?>
                </center>

                <?php if(@$v->product_availability == PRODUCT_STATUS_AVAILABLE){ ?>
                <div class="btn btn-orange m-t wd_auto add_to_cart_btns1" id="add_cart123_2069"
                     data-product_id="<?= @$v->product_id ?>" data-qty="1"
                     onclick="add_to_cart('<?= @$v->product_id ?>',1)"><i
                        class="icon-cart"></i> <?= $this->lang->line('MSG_PRODUCT_02') ?></div>
                        <?php } else{?>
                            <div class="btn btn-red-disabled m-t wd_auto add_to_cart_btns1" id="add_cart123_2069"
                     data-product_id="<?= @$v->product_id ?>" data-qty="1"
                     onclick="add_to_cart('<?= @$v->product_id ?>',1)" disabled><i
                        class="icon-cart"></i> <?= $this->lang->line('MSG_PRODUCT_02_NOT_AVAILABLE') ?></div>
                      <?php   }?>
                <div id="preview245_2069"></div>
            </div>
        </div>
    <?php } }else { ?>
    <script>
        $('.load_more').hide();
    </script>
<?php } ?>
