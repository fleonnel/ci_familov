<section id="hero12" class="hero hero-countdown bg-img">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 text-center">
                <h1 class="text-white"><?= $this->lang->line('LBL_CONTACT_US_01') ?></h1>
                <!--<a href="#pricing6-1" class="btn btn-shadow btn-green btn-lg smooth-scroll m-b-md">RESERVE YOUR SEAT</a>-->
            </div>
            <div class="col-md-6 col-md-offset-3 text-white text-center">
                <!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae alias perspiciatis nisi expedita reprehenderi. Sariatur asperiores adipisci et, sint incidunt.</p>-->
            </div>
        </div>
    </div>
</section>
<!-- =========================
   Contact Us
============================== -->
<section id="faq3-1" class="p-y-lg faqs schedule">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="section-header text-center wow fadeIn">
                    <p class="lead m-b-md">
                    <h4><b> Email: </b> <a href="mailto:hello@familov.com">hello@familov.com</a><br><b>Whatsapp :</b>
                        (+49 1525 9948834)<br/> <b> Chat :</b> www.familov.com</h4>    <br><br>
                    <h4>
                        <b>Adresse Familov :</b></br>
                        Innovation Campus Saar<br>
                        Altenkesseler Str. 17 / A4,<br>D-66115 Saarbrücken
                        <br><br>
                    </h4>
                </div>
            </div>
        </div>
    </div>
</section>