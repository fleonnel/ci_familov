<section id="hero12" class="hero hero-countdown bg-img">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 text-center">
                <h1 class="text-white"><?= $this->lang->line('LBL_TERMS_01') ?></h1>
                <!--<a href="#pricing6-1" class="btn btn-shadow btn-green btn-lg smooth-scroll m-b-md">RESERVE YOUR SEAT</a>-->
            </div>
            <div class="col-md-6 col-md-offset-3 text-white text-center">
                <!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae alias perspiciatis nisi expedita reprehenderi. Sariatur asperiores adipisci et, sint incidunt.</p>-->
            </div>
        </div>
    </div>
</section>
<!-- =========================
   TERMS SECTION
============================== -->
<section class="p-y-lg faqs schedule">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!--<h3 class="text-center">CONDITIONS GENERALES DE VENTE</h3>-->
                <h5 style="color:#ec1e73;font-weight:bold;">1. Préambule</h5>

                <p>Les présentes conditions générales d'utilisation et de vente du service au nom commercial Familov.com
                    régissent exclusivement les relations entre Familov.com et l’utilisateur du Service.</p>
                <ol class="m-t-md">
                    <li class="m-b"><p>Familov.com permet à des Utilisateurs de commander des produits ou bons d´achat,
                            sur internet, pour des personnes physiques résidant au Cameroun (le ou les « Bénéficiaires
                            »).Dès que le payement de la commande est validé, le Bénéficiaire en est informé par
                            Familov.com au moyen d’un code confidentiel unique communiqué par SMS.</p></li>
                    <li class="m-b"><p>Le code unique fourni par Familov.com permet au Bénéficiaire d´entrer en
                            possession des produits auprès des commerces partenaires , dont une liste est disponible sur
                            le site internet de Familov.com.</p></li>
                    <li><p>Les présentes conditions générales de vente definissent les conditions et modalités d´une
                            commande sur le site www.familov.com. L’Utilisateur reconnaît lorsqu’il passe une commande
                            via le site avoir pris connaissance des conditions générales de vente et déclare
                            expressément les accepter sans réserve et ne nécessite pas une signature manuscrite de ce
                            document.</p></li>
                </ol>
                <br/>
                <h5 style="color:#ec1e73;font-weight:bold;">2. Ouverture du compte et conditions d´utilisation</h5>

                <p>L’Utilisateur certifie être âgé d'au moins 18 ans et avoir la capacité juridique ou être titulaire
                    d'une autorisation parentale pour ouvrir un compte utilisateurs auprès de Familov.com et effectuer
                    des commandes.</p>

                <p>Lors de la création du Compte, l’Utilisateur communique à Familov.com ses nom(s), prénom(s).
                    L’Utilisateur communique également une adresse e-mail valide, choisit son nom d’utilisateur et un
                    mot de passe.Le nom d'utilisateur et le mot de passe valent preuve de l'identité de l´Utilisateur et
                    l'engagent sur toute commande passée par leur intermédiaire.</p>

                <p>L’utilisateur est le responsable entier et exclusif de son identifiant et de son mot de passe. Il
                    supportera seul les conséquences qui pourraient résulter de toute utilisation par des tiers qui
                    auraient eu connaissance de ceux-ci, à moins qu'il ne démontre que la connaissance de son
                    identifiant et de son mot de passe par une autre personne résulte d'une faute de Familov. En cas
                    d'oubli de son mot de passe ou de son identifiant ou en cas de crainte qu'un tiers ait pu en avoir
                    connaissance, l´utilisateur dispose sur le site d'une fonction lui permettant de retrouver son
                    identifiant et de choisir un nouveau mot de passe. </p>
                <br/>
                <h5 style="color:#ec1e73;font-weight:bold;">3. Utilisation du Compte et commande des produits</h5>
                <ul class="m-t-md">
                    <li class="m-b"><p>Le Compte est rigoureusement personnel, et ne peut donc être partagé ou échangé
                            avec un tier.</p></li>
                    <li class="m-b"><p>Les commandes sont perçues en Euros, monnaie ayant cours légal dans l'union
                            monétaire européenne. Dans l’hypothèse où l’Utilisateur ne réside pas dans un pays où l’Euro
                            a cours légal, l’Utilisateur fera son affaire avec la banque teneuse de son compte bancaire
                            du taux de change qui sera appliqué à cet effet.</p></li>
                    <li><p>Les achats effectués par les Utilisateurs sont effectués par internet, au moyen d’une carte
                            bancaire valide , d´un compte paypal ou par virment bancaire. </p></li>
                    <li class="m-b"><p>Pour chaque commande, l’Utilisateur indique l’identité (nom et prénom) et le
                            numéro du téléphone mobile du Bénéficiaire.L’Utilisateur reçoit une confirmation de l’achat
                            effectué par e-mail. </p></li>
                    <li class="m-b"><p>Familov.com s’engage à communiquer par SMS au Bénéficiaire désigné par
                            l’Utilisateur lors de la commande, un code unique permettant d´entrer en possission des
                            produits. En cas de retards, pertes, erreurs ou omissions résultant d’une rupture des
                            télécommunications, Familov.com ne peut être tenue pour responsable de la non-délivrance du
                            SMS.</p></li>
                    <li><p>Le code unique fourni par Familov.com est indispensable au Bénéficiaire et doit être présenté
                            auprès des Partenaires pour entrer en possession des produits.</p></li>
                </ul>
                <br/>
                <h5 style="color:#ec1e73;font-weight:bold;">4. Exclusion du service</h5>

                <p>Familov.com peut suspendre de plein droit et immédiatement l'utilisation du Service par l’Utilisateur
                    ou le Bénéficiaire en cas de suspicion de fraude, de tentative de fraude ou de fraude de ces
                    personnes . Familov.com peut suspendre de plein droit et immédiatement l'utilisation du Service par
                    l’Utilisateur pour avoir manqué à l´une de ses obligations comme le defaut de payement des sommes
                    dues lors d´une commande.</p>
                <br/>
                <h5 style="color:#ec1e73;font-weight:bold;">5. Données personnelles et contrôle</h5>

                <p>L’Utilisateur doit à tout moment s'assurer que les informations qui sont fournies par lui sont
                    exactes et à jour. Toutes les cases portant la mention obligatoire dans la procédure
                    d'enregistrement sur le Site doivent contenir des informations valides. Toute tentative de fraude
                    via la présentation d'informations inexactes sur l’Utilisateur ou le Bénéficiaire sera communiquée
                    aux autorités compétentes et peut conduire au refus immédiat d'accès au Site et au Service. </p>

                <p>Chaque Utilisateur bénéficie d’un droit d’accès aux données qui le concernent collectées et traitées
                    par Familov.com ainsi que d’un droit de modification et de suppression des données le concernant.Il
                    autorise Familov.com et ses partenaires à transmettre des informations aux autorités en charge de la
                    régulation tel que requis par la loi. Les coordonnées de l’Utilisateur ne seront pas divulguées à
                    des tiers, sauf accord exprès de l’Utilisateur </p>

                <p> Toutes les informations en rapport avec toute commande sur Familov.com font l'objet d'un traitement
                    de données. Ce traitement a pour but de lutter contre les fraudes de toutes sortes. Les prestataires
                    de services de paiement par carte de crédit ou bancaire et Familov sont les bénéficiaires des
                    données en rapport avec toutes les commandes. Toute commande impayé ou toute suspicion de fraude
                    dans le payement pour quelque raison que ce soit annule automatiquement la commande. </p>
                <br/>
                <h5 style="color:#ec1e73;font-weight:bold;">6. Les mode de paiement</h5>

                <p>Le paiement s'effectue par carte de credit, par l'intermédiaire de Pay Pal, par virement bancaire. A
                    défaut d'autorisation de paiement par carte bancaire, la commande n'est pas mise à la disposition du
                    Utilisateur. Familov.com se réserve la possibilité de ne pas donner suite à la commande d'un
                    Utilisateur pour lequel des problèmes seraient d'ores et déjà survenus. Dans le cas d'un paiement
                    par virement, l'utilisateur dispose d'un délai de 7 jours pour faire parvenir le réglement sur le
                    compte de la Familov. </p>
                <br/>
                <h5 style="color:#ec1e73;font-weight:bold;">7. Les prix</h5>

                <p>Les prix indiqués sur les listes de produits et sur les fiches produits ne comprennent pas les frais
                    de livraisons à domicile. Le prix total indiqué dans la confirmation de la réception de la commande
                    comprend le prix des produits et le cas échéant les frais de livraison. </p>

                <p></p>
                <br/>
                <h5 style="color:#ec1e73;font-weight:bold;">8. La livraison et retrait</h5>

                <p>Les delais de livraison sont de 36 heures en fonction de la disponibilité du bénéficaire. Le
                    bénéficiaire est tenue de verifier l´etat des produits et les produits ouverts ou marqués ne
                    pourront être retournés.
                </p>

                <p>L'Utilisateur communique au moment de sa commande le nom et le numéro de téléphone portable du
                    bénéficiaire de la commande au cameroun. Ce numéro de téléphone sera utilisé pour vérifier
                    l'identité du Benéficiaire lors de la livraison ou du retrait en magasin.
                </p>

                <p><strong> a- Livraison dans un point partenaire :</strong><br/>L'Utilisateur peut choisir de mettre la
                    commande à disposition de son Bénéficiaire dans un point de vente partenaire. La commande sera
                    disponible 36 heures après le paiement.Lorsque le Bénéficiaire se présente au point de vente
                    partenaire, pour retirer sa commande il doit :
                <ul class="m-t-md">
                    <li class="m-b"><p>Présenter sa pièce d’identité (carte nationale d’identité) </p></li>
                    <li class="m-b"><p>Présenter le code SMS du numéro de téléphone enregistré pour le Bénéficiaire lors
                            de la prise de commande par l'utilisateur</p></li>
                    <li class="m-b"><p>Signer un bon de livraison </p></li>
                </ul>
                </p>
                <p><strong> b- Livraison a domicile :</strong><br/>L'utilisateur peut opter pour la livraison à domicile
                    et les produits seront livrés sous 36h jours ouvrables suivant le paiement de la commande. Familov
                    prend alors contact avec le Bénéficiaire pour planifier la livraison.
                    Si le numéro de téléphone portable indiqué pour joindre le Bénéficiaire n'est pas valide, Familov en
                    informe immédiatement l'utilisateur par mail qui choisit alors soit d’annuler la commande, soit de
                    communiquer un autre numéro de téléphone portable ; il dispose pour ce faire d’un délai de 24h à
                    compter du mail reçu par Familov et le livreur tentera alors une nouvelle fois de joindre le
                    Bénéficiaire pour organiser la livraison. Lors de la livraison, le Bénéficiaire de la commande devra
                    :
                <ul class="m-t-md">
                    <li class="m-b"><p>Présenter sa pièce d’identité (carte nationale d’identité) </p></li>
                    <li class="m-b"><p>Présenter le code SMS du numéro de téléphone enregistré pour le Bénéficiaire lors
                            de la prise de commande par l'utilisateur</p></li>
                    <li class="m-b"><p>Signer un bon de livraison </p></li>
                </ul>
                </p>
                <p>Les possibles retards de livraison ne donnent pas droit à l´Utilisateur d´annuler la commande , de
                    refuser les produits ou de reclamer des dommages et interets.</p>
                <br/>
                <h5 style="color:#ec1e73;font-weight:bold;">9. Réclamation et Rétractation</h5>

                <p>En cas de reclamation sur les produits, l´Utilisateur et le bénéficiaire disposent d´un maximum de 24
                    heures suivant la livraison pour formuler des reserves précises et Familov.com se reserve le droit
                    d´analyser le bien fondé de celles-ci et de communiquer sa reponse à l´Utilisateur dans les brefs
                    delais. L’Utilisateur dispose d’un droit de rétractation de sept jours ouvrables. La retratation
                    devra se faire sour forme écrite et ne s’applique pas aux produits frais et périssables.
                </p>
                <br/>
                <h5 style="color:#ec1e73;font-weight:bold;">10. Responsabilité de Familov.com </h5>

                <p>La responsabilité de Familov.com ne pourra être engagée que pour des faits prouvés qui lui seraient
                    exclusivement et directement imputables. Seuls sont susceptibles d’être indemnisés les préjudices
                    directs et certains subis par l’Utilisateur.
                </p>

                <p>Familov.com agissant comme intermediaire ne peut être tenue pour responsable des dommages de toute
                    nature tant matériels qu'immatériels ou corporels qui pourraient résulter d´un vol ou d'une mauvaise
                    utilisation du Site. La responsabilité de Familov.com ne saurait être engagée pour tout risque
                    inhérent à l'utilisation d'internet, comme par exemple une suspension du service, une intrusion , la
                    présence de virus informatique ou des achats fauduleux par un tiers.
                </p>

                <p>Familov.com ne peut être tenue pour responsable des erreurs commises par l’Utilisateur ou le
                    Bénéficiaire dans le cadre de l'utilisation du Service.
                </p>

                <p>Familov.com met en place des moyens nécessaires à la bonne marche du Service. À ce titre, la
                    responsabilité de Familov.com ne sera pas engagée en cas de force majeure au sens de la
                    jurisprudence allemande.
                </p>
                <br/>
                <h5 style="color:#ec1e73;font-weight:bold;">11. Responsabilité de l’Utilisateur et du Bénéficiaire</h5>
                <ul class="m-t-md">
                    <li class="m-b"><p>Le Bénéficiaire doit assurer la conservation de son téléphone mobile et du code
                            unique qui lui a été communiqué par Familov.com par SMS. Le code unique transmis par
                            Familov.com est intransmissible et à usage strictement personnel. Dans le cas où le
                            Bénéficiaire ferait une utilisation partagée du téléphone mobile sur lequel Familov.com
                            envoie le code unique au Bénéficiaire, Familov.com ne saurait être tenu pour responsable des
                            retraits effectués par toute autre personne utilisant le téléphone mobile.</p></li>
                    <li class="m-b"><p>Tant qu’il n’a pas été fait opposition auprès de Familov.com en raison de la
                            perte ou du vol du téléphone mobile du Bénéficiaire, l’Utilisateur et le Bénéficiaire
                            assument les conséquences de tous les retraits effectués.</p></li>
                    <li><p>Le Service doit être utilisé dans le respect des présentes conditions générales
                            d'utilisation, de la législation en vigueur et conformément à l'usage auquel il est
                            destiné. </p></li>
                    <li class="m-b"><p>Familov.com se dégage de toute responsabilité en cas d'utilisation du Service non
                            conforme aux présentes conditions. </p></li>
                </ul>
                <br/>
                <h5 style="color:#ec1e73;font-weight:bold;">12. Formation du contrat et preuve de la transaction</h5>
                <ul class="m-t-md">
                    <li class="m-b"><p>Les commandes sont récapitulées avant que l´Utilisateur ne soit invité à les
                            valider. L´ Utilisateur est ensuite invité à "confirmer le payement" ; La validation de
                            commande par un clic constitue validation ferme et définitive sous la réserve visée à
                            l'alinéa suivant.</p></li>
                    <li class="m-b"><p>Toute commande avec choix du paiement par carte bancaire n'est considérée comme
                            effective que lorsque les centres de paiement concernés ont donné leur accord. En cas de
                            refus des dits centres, la commande est automatiquement annulée et l´ Utilisateur prévenu
                            par un message à l'écran.</p></li>
                    <li><p>Les données informatisées, sauvegardées en toute sécurité dans les systèmes informatiques
                            Familov.com seront considérés comme les preuves des communications, des commandes et des
                            paiements intervenus entre les parties. Ils sont conservés dans les délais légaux de vente
                            commerciale et peuvent être produits à titre de preuve. </p></li>
                </ul>
                <br/>
                <h5 style="color:#ec1e73;font-weight:bold;">13. Confirmation, préparation et formation du contrat </h5>

                <p>Familov.com confirme à l´ Utilisateur la réception de sa commande par un courrier électronique
                    récapitulant les produits commandés, le moyen de paiement choisit, le moyen de mise à disposition
                    choisit, la facture totale de la prestation et un code unique de commande.
                </p>

                <p>Familov.com se réserve la possibilité de ne pas accepter une commande, pour un motif légitime, tenant
                    par exemple à une difficulté d'approvisionnement d'un produit, un problème concernant la
                    compréhension de la commande reçue (document illisible...), un problème prévisible concernant la
                    livraison à effectuer (exemple : livraison demandée dans une zone géographique où il existe des
                    risques avérés d'agression) ou encore tenant a l'anormalité de la commande effectuée sur le site
                    Familov.com.
                </p>

                <p>Dans l'hypothèse où une commande ne peut être acceptée pour l'un des motifs susvisés, Familov.com en
                    informe l´ Utilisateur. Le transfert des risques sur l´ Utilisateur intervient au moment de la mise
                    à disposition ou de la livraison. Les informations énoncées par l'utilisateur, lors de la prise de
                    commande, engagent celui-ci : en cas d'erreur de l´Utilisateur dans le libellé de ses coordonnées ou
                    de celles du lieu de livraison, notamment son nom, prénom, adresse, numéro de téléphone, adresse
                    électronique ou numéro de carte bancaire ou de paiement, ayant pour conséquence la perte des
                    produits, l´ Utilisateur reste responsable du paiement des produits perdus.
                </p>
                <br/>
                <h5 style="color:#ec1e73;font-weight:bold;">13. Conditions financières et frais de service</h5>

                <p>Les tarifs applicables par Familov.com sont disponibles sur le Site ces tarifs sont susceptibles
                    d'évolution.
                    Familov.com facturera à l’Utilisateur une commission pour l’utilisation du Service. Cette Commission
                    correspond à 2,50€.
                </p>

                <p>Sous réserve du droit de rétractation prévu à l’ARTICLE 9, l’Utilisateur donne mandat irrévocable à
                    Familov.com de créditer, pour son ordre et pour son compte, le compte du Partenaire du montant de la
                    commande réalisée pour le Bénéficiaire, diminué de toutes sommes dues par le Partenaire à
                    Familov.com, dans la limite du montant des sommes électroniques figurant au crédit du Compte.
                </p>
                <br/>
                <h5 style="color:#ec1e73;font-weight:bold;">14. Produits, disponibilité et garantie</h5>

                <p>Familov.com met à la disposition de l´utilisateur avant toute la commande toutes les caratéristiques
                    essentielles des produits présent dans les catalogues publiés par familov.com et ce en fonction de
                    la disponibilité des stock. La garantie légales des produits disponible est assuré par le
                    fournisseur et le fabricant. Les photographies pouvant accompagner cette description n’ont pas
                    valeur contractuelle. Les produits proposés par Familov ont vocation à être mis à disposition ou
                    livrés au Cameroun.
                </p>

                <p>La commande est exécutée au plus tard dans un délai de 7 jours ouvrés à compter du jour ouvré suivant
                    celui où l’acheteur a passé sa commande.
                    Toutefois, en cas d'indisponibilité du produit commandé, notamment du fait de nos supermarchés
                    partenaires, l´Utilisateur en sera informé au plus tôt. Dans un tel cas, familov s’engage à proposer
                    à l’acheteur un produit similaire à un prix similaire.
                </p>

                <p>Le bénéficiaire bénéficie de la garantie légale de conformité et des vices cachés sur les produits
                    vendus </p>
                <br/>
                <h5 style="color:#ec1e73;font-weight:bold;">15. Dispositions en cas de litige</h5>

                <p>Les présentes conditions de vente en ligne sont soumises à la loi Allamande. En cas de litige ou de
                    réclamation, l´Utilisateur s’adressera en priorité à Familov.com pour une conciliation directe des
                    parties. Lorsqu’une solution n’est pas trouvée un mois après la réclamation, compétence est
                    attribuée aux tribunaux compétents.
                </p>
                <br/>
                <h5 style="color:#ec1e73;font-weight:bold;">16. Propriété intellectuelle </h5>

                <p>Le Site, les applications et l’ensemble des éléments y figurant (informations, données, sons, images,
                    dessins, graphismes, signes distinctifs, logos et marques) sont la propriété exclusive de
                    Familov.com. L’ensemble de ces éléments est protégé par des droits de propriété intellectuelle et à
                    ce titre, est protégé contre toute utilisation non autorisée par la loi.
                </p>

                <p>Il est notamment strictement interdit d’utiliser ou de reproduire le nom Familov.com et/ou son logo,
                    seuls ou associés, à quelque titre que ce soit, et notamment à des fins publicitaires, sans l’accord
                    préalable et écrit de Familov.com.
                </p>

                <p>Il est également interdit à l’Utilisateur de décompiler ou désassembler le Site, les applications, de
                    les utiliser à des fins commerciales ou de porter atteinte de quelque manière que ce soit aux droits
                    d’auteur et de propriété intellectuelle de Familov.com
                </p>
                <br/>
                <h5 style="color:#ec1e73;font-weight:bold;">17. Conditions générales</h5>

                <p>Les présentes conditions générales entrent en vigueur à compter de sa mise en ligne. Elles régissent
                    toute commande passée jusqu’à son retrait.
                </p>

                <p>Il est notamment strictement interdit d’utiliser ou de reproduire le nom Familov.com et/ou son logo,
                    seuls ou associés, à quelque titre que ce soit, et notamment à des fins publicitaires, sans l’accord
                    préalable et écrit de Familov.com.
                </p>

                <p>Familov.com se réserve le droit de modifier le Site, le service fourni et les conditions générales
                    d’utilisation et invite l´ Utilisateur à chaque nouvelle commande à relire attentivement les
                    conditions générales de vente en vigueur.
                </p>
                <br/>
                <h5 style="color:#ec1e73;font-weight:bold;">18. Service Clients </h5>

                <p>Pour toute difficulté, l’utilisateur ou le Bénéficiaire peut contacter Familov.com
                </p>
                <ul class="m-t-md">
                    <li class="m-b"><p>Par email : hello@familov.com.</p></li>
                    <li class="m-b"><p>Par téléphone ou Whatsapp : +49 1525 9948834 </p></li>
                    <li><p> Chat : www.familov.com </p></li>
                </ul>
            </div>
        </div>
    </div>
</section>
