<section id="hero2" class="hero bg-edit bg-blue height-800x-fix">
    <script type="text/javascript">
        $(document).ready(function() {
            var img_array = [];
            <?php
            foreach(@$banner_list as $k=>$v)
             {
             ?>
            img_array.push('<?php echo(DIR_BANNER_URL . @$v->banner_image); ?>');
            <?php } ?>

            $("#hero2").backgroundCycle({
                imageUrls: img_array,
                fadeSpeed: 2000,
                duration: 6000,
                backgroundSize: SCALING_MODE_COVER,
            });
        });
    </script>
    <style>
        .wd_250 {
            width: 250px !important;
        }

        .wd_500 {
            width: 504px !important;
        }
        .cycle-bg-image {
            background-position:center;
            -webkit-animation: zoomin 10s ease-in infinite;
          animation: zoomin 10s ease-in infinite;
           
          overflow: hidden;
       
            
        
        }
        
        .cycle-bg-image:before  {
            content: "";
            position: absolute !important;
             display: block;
            top: 0px !important;
            height: 100% !important;
            width: 100% !important;
            min-height: 90px !important;
            background-image: -webkit-linear-gradient(to bottom, rgba(86,105,133, 0.5), rgba(0, 0, 0, 0)) !important;
            background-image: -moz-linear-gradient(to bottom, rgba(86,105,133, 0.5), rgba(0, 0, 0, 0)) !important;
            background-image: linear-gradient(to bottom, rgba(86,105,133, 0.5), rgba(0, 0, 0, 0)) !important;
        }
        
       /*.cycle-bg-image:before {
            content: "";
            position: absolute;
            height: 100%;
            width: 100%;
            display: block;
            background: linear-gradient(to right, rgba(86,105,133,0.1) 0%, rgba(86,105,133,0.1) 100%, rgba(86,105,133,0.5) 100%);
        }*/
        
        .hero .lead , .hero .sub-titel {
            /* font-weight: 700; */
            color: #fff;
            /* text-transform: uppercase; */
        }
        .form-bg {
            background: rgba(255, 255, 255, 0.4); 
           /* background:#ffffff;
             border: 1px solid #dfdfdf; */
            padding: 15px 15px 0px 15px;
            border-radius: 6px;
            position: absolute;
        }
    </style>
    <div class='centre-container'>
    </div>
    
    <div class="container vertical-center-rel">
        <div class="row">
            <?php _notify(); ?>
        </div>
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2 col-md-12 col-md-offset-0" style="text-align:left;">
                <!--<select onchange="javascript:window.location.href='<?php /*echo base_url(); */ ?>LanguageSwitcher/switchLang/'+this.value;">
                    <option value="english" <?php /*if($this->session->userdata('site_lang') == 'english') echo 'selected="selected"'; */ ?>>English</option>
                    <option value="french" <?php /*if($this->session->userdata('site_lang') == 'french') echo 'selected="selected"'; */ ?>>French</option>
                    <option value="german" <?php /*if($this->session->userdata('site_lang') == 'german') echo 'selected="selected"'; */ ?>>German</option>
                </select>-->
               <!-- <div class="lead hidden-xs type-animation "> <?php echo $this->lang->line('LBL_HERO_ONE'); ?></div>-->
                <p class="lead   "><?php echo $this->lang->line('LBL_HERO_TWO'); ?></p>

                <p class="m-b-md sub-titel"><?php echo $this->lang->line('LBL_HERO_THREE'); ?></p>

                <div class="col-md-12 form-bg">
                    <form class="form-horizontal_1" id="form_search" role="form" method="post">
                        <!-- <div class="form-inline double-input1 form-white m-b"> -->
                        <div class="row">
                                <div class="form-group col-md-3">
                                    <select name="country" id="country" class="form-control country wd_2501" style="" required>
                                        <option
                                            value=""><?php echo trim($this->lang->line('LBL_RECEIVER_COUNTRY')); ?></option>
                                        <?php foreach (@$country_list as $k => $v) { ?>
                                        <option value="<?= $v->country_id ?>"><?php echo $v->country_name; ?>
                                            <?php } ?>
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <select name="city" id="city" class="form-control wd_2501 city" style="" required>
                                        <option value=""><?php echo $this->lang->line('LBL_RECEIVER_CITY'); ?></option>
                                    </select>
                                </div>
                           
                                <div class="form-group col-md-3">
                                    <select name="shop_id" id="shop_id" class="form-control wd_5001 shop_select" style="" required>
                                        <option value=""><?php echo $this->lang->line('LBL_CHOOSE_SHOP'); ?></option>
                                    </select>
                                </div>
                            <div class="form-group col-md-3">
                                    <button type="submit" id="search_shop" style=" margin-top:0px;height: 50px;padding: 0px !important;margin: 0px; background:#36cd75;border: 1px solid transparent;color: #ffff;font-weight: bold;border-radius: 6px;"
                                    class="btn btn-green"><?php echo $this->lang->line('LBL_RECEIVER_GO'); ?></button>
                            </div>
                        </div>
                      
                    </form>
                   
                </div>
                
               
            </div>
        </div>
    </div>
    <!-- /End Container -->
</section>
<!-- =========================
      FEATURES SECTION
============================== -->
<section id="features2-1" class="p-y-md" style="background:#ffffff; ">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="section-header text-center wow fadeIn">
                    <h5 style="font-weight:bold"><?php echo $this->lang->line('LBL_HOW_IT_WORKS_01'); ?></h5>

                    <p><?php echo $this->lang->line('LBL_HIW_02'); ?><?php echo $this->lang->line('LBL_HOW_IT_WORKS_03'); ?></p>
                </div>
            </div>
        </div>
        <div class="row text-center features-block c3">
            <div class="col-md-3">
                <div class="gt_main_services bg_3">
                    <!--<h2><span class="number">1</span></h2><br/>-->
                    <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/familov-shop.svg'); ?>" alt="">

                    <h5>1- <?php echo $this->lang->line('LBL_HIW_03'); ?></h5>

                    <p><?php echo $this->lang->line('LBL_HIW_04'); ?></p>
                    <br/>
                </div>
            </div>

            <div class="col-md-3">
                <div class="gt_main_services bg_3">
                    <!--<h2><span class="number">2</span></h2><br/>-->
                    <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/familov-shield.svg'); ?>" alt="">

                    <h5><?php echo $this->lang->line('LBL_HIW_05'); ?></h5>

                    <p><?php echo $this->lang->line('LBL_HIW_06'); ?></p>

                </div>
            </div>

            <div class="col-md-3">
                <div class="gt_main_services bg_3">
                    <!--<h2><span class="number">3</span></h2><br/>-->
                    <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/mail.svg'); ?>" alt="">

                    <h5><?php echo $this->lang->line('LBL_HIW_07'); ?></h5>

                    <p><?php echo $this->lang->line('LBL_HIW_08'); ?></p>

                </div>
            </div>
            <div class="col-md-3">
                <div class="gt_main_services bg_3">
                    <!--<h2><span class="number">✓</span></h2><br/>-->
                    <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/familov-delivery-man.svg'); ?>" alt="">
                    <h5><?php echo $this->lang->line('LBL_HIW_09'); ?></h5>

                    <p><?php echo $this->lang->line('LBL_HIW_10'); ?></p>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- /End Features Section -->
<!-- =========================
     TESTIMONIAL
============================== -->
<section id="testimonials3-2" class="p-y-lg bg-edit bg-green">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="m-b-md text-center wow fadeIn">
                    <p class="lead"><?php echo $this->lang->line('LBL_TESTIMONIAL_01'); ?></p>

                    <p><?php echo $this->lang->line('LBL_TESTIMONIAL_02'); ?></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 testimonials ">
                <div class="col-md-4 text-center p-t-md  clearfix">
                    <blockquote class="quote-border">
                        <figure><img src="<?php echo(ASSETS_URL_FRONT . 'img/images/testimonial1.jpg'); ?>"
                                     class="img-circle" alt="" width="90" height="90"></figure>
                        <p><?php echo $this->lang->line('LBL_TESTIMONIAL_PERSON_1_03'); ?> <br><br/> <i
                                class="fa fa-star"></i><i
                                class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i
                                class="fa fa-star"></i></p>

                        <div class="cite text-edit">
                            <?php echo $this->lang->line('LBL_TESTIMONIAL_PERSON_1_01'); ?>
                            <span
                                class="cite-info p-opacity "><?php echo $this->lang->line('LBL_TESTIMONIAL_PERSON_1_02'); ?></span>
                        </div>
                    </blockquote>
                </div>
                <div class="col-md-4 text-center p-t-md clearfix">
                    <blockquote class="quote-border">
                        <figure><img src="<?php echo(ASSETS_URL_FRONT . 'img/images/Alain.jpg'); ?>" alt=""
                                     class="img-circle" width="90" height="90"></figure>
                        <p><?php echo $this->lang->line('LBL_TESTIMONIAL_PERSON_2_03'); ?><br><br/> <i
                                class="fa fa-star"></i><i
                                class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i
                                class="fa fa-star"></i></p>

                        <div class="cite text-edit">
                            <?php echo $this->lang->line('LBL_TESTIMONIAL_PERSON_2_01'); ?>
                            <span
                                class="cite-info p-opacity"><?php echo $this->lang->line('LBL_TESTIMONIAL_PERSON_2_02'); ?></span>
                        </div>
                    </blockquote>
                </div>
                <div class="col-md-4 text-center p-t-md clearfix">
                    <blockquote class="quote-border">
                        <figure><img src="<?php echo(ASSETS_URL_FRONT . 'img/images/testimonial3.jpg'); ?>" alt=""
                                     class="img-circle" width="90" height="90"></figure>
                        <p><?php echo $this->lang->line('LBL_TESTIMONIAL_PERSON_3_03'); ?><br><br/> <i
                                class="fa fa-star"></i><i
                                class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i
                                class="fa fa-star"></i></p>

                        <div class="cite text-edit">
                            <?php echo $this->lang->line('LBL_TESTIMONIAL_PERSON_3_01'); ?>
                            <span
                                class="cite-info p-opacity"><?php echo $this->lang->line('LBL_TESTIMONIAL_PERSON_3_02'); ?></span>
                        </div>
                    </blockquote>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /End Testimonial Section -->
<!-- =========================
Why
============================== -->
<section id="features2-1" class="p-y-lg" style="background:#f5f5f5">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="section-header text-center wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                    <p class="lead"> <?php echo $this->lang->line('LBL_HOME_01'); ?></p>
                    <!--   <p class="lead">Harum voluptate sunt alias eveniet necessitatibus natus, odio quisquam cumque, laudantium quasi ut ipsam. Omnis quaerat veniam nesciunt earum.</p>    -->
                </div>
            </div>
        </div>
        <div class="row text-center features-block c3">
            <div class="col-sm-4">
                <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/right/coin6.png'); ?>" alt="">
                <h4><?php echo $this->lang->line('LBL_HOME_02'); ?></h4>

                <p><?php echo $this->lang->line('LBL_HOME_03'); ?></p>
            </div>
            <div class="col-sm-4">
                <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/right/coin1.png'); ?>" alt="">
                <h4><?php echo $this->lang->line('LBL_HOME_04'); ?></h4>

                <p><?php echo $this->lang->line('LBL_HOME_05'); ?></p>
            </div>
            <div class="col-sm-4">
                <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/right/coin3.png'); ?>" alt="">
                <h4><?php echo $this->lang->line('LBL_HOME_06'); ?></h4>

                <p><?php echo $this->lang->line('LBL_HOME_07'); ?></p>
            </div>
        </div>
        <div class="row text-center features-block c3 p-y-md">
            <div class="col-sm-4">
                <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/right/coin2.png'); ?>" alt="">
                <h4><?php echo $this->lang->line('LBL_HOME_08'); ?></h4>

                <p><?php echo $this->lang->line('LBL_HOME_09'); ?></p>
            </div>
            <div class="col-sm-4">
                <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/right/coin4.png'); ?>" alt="">
                <h4><?php echo $this->lang->line('LBL_HOME_10'); ?></h4>

                <p><?php echo $this->lang->line('LBL_HOME_11'); ?></p>
            </div>
            <div class="col-sm-4">
                <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/right/coin5.png'); ?>" alt="">
                <h4><?php echo $this->lang->line('LBL_HOME_12'); ?></h4>

                <p><?php echo $this->lang->line('LBL_HOME_13'); ?></p>
            </div>
        </div>
    </div>
    <div class="col-md-8 col-md-offset-2 text-center p-t-md wow fadeIn"
         style="visibility: visible; animation-name: fadeIn;">
        <a href="<?= MENU_SIGN_UP ?>" class="btn  btn-green "><?php echo $this->lang->line('LBL_HOME_14'); ?></a>
    </div>
</section>
<section id="testimonials3-2" class="p-y-xs " style="background:#fff">
    <div class="container-fluid container p-y-md">
        <div class="row-padding">
            <div class="row ">
                <div class="col-sm-2 logo-footer-payoff">
                    <b>featured on:</b>
                </div>
                <div class="col-sm-10">
                    <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/partnerlogos_new.png'); ?>"
                         class="img-responsive" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<section id="pricing1-1" class="p-t-lg p-b-lg hidden-xs"
         style=" padding-bottom:50px; background:url('<?= ASSETS_URL_FRONT ?>img/images/familov-story.png');background-size:cover;">
    <div class="container">
        <div class="row pricing-3po">
            <div class=" col-md-offset-6 col-md-6">
                <div class="info text-center ">
                    <h4 class="m-b-md"><?php echo $this->lang->line('LBL_HOME_15'); ?> </h4>
                    <a class="btn btn-green wow pulse smooth-scroll" data-wow-iteration="12" href="<?= MENU_SIGN_UP ?>"
                       style="visibility: visible; animation-iteration-count: 12; animation-name: pulse;"><?= $this->lang->line('LBL_HOME_16'); ?> </a>
                </div>
            </div>
        </div>
    </div>
</section>
  <style>
        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            vertical-align: inherit !important;
        }
        
        input[type=text],.form-control {   
            /* Remove First */
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
        
            /* Then Style */
           
            outline: none;    
        }
        .hero .lead, .hero .sub-titel {
          
            text-shadow: 1px 1px 40px rgba(22, 23, 31, 0.4) !important;
        }
                
        /* Zoom in Keyframes */
        @-webkit-keyframes zoomin {
          0% {transform: scale(1);}
          500% {transform:translateY(-20px);}
          100% {transform:translateY(-10px);}
        }
        @keyframes zoomin {
          0% {transform: scale(1);}
          50% {transform:translateY(-20px);}
          100% {transform:translateY(-10px);}
        } /*End of Zoom in Keyframes */
        
        /* Zoom out Keyframes */
        @-webkit-keyframes zoomout {
          0% {transform: scale(1);}
          50% {transform: scale(0.67);}
          100% {transform: scale(1);}
        }
        @keyframes zoomout {
            0% {transform: scale(1);}
          50% {transform: scale(0.67);}
          100% {transform: scale(1);}
        }/*End of Zoom out Keyframes */

        @media (max-width: 960px) {
           
            
            .hero p.m-b-md ,.section-header p , .gt_main_services p {
                font-size: 11px;
                line-height: 22px;
            }
            #features2-1 img {
                max-width: 50px;
            }
            .vertical-center-rel {
                  margin-top: 150px !important;
            }
            .form-control,.form-group button{
                width:97%;
            }
            
    
            .gt_main_services {
                float: left;
                width: 100%;
                padding: 10px 30px 10px;
                position: relative;
            }

        }

    </style>

<script>
    $(document).ready(function () {
        //swal("Login Error !", "asda", "error");
        //toastr.error('Vikrant', '<?=ERROR?>',{positionClass: 'toast-top-full-width'});
        $("#form_search").validate({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                error.insertAfter(element);
                // for other inputs, just perform default behavior
            },
            ignore: "",
            rules: {
                country: {
                    required: true
                },
                city: {
                    required: true
                },
                shop_id: {
                    required: true
                }
            },
            messages: {
                country: {
                    required: '<?php echo $this->lang->line('LBL_SEARCH_VALIDATION_COUNTRY');  ?>'
                },
                city: {
                    required: '<?php echo $this->lang->line('LBL_SEARCH_VALIDATION_CITY');  ?>'
                },
                shop_id: {
                    required: '<?php echo $this->lang->line('LBL_SEARCH_VALIDATION_SHOP');  ?>'
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                //successHandler1.hide();
                //errorHandler1.show();
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            },
            submitHandler: function (frmadd) {
                successHandler1.show();
                errorHandler1.hide();
            }
        });
    });
    $(".country").change(function (e) {
        var country_id = $(this).val();
        $.ajax({
            url: '<?=base_url('get-city-list')?>',
            type: "POST",
            data: {country_id: country_id},
            dataType: "JSON",
            success: function (data) {
                $('#city').html(data.option_html);
                $('.shop_select').html(data.option_html_shop);
                if (data.FLASH_STATUS == '<?=FLASH_STATUS_SUCCESS?>') {
                    //toastr.success(data.FLASH_MESSAGE, '<?=SUCCESS?>');
                }
                else if (data.FLASH_STATUS == '<?=FLASH_STATUS_INFO?>') {
                    toastr.info(data.FLASH_MESSAGE, '<?=INFO?>');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error('<?=DATA_TABLE_ERROR?>', '<?=ERROR?>');
            }
        });
    });
    $(".city").change(function (e) {
        var city_id = $(this).val();
        $.ajax({
            url: '<?=base_url('get-shop-list')?>',
            type: "POST",
            data: {city_id: city_id},
            dataType: "JSON",
            success: function (data) {
                $('.shop_select').html(data.option_html_shop);
                if (data.FLASH_STATUS == '<?=FLASH_STATUS_SUCCESS?>') {

                    //toastr.success(data.FLASH_MESSAGE, '<?=SUCCESS?>');
                }
                else if (data.FLASH_STATUS == '<?=FLASH_STATUS_INFO?>') {
                    toastr.info(data.FLASH_MESSAGE, '<?=INFO?>');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error('<?=DATA_TABLE_ERROR?>', '<?=ERROR?>');
            }
        });
    });
</script>