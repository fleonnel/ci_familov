<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class LanguageSwitcher extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    function switchLang($language = "")
    {
        $language = ($language != "") ? $language : "english";
        $this->session->set_userdata('site_lang', $language);
        redirect($_SERVER['HTTP_REFERER']);
    }

    function switch_currency($curr = "")
    {
        $curr = ($curr != "") ? $curr : "USD";
        $this->session->set_userdata('site_curr', $curr);
        set_currency();
        $this->cart();
        redirect($_SERVER['HTTP_REFERER']);
    }

    private function cart()
    {
        $cart_array = @$this->session->userdata('cart');
        #_pre($this->session->userdata(),0);
        if (count($cart_array) > 0) {
            foreach ($cart_array as $k => $v) {
                #_e(($v['price_temp']),0);
                $cart_array[$k]['price'] = convert_price($v['price_temp']);
            }
            $cart['cart'] = $cart_array;
            # _pre($cart_array);
            $this->session->set_userdata($cart);
        }
    }
}