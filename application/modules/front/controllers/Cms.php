<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cms extends Front_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function index()
    {
        redirect('/');
    }

    public function faq()
    {
        $this->data['page_title'] = $this->lang->line('PAGE_TITLE_FAQ');
        $this->render(CMS_D . FAQ_V);
    }

    public function how_it_works()
    {
        $this->data['page_title'] = $this->lang->line('PAGE_TITLE_HIW');
        $this->render(CMS_D . HOW_IT_WORKS_V);
    }

    public function terms()
    {
        $this->data['page_title'] = $this->lang->line('PAGE_TITLE_TERMS');
        $this->render(CMS_D . TERMS_V);
    }

    public function privacy()
    {
        $this->data['page_title'] = $this->lang->line('PAGE_TITLE_PRIVACY');
        $this->render(CMS_D . PRIVACY_V);
    }

    public function about_us()
    {
        $this->data['page_title'] = $this->lang->line('PAGE_TITLE_ABOUT_US');
        $this->render(CMS_D . ABOUT_US_V);
    }

    public function contact_us()
    {
        $this->data['page_title'] = $this->lang->line('PAGE_TITLE_CONTACT_US');
        $this->render(CMS_D . CONTACT_US_V);
    }

    public function press()
    {
        $this->data['page_title'] = $this->lang->line('PAGE_TITLE_PRESS');
        $this->render(CMS_D . PRESS_V);
    }

    public function jobs()
    {
        $this->data['page_title'] = $this->lang->line('PAGE_TITLE_JOBS');
        $this->render(CMS_D . JOBS_V);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */