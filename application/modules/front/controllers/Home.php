<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends Front_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model("customer_model");
        $this->load->model("country_model");
        $this->load->model("city_model");
        $this->load->model("shop_model");
        $this->load->model("banner_model");
    }
    public function index() {
        $this->data['page_title'] = $this->lang->line('welcome_message');
        $country_list = $this->country_model->get_country_list();
        $this->data['country_list'] = $country_list;
        $banner_list = $this->banner_model->get_banner_list();
        $this->data['banner_list'] = $banner_list;
        if (count($this->input->post()) > 0) {
            $this->form_validation->set_rules('country', 'Country', 'trim|required');
            $this->form_validation->set_rules('city', 'City', 'trim|required');
            $this->form_validation->set_rules('shop_id', 'Shop', 'required');
            if ($this->form_validation->run() == FALSE) {
                $form_errors = _array_to_obj($this->form_validation->error_array());
                _set_flashdata(FLASH_STATUS_ERROR, $form_errors);
            } else {
                $country = trim($this->input->post('country'));
                $city = trim($this->input->post('city'));
                $shop = trim($this->input->post('shop_id'));
                redirect(base_url(MENU_PRODUCT_LIST . "/" . $country . "/" . $city . "/" . $shop));
                exit;
            }

        }
        $this->render(HOME_D . HOME_V);
    }

    public function get_city_list()
    {
        $country_id = $this->db->escape_str(trim($this->input->post('country_id')));
        $query_response = null;
        if ($country_id != '') {
            $query_response = $this->city_model->get_city_list_by_country($country_id);
        }
        #_pre($query_response);
        $select_html = '';
        $select_html .= '<option value="" disabled selected>' . $this->lang->line('LBL_RECEIVER_CITY') . '</option>';

        $select_html_shop = '';
        $select_html_shop .= '<option value="" disabled selected>' . $this->lang->line('LBL_CHOOSE_SHOP') . '</option>';
        if (count($query_response) > 0) {
            foreach ($query_response as $k => $v) {
                $select_html .= '<option value="' . $v->city_id . '">' . $v->city_name . '</option>';
            }
            echo json_encode(array(FLASH_STATUS => FLASH_STATUS_SUCCESS, 'option_html' => $select_html, 'option_html_shop' => $select_html_shop));
            exit;
        } else {
            echo json_encode(array(FLASH_STATUS => FLASH_STATUS_INFO, FLASH_MESSAGE => $this->lang->line('LBL_SEARCH_CITY_LIST'), 'option_html' => $select_html, 'option_html_shop' => $select_html_shop));
            exit;
        }
    }

    public function get_shop_list()
    {
        $city_id = $this->db->escape_str(trim($this->input->post('city_id')));
        $query_response = null;
        if ($city_id != '') {
            $query_response = $this->shop_model->get_shop_list_by_city($city_id);
        }
        $select_html = '';
        $select_html .= '<option value="" disabled selected>' . $this->lang->line('LBL_RECEIVER_CITY') . '</option>';
        $select_html_shop = '';
        $select_html_shop .= '<option value="" disabled selected>' . $this->lang->line('LBL_CHOOSE_SHOP') . '</option>';
        if (count($query_response) > 0) {
            foreach ($query_response as $k => $v) {
                $select_html_shop .= '<option value="' . $v->shop_id . '">' . stripslashes($v->shop_name) . '</option>';
            }
            echo json_encode(array(FLASH_STATUS => FLASH_STATUS_SUCCESS, 'option_html' => $select_html, 'option_html_shop' => $select_html_shop));
            exit;
        } else {
            echo json_encode(array(FLASH_STATUS => FLASH_STATUS_INFO, FLASH_MESSAGE => $this->lang->line('LBL_SEARCH_CITY_LIST'), 'option_html' => $select_html, 'option_html_shop' => $select_html_shop));
            exit;
        }
    }
    public function sign_up($friend_referral_id = '')
    {
        if (@$this->is_login) {
            redirect(base_url());
        }
        if (count($this->input->post()) > 0) {
            $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
            $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('phone_code', 'Phone Code', 'trim|required');
            $this->form_validation->set_rules('phone_number', 'Phone Number', 'trim|required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            if ($this->form_validation->run() == FALSE) {
                $form_errors = _array_to_obj($this->form_validation->error_array());
                _set_flashdata(FLASH_STATUS_ERROR, $form_errors, FLASH_HTML);
            } else {
                $email = $this->db->escape_str(trim($this->input->post('email')));
                $select_response = $this->customer_model->check_email(array('email' => $email));
                if (count($select_response)) {
                    _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_SIGN_UP_EMAIL_EXIST'), FLASH_HTML);
                } else {
                    /*$first_name=$this->db->escape_str(trim($this->input->post('first_name')));
                    $first_name = iconv(mb_detect_encoding($first_name, mb_detect_order(), true), "UTF-8", $first_name);
                    _e($first_name);*/
                    $first_name = $this->db->escape_str(trim($this->input->post('first_name')));
                    $last_name = $this->db->escape_str(trim($this->input->post('last_name')));
                    $password = (md5(trim($this->input->post('password'))));
                    $new_password = $this->db->escape_str(trim($this->input->post('password')));
                    $phone_code = $this->db->escape_str(trim($this->input->post('phone_code')));
                    $phone_number = $this->db->escape_str(trim($this->input->post('phone_number')));
                    $referral_id = uniqid("referral", true);
                    $crud_data = array(
                        'email_address' => $email,
                        'username' => $first_name,
                        'lastname' => $last_name,
                        'password' => $password,
                        'phone_code' => $phone_code,
                        'sender_phone' => $phone_number,
                        'phone_number' => $phone_number,
                        'referral' => $referral_id,
                        'signup_date' => date("Y-m-d"),
                    );
                    $insert_response = $this->dbcommon->insert('customers', $crud_data);
                    $insert_id = $this->db->insert_id();
                    if ($insert_response) {
                        /* friend referral Start */
                        if ($friend_referral_id != null && $friend_referral_id != '') {
                            $crud_referral_data = array(
                                'my_referrel_id' => $referral_id,
                                'friend_referrel_id' => $friend_referral_id,
                                'promo_code' => 'fixed',
                                'discount' => 2,
                                'status' => 'unused'
                            );
                            $insert_referral_response = $this->dbcommon->insert('referrels', $crud_referral_data);
                        }
                        /* friend referral End*/
                        $user_name = trim($this->input->post('first_name'));
                        $mail_content_data = new stdClass();
                        $mail_content_data->var_user_name = $user_name;
                        $mail_content_data->var_email = $email;
                        $mail_content_data->var_password = $new_password;
                        $mail_data = email_template('MAIL_SIGN_UP', $mail_content_data); #TODO MAIL
                        if (MAIL_TEST_MODE == MAIL_TEST_MODE_LIVE) {
                            $email = MAIL_TEST_EMAIL;
                        }
                        if (is_object($mail_data)) {
                            $this->load->library('mail');
                            $this->mail->setTo($email, $user_name);
                            $this->mail->setSubject($mail_data->email_subject);
                            $this->mail->setBody(_header_footer($mail_data->email_html));
                            $status = $this->mail->send();
                        }
                        _set_flashdata(FLASH_STATUS_SUCCESS, $this->lang->line('MSG_SIGN_UP_SUCCESS'), FLASH_HTML);
                        if ($this->session->userdata('page') != NULL) {
                            redirect(base_url(MENU_CHECKOUT));
                        } else {
                            redirect(base_url(MENU_LOG_IN));
                        }

                    } else {
                        _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_SIGN_UP_FAILED'), FLASH_HTML);
                    }
                }
            }
        }
        #_set_flashdata(FLASH_STATUS_SUCCESS, $this->lang->line('MSG_LOGOUT_SUCCESS'),FLASH_HTML);
        $this->data['page_title'] = $this->lang->line('PAGE_TITLE_SIGN_UP');
        $this->render(USER_D . SIGN_UP_V);
    }

    public function log_in()
    {
        if (@$this->is_login) {
            redirect(base_url());
        }
        if (count($this->input->post()) > 0) {
            #$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'required');
            #_pre($_REQUEST,1);
            if ($this->form_validation->run() == FALSE) {
                $form_errors = _array_to_obj($this->form_validation->error_array());
                _set_flashdata(FLASH_STATUS_ERROR, $form_errors, FLASH_HTML);
            } else {
                $user_name = $this->db->escape_str(trim($this->input->post('email')));
                $password = (md5(trim($this->input->post('password'))));
                $select_response = $this->customer_model->check_login(array('email' => $user_name, 'password' => $password));
                if (count($select_response)) {
                    if (ucfirst(strtolower($select_response->status)) == STATUS_CUSTOMER_ACTIVE) {
                        # _e();
                        #_pre($select_response);
                        unset($select_response->password);
                        $session[SESSION_SITE] = $select_response;
                        $session[SESSION_SITE]->is_login = TRUE;
                        $this->session->set_userdata($session);
                        _set_flashdata(FLASH_STATUS_SUCCESS, $this->lang->line('MSG_LOGIN_SUCCESS'));
                        if ($this->session->userdata('page') != NULL) {
                            redirect(base_url(MENU_CHECKOUT));
                        } else {
                            redirect('/');
                        }

                    } else {
                        _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_LOGIN_STATUS_INACTIVE'), FLASH_HTML);
                        $this->data['user_name'] = $this->input->post('username');
                        $this->data['password'] = $this->input->post('password');
                    }
                } else {
                    _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_LOGIN_INVALID'), FLASH_HTML);
                    $this->data['user_name'] = $this->input->post('username');
                    $this->data['password'] = $this->input->post('password');
                }
            }
        }
        $this->data['page_title'] = $this->lang->line('PAGE_TITLE_LOG_IN');
        $this->render(USER_D . LOG_IN_V);
    }



    public function log_out()
    {
        $this->session->unset_userdata(SESSION_SITE);
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
        _set_flashdata(FLASH_STATUS_SUCCESS, $this->lang->line('MSG_LOGOUT_SUCCESS'), FLASH_HTML);
        redirect(base_url(MENU_LOG_IN));
        exit;
    }


    public function change_password()
    {
        if (!@$this->is_login) {
            redirect(base_url());
        }
        $this->data['page_title'] = $this->lang->line('MSG_CP_TITLE');
        $this->render(USER_D . CHANGE_PASSWORD_V);
    }

    public function change_password_update()
    {
        if (!@$this->is_login) {
            redirect(base_url());
        }
        if (count($this->input->post()) > 0) {
            $this->form_validation->set_rules('current_password', 'Current Password', 'trim|required');
            $this->form_validation->set_rules('new_password', 'New Password', 'required');
            if ($this->form_validation->run() == FALSE) {
                $form_errors = _array_to_obj($this->form_validation->error_array());
                _set_flashdata(FLASH_STATUS_ERROR, $form_errors, FLASH_HTML);
            } else {
                $current_password = $this->db->escape_str(md5(trim($this->input->post('current_password'))));
                $new_password = $this->db->escape_str(md5(trim($this->input->post('new_password'))));
                $select_response = $this->customer_model->check_password(array('password' => $current_password, 'customer_id' => $this->customer_id));
                #_pre($select_response);
                if (count($select_response)) {
                    $crud_password = array('password' => $new_password);
                    $where = array('customer_id' => $this->customer_id);
                    $update_response = $this->dbcommon->update('customers', $where, $crud_password);
                    if ($update_response)
                        _set_flashdata(FLASH_STATUS_SUCCESS, $this->lang->line('MSG_CP_SUCCESS'), FLASH_HTML);
                    else
                        _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_CP_FAILED'), FLASH_HTML);
                } else {
                    _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_CP_INVALID'), FLASH_HTML);
                }
            }
        }
        redirect(base_url(MENU_CHANGE_PASSWORD));
    }

    public function my_account()
    {
        if (!@$this->is_login) {
            redirect(base_url());
        }
        $select_response = $this->customer_model->get_info_by_id($this->customer_id);
        unset($select_response->password);
        $this->data['page_title'] = $this->lang->line('MSG_MA_TITLE');
        $this->data['customer_info'] = $select_response;
        $this->render(USER_D . MY_ACCOUNT_V);
    }

    public function my_account_update()
    {
        if (!@$this->is_login) {
            redirect(base_url());
        }
        if (count($this->input->post()) > 0) {
            $this->form_validation->set_rules('username', 'First Name', 'trim|required');
            $this->form_validation->set_rules('lastname', 'Last Name', 'required');
            $this->form_validation->set_rules('sender_number', 'Phone Number', 'required');
            $this->form_validation->set_rules('phone_code', 'Phone Code', 'required');
            $this->form_validation->set_rules('country_id', 'Country', 'required');
            $this->form_validation->set_rules('city_id', 'City', 'required');
            $this->form_validation->set_rules('home_address', 'Address', 'required');
            $this->form_validation->set_rules('postal_code', 'Postal Code', 'required');
            if ($this->form_validation->run() == FALSE) {
                $form_errors = _array_to_obj($this->form_validation->error_array());
                $errors = '';
                foreach ($form_errors as $k => $v) {
                    if ($errors != '') {
                        $errors .= "<br>";
                    }
                    $errors .= $v;
                }
                echo json_encode(array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => $errors));
                exit;
            } else {
                #_pre($_REQUEST);
                $customer_id = $this->customer_id;
                $home_gender = $this->db->escape_str((trim($this->input->post('home_gender'))));
                $username = $this->db->escape_str((trim($this->input->post('username'))));
                $lastname = $this->db->escape_str((trim($this->input->post('lastname'))));
                $dob = $this->db->escape_str((trim($this->input->post('dob'))));
                $sender_phone = $this->db->escape_str((trim($this->input->post('sender_number'))));
                $home_address = $this->db->escape_str((trim($this->input->post('home_address'))));
                $country_id = $this->db->escape_str((trim($this->input->post('country_id'))));
                $city_id = $this->db->escape_str((trim($this->input->post('city_id'))));
                $country_code = $this->db->escape_str((trim($this->input->post('country_code'))));
                $postal_code = $this->db->escape_str((trim($this->input->post('postal_code'))));
                $phone_code = $this->db->escape_str((trim($this->input->post('phone_code'))));
                $notification_email = ($this->input->post('notification_email') == 1) ? 1 : 0;
                $notification_text = ($this->input->post('notification_text') == 1) ? 1 : 0;
                $update_data = array(
                    'username' => $username,
                    'lastname' => $lastname,
                    'sender_phone' => $sender_phone,
                    'home_address' => $home_address,
                    'country_id' => $country_id,
                    'city_id' => $city_id,
                    'dob' => $dob,
                    'notification_email' => $notification_email,
                    'notification_text' => $notification_text,
                    'country_code' => $country_code,
                    'postal_code' => $postal_code,
                    'home_gender' => $home_gender,
                    'phone_code' => $phone_code,
                );
                $where = array('customer_id' => $this->customer_id);
                $update_response = $this->dbcommon->update('customers', $where, $update_data);
                if ($update_response) {
                    _set_flashdata(FLASH_STATUS_SUCCESS, $this->lang->line('MSG_MA_22'));
                    echo json_encode(array(FLASH_STATUS => FLASH_STATUS_SUCCESS, FLASH_MESSAGE => $this->lang->line('MSG_MA_22')));
                    exit;
                } else {
                    echo json_encode(array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => $this->lang->line('MSG_MA_23')));
                    exit;
                }
            }
        }
        echo json_encode(array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => $this->lang->line('MSG_MA_23')));
        exit;
    }

    public function invite_friends()
    {
        if (!@$this->is_login) {
            redirect(base_url());
        }
        $this->data['page_title'] = $this->lang->line('MSG_IF_TITLE');
        $this->data['referral_url'] = base_url(MENU_SIGN_UP . "/" . @$this->session->userdata(SESSION_SITE)->referral);
        $this->render(USER_D . INVITE_FRIENDS_V);
    }

    public function forgot_password()
    {
        if (@$this->is_login) {
            redirect(base_url());
        }
        $this->data['page_title'] = $this->lang->line('MSG_FP_TITLE');
        $this->render(USER_D . FORGOT_PASSWORD_V);
    }

    public function forgot_password_send()
    {
        if (@$this->is_login) {
            redirect(base_url());
        }
        if (count($this->input->post()) > 0) {
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            if ($this->form_validation->run() == FALSE) {
                $form_errors = _array_to_obj($this->form_validation->error_array());
                _set_flashdata(FLASH_STATUS_ERROR, $form_errors, FLASH_HTML);
            } else {
                $email = $this->db->escape_str(trim($this->input->post('email')));
                $select_response = $this->customer_model->check_email(array('email' => $email));
                if (count($select_response)) {
                    if ($select_response->status == STATUS_CUSTOMER_ACTIVE) {
                        $token = md5(time());
                        #$reset_link = base_url() . "reset-password?token=" . $token . "&e_token=" . base64_encode($email) ;
                        $reset_link = base_url() . "reset-password/" . $token . "/" . base64_encode($email);
                        $crud_token = array('pwd_token' => $token);
                        $where = array('customer_id' => @$select_response->customer_id);
                        $update_response = $this->dbcommon->update('customers', $where, $crud_token);
                        $to = $email;
                        if (MAIL_TEST_MODE == MAIL_TEST_MODE_LIVE) {
                            $to = MAIL_TEST_EMAIL;
                        } else {

                        }
                        if ($update_response) {
                            $user_name = $select_response->username;
                            $mail_content_data = new stdClass();
                            $mail_content_data->var_user_name = $user_name;
                            $mail_content_data->var_email = $email;
                            $mail_content_data->var_reset_url = $reset_link;
                            $mail_data = email_template('MAIL_FORGOT_PASSWORD', $mail_content_data);
                            if (MAIL_TEST_MODE == MAIL_TEST_MODE_LIVE) {
                                $email = MAIL_TEST_EMAIL;
                            }
                            if (is_object($mail_data)) {
                                #_pre($mail_content_data,0);
                                $this->load->library('mail');
                                $this->mail->setTo($to, $user_name);
                                $this->mail->setSubject($mail_data->email_subject);
                                $this->mail->setBody(_header_footer($mail_data->email_html));
                                $status = $this->mail->send();
                                #_e($status." => ".$to);
                            }
                        }
                        _set_flashdata(FLASH_STATUS_SUCCESS, str_replace('#EMAIL#', $email, $this->lang->line('MSG_FORGOT_PASSWORD_SUCCESS')), FLASH_HTML);
                    } else {
                        _set_flashdata(FLASH_STATUS_ERROR, str_replace('#EMAIL#', $email, $this->lang->line('MSG_FORGOT_PASSWORD_INACTIVE')), FLASH_HTML);
                    }
                } else {
                    _set_flashdata(FLASH_STATUS_ERROR, str_replace('#EMAIL#', $email, $this->lang->line('MSG_FORGOT_PASSWORD_INVALID')), FLASH_HTML);
                }
            }
        }
        redirect(base_url(MENU_FORGOT_PASSWORD));
    }

    public function reset_password($token = '', $e_token = '')
    {
        if (@$this->is_login) {
            redirect(base_url());
        }
        $reset_status = 1;
        if ($token == '' || $e_token == '') {
            $reset_status = 0;
            #_set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_FORGOT_PASSWORD_INACTIVE'), FLASH_HTML);
            $this->data[FLASH_STATUS] = FLASH_STATUS_ERROR;
            $this->data[FLASH_MESSAGE] = $this->lang->line('MSG_RESET_PASSWORD_MISSING');
        } else {
            $reset_status = 1;
            $email = $this->db->escape_str(trim(base64_decode($e_token)));
            $token = $this->db->escape_str(trim($token));
            $select_response = $this->customer_model->check_token($email, $token);
            #$select_response = $this->customer_model->check_token(array('email' => $email, 'pwd_token' => $token));
            if (count($select_response)) {
                /*$crud_token = array('pwd_token' => $token);
                $where = array('customer_id' => @$select_response->customer_id);
                $update_response = $this->dbcommon->update('customers', $where, $crud_token);*/
                #_set_flashdata(FLASH_STATUS_SUCCESS, $this->lang->line('MSG_RESET_PASSWORD_SUCCESS'), FLASH_HTML);
                # redirect(base_url(MENU_LOG_IN));exit;
            } else {
                //_set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_RESET_PASSWORD_SUCCESS'), FLASH_HTML);
                $this->data[FLASH_STATUS] = FLASH_STATUS_ERROR;
                $this->data[FLASH_MESSAGE] = $this->lang->line('MSG_RESET_PASSWORD_INVALID');
            }

        }
        $this->data['token'] = $token;
        $this->data['e_token'] = $e_token;
        $this->data['page_title'] = $this->lang->line('MSG_RP_TITLE');
        $this->data['reset_status'] = $reset_status;
        $this->render(USER_D . RESET_PASSWORD_V);
    }

    public function reset_password_update()
    {
        if (@$this->is_login) {
            redirect(base_url());
        }
        if (count($this->input->post()) > 0) {
            $this->form_validation->set_rules('new_password', 'New Password', 'trim|required');
            $this->form_validation->set_rules('repeat_password', 'Repeat Password', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                $form_errors = _array_to_obj($this->form_validation->error_array());
                _set_flashdata(FLASH_STATUS_ERROR, $form_errors, FLASH_HTML);
            } else {
                $new_password = md5(trim($this->input->post('new_password')));

                $token = trim($this->input->post('token'));
                $e_token = $this->input->post('e_token');
                $email = $this->db->escape_str(trim(base64_decode($e_token)));
                $token = $this->db->escape_str(trim($token));
                $select_response = $this->customer_model->check_token($email, $token);
                #_pre($select_response);
                if (count($select_response)) {
                    if ($select_response->status == STATUS_CUSTOMER_ACTIVE) {
                        $crud_token = array('pwd_token' => '', 'password' => $new_password);
                        $where = array('customer_id' => @$select_response->customer_id);
                        $update_response = $this->dbcommon->update('customers', $where, $crud_token);
                        $to = $email;
                        if (MAIL_TEST_MODE == MAIL_TEST_MODE_LIVE) {
                            $to = MAIL_TEST_EMAIL;
                        } else {

                        }
                        if ($update_response) {
                            /*$user_name = $select_response->username;
                            $mail_content_data = new stdClass();
                            $mail_content_data->var_name = $user_name;
                            $mail_content_data->var_email = $email;
                            $mail_data = email_template('MAIL_RESET_PASSWORD', $mail_content_data);

                            if (is_object($mail_data)) {
                                $this->load->library('mail');
                                $this->mail->setTo($to, $user_name);
                                $this->mail->setSubject($mail_data->email_subject);
                                $this->mail->setBody(_header_footer($mail_data->email_html));
                                $status = $this->mail->send();
                            }*/
                            _set_flashdata(FLASH_STATUS_SUCCESS, $this->lang->line('MSG_RESET_PASSWORD_SUCCESS'), FLASH_HTML);
                        } else {
                            _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_RESET_PASSWORD_INACTIVE'), FLASH_HTML);
                        }
                    } else {
                        _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_RESET_PASSWORD_INACTIVE'), FLASH_HTML);
                    }
                } else {
                    _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_RESET_PASSWORD_INVALID'), FLASH_HTML);
                }
            }
        }
        redirect(base_url(MENU_LOG_IN));
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */