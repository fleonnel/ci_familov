<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*  * CodeIgniter ACL Class
 * @author Backend Brains
 *  Vikrant : 8000255245
 *  Vishal : 9033966266
 */
class Acl_model extends CI_Model {

    protected $table = 'access';
    protected $primary_key = 'access_id';
	private $is_super_admin = false;
	private $acl = [];
    private $allowResource = [
        'dashboard' => ['*'],
        'admin' => ['change_password']
    ];
    private $access_data = null;
    
    // constructor code

	public function __construct() {
        parent::__construct();
    }

    public function getAccess($roles_id)
    {

        if ($roles_id == 1) {
            $this->is_super_admin = true;
        } else {
            $this->db->select('*');
            $this->db->from($this->table);
            $this->db->where('roles_id =' . $roles_id);
            $response_data = $this->db->get()->row();
            $this->acl = json_decode(@$response_data->access_data);
        }
    }

    /**
     * Check access permission for seller staff
     * @param string $controller
     * @param string $action
     * @return boolean
     */
    public function isAllowed($controller, $action = 'index')
    {
        $allowed = FALSE;
        if (isset($this->allowResource[$controller])) {
            $allowed = TRUE;
            if (!in_array('*', $this->allowResource[$controller])) {
                if (!in_array($action, $this->allowResource[$controller])) {
                    $allowed = FALSE;
                }
            }
        }
        $accessResourceMap = $this->accessResourceMap();
        if (isset($this->acl->$controller) && count($this->acl->$controller) > 0 && isset($accessResourceMap[$controller])) {
            foreach ($this->acl->$controller as $accessName) {
                if (in_array($action, $accessResourceMap[$controller]['actions'][$accessName][1])) {
                    $allowed = TRUE;
                }
            }
        }

        if ($this->is_super_admin) {
            $allowed = TRUE;
        }

        return $allowed;
    }

    /**
     * Access Resource Map
     * @return array
     */
    public function accessResourceMap()
    {
        $accessResourceMap = [
            'admin' => [
                'title' => 'Admin',
                'icon' => 'fa fa-user-secret text-red',
                'actions' => [
                    'view' => ['View', ['lists', 'data_list']],
                    'add' => ['Add', ['add']],
                    'edit' => ['Edit', ['update', 'change_password']],
                    'delete' => ['Delete', ['action_delete']]
                ]
            ],
            'country' => [
                'title' => 'Country',
                'icon' => 'fa fa-globe text-green',
                'actions' => [
                    'view' => ['View', ['lists', 'data_list']],
                    'add' => ['Add', ['add']],
                    'edit' => ['Edit', ['update']],
                    'delete' => ['Delete', ['action_delete']]
                ]
            ],
            'city' => [
                'title' => 'City',
                'icon' => 'fa fa-map-marker text-green',
                'actions' => [
                    'view' => ['View', ['lists', 'data_list']],
                    'add' => ['Add', ['add']],
                    'edit' => ['Edit', ['update']],
                    'delete' => ['Delete', ['action_delete']]
                ]
            ],

            'shop' => [
                'title' => 'Shop',
                'icon' => 'fa fa-building-o text-green',
                'actions' => [
                    'view' => ['View', ['lists', 'data_list']],
                    'add' => ['Add', ['add']],
                    'edit' => ['Edit', ['update']],
                    'delete' => ['Delete', ['action_delete']]
                ]
            ],
            'categories' => [
                'title' => 'Categoriest',
                'icon' => 'fa fa-bars text-green',
                'actions' => [
                    'view' => ['View', ['lists', 'data_list']],
                    'add' => ['Add', ['add']],
                    'edit' => ['Edit', ['update']],
                    'delete' => ['Delete', ['action_delete']]
                ]
            ],
            'product' => [
                'title' => 'Product',
                'icon' => 'fa fa-database text-green',
                'actions' => [
                    'view' => ['View', ['lists', 'data_list']],
                    'add' => ['Add', ['add']],
                    'edit' => ['Edit', ['update', 'change_availability']],
                    'delete' => ['Delete', ['action_delete']]
                ]
            ],
            'customer' => [
                'title' => 'Customer',
                'icon' => 'fa fa-users text-green',
                'actions' => [
                    'view' => ['View', ['lists', 'data_list']],
                    'add' => ['Add', ['add']],
                    'edit' => ['Edit', ['update']],
                    'delete' => ['Delete', ['action_delete']]
                ]
            ],
            'order' => [
                'title' => 'Order',
                'icon' => 'fa fa-shopping-cart text-green',
                'actions' => [
                    'view' => ['View', ['lists', 'data_list','view_order','view_order_manager','upload_billing']],
                    'add' => ['Add', ['add']],
                    'edit' => ['Edit', ['update','change_status']],
                    'delete' => ['Delete', ['action_delete']],
                    //'view_order' => ['View Order', ['detail_view']],
                ]
            ],
            'promotion' => [
                'title' => 'Promotion',
                'icon' => 'fa fa-bullhorn text-green',
                'actions' => [
                    'view' => ['View', ['lists', 'data_list']],
                    'add' => ['Add', ['add']],
                    'edit' => ['Edit', ['update']],
                    'delete' => ['Delete', ['action_delete']]
                ]
            ],
            'currency' => [
                'title' => 'Currency',
                'icon' => 'fa fa-euro text-green',
                'actions' => [
                    'view' => ['View', ['lists', 'data_list']],
                    'add' => ['Add', ['add']],
                    'edit' => ['Edit', ['update']],
                    'delete' => ['Delete', ['action_delete']]
                ]
            ],
            'label' => [
                'title' => 'Label',
                'icon' => 'fa fa-language text-green',
                'actions' => [
                    'view' => ['View', ['lists', 'data_list']],
                    'add' => ['Add', ['add']],
                    'edit' => ['Edit', ['update']],
                    'delete' => ['Delete', ['action_delete']]
                ]
            ],
            'banner' => [
                'title' => 'Label',
                'icon' => 'fa fa-image text-green',
                'actions' => [
                    'view' => ['View', ['lists', 'data_list']],
                    'add' => ['Add', ['add']],
                    'edit' => ['Edit', ['update']],
                    'delete' => ['Delete', ['action_delete']]
                ]
            ],
            'email_template' => [
                'title' => 'Email Template Management',
                'icon' => 'fa fa-envelope-o text-yellow',
                'actions' => [
                    'view' => ['View', ['lists', 'data_list']],
                    'add' => ['Add', ['add_update']],
                    'edit' => ['Edit', ['add_update']],
                    'delete' => ['Delete', ['action_delete']]
                ]
            ],
            // 'setting' => [
            //     'title' => 'Setting',
            //     'icon' => 'fa fa-cog text-grey',
            //     'actions' => [
            //         'view' => ['View', ['lists']],
            //         'add' => ['Add', ['lists']],
            //         'edit' => ['Edit', ['lists']],
            //         'delete' => ['Delete', ['action_delete']]
            //     ]
            // ],
            // 'permission' => [
            //     'title' => 'Permission Management',
            //     'icon' => 'fa fa-unlock-alt text-red',
            //     'actions' => [
            //         'view' => ['View', ['lists', 'data_list']],
            //         'add' => ['Add', ['add_update']],
            //         'edit' => ['Edit', ['add_update']],
            //         'delete' => ['Delete', ['action_delete']]
            //     ]
            // ]
        ];
        return $accessResourceMap;
    }

    /**
     * Check access permission for seller staff
     * @param string $controller
     * @param string $action
     * @return boolean
     */
    public function isAccess($controller, $action = 'view') {
        $allowed = FALSE;
        if (isset($this->allowResource[$controller])) {
            $allowed = TRUE;
            if (!in_array('*', $this->allowResource[$controller])) {
                if (!in_array($action, $this->allowResource[$controller])) {
                    $allowed = FALSE;
                }
            }
        }
        $accessResourceMap = $this->accessResourceMap();
        if (isset($this->acl->$controller) && count($this->acl->$controller) > 0 && isset($accessResourceMap[$controller])) {
            if (isset($accessResourceMap[$controller]['actions'][$action])) {
                $allowed = TRUE;
            }
        }

        if ($this->is_super_admin) {
                $allowed = TRUE;
        }
        
        return $allowed;
    }
}
// END Acl_model class
