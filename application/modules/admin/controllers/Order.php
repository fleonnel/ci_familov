<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Order extends Admin_Controller
{
    var $table = 'order';
    var $primary_key = 'order_id';
    var $dir = '';

    public function __construct()
    {
        parent::__construct();
        $this->dir = ORDER; #TODO : Change Directory Name
        $this->load->library('form_validation');
        $this->load->model("order_model");
        $this->load->model("shop_model");
        $this->load->model("admin_model");
        $this->load->model("customer_model");

        /*require_once APPPATH . "third_party/messageapi/autoload.php";
        $sms_content = 'Hello Vix , your Order was successfully updated. Thank you for using our Service.-Familov';
        $sms_destinataire = '+918000255245';
        $MessageBird = new \MessageBird\Client('live_oZ0VmyK80wtEcg8BNJvdq6fdz'); // Set your own API access key here.
        $Message = new \MessageBird\Objects\Message();
        $Message->originator = 'familov.com';
        $Message->recipients = array($sms_destinataire);
        $Message->body = $sms_content;

        try {
            $MessageResult = $MessageBird->messages->create($Message);
            #var_dump($MessageResult);
            _pre($MessageResult);

        } catch (\MessageBird\Exceptions\AuthenticateException $e) {
            // That means that your accessKey is unknown
            echo 'wrong login';

        } catch (\MessageBird\Exceptions\BalanceException $e) {
            // That means that you are out of credits, so do something about it.
            echo 'no balance';

        } catch (\Exception $e) {
            echo $e->getMessage();
        }
        _e();*/

    }

    public function index()
    {
        redirect(admin_url(ORDER . 'lists'));
    }

    /**
     * This function is used for redirect on List Page
     * @return void
     */
    public function lists()
    {
        $this->data['page_title'] = 'Order List';
        $this->data['level_one'] = 'Order Management';
        $this->data['level_two'] = 'Order List';

        $employee_id = $this->admin_id;
        $this->data['customer_data'] = $this->admin_model->get_info_admin_by_id($employee_id);
        $shop_id = $this->data['customer_data']->shop_id;

        $total_order_in_process = 0;
        $total_order_buy = 0;
        if($shop_id != "")
        {
            if($this->admin_id == SUPER_ADMIN_ROLE_ID || $this->admin_id == ADMIN_ROLE_ID){}else{ $total_order_in_process = $this->order_model->count_order(ORDER_STATUS_IN_PROCESS,$shop_id); $total_order_buy = $this->order_model->count_order(ORDER_STATUS_BUY,$shop_id); }
        }

        $this->data['total_order_in_process'] = $total_order_in_process;
        $this->data['total_order_buy'] = $total_order_buy;

        $this->breadcrumbs->push('Order List', 'admin/order/lists');
        $this->render($this->dir . LIST_ORDER); #TODO : Change View File Name
    }

    /**
     * This function is fetch Data and set them into datatable.
     * @return array (json)
     */
    public function data_list()
    {
        //#todo model name change at 3 places, set $p_key as per table
        $shop_id = '';
        $employee_id = $this->admin_id;
        $customer_data = $this->admin_model->get_info_admin_by_id($employee_id);
        $shop_id = $customer_data->shop_id;
        $data = array();
        // if($shop_id != "")
        // {
            $list = $this->order_model->get_list($shop_id, $this->roles_id);
            // _pre($this->input->post());
            // _pre($list);
            
            $no = $_POST['start'];
            foreach ($list as $list_key => $list_value) {
                $no++;
                $p_key = $list_value->order_id;
                $view_base_url = base_url(ROOT_DIR_ADMIN . ORDER . "view_order");
                $view_url = $view_base_url . '/' . $p_key;
                $currency_code = $list_value->payment_curr_code;
               //_pre($tr_color);
                #$style = 'style="background:'.$tr_color.' !important"' ;
                $row = array();
                if($list_value->order_status == ORDER_STATUS_IN_PROCESS || $list_value->order_status == ORDER_STATUS_BUY)
                {
                    $tr_color =  total_days($list_value->date_time,$list_value->order_id);
                    $row[] = '<span class='.$tr_color.'>'.$no.'</span>';
                }
                else
                    $row[] = $no;

                $row[] = ucfirst(trim($list_value->generate_code));
                $row[] = stripslashes(ucfirst(trim($list_value->shop_name)));
                $row[] = ucfirst(trim($list_value->username)) . ' '.ucfirst(trim($list_value->lastname));
                $row[] = ucfirst(trim($list_value->receiver_name));
                // $update_url = admin_url("order/receiver_phone") . '/' . $p_key; #TODO : Change Controller Name
                
                $btn = '<div class="btn-group"><a href="'.A_TAG_DISABLE.'" class="btn btn-light btn-xs" onclick="editRecipientPhone('.$p_key.')"><i class="fa fa-edit"></i></a></div>';
                   
                if($this->roles_id == SUPER_ADMIN_ROLE_ID || $this->roles_id == ADMIN_ROLE_ID) {
                    $row[] = ucfirst(trim($list_value->receiver_phone)).' '.$btn;
                } else {
                    $row[] = ucfirst(trim($list_value->receiver_phone));
                }
                
                if($this->roles_id != 3){
                    $row[] = $currency_code .''.($list_value->grand_total);
                }
                if($this->roles_id == SUPER_ADMIN_ROLE_ID || $this->roles_id == ADMIN_ROLE_ID) {
                    $row[] = ucfirst(trim($list_value->payment_method));
                }else{
                    
                }
                
                $row[] = date_time($list_value->date_time, 1);
              
                if ($list_value->delivery_option == 'Home Delivery' || $list_value->delivery_option == 'Home delivery ') {
                    $delivery_option = '<img src="'.base_url().'/assets/admin/dist/img/home_delivery_admin.png " width="30" height="30"  />';
                } else if ($list_value->delivery_option == 'Pick up from the shop ') {
                    $delivery_option = '<img src="'.base_url().'/assets/admin/dist/img/Shop_delivery_admin.png " width="30" height="30"  />';
                } else {
                    $delivery_option = ucfirst(trim($list_value->delivery_option));
                }
                $row[] = $delivery_option;
                if ($this->roles_id == SUPER_ADMIN_ROLE_ID ) {
                $row[] = ($list_value->order_billing_price != '') ? $currency_code .''. ($list_value->grand_total - ($list_value->order_billing_price + $list_value->service_tax)) : '-';
                }
                 //$row[] = $currency_code .''. ($list_value->grand_total - ($list_value->order_billing_price + $list_value->service_tax));
                $row[] = "<center>" . view_button($p_key) . "</center>";
                $status = '';//date_time($v->date_time, 7)

                if ($list_value->order_status == ORDER_STATUS_WAIT) {
                    $status .= status_tag(ucfirst(trim($list_value->order_status)), COLOR_W);
                } else if ($list_value->order_status == ORDER_STATUS_IN_PROCESS) {
                    $status .= status_tag(ucfirst(trim($list_value->order_status)), COLOR_P);
                } else if ($list_value->order_status == ORDER_STATUS_DELIVERED) {
                    $status .= status_tag(ucfirst(trim($list_value->order_status)), COLOR_S);
                } else if ($list_value->order_status == ORDER_STATUS_BUY) {
                    $status .= status_tag(ucfirst(trim($list_value->order_status)), COLOR_I);
                } else if ($list_value->order_status == ORDER_STATUS_CANCEL) {
                    $status .= status_tag(ucfirst(trim($list_value->order_status)), COLOR_E);
                }
                $row[] = "<center>" . $status;
                if ($this->roles_id == SUPER_ADMIN_ROLE_ID || $this->roles_id == ADMIN_ROLE_ID) {
                    #$row[] = change_button($p_key, ucfirst(trim($list_value->generate_code)), ucfirst(trim($list_value->order_status))) . "</center>";
                    $row[] = '<span id="o_' . $p_key . '" data-number="' . ucfirst(trim($list_value->generate_code)) . '">' . change_button($p_key, ucfirst(trim($list_value->generate_code)), ucfirst(trim($list_value->order_status))) . '</span>';
                } else {
                    $row[] = change_button_for_manager($p_key, ucfirst(trim($list_value->generate_code)), ucfirst(trim($list_value->order_status))) . "</center>";
                }
                $data[] = $row;
            }           
        // }
         $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->order_model->count_all($shop_id, $this->roles_id),
                "recordsFiltered" => $this->order_model->count_filtered($shop_id, $this->roles_id),
                "data" => $data,
            );
            //output to json format
            echo json_encode($output);
        
    }
    public function view_order()
    {
        $order_id = $this->db->escape_str(trim($this->input->post('id')));
        $order_title = 'Order Detail ';
        if ($order_id != '') {
            $order_response = $this->order_model->get_order_by_id('', $order_id);
            //_pre($order_response);
            $order_detail_array = array();
            if (count($order_response) > 0) {
                $order_detail_array['order_info'] = $order_response;
                $order_detail_response = $this->order_model->get_order_detail_by_id('', $order_id);
                #_pre($order_detail_response);
                if (count($order_detail_response) > 0) {
                    $order_detail_array['order_detail'] = $order_detail_response;
                    $order_detail_array['shop'] = $this->shop_model->get_shop_name($order_response->shop_id);
                    #$select_response = $this->customer_model->customer_info($order_response->customer_id);
                    $select_response = $this->customer_model->get_customer_name($order_response->customer_id);
                    unset($select_response->password);
                    $order_detail_array['customer'] = $select_response;
                    $order_detail = $this->detail_view($order_detail_array);
                    $order_detail_array['html'] = $order_detail;
                    #_pre($order_detail_array);
                    $html = $this->load->view('order/_order_detail', $order_detail_array, true);
                    $order_title = 'Order Detail ' . _set_dash(@$order_response->generate_code);
                    echo json_encode(array(FLASH_STATUS => FLASH_STATUS_SUCCESS, FLASH_MESSAGE => $this->lang->line('MSG_VO_05'), 'html' => $html, 'order_title' => $order_title));
                    exit;
                } else {
                    $html = '';
                    echo json_encode(array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => $this->lang->line('MSG_VO_06'), 'order_title' => $order_title));
                    exit;
                }
            } else {
                echo json_encode(array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => $this->lang->line('MSG_VO_06'), 'order_title' => $order_title));
                exit;
            }
        } else {
            $res_array = array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => MESSAGE_INFORMATION_NOT_AVAILABLE, 'html' => '', 'order_title' => $order_title);
        }
        echo json_encode($res_array);
        exit;
    }

    public function detail_view($order_detail_array)
    {
        $order_info = $order_detail_array['order_info'];
        $order_detail = $order_detail_array['order_detail'];
        $html = '';
        $html .= '<div class="col-md-12"><table class="">
                        <thead>
                        <tr>
                            <!--<th class="text-center">#</th>-->

                            <th><strong>' . $this->lang->line('MSG_PRODUCT_CART_06') . '</strong></th>
                            <th class="text-right"><strong>' . $this->lang->line('MSG_PRODUCT_CART_07') . '</strong></th>
                            <th><strong></strong></th>
                            <th class="text-center"><strong>' . $this->lang->line('MSG_PRODUCT_CART_08') . '</strong>
                            </th>
                            <th class="text-right"><strong>' . $this->lang->line('MSG_PRODUCT_CART_09') . '</strong></th>
                        </tr>
                        </thead>
                        <tbody>';

        $ctr = 0;
        $cart_total_item_qty = 0;
        $cart_amount_total = 0.0;
        $currency_code = @$order_info->payment_curr_code;
        /*<td><div
                                    style="background-image:url(' . (DIR_PRODUCT_URL . $v->product_image) . '); background-size: cover; height:100px; width:100px;"></div>
</td>*/
        foreach (@$order_detail as $k => $v) {
            $ctr++;
            $cart_total_item_qty += @$v->quantity;
            $cart_amount_total = $cart_amount_total + @$v->total_prize;
            $html .= '
            <tr>
                    <!--<td class="text-center">' . $ctr . '</td>-->

                    <td>' . @$v->product_name . '</td>
                    <td class="text-right">' . @$currency_code . @$v->product_prize . '</td>
                    <td class="text-center">X</td>
                    <td class="text-center">' . @$v->quantity . '</td>
                    <td class="text-right">' . @$currency_code . @$v->total_prize . '</td>
            </tr>
                ';
        }
        $html .= '</table>';
        #sub total
        $html .= '<div class="col-md-4 col-md-offset-8"><table id="totals"><tr>
                            <td ><strong>' . $this->lang->line('MSG_PRODUCT_CART_10') . '</strong></td>
                            <td class="text-center"><b></b></td>
                            <td class="text-right"
                                style="color:#36cd75 ;font-weight:bold;">' . @$currency_code . _number_format($cart_amount_total) . '</td>
                            <td></td>
                        </tr>';
        #Service Fees
        $html .= '<tr>
                            <td ><strong>' . $this->lang->line('MSG_PRODUCT_CART_11') . '</strong></td>
                            <td class="text-center"><b></b></td>
                            <td class="text-right">' . @$currency_code . _number_format(@$order_info->service_tax) . '</td>
                            <td></td>
                        </tr>';
        #Delivery Type section
        $html .= '<tr>
                            <td><strong>' . @$order_info->delivery_option . '</strong></td>
                            <td class="text-center"><b></b></td>
                            <td class="text-right">' . @$currency_code . _number_format(@$order_info->delivery_charges) . '</td>
                            <td></td>
                        </tr>';
        #Discount section
        if (@$order_info->promo_code != '') {
            $html .= '<tr>
                            <td><strong>' . $this->lang->line('MSG_PRODUCT_CHECKOUT_10') . "(" . @$order_info->promo_code . ")" . '</strong></td>
                            <td class="text-center"><b></b></td>
                            <td class="text-right">' . @$currency_code . _number_format(@$order_info->promo_discount_amount) . '</td>
                            <td></td>
                        </tr>';
        }
        #Grand Total section
        $html .= '<tr>
                            <td><strong>' . $this->lang->line('MSG_PRODUCT_CART_16') . '</strong></td>
                            <td class="text-center"><b></b></td>
                            <td class="text-right"><strong id="grandprice" style="color:#000;font-size:20px">' . @$currency_code . _number_format(@$order_info->grand_total) . '</strong></td>
                            <td></td>
                        </tr>';


        if (@$order_info->upload_billing != '') {
            $html .= '<tr>
                            <td class="text-right"> <img src="' . DIR_ORDER_BILL_URL . @$order_info->upload_billing . '" width="200" height="200" onclick="window.open(this.src); return false;"/></td>
                        </tr>';
        }

        $html .= '</tbody>
                    </table></div>';
        if ($this->roles_id == SUPER_ADMIN_ROLE_ID) {
            $html .= '<tr>
                           <!-- <td class="text-right"> <div class="form-group">
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="order_bill_price"  name="order_bill_price" placeholder="Enter Price" value="' . @$order_info->order_billing_price . '"  required>
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-success " onclick="savedata(' . @$order_info->order_id . ')">Save</button>
                                    </div>-->
                                </div></td>
                        </tr>';
        }


        return $html;
    }

    public function view_order_manager()
    {
        $order_id = $this->db->escape_str(trim($this->input->post('id')));
        $order_title = 'Order Detail ';
        if ($order_id != '') {
            $order_response = $this->order_model->get_order_by_id('', $order_id);
            //_pre($order_response);
            $order_detail_array = array();
            if (count($order_response) > 0) {
                $order_detail_array['order_info'] = $order_response;
                $order_detail_response = $this->order_model->get_order_detail_by_id('', $order_id);
                #_pre($order_detail_response);
                if (count($order_detail_response) > 0) {
                    $order_detail_array['order_detail'] = $order_detail_response;
                    $order_detail_array['shop'] = $this->shop_model->get_shop_name($order_response->shop_id);
                    #$select_response = $this->customer_model->customer_info($order_response->customer_id);
                    $select_response = $this->customer_model->get_customer_name($order_response->customer_id);
                    unset($select_response->password);
                    $order_detail_array['customer'] = $select_response;
                    $order_detail = $this->detail_view_manager($order_detail_array);
                    $order_detail_array['html'] = $order_detail;
                   // $order_detail_array['order_info'] = $order_info;
                   // $order_detail_array['order_detail'] = $order_detail_info;
                    // $order_info = $order_detail_array['order_info'];
                    // $order_detail = $order_detail_array['order_detail'];
                    #_pre($order_detail_array);
                    $html = $this->load->view('order/_order_detail', $order_detail_array, true);
                    $order_title = 'Order Detail ' . _set_dash(@$order_response->generate_code);
                    echo json_encode(array(FLASH_STATUS => FLASH_STATUS_SUCCESS, FLASH_MESSAGE => $this->lang->line('MSG_VO_05'), 'html' => $html, 'order_title' => $order_title));
                    exit;
                } else {
                    $html = '';
                    echo json_encode(array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => $this->lang->line('MSG_VO_06'), 'order_title' => $order_title));
                    exit;
                }
            } else {
                echo json_encode(array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => $this->lang->line('MSG_VO_06'), 'order_title' => $order_title));
                exit;
            }
        } else {
            $res_array = array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => MESSAGE_INFORMATION_NOT_AVAILABLE, 'html' => '', 'order_title' => $order_title);
        }
        echo json_encode($res_array);
        exit;
    }

    public function detail_view_manager($order_detail_array)
    {
        $order_info = $order_detail_array['order_info'];
        $order_detail = $order_detail_array['order_detail'];
        $html = '';
        /*<th><strong>' . $this->lang->line('MSG_PRODUCT_CART_05') . '</strong></th>*/
        $html .= '<div class="col-md-12"><table class="">
                        <thead>
                        <tr>
                           <!-- <th class="text-center">#</th>-->

                            <th><strong>' . $this->lang->line('MSG_PRODUCT_CART_06') . '</strong></th>

                            <th><strong></strong></th>
                            <th class="text-center"><strong>' . $this->lang->line('MSG_PRODUCT_CART_08') . '</strong>
                            </th>

                        </tr>
                        </thead>
                        <tbody>';
        $ctr = 0;
        $cart_total_item_qty = 0;
        $cart_amount_total = 0.0;
        $currency_code = @$order_info->payment_curr_code;
        /*<td><div
                                    style="background-image:url(' . (DIR_PRODUCT_URL . $v->product_image) . '); background-size: cover; height:100px; width:100px;"></div>
</td>*/
        foreach (@$order_detail as $k => $v) {
            $ctr++;
            $cart_total_item_qty += @$v->quantity;
            $cart_amount_total = $cart_amount_total + @$v->total_prize;
            $html .= '
            <tr>
                    <!--<td class="text-center">' . $ctr . '</td>-->

                    <td>' . @$v->product_name . '</td>
                    <td class="text-center">-----</td>
                    <td class="text-center">' . @$v->quantity . ' x</td>
            </tr>

                ';
        }
        #sub total


        $html .= '</tbody>
                    </table></div></div>';

        return $html;
    }

public function detail_view_manager_backup($order_detail_array)
{
        $order_info = $order_detail_array['order_info'];
        $order_detail = $order_detail_array['order_detail'];
        $html = '';
        $html .= '<div class="col-md-12"><table class="">
                    <tr>
                      <td class="text-left" width="20%">' . $this->lang->line('MSG_VO_08') . '</td>
                      <td class="text-left" width="80%">' . _set_dash(@$order_info->receiver_name) . '</td>
                    </tr>
                      <tr>
                          <td class="text-left" width="20%">' . $this->lang->line('MSG_VO_09') . '</td>
                          <td class="text-left" width="80%">' . _set_dash(@$order_info->receiver_phone) . '</td>
                      </tr>
                      <tr>
                          <td class="text-left" width="20%">' . $this->lang->line('MSG_VO_10') . '</td>
                          <td class="text-left" width="80%">' . _set_dash(@$order_info->greeting_msg) . '</td>
                      </tr>
                      <tr>
                          <td class="text-left" width="20%">' . $this->lang->line('MSG_VO_11') . '</td>
                          <td class="text-left" width="80%">' . date_time(@$order_info->date_time, 7) . '</td>
                      </tr>
                      <tr>
                          <td class="text-left" width="20%">' . $this->lang->line('MSG_VO_12') . '</td>
                          <td class="text-left" width="80%">' . @$order_info->payment_method . '</td>
                      </tr>
                       <tr>
                          <td class="text-left" width="20%">' . $this->lang->line('MSG_VO_13') . '</td>
                          <td class="text-left" width="80%">' . @$order_info->order_status . '</td>
                      </tr>
                  </table></div>';
        /*<th><strong>' . $this->lang->line('MSG_PRODUCT_CART_05') . '</strong></th>*/
        $html .= '<div class="col-md-4 col-md-offset-8"><table id="totals">
                        <thead>
                        <tr>
                           <th class="text-center">#</th>

                            <th><strong>' . $this->lang->line('MSG_PRODUCT_CART_06') . '</strong></th>

                            <th><strong></strong></th>
                            <th class="text-center"><strong>' . $this->lang->line('MSG_PRODUCT_CART_08') . '</strong>
                            </th>

                        </tr>
                        </thead>
                        <tbody>';
                        $ctr = 0;
        $cart_total_item_qty = 0;
        $cart_amount_total = 0.0;
        $currency_code = @$order_info->payment_curr_code;
        /*<td><div
                                    style="background-image:url(' . (DIR_PRODUCT_URL . $v->product_image) . '); background-size: cover; height:100px; width:100px;"></div>
</td>*/
        foreach (@$order_detail as $k => $v) {
            $ctr++;
            $cart_total_item_qty += @$v->quantity;
            $cart_amount_total = $cart_amount_total + @$v->total_prize;
            $html .= '
            <tr>
                    <td class="text-center">' . $ctr . '</td>

                    <td>' . @$v->product_name . '</td>
                    <td class="text-center">X</td>
                    <td class="text-center">' . @$v->quantity . '</td>
            </tr>
                ';
        }
        #sub total


    $html .= '</tbody>
                    </table></div>';

        return $html;
}

    public function change_status()
    {
        $id = $this->db->escape_str(trim($this->input->post('id')));
        $status = $this->db->escape_str(trim($this->input->post('order_status')));
        $change_response=$this->order_model->change_status($id,$status);
        $temp_msg= '';
            //$delete_response = $this->dbcommon->delete($id);
            if ($change_response) {
                $data[] = $this->order_model->get_info_by_id($id);
                                $query_response_contact = $this->customer_model->get_customer_name($data[0]->customer_id);
                                $data['order'] = $this->order_model->get_info_by_id($id);
                                $shop_data = $this->shop_model->get_info_by_id(@$data['order']->shop_id);

                                if (count($query_response_contact) > 0 && ($status == ORDER_STATUS_DELIVERED || $status == ORDER_STATUS_BUY)) {
                                    #_pre($query_response_contact,0);
                                   # _pre($shop_data,0);
                                    #_pre($data['order']);
                                    $name = _ucwords($query_response_contact->username .' '.$query_response_contact->lastname);
                                    $user_name = _ucwords($query_response_contact->username);
                                    $to_email = $query_response_contact->email_address;
                                    // if (MAIL_TEST_MODE == MAIL_TEST_MODE_TEST) {
                                    //     $to_email = MAIL_TEST_EMAIL;
                                    // }
                                    $receiver_name = $data['order']->receiver_name;
                                    $generate_code = $data['order']->generate_code;
                                    $mail_content_data = new stdClass();
                                    $mail_content_data->var_user_name = $name;
                                    $mail_content_data->var_order_code = $generate_code;
                                    $mail_content_data->var_reciever_name = $receiver_name;
                                    $mail_content_data->var_email = $to_email;

                                    if($status ==ORDER_STATUS_DELIVERED){
                                    $mail_data = email_template('ADMIN_PACKET_DELIVERED_SUCCESSFULLY', $mail_content_data);
                                        require_once APPPATH . "third_party/messageapi/autoload.php";
                                        $sms_content = '';
                                        $sms_content = str_replace("#USER_NAME#",$user_name,SMS_MESSAGE_TEXT_FOR_DELIVERED);
                                        $sms_content = str_replace("#RECP_NAME#",$receiver_name,$sms_content);
                                        #_pre($sms_content);
                                        $sms_destinataire = $data['order']->receiver_phone;
                                        $MessageBird = new \MessageBird\Client(SMS_API_LIVE_KEY); // Set your own API access key here.
                                        $Message = new \MessageBird\Objects\Message();
                                        $Message->originator = STRIPE_SITE_NAME;//'familov.com';
                                        $Message->recipients = array($sms_destinataire);
                                        $Message->body = $sms_content;

                                        try {
                                            $MessageResult = $MessageBird->messages->create($Message);
                                            #var_dump($MessageResult);
                                            #_pre($MessageResult);
                                        } catch (\MessageBird\Exceptions\AuthenticateException $e) {
                                            // That means that your accessKey is unknown
                                            #echo 'wrong login';
                                            $temp_msg = '<br> SMS sending failed due to invalid SMS details.';

                                        } catch (\MessageBird\Exceptions\BalanceException $e) {
                                            // That means that you are out of credits, so do something about it.
                                            #echo 'no balance';
                                            $temp_msg = '<br> SMS sending failed due to SMS balance is over.';

                                        } catch (\Exception $e) {
                                            $temp_msg = '<br> '.$e->getMessage();
                                        }
                                    }
                                    else if($status ==ORDER_STATUS_BUY){
                                    $mail_data = email_template('ADMIN_PACKET_BUY_SUCCESSFULLY', $mail_content_data);
                                        require_once APPPATH . "third_party/messageapi/autoload.php";
                                        $shop_name = $shop_data->shop_name;
                                        $sms_content = '';
                                        $sms_content = str_replace("#RECP_NAME#",$receiver_name,SMS_MESSAGE_TEXT_FOR_BUY);
                                        $sms_content = str_replace("#USER_NAME#",$user_name,$sms_content);
                                        $sms_content = str_replace("#SHOP_NAME#",$shop_name,$sms_content);
                                        $sms_content = str_replace("#ORDER_CODE#",$generate_code,$sms_content);
                                        $sms_destinataire = $data['order']->receiver_phone;
                                        $MessageBird = new \MessageBird\Client(SMS_API_LIVE_KEY); // Set your own API access key here.
                                        $Message = new \MessageBird\Objects\Message();
                                        $Message->originator = STRIPE_SITE_NAME;//'familov.com';
                                        $Message->recipients = array($sms_destinataire);
                                        $Message->body = $sms_content;

                                        try {
                                            $MessageResult = $MessageBird->messages->create($Message);
                                            #var_dump($MessageResult);
                                           # _pre($MessageResult);
                                        } catch (\MessageBird\Exceptions\AuthenticateException $e) {
                                            // That means that your accessKey is unknown
                                            #echo 'wrong login';
                                            $temp_msg = '<br> SMS sending failed due to invalid SMS details.';

                                        } catch (\MessageBird\Exceptions\BalanceException $e) {
                                            // That means that you are out of credits, so do something about it.
                                            #echo 'no balance';
                                            $temp_msg = '<br> SMS sending failed due to SMS balance is over.';

                                        } catch (\Exception $e) {
                                            $temp_msg = '<br> '.$e->getMessage();
                                        }
                                    }

                                    if($status ==ORDER_STATUS_DELIVERED || $status ==ORDER_STATUS_BUY){
                                       /* require_once APPPATH . "third_party/messageapi/autoload.php";
                                        $sms_content = str_replace("#USER_NAME#",$data['order']->receiver_name,SMS_MESSAGE_TEXT);
                                        $sms_content = str_replace("#CODE#",$generate_code,$sms_content);
                                        $sms_destinataire = $data['order']->receiver_phone;
                                        $MessageBird = new \MessageBird\Client(SMS_API_LIVE_KEY); // Set your own API access key here.
                                        $Message = new \MessageBird\Objects\Message();
                                        $Message->originator = STRIPE_SITE_NAME;//'familov.com';
                                        $Message->recipients = array($sms_destinataire);
                                        $Message->body = $sms_content;

                                        try {
                                            $MessageResult = $MessageBird->messages->create($Message);
                                            #var_dump($MessageResult);
                                            #_pre($MessageResult);
                                        } catch (\MessageBird\Exceptions\AuthenticateException $e) {
                                            // That means that your accessKey is unknown
                                            #echo 'wrong login';
                                            $temp_msg = '<br> SMS sending failed due to invalid SMS details.';

                                        } catch (\MessageBird\Exceptions\BalanceException $e) {
                                            // That means that you are out of credits, so do something about it.
                                            #echo 'no balance';
                                            $temp_msg = '<br> SMS sending failed due to SMS balance is over.';

                                        } catch (\Exception $e) {
                                            $temp_msg = '<br> '.$e->getMessage();
                                        }*/
                                    }

                                    if (is_object($mail_data)) {
                                        $this->load->library('mail');
                                        $this->mail->setTo($to_email, $name);
                                        $this->mail->setSubject($mail_data->email_subject);
                                        $this->mail->setBody(_header_footer($mail_data->email_html));
                                        $status = $this->mail->send();
                                    }
                                }
                echo json_encode(array(FLASH_STATUS => FLASH_STATUS_SUCCESS, FLASH_MESSAGE => MESSAGE_CHANGE_STATUS_SUCCESS.@$temp_msg));
                exit;
            } else {
                echo json_encode(array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => MESSAGE_DELETE_ERROR));
                exit;
            }
    }
    public function upload_billing()
    {
        $id = $this->db->escape_str(trim($this->input->post('id')));
        $data['data'] = $this->order_model->get_info_by_id($id);
       // $status = $this->db->escape_str(trim($this->input->post('order_status')));
        //_pre($this->input->post());
         $success_url = admin_url(ORDER . 'lists');
        if($data['data']->upload_billing == ''){
            if (!empty($_FILES)) {
                            $files = $_FILES['upload_bill'];
                            $files = is_array($files) ? $files : array($files);
                            $new_file_name = 'order_bill_' . $id;
                            $path = DIR_ORDER_BILL_PATH;
                            $img_response = image_upload($files, 'upload_bill', $path, $new_file_name);
                            $image_name_actual = '';
                            if (!$img_response['status']) {
                                $error_msg = "Can not upload Image for " . $img_response['error'] . " ";
                                _set_flashdata(FLASH_STATUS_ERROR, $error_msg, FLASH_HTML);
                                redirect($error_url);
                            } else {
                                $image_name_actual = $img_response['upload_data']['file_name'];
                                $file_path_org = $img_response['upload_data']['file_path'];
                                $file_full_path = $img_response['upload_data']['full_path'];
                            }
                        } else {
                            $image_name_actual = NULL;
                        }
                        $crud_data = array(
                            'upload_billing' => $image_name_actual,
                            
                        );
                        $where = array($this->primary_key => $id);
                        $update_response = $this->dbcommon->update($this->table, $where, $crud_data);
                        if ($update_response) {
                            _set_flashdata(FLASH_STATUS_SUCCESS, MESSAGE_BILL_UPLOAD_SUCCESS);
                            redirect($success_url);
                        } else {
                            _set_flashdata(FLASH_STATUS_ERROR, MESSAGE_UPDATE_ERROR);
                            redirect($error_url);
                        }
            
        }
    else{
                    if (!empty($_FILES)) {
                                    if ($data['data']->upload_billing != '') {
                                        if (file_exists(DIR_ORDER_BILL_PATH.$data['data']->upload_billing)) {
                                            unlink(DIR_ORDER_BILL_PATH.$data['data']->upload_billing);
                                        }
                                    }
                                    //$this->product_model->update_files($files_url['file_name'],$id);
                            $files = $_FILES['upload_bill'];
                            $files = is_array($files) ? $files : array($files);
                            $new_file_name = 'order_bill_' . $id;
                            $path = DIR_ORDER_BILL_PATH;
                            $img_response = image_upload($files, 'upload_bill', $path, $new_file_name);
                            $image_name_actual = '';
                            if (!$img_response['status']) {
                                $error_msg = "Can not upload Image for " . $img_response['error'] . " ";
                                _set_flashdata(FLASH_STATUS_ERROR, $error_msg, FLASH_HTML);
                                redirect($error_url);
                            } else {
                                $image_name_actual = $img_response['upload_data']['file_name'];
                                $file_path_org = $img_response['upload_data']['file_path'];
                                $file_full_path = $img_response['upload_data']['full_path'];
                            }
                        } else {
                            $image_name_actual = NULL;
                        }
                        $crud_data = array(
                            'upload_billing' => $image_name_actual,
                            
                        );
                        $where = array($this->primary_key => $id);
                        $update_response = $this->dbcommon->update($this->table, $where, $crud_data);
                        if ($update_response) {
                            _set_flashdata(FLASH_STATUS_SUCCESS, MESSAGE_BILL_UPLOAD_SUCCESS);
                            redirect($success_url);
                        } else {
                            _set_flashdata(FLASH_STATUS_ERROR, MESSAGE_UPDATE_ERROR);
                            redirect($error_url);
                        }
            
                    }


    }
    public function remove($id)
    {
        $data['id'] = $this->order_model->get_info_by_id($id);
        if($data['id'] != ''){
        unlink(DIR_ORDER_BILL_PATH.$data['id']->upload_billing);
        return $this->order_model->update_bill($id);
         }
           // redirect(admin_url(NEWS_C . 'lists'));
    }
    public function savebilldata()
    {
        $id=$this->input->post('id');
        $crud_data = array(
            'order_billing_price' => $this->db->escape_str(trim($this->input->post('price'))),
        );
        $where = array($this->primary_key => $id);
        $update_response = $this->dbcommon->update($this->table, $where, $crud_data);
        echo json_encode(array(FLASH_STATUS => FLASH_STATUS_SUCCESS, FLASH_MESSAGE => MESSAGE_CHANGE_STATUS_SUCCESS));
        exit;
    }

    public function getOrderInfo($order_id)
    {
        $response = array();
        if($order_id != ""){
            $data = $this->order_model->get_info_by_id($order_id);
            if( count($data) == 1 ){
                $response["order_id"] = $data->order_id;
                $response["generate_code"] = $data->generate_code;
                $response["receiver_name"] = $data->receiver_name;
                $response["receiver_phone"] = $data->receiver_phone;
                $response["status"] = "true";
            } else {
                $response["status"] = "false";
            }
        } else {
            $response["status"] = "false";
        }
        echo json_encode($response);
    }

    public function update_Recipient_Phone() {
        if (count($this->input->post()) > 0) {
            $this->form_validation->set_rules('order_id', 'Order', 'trim|required');
            $this->form_validation->set_rules('receiver_phone', 'Recipient Phone', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                $form_errors = _array_to_obj($this->form_validation->error_array());
                // _set_flashdata(FLASH_STATUS_ERROR, $form_errors, FLASH_HTML);
                $response = array('status'=>false, 'message'=>$form_errors->receiver_phone);
                echo json_encode($response);
                die();
            }
            else {

                $crud_data = array(
                    'receiver_phone' => $this->input->post('receiver_phone'),
                );
                $where = array($this->primary_key => $this->input->post('order_id'));
                $update_response = $this->dbcommon->update($this->table, $where, $crud_data);
                if ($update_response) {
                    $response = array('status'=>true, 'message'=>'');
                    echo json_encode($response);
                    die();
                } else {
                    $response = array('status'=>false, 'message'=>'Sorry something went wrong with Recipient Phone update process. Please try after some time.');
                    echo json_encode($response);
                    die();
                }

            }

        }        

    }

}