<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends Admin_Controller
{
    var $table = 'admin';
    var $primary_key = 'admin_id';
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model("shop_model");
        #$this->load->library('session');
    }

    public function index()
    {
        redirect(ADMIN_C . 'lists');
    }

    /**
     * This function is used for redirect on Admin List Page
     * @return void
     */
    public function lists()
    {
        $this->data['page_title'] = 'Admin List';
        $this->data['level_one'] = 'Admin Management';
        $this->data['level_two'] = 'Admin List';
        $this->breadcrumbs->push('Admin List', ADMIN_C . 'lists');
        $this->render(ADMIN_D . LIST_ADMIN);
    }

    /**
     * This function is fetch Admin Data and set them into datatable.
     * @return array (json)
     */
    public function data_list()
    {
        //#todo model name change at 3 places, set $p_key, set $list_admin_status as per table
        $list = $this->admin_model->get_list();
        #_pre($list);
        $data = array();
        $no = $_POST['start'];
        $list_admin_status = $this->config->item('tbl_column_status_array')['admin_status'];
        foreach ($list as $list_key => $list_value) {
            $no++;
            $p_key = $list_value->admin_id;
            $status_view = '';
            $status_menu = '';
            $view_url = base_url(ROOT_DIR_ADMIN. ADMIN_C . "view_admin") . '/' . $p_key;
            $edit_url = base_url(ADMIN_C . "update") . '/' . $p_key;
            $edit_shop_url = base_url(ADMIN_C . "update_admin") . '/' . $p_key;
            $row = array();
            $row[] = $no;
            $row[] = ucfirst(trim($list_value->first_name . " " . $list_value->last_name));
            $row[] = $list_value->email;
            $row[] = $list_value->name;
            if (ADMIN_STATUS_ACTIVE == $list_value->admin_status) {
                $status_view = status_tag($list_admin_status[ADMIN_STATUS_ACTIVE],COLOR_S);
                $status_menu .= action_event(ADMIN_STATUS_BLOCK,COLOR_E,$p_key);
            } else if (ADMIN_STATUS_ACTIVE == $list_value->admin_status) {
                $status_view = status_tag($list_admin_status[ADMIN_STATUS_PENDING],COLOR_W);
                $status_menu .= action_event(ADMIN_STATUS_ACTIVE,COLOR_S,$p_key);
                $status_menu .= action_event(ADMIN_STATUS_BLOCK,COLOR_E,$p_key);
            } else {
                $status_view = status_tag($list_admin_status[ADMIN_STATUS_BLOCK],COLOR_E);
                $status_menu .= action_event(ADMIN_STATUS_ACTIVE,COLOR_S,$p_key);
            }
            $row[] = $status_view; # . ' ' . delete_button($p_key, DELETE_USER)
            $shop_btn = '';
            if ($list_value->roles_id == '3') {
            $shop_btn = shop_button($edit_shop_url); 
            }
            $row[] = ($list_value->roles_id != 1) ? ($this->acl_model->isAccess($this->class, $this->method)) ? edit_button($edit_url) . ' ' . action_button($status_menu) . ' ' . $shop_btn : status_tag(N_A, COLOR_W) : status_tag(N_A, COLOR_W);
            
            $row[] = view_button($view_url);
            //}
            #$row[] = ($list_value->admin_type_id != 1) ? view_button($view_url).' '.edit_button($edit_url).' '.action_button($status_menu): status_tag(N_A,COLOR_W);
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->admin_model->count_all(),
            "recordsFiltered" => $this->admin_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    /**
     * This function is update admin status.
     * @return boolean
     */
    public function update_status()
    {
        $data = array(
            'admin_status' => $this->db->escape_str(trim($this->input->post('status'))),
        );
        $this->admin_model->update_status(array('admin_id' => $this->db->escape_str(trim($this->input->post('id')))), $data);
        echo json_encode(array(FLASH_STATUS => FLASH_STATUS_SUCCESS, FLASH_MESSAGE => MESSAGE_UPDATE_SUCCESS));
    }

    /**
     * This function is Delete action.
     * @return boolean
     */
    public function action_delete()
    {
        $id = $this->db->escape_str(trim($this->input->post('id')));
        $query_response = $this->admin_model->check_dependency_request($id);
        if ($query_response > 0) {
            echo json_encode(array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => MESSAGE_DELETE_ERROR_LINK));
            exit;
        } else {
            $crud_data = array(
                'admin_is_deleted' => 'Yes'
            );
            $where = array("admin_id" => $id);
            $update_response = $this->dbcommon->update('admin', $where, $crud_data);
            if ($update_response) {
                echo json_encode(array(FLASH_STATUS => FLASH_STATUS_SUCCESS, FLASH_MESSAGE => MESSAGE_DELETE_SUCCESS));
                exit;
            } else {
                echo json_encode(array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => MESSAGE_DELETE_ERROR));
                exit;
            }

        }
    }

    /**
     * This function is Add Admin Data.
     * @return void
     */
    public function add()
    {
        $this->add_update();
        $this->render(ADMIN_D . CRUD_ADMIN);
    }

    private function add_update($id = '')
    {
        #$this->data['info_admin'] = $this->admin_model->get_info_admin_by_id();
        $this->form_validation->set_rules('first_name', 'First name', 'required|trim');
        $this->form_validation->set_rules('last_name', 'Last name', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        #$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[admin.email]');
        $this->form_validation->set_error_delimiters('', '');
        if ($id) {
            $title = 'Admin Update Section';
            $error_url = base_url(ROOT_DIR_ADMIN . ADMIN_C . "update");
        } else {
            $error_url = base_url(ROOT_DIR_ADMIN . ADMIN_C . "add");
            $title = 'Admin Add Section';
        }
        if (count($this->input->post()) > 0) {
            if ($this->form_validation->run() === FALSE) {
                if ($id != "" && is_numeric($id)) {
                    $this->data['info'] = $this->admin_model->get_info_admin_by_id($id);
                }
                $form_errors = _array_to_obj($this->form_validation->error_array());
                _set_flashdata(FLASH_STATUS_ERROR, $form_errors, FLASH_HTML);
            } else {
                $email = trim($this->input->post('email'));
                $name = trim($this->input->post('first_name')) . ' ' . trim($this->input->post('last_name'));
                $temp_array = explode("@", $email);
                $user_name = $temp_array[0];
                $new_password = _random_string(6);
                $crud_data = array(
                    'email' => $this->db->escape_str(trim($this->input->post('email'))),
                    'user_name' => $user_name,
                    'first_name' => $this->db->escape_str(trim($this->input->post('first_name'))),
                    'last_name' => $this->db->escape_str(trim($this->input->post('last_name'))),
                    'admin_type_id' => $this->db->escape_str(trim($this->input->post('admin_type_id'))),
                    'roles_id' => $this->db->escape_str(trim($this->input->post('admin_type_id'))),
                    'admin_status' => $this->db->escape_str(trim($this->input->post('admin_status'))),
                );
                # _pre($crud_data);
                $success_url = base_url(ROOT_DIR_ADMIN . ADMIN_C . 'lists');

                if ($id == '') {
                    $crud_data['password'] = md5($new_password);
                    $insert_response = $this->dbcommon->insert('admin', $crud_data);
                    $insert_id = $this->db->insert_id();
                    if ($insert_response) {
                        $mail_content_data = new stdClass();
                        $mail_content_data->var_greeting_name = $name;
                        $mail_content_data->var_email = $email;
                        $mail_content_data->var_user_name = $user_name;
                        $mail_content_data->var_password = $new_password;
                        $mail_data = email_template('ADMIN_REGISTRATION_NEW', $mail_content_data);
                        if (MAIL_TEST_MODE == MAIL_TEST_MODE_LIVE) {
                            $email = MAIL_TEST_EMAIL;
                        }
                        if (is_object($mail_data)) {
                            $this->load->library('mail');
                            $this->mail->setTo($email, $name);
                            $this->mail->setSubject($mail_data->email_subject);
                            $this->mail->setBody(_header_footer($mail_data->email_html));
                            $status = $this->mail->send();
                        }
                        _set_flashdata(FLASH_STATUS_SUCCESS, MESSAGE_ADD_SUCCESS);
                        redirect($success_url);
                    } else {
                        _set_flashdata(FLASH_STATUS_ERROR, MESSAGE_ADD_ERROR);
                        redirect($error_url);
                    }
                } else {
                    $where = array("admin_id" => $id);
                    $update_response = $this->dbcommon->update('admin', $where, $crud_data);
                    if ($update_response) {
                        _set_flashdata(FLASH_STATUS_SUCCESS, MESSAGE_UPDATE_SUCCESS);
                        redirect($success_url);
                    } else {
                        _set_flashdata(FLASH_STATUS_ERROR, MESSAGE_UPDATE_ERROR);
                        redirect($error_url);
                    }
                }
            }
        }
        if ($id != "" && is_numeric($id)) {
            $this->data['info'] = $this->admin_model->get_info_admin_by_id($id);
        }
        $this->data['page_title'] = $title;
        $this->data['level_one'] = 'Admin Management';
        $this->data['level_two'] = $title;
        $this->breadcrumbs->push('Admin Management', ADMIN_C . ADMIN_D . 'lists');
        $this->breadcrumbs->push($title, ADMIN_C . ADMIN_D . 'add_update');
        $this->data['list_roles'] = $this->admin_model->get_admin_type_list();
        $this->data['list_admin_status'] = _array_to_obj($this->config->item('tbl_column_status_array')['admin_status']);
        //$this->render(ADMIN_D . CRUD_ADMIN);
    }

    public function update($id = '')
    {
        $this->add_update($id);
        $this->render(ADMIN_D . CRUD_ADMIN);
    }

    /**
     * This function is change admin password.
     * @return void
     */

    public function change_password()
    {
        if (count($this->input->post()) > 0) {
            $success_url = base_url(ROOT_DIR_ADMIN . ADMIN_C . CHANGE_PASSWORD);
            $error_url = base_url(ROOT_DIR_ADMIN . ADMIN_C . CHANGE_PASSWORD);
            $id = $this->admin_id;

            $this->form_validation->set_rules('current_password', 'Current Password', 'required|trim');
            $this->form_validation->set_rules('new_password', 'New Password', 'required|trim');
            $this->form_validation->set_rules('new_confirm_password', 'New Confirm Password', 'trim|required|matches[new_password]');

            if ($this->form_validation->run() === FALSE) {
                $this->data['form_errors'] = _array_to_obj($this->form_validation->error_array());
            } else {
                $current_password = trim($this->input->post('current_password'));
                $new_password = trim($this->input->post('new_password'));
                $query_response = $this->admin_model->check_old_password($id,md5($current_password));

                if($query_response){
                    #_pre($query_response);
                    $crud_data = array(
                        'password' => $this->db->escape_str(md5($new_password)),
                    );
                    $where = array("admin_id" => $id);
                    $update_response = $this->admin_model->update_column($where, $crud_data);
                    if ($update_response) {
                        _set_flashdata(FLASH_STATUS_SUCCESS, MSG_ADMIN_PASSWORD_UPDATE_SUCCESS);
                        redirect($success_url);
                    } else {
                        _set_flashdata(FLASH_STATUS_ERROR, MSG_ADMIN_PASSWORD_UPDATE_ERROR);
                        redirect($error_url);
                    }
                }else{
                    _set_flashdata(FLASH_STATUS_ERROR, MSG_ADMIN_PASSWORD_NOT_MATCH_ERROR, FLASH_HTML);
                    redirect($error_url);
                }
            }
        }
        $this->data['page_title'] = 'Change Password';
        $this->data['level_one'] = 'Admin Management';
        $this->data['level_two'] = 'Change Password';
        $this->render(ADMIN_D . CHANGE_PASSWORD);
    }
    public function update_admin($id)
    {
        $this->data['info'] = $this->admin_model->get_info_admin_by_id($id);
        $list_shop = _array_obj($this->shop_model->get_shop_list(), 'A', 'Shops', array('shop_id', 'shop_name'), 1);
        $list_shop1 = array();
        foreach ($list_shop as $key => $value) {
            $list_shop1[$key] = stripslashes($value);
        }
        $this->data['list_shop'] = $list_shop1;
        $this->data['level_one'] = 'Admin Management';
        $this->breadcrumbs->push('Admin Management', 'admin/admin/lists');
        $this->breadcrumbs->push('Admin Update', 'admin/admin/update_admin');
        $this->render(ADMIN_C . UPDATE_ADMIN);
    }
    public function admin_update($id = '')
    {
        //echo $id;exit;
        $success_url = base_url(ROOT_DIR_ADMIN . ADMIN_C . "lists");
        if (count($this->input->post()) > 0) {
            //$this->form_validation->set_rules('current_password', 'Current Password', 'trim|required');
           // $this->form_validation->set_rules('new_password', 'New Password', 'required');
            if ($this->form_validation->run() != FALSE) {
                $form_errors = _array_to_obj($this->form_validation->error_array());
                _set_flashdata(FLASH_STATUS_ERROR, $form_errors, FLASH_HTML);
            } else {
                $crud_data = array(
                        'shop_id' => implode(",", $this->input->post('shop_ids')),
                    );
                    $where = array($this->primary_key => $id);
                    $update_response = $this->dbcommon->update($this->table, $where, $crud_data);
                    if ($update_response) {
                        _set_flashdata(FLASH_STATUS_SUCCESS, MESSAGE_UPDATE_SUCCESS);
                        redirect($success_url);
                    } else {
                        _set_flashdata(FLASH_STATUS_ERROR, MESSAGE_UPDATE_ERROR);
                        redirect($error_url);
                    }
            }
        }
        redirect(base_url(ADMIN_C . LIST_ADMIN));
    }

    public function change_admin_manager_password()
    {
        $this->form_validation->set_rules('admin_id', 'Admin/Manager', 'required|trim');
        $this->form_validation->set_rules('new_password', 'New Password', 'required|trim|min_length[6]');
        $this->form_validation->set_rules('new_confirm_password', 'New Confirm Password', 'trim|required|matches[new_password]');
        $this->form_validation->set_error_delimiters('', '');
        if (count($this->input->post()) > 0) {
            
            $success_url = base_url(ROOT_DIR_ADMIN . ADMIN_C . 'lists');
            $error_url = base_url(ROOT_DIR_ADMIN . ADMIN_C . 'change_admin_manager_password');
            $id = $this->admin_id;

            if ($this->form_validation->run() === FALSE) {
                $form_errors = _array_to_obj($this->form_validation->error_array());
                _set_flashdata(FLASH_STATUS_ERROR, $form_errors, FLASH_HTML);
                redirect($error_url);

            } else {
                // _pre($this->input->post());
                $admin_id = trim($this->input->post('admin_id'));
                $new_password = trim($this->input->post('new_password'));
                
                $crud_data = array(
                    'password' => $this->db->escape_str(md5($new_password)),
                );
                $where = array("admin_id" => $admin_id);
                // _pre($where);
                $update_response = $this->admin_model->update_column($where, $crud_data);
                
                if ($update_response) {
                    // _pre($update_response);
                    $user_data = $this->admin_model->get_info_admin_by_id($admin_id);                    
                    $name = trim($user_data->first_name) . ' ' . trim($user_data->last_name);
                    $email = $user_data->email;

                    $mail_content_data = new stdClass();
                    $mail_content_data->var_user_name = $user_data->user_name;
                    $mail_content_data->var_password = $new_password;
                    $mail_data = email_template('CHANGE_ADMIN_MANAGER_PASSWORD', $mail_content_data);
                    if (MAIL_TEST_MODE == MAIL_TEST_MODE_LIVE) {
                        $email = MAIL_TEST_EMAIL;
                    }
                    if (is_object($mail_data)) {
                        $this->load->library('mail');
                        $this->mail->setTo($email, $name);
                        $this->mail->setSubject($mail_data->email_subject);
                        $this->mail->setBody(_header_footer($mail_data->email_html));
                        $status = $this->mail->send();
                    }

                    _set_flashdata(FLASH_STATUS_SUCCESS, MSG_ADMIN_PASSWORD_UPDATE_SUCCESS);
                    redirect($success_url);
                } else {
                    _set_flashdata(FLASH_STATUS_ERROR, MSG_ADMIN_PASSWORD_UPDATE_ERROR);
                    redirect($error_url);
                }
                

            }

        }        
        
        $this->data['list_admin_manager'] = $this->admin_model->get_admin_manager();
        $this->data['page_title'] = 'Change Admin/Manager Password';
        $this->data['level_one'] = 'Admin Management';
        $this->data['level_two'] = 'Change Admin/Manager Password';
        $this->render(ADMIN_D . CHANGE_ADMIN_MANAGER_PASSWORD);
    }
}


