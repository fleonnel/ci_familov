<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reports extends Admin_Controller
{
    var $table_feedback = 'feedback';
    var $primary_key_feedback = 'feedback_id';
    var $dir = '';

    public function __construct()
    {
        parent::__construct();
        $this->dir = REPORTS_D; #TODO : Change Directory Name
        $this->load->library('form_validation');
        $this->load->model('feedback_model');
        $this->load->model("service_model");
        $this->load->model("customer_model");
        $ticket_status_list = $this->config->item('ticket_status_list');
        $this->data['ticket_status_list'] = $ticket_status_list;
    }

    public function index()
    {
        redirect(admin_url(REPORTS_C . 'system_report'));
    }

    /**
     * This function is used for system report
     * @return void
     */

    public function system_report()
    {
        $this->data['page_title'] = 'System Report';
        $this->data['level_one'] = 'Report Management';
        $this->data['level_two'] = 'System Report';
        $this->data['ticket_status'] = 1;
        $rank = array(1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0);
        $list = $this->feedback_model->get_system_report();
        $rank_total = 0;
        $rank_sum = 0;
        if (count($list)) {
            foreach ($list as $k => $v) {
                $rank[$v->feedback_rank] = $v->TOTAL;
                $rank_total = $rank_total + $v->TOTAL;
            }
        }
        $rank_response = rating_calculation($rank, $rank_total);
        $this->data['ranking'] = $rank_response;
        $this->breadcrumbs->push('System report', 'admin/reports/report_system');
        $this->render($this->dir . REPORT_SYSTEM); #TODO : Change View File Name
    }

    /**
     * This function is used for client wise report
     * @return void
     */

    public function client_report()
    {
        $this->data['page_title'] = 'Client Report';
        $this->data['level_one'] = 'Report Management';
        $this->data['level_two'] = 'Client Report';
        $this->data['ticket_status'] = 1;
        $list_customer = $this->customer_model->get_customer(); #Customer List
        #_pre($list_customer,0);
        if (count($list_customer) > 0) {
            foreach ($list_customer as $k => $v) {
                $rank = array(1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0);
                $rank_total = 0;
                $rank_sum = 0;
                $rank_response = array();
                $customer_id = $v->customer_id;
                $ranking_response = $this->feedback_model->get_customer_report($customer_id); #get Client wise ranking
                if (count($ranking_response) > 0) {
                    if (count($ranking_response)) {
                        foreach ($ranking_response as $k_rank => $v_rank) {
                            $rank[$v_rank->feedback_rank] = $v_rank->TOTAL;
                            $rank_total = $rank_total + $v_rank->TOTAL;
                        }
                    }
                    $rank_response = rating_calculation($rank, $rank_total);
                    $list_customer[$k]->rank = $rank_response;
                    $list_customer[$k]->rank_status = 1;
                } else {
                    $list_customer[$k]->rank = $rank;
                    $list_customer[$k]->rank_status = 0;
                }
            }
        }
        #_pre($list_customer);
        $this->data['list_customer'] = $list_customer;
        $this->breadcrumbs->push('Client Report', 'admin/reports/report_client');
        $this->render($this->dir . REPORT_CLIENT); #TODO : Change View File Name
    }

    /**
     * This function is used for Service wise report
     * @return void
     */
    public function report_service()
    {
        $this->data['page_title'] = 'Service Report';
        $this->data['level_one'] = 'Report Management';
        $this->data['level_two'] = 'Service Report';
        $this->data['ticket_status'] = 1;
        $list_service = $this->service_model->get_services(); #service List
        if (count($list_service) > 0) {
            foreach ($list_service as $k => $v) {
                $rank = array(1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0);
                $rank_total = 0;
                $rank_sum = 0;
                $rank_response = array();
                $service_id = $v->service_id;
                $ranking_response = $this->feedback_model->get_service_report($service_id); #get service wise ranking
                if (count($ranking_response) > 0) {
                    if (count($ranking_response)) {
                        foreach ($ranking_response as $k_rank => $v_rank) {
                            $rank[$v_rank->feedback_rank] = $v_rank->TOTAL;
                            $rank_total = $rank_total + $v_rank->TOTAL;
                        }
                    }
                    $rank_response = rating_calculation($rank, $rank_total);
                    $list_service[$k]->rank = $rank_response;
                    $list_service[$k]->rank_status = 1;
                } else {
                    $list_service[$k]->rank = $rank;
                    $list_service[$k]->rank_status = 0;
                }
            }
        }
        #_pre($list_service);
        $this->data['list_service'] = $list_service;
        $this->breadcrumbs->push('Service Report', 'admin/reports/report_service');
        $this->render($this->dir . REPORT_SERVICE); #TODO : Change View File Name
    }

    /**
     * This function is used for client wise report
     * @return void
     */

    public function report_employee()
    {
        $this->data['page_title'] = 'Employee Report';
        $this->data['level_one'] = 'Report Management';
        $this->data['level_two'] = 'Employee Report';
        $this->data['ticket_status'] = 1;
        $list_employee = $this->admin_model->get_employee(); #employee List
        #_pre($list_employee,0);
        if (count($list_employee) > 0) {
            foreach ($list_employee as $k => $v) {
                $rank = array(1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0);
                $rank_total = 0;
                $rank_sum = 0;
                $rank_response = array();
                $employee_id = $v->admin_id;
                $ranking_response = $this->feedback_model->get_employee_report($employee_id); #get Employee wise ranking
                if (count($ranking_response) > 0) {
                    if (count($ranking_response)) {
                        foreach ($ranking_response as $k_rank => $v_rank) {
                            $rank[$v_rank->feedback_rank] = $v_rank->TOTAL;
                            $rank_total = $rank_total + $v_rank->TOTAL;
                        }
                    }
                    $rank_response = rating_calculation($rank, $rank_total);
                    $list_employee[$k]->rank = $rank_response;
                    $list_employee[$k]->rank_status = 1;
                } else {
                    $list_employee[$k]->rank = $rank;
                    $list_employee[$k]->rank_status = 0;
                }
            }
        }
        #_pre($list_employee);
        $this->data['list_employee'] = $list_employee;
        $this->breadcrumbs->push('Employee Report', 'admin/reports/report_employee');
        $this->render($this->dir . REPORT_EMPLOYEE); #TODO : Change View File Name
    }

}


