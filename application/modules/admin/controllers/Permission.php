<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Permission extends Admin_Controller
{
    var $dir = '';
    var $table = 'access';
    var $table_roles = 'roles';
    var $primary_key = 'access_id';
    var $primary_key_roles = 'roles_id';
    public function __construct()
    {
        parent::__construct();
        $this->dir = PERMISSION_D;
        $this->load->library('form_validation');
        $this->load->model("permission_model");
        $this->load->model("acl_model");
    }

    public function index()
    {
        redirect(admin_url(PERMISSION_C . 'add_update'));
    }

    /**
     * This function is used for redirect on Listing Page
     * @return void
     */
    public function lists()
    {
        $this->data['page_title'] = 'Permission List';
        $this->data['level_one'] = 'Permission Management';
        $this->data['level_two'] = 'Permission List';

        $this->render($this->dir . LIST_SERVICE);
    }

    /**
     * This function is fetch Data and set them into datatable.
     * @return array (json)
     */
    public function data_list()
    {
        //#todo model name change at 3 places, set $p_key as per table
        $list = $this->service_model->get_list();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $list_key => $list_value) {
            $no++;
            $p_key = $list_value->service_id;
            $status_view = '';
            $status_menu = '';
            $update_url = admin_url(SERVICE_C . "add_update") . '/' . $p_key;
            $row = array();
            $row[] = $no;
            $row[] = ucfirst(trim($list_value->service_name));
            $row[] = ucfirst(trim($list_value->dep_service_name));
            $row[] = ucfirst(trim($list_value->service_periodicity));
            $row[] = text_limiter($list_value->service_description, 0, 20);
            $row[] = edit_button($update_url);
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->service_model->count_all(),
            "recordsFiltered" => $this->service_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    /**
     * This function is Add ad Update Data.
     * @return void
     */
    public function add_update($roles_id = '')
    {
        $level_one = 'Permission Management';
        $title = 'Permission View Section';
        $this->breadcrumbs->push('Permission Management','admin/permission/add_update');
        $list_roles = $this->permission_model->get_roles_list();
        $this->data['list_roles'] = $list_roles;
        if ($roles_id == '' && count($list_roles) > 0) {
            $roles_id = $list_roles[0]->roles_id;
            $role_name = $list_roles[0]->name;
        } else {
            if ($roles_id == SUPER_ADMIN_ROLE_ID) {
                _set_flashdata(FLASH_STATUS_ERROR, MESSAGE_ACCESS_DENIED);
                redirect(admin_url(PERMISSION_C . 'add_update'));
            } else {
                $query_response = $this->permission_model->get_role_detail_by_id($roles_id);
                if (count($query_response) > 0) {
                    $roles_id = $query_response->roles_id;
                    $role_name = $query_response->name;
                } else {
                    _set_flashdata(FLASH_STATUS_ERROR, MESSAGE_ACCESS_DENIED);
                    redirect(admin_url(PERMISSION_C . 'add_update'));
                }
            }
        }
        if (count($this->input->post()) > 0) {
            $success_url = admin_url(PERMISSION_C . 'add_update/' . $roles_id);
            #$error_url = admin_url(PERMISSION_C. "add_update");
            $post_data = $this->input->post();
            foreach ($post_data['action'] as $ak => $av) {
                $roles_id = $ak;
                $json = json_encode($av);
                $query_response = $this->permission_model->check_permission($roles_id);
                if (!$query_response) {
                    $crud_data = array(
                        'roles_id' => $this->db->escape_str($roles_id),
                        'access_data' => $json,
                    );
                    $insert_response = $this->dbcommon->insert($this->table, $crud_data);
                    $insert_id = $this->db->insert_id();
                    if ($insert_response) {
                        _set_flashdata(FLASH_STATUS_SUCCESS, MESSAGE_ADD_SUCCESS);
                        redirect($success_url);
                    } else {
                        _set_flashdata(FLASH_STATUS_ERROR, MESSAGE_ADD_ERROR);
                        redirect($success_url);
                    }
                } else {
                    $crud_data = array(
                        'access_data' => $json,//$this->db->escape_str($json),
                    );
                    $where = array($this->primary_key_roles => $roles_id);
                    $update_response = $this->dbcommon->update($this->table, $where, $crud_data);
                    if ($update_response) {
                        _set_flashdata(FLASH_STATUS_SUCCESS, MESSAGE_UPDATE_SUCCESS);
                        redirect($success_url);
                    } else {
                        _set_flashdata(FLASH_STATUS_ERROR, MESSAGE_UPDATE_ERROR);
                        redirect($success_url);
                    }
                }
            }
        }
        $list_roles_permission = $this->permission_model->get_permission_list($roles_id);
        $list_permission = $this->acl_model->accessResourceMap();
        $this->data['list_permission'] = $list_permission;
        $this->data['list_roles_permission'] = $list_roles_permission;
        $this->data['roles_id'] = @$roles_id;
        $this->data['role_name'] = @$role_name;
        $this->data['page_title'] = $title;
        $this->data['level_one'] = $level_one;
        $this->data['level_two'] = $title;
        $this->render($this->dir . CRUD_PERMISSION);
    }

}


