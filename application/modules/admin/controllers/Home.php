<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Admin_Controller
{
    /**
     * Admin Home Controller
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_model');
        // $this->load->model("requests_model");
    }

    public function index()
    {
        redirect(ROOT_DIR_ADMIN.'login');
        /*$this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view(LOGIN_V, $this->data);
            #$this->load->view('login_view',$this->data);
        } else {
            $user_name = $this->db->escape_str(trim($this->input->post('username')));
            $password = $this->db->escape_str(md5(trim($this->input->post('password'))));
            $response_admin_info = $this->admin_model->check_login(array('email' => $user_name, 'password' => $password));
            if (count($response_admin_info)) {
                $session[SESSION_NAME] = $response_admin_info;
                $session[SESSION_NAME]->is_master_admin_login = TRUE;
                $session[SESSION_NAME]->is_admin_login = TRUE;
                $session[SESSION_NAME]->user_type = 'SUPERADMIN';
                $this->session->set_userdata($session);
                $this->session->set_flashdata(FLASH_STATUS, FLASH_STATUS_SUCCESS);
                $this->session->set_flashdata(FLASH_MESSAGE, MSG_ADMIN_LOGIN_SUCCESS);
                redirect(base_url(ROOT_DIR_ADMIN . DASHBOARD_C));
                #redirect(admin_url(DASHBOARD_C));
            } else {
                $session[SESSION_FLASH] = array(
                    FLASH_STATUS => FLASH_STATUS_ERROR,
                    FLASH_MESSAGE => MSG_LOGIN_INVALID
                );
                $this->session->set_userdata($session);
                $this->data['user_name'] = $this->input->post('username');
                $this->data['password'] = $this->input->post('password');
                $this->load->view(LOGIN_V, $this->data);
                #$this->load->view('login_view');
            }
        }*/
    }

    public function login(){

        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view(LOGIN_V, $this->data);
            $this->session->unset_userdata(SESSION_FLASH);
        } else {
            $user_name = $this->db->escape_str(trim($this->input->post('username')));
            $password = $this->db->escape_str(md5(trim($this->input->post('password'))));
            $response_admin_info = $this->admin_model->check_login(array('email' => $user_name, 'password' => $password));
            //_pre($response_admin_info);
            if (count($response_admin_info)) {
                if ($response_admin_info->admin_status == STATUS_CUSTOMER_ACTIVE) {
                $session[SESSION_NAME] = $response_admin_info;
                $session[SESSION_NAME]->is_master_admin_login = TRUE;
                $session[SESSION_NAME]->is_admin_login = TRUE;
                #$session[SESSION_NAME]->admin_type_id = 'SUPERADMIN';
                $this->session->set_userdata($session);
                $this->session->set_flashdata(FLASH_STATUS, FLASH_STATUS_SUCCESS);
                $this->session->set_flashdata(FLASH_MESSAGE, MSG_ADMIN_LOGIN_SUCCESS);
                redirect(base_url(ROOT_DIR_ADMIN . DASHBOARD_C));
                }
                else {
                        //_set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_LOGIN_STATUS_INACTIVE'), FLASH_HTML);
                        $session[SESSION_FLASH] = array(
                    FLASH_STATUS => FLASH_STATUS_ERROR,
                    FLASH_MESSAGE => MSG_LOGIN_STATUS_INACTIVE
                );
                $this->session->set_userdata($session);
                $this->data['user_name'] = $this->input->post('username');
                $this->data['password'] = $this->input->post('password');
                $this->load->view(LOGIN_V, $this->data);
                    }
            } else {
                $session[SESSION_FLASH] = array(
                    FLASH_STATUS => FLASH_STATUS_ERROR,
                    FLASH_MESSAGE => MSG_LOGIN_INVALID
                );
                $this->session->set_userdata($session);
                $this->data['user_name'] = $this->input->post('username');
                $this->data['password'] = $this->input->post('password');
                $this->load->view(LOGIN_V, $this->data);
            }
        }

    }
    public function forgot_password(){
        $this->form_validation->set_rules('username', 'Username', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view(FORGOT_PASSWORD_V,$this->data);
        } else {
            $user_name = $this->db->escape_str(trim($this->input->post('username')));
            $response_admin_info = $this->admin_model->check_forgot_password($user_name);
            if (count($response_admin_info)) {
                $session[SESSION_FLASH] = array(
                    FLASH_STATUS => FLASH_STATUS_SUCCESS,
                    FLASH_MESSAGE => str_replace("{EMAIL}",$response_admin_info->email,MSG_FORGOT_PASSWORD_SUCCESS)
                );
                $this->session->set_userdata($session);
                $new_password = _random_string(6);

                $to_email = $response_admin_info->email;

                if (MAIL_TEST_MODE == MAIL_TEST_MODE_LIVE) {
                    $to_email = MAIL_TEST_EMAIL;
                }

                $name = _ucwords($response_admin_info->first_name. " ".$response_admin_info->last_name);
                $user_name = $response_admin_info->user_name ;

                $mail_content_data = new stdClass();
                $mail_content_data->var_greeting_name = $name;
                $mail_content_data->var_email = $to_email;
                $mail_content_data->var_user_name = $user_name;
                $mail_content_data->var_password = $new_password;
                $mail_data = email_template('ADMIN_FORGOT_PASSWORD',$mail_content_data);
                if(is_object($mail_data)){
                    $crud_data = array(
                        'password' => $this->db->escape_str(md5($new_password)),
                    );
                    $where = array("admin_id" => $response_admin_info->admin_id);
                    $update_response = $this->dbcommon->update('admin', $where, $crud_data); #update password in table
                    $this->load->library('mail');
                    $this->mail->setTo($to_email, $name);
                    $this->mail->setSubject($mail_data->email_subject);
                    $this->mail->setBody(_header_footer($mail_data->email_html));
                    $status = $this->mail->send();
                }
                redirect(ROOT_DIR_ADMIN.'login');
            } else {
                $session[SESSION_FLASH] = array(
                    FLASH_STATUS => FLASH_STATUS_ERROR,
                    FLASH_MESSAGE => MSG_FORGOT_PASSWORD_INVALID
                );
                $this->session->set_userdata($session);
                $this->data['user_name'] = $this->input->post('username');
                $this->load->view(FORGOT_PASSWORD_V,$this->data);
            }
        }
        //$this->load->view(FORGOT_PASSWORD_V);
    }
    public function logout()
    {
        $this->session->unset_userdata(SESSION_NAME);
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
        $session[SESSION_FLASH] = array(
            FLASH_STATUS => FLASH_STATUS_SUCCESS,
            FLASH_MESSAGE => MSG_ADMIN_LOGOUT_SUCCESS
        );
        $this->session->set_userdata($session);
        redirect(ADMIN_C);
        exit;
        if ($this->roles_id == SUPER_ADMIN_ROLE_ID) {
            $this->session->unset_userdata(SESSION_NAME);
            $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
            $this->output->set_header("Pragma: no-cache");
            $session[SESSION_FLASH] = array(
                FLASH_STATUS => FLASH_STATUS_SUCCESS,
                FLASH_MESSAGE => MSG_ADMIN_LOGOUT_SUCCESS
            );
            $this->session->set_userdata($session);
            redirect(ADMIN_C);
            exit;
        } else {
            // $query_response = $this->requests_model->check_open_request($this->admin_id, array(REQUEST_STATUS_OPEN, REQUEST_STATUS_REOPEN));
            if ($query_response > 0) {
                _set_flashdata(FLASH_STATUS_ERROR, MSG_LOGOUT_EMPLOYEE_ERROR, FLASH_HTML);
                redirect(admin_url(REQUEST_SECTION_C . "lists"));
            } else {
                $this->session->unset_userdata(SESSION_NAME);
                $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
                $this->output->set_header("Pragma: no-cache");
                $session[SESSION_FLASH] = array(
                    FLASH_STATUS => FLASH_STATUS_SUCCESS,
                    FLASH_MESSAGE => MSG_ADMIN_LOGOUT_SUCCESS
                );
                $this->session->set_userdata($session);
                redirect(ADMIN_C);
            }
        }
    }
    
    public function test_mail()
    {
        $this->load->library('mail');
        $this->mail->setTo(MAIL_TEST_EMAIL, 'Vikrant Parmar');
        $this->mail->setSubject('Testing Mail.');
        $this->mail->setBody('<h2> Hello ! This is testing Mail.');
        $status = $this->mail->send();
        _e("Mail Sent Status : ".$status);


        /*echo $key = $this->encryptionlib->encode('hii');
        echo "\n";
        echo $this->encryptionlib->decode($key);*/

        /*
            $content_data = new stdClass();
            $content_data->user_name = 'Vikrant Parmar';
            $content_data->action_url = base_url();
            $content_data->sender_name = 'Backend Brains';
            $content_data->website_name = 'wwww.backendbrains.com';
            $content_data->varification_link = base_url().'verification';
            $content_data->url_link = base_url().'verification';
            $mail_data = email_template('TEST_MAIL',$content_data);
        if(is_object($mail_data)){
            $this->load->library('mail');
            $this->mail->setTo(MAIL_TEST_EMAIL, 'Vikrant Parmar');
            $this->mail->setSubject($mail_data->email_subject);
            $this->mail->setBody($mail_data->email_html);
            $status = $this->mail->send();
        }*/
    }

}