<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Customer extends Admin_Controller
{
    var $table = 'customers';
    var $primary_key = 'customer_id';
    var $dir = '';

    public function __construct()
    {
        parent::__construct();
        $this->dir = CUSTOMER; #TODO : Change Directory Name
        $this->load->library('form_validation');
        $this->load->model("customers_model");
        $this->load->model("country_model");
        $this->load->model("city_model");
    }

    public function index()
    {
        redirect(admin_url(CUSTOMER . 'lists'));
    }

    /**
     * This function is used for redirect on List Page
     * @return void
     */
    public function lists()
    {
        $this->data['page_title'] = 'Customer List';
        $this->data['level_one'] = 'Customer Management';
        $this->data['level_two'] = 'Customer List';
        $this->breadcrumbs->push('Customer List', 'admin/customer/lists');
        $this->render($this->dir . LIST_CUSTOMER); #TODO : Change View File Name
    }

    /**
     * This function is fetch Data and set them into datatable.
     * @return array (json)
     */
    public function data_list()
    {
        //#todo model name change at 3 places, set $p_key as per table
        $list = $this->customers_model->get_list();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $list_key => $list_value) {
            $no++;
            $p_key = $list_value->customer_id;
            
           // $status_menu = '';
            $view_base_url = base_url(ROOT_DIR_ADMIN . CUSTOMER. "view_profile");
            $update_url = admin_url(CUSTOMER_C . "update") . '/' . $p_key;

            $view_url = $view_base_url . '/' . $p_key;
            $edit_url = base_url(ROOT_DIR_ADMIN .CUSTOMER_C . "update") . '/' . $p_key;
            $row = array();
            $row[] = $no;
            $row[] = ucfirst(trim($list_value->username)) . ' ' . ucfirst(trim($list_value->lastname));
            $row[] = ucfirst(trim($list_value->email_address));

            $row[] = _set_dash(ucfirst(trim($list_value->home_address)));
            $row[] = ($list_value->sender_phone != '') ? ucfirst(trim($list_value->phone_code . " " . $list_value->sender_phone)) : '-';
            $row[] = _set_dash(ucfirst(trim($list_value->signup_date)));
            $row[] = _set_dash(ucfirst(trim($list_value->dob)));
            $row[] = _set_dash(ucfirst(trim($list_value->country_id)));
            $row[] = _set_dash(ucfirst(trim($list_value->city_id)));
            #$row[] = _set_dash(ucfirst(trim($list_value->status)));
            $status_view = '';
            $status_menu = '';
            if (ucfirst(trim($list_value->status)) != '') {
                if (CUSTOMER_STATUS_ACTIVE == $list_value->status) {
                    $status_view = status_tag(CUSTOMER_STATUS_ACTIVE, COLOR_S);
                    $status_menu .= action_event(ADMIN_STATUS_INACTIVE, COLOR_E, $p_key);
                } else if (CUSTOMER_STATUS_IN_ACTIVE == $list_value->status) {
                    $status_view = status_tag(CUSTOMER_STATUS_IN_ACTIVE, COLOR_W);
                    $status_menu .= action_event(ADMIN_STATUS_ACTIVE, COLOR_S, $p_key);
                    #$status_menu .= action_event(ADMIN_STATUS_BLOCK,COLOR_E,$p_key);
                }
            }
            $row[] = $status_view;//_set_dash(ucfirst(trim($list_value->status)));
            $row[] = view_button($p_key) . ' ' . edit_button($edit_url) . ' ' . action_button($status_menu);
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->customers_model->count_all(),
            "recordsFiltered" => $this->customers_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    /**
     * This function is Add Data.
     * @return void
     */
    public function update($id = '')
    {
        $this->breadcrumbs->push('Customer Management', 'admin/customer/lists');
        $this->breadcrumbs->push('Update', 'admin/customer/update');
        $this->add_update($id);
        $this->render($this->dir . CRUD_CUSTOMER);
    }

    private function add_update($id = '')
    {
        $success_url = admin_url(CUSTOMER_C . 'lists'); #TODO : Change Controller Name
        $level_one = 'Customer Management';
            $title = 'Customer Update Section';
            $error_url = admin_url(CUSTOMER_C . "update"); #TODO : Change Controller Name
        if (count($this->input->post()) > 0) {

            $this->form_validation->set_rules('username', 'Username', 'required|trim');
            $this->form_validation->set_rules('lastname', 'Lastname', 'required|trim');
            $this->form_validation->set_rules('dob', 'DOB', 'required|trim');
            $this->form_validation->set_rules('phone_code', 'Phone Code', 'required|trim');
            $this->form_validation->set_rules('sender_phone', 'Sender Phone Number', 'required|trim');
            $this->form_validation->set_rules('city_id', 'City Name', 'required|trim');
            $this->form_validation->set_rules('country_id', 'Country Name', 'required|trim');
            $this->form_validation->set_rules('postal_code', 'Postal Code', 'required|trim');
            $this->form_validation->set_rules('status', 'Status', 'required|trim');
            $this->form_validation->set_error_delimiters('', '');
            if ($this->form_validation->run() === FALSE) {
                $form_errors = _array_to_obj($this->form_validation->error_array());
                _set_flashdata(FLASH_STATUS_ERROR, $form_errors, FLASH_HTML);
            } else {
                    $crud_data = array(
                        'username' => $this->db->escape_str(trim($this->input->post('username'))),
                        'lastname' => $this->db->escape_str(trim($this->input->post('lastname'))),
                        'dob' => $this->db->escape_str(trim($this->input->post('dob'))),
                        'phone_code' => $this->db->escape_str(trim($this->input->post('phone_code'))),
                        'sender_phone' => $this->db->escape_str(trim($this->input->post('sender_phone'))),
                        'phone_number' => $this->db->escape_str(trim($this->input->post('sender_phone'))),
                        'city_id' => $this->db->escape_str(trim($this->input->post('city_id'))),
                        'country_id' => $this->db->escape_str(trim($this->input->post('country_id'))),
                        'postal_code' => $this->db->escape_str(trim($this->input->post('postal_code'))),
                        'status' => $this->db->escape_str(trim($this->input->post('status'))),
                        'notification_email' => $this->db->escape_str(trim($this->input->post('notification_email'))),
                        'notification_text' => $this->db->escape_str(trim($this->input->post('notification_text'))),
                    );
                        $where = array($this->primary_key => $id);
                        $update_response = $this->dbcommon->update($this->table, $where, $crud_data);


                if ($update_response) {
                            _set_flashdata(FLASH_STATUS_SUCCESS, MESSAGE_UPDATE_SUCCESS);
                            redirect($success_url);
                        } else {
                            _set_flashdata(FLASH_STATUS_ERROR, MESSAGE_UPDATE_ERROR);
                            redirect($error_url);
                        }
                    }
        } else {
             if ($id != "" && is_numeric($id)) {
                 $this->data['info'] = $this->customers_model->get_info_by_id($id);
             }
        }
        $this->data['list_country'] = $this->country_model->get_country_list();
        $this->data['list_city'] = $this->city_model->get_city_list();
        $this->data['page_title'] = $title;
        $this->data['level_one'] = $level_one;
        $this->data['level_two'] = $title;
#        $this->render($this->dir . CRUD_CUSTOMER_CONTACT); #TODO : Change View File Name
    }

    public function view_profile()
    {
        $id = $this->db->escape_str(trim($this->input->post('id')));
        $title = 'Customer Detail';
        $html = '';
        if ($id != '') {
            $select_response = $this->customers_model->get_info_by_id($id);
            $data_array = array();
            if (count($select_response) > 0) {
                $data_array['customer_info'] = $select_response;
                unset($select_response->password);
                $html = $this->load->view(CUSTOMER_D . '/_customer_detail', $data_array, true);
                echo json_encode(array(FLASH_STATUS => FLASH_STATUS_SUCCESS, FLASH_MESSAGE => $this->lang->line('MSG_VO_05'), 'html' => $html, 'title' => $title));
                exit;

            } else {
                echo json_encode(array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => $this->lang->line('MSG_VO_06'), 'title' => $title));
                exit;
            }
        } else {
            $res_array = array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => MESSAGE_INFORMATION_NOT_AVAILABLE, 'html' => '', 'title' => $title);
        }
        echo json_encode($res_array);
        exit;
    }
    public function update_status()
    {
        $id = $this->db->escape_str(trim($this->input->post('id')));
        //echo $id;exit;
        $status = $this->db->escape_str(trim($this->input->post('status')));
        $change_response=$this->customers_model->change_status($id,$status);

            //$delete_response = $this->dbcommon->delete($id);
            if ($change_response) {
                // $data[] = $this->order_model->get_info_by_id($id);
                //                 $query_response_contact = $this->customer_model->get_customer_name($data[0]->customer_id);
                //                 $data['order'] = $this->order_model->get_info_by_id($id);
                //                 if (count($query_response_contact) > 0 && ($status == ORDER_STATUS_DELIVERED || $status == ORDER_STATUS_BUY)) {
                //                     $name = _ucwords($query_response_contact->username .' '.$query_response_contact->lastname);
                //                     $to_email = 'vikas.maheshwari1991.vm@gmail.com';//$query_response_contact->email_address;
                //                     // if (MAIL_TEST_MODE == MAIL_TEST_MODE_TEST) {
                //                     //     $to_email = MAIL_TEST_EMAIL;
                //                     // }
                //                     $receiver_name = $query_response_contact->recp_name;
                //                     $generate_code = $data['order']->generate_code;
                //                     $mail_content_data = new stdClass();
                //                     $mail_content_data->var_user_name = $name;
                //                     $mail_content_data->var_order_code = $generate_code;
                //                     $mail_content_data->var_reciever_name = $receiver_name;
                //                     $mail_content_data->var_email = $to_email;

                //                     if($status ==ORDER_STATUS_DELIVERED){
                //                     $mail_data = email_template('ADMIN_PACKET_DELIVERED_SUCCESSFULLY', $mail_content_data);
                //                     }
                //                     else if($status ==ORDER_STATUS_BUY){
                //                     $mail_data = email_template('ADMIN_PACKET_BUY_SUCCESSFULLY', $mail_content_data);
                //                     }
                //                     if (is_object($mail_data)) {
                                       
                //                         $this->load->library('mail');
                //                         $this->mail->setTo($to_email, $name);
                //                         $this->mail->setSubject($mail_data->email_subject);
                //                         $this->mail->setBody(_header_footer($mail_data->email_html));
                //                         $status = $this->mail->send();
                //                     }
                //                 }
                echo json_encode(array(FLASH_STATUS => FLASH_STATUS_SUCCESS, FLASH_MESSAGE => MESSAGE_CHANGE_STATUS_SUCCESS));
                exit;
            } else {
                echo json_encode(array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => MESSAGE_DELETE_ERROR));
                exit;
            }
    }
}