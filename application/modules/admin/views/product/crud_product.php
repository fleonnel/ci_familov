<!-- Select2 -->
<script src="<?php echo base_url(ASSETS_ADMIN . 'plugins/select2/select2.full.min.js'); ?>"></script>
<?php
$action_url = '';
$list_url = admin_url(PRODUCT . 'lists');
?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><strong><?= @$level_two ?></strong></h3>
                        <span class="pull-right text-muted">Mandatory information is marked with an asterisk <span
                                class="text-red">*</span></span>
                </div>
                <!-- /.box-header -->
                <?php _notify(); ?>
                <div class="box-body">
                    <?php echo form_open('', 'class="form-horizontal" id="crud_form"  name="crud_form" enctype="multipart/form-data"'); ?>
                    <div class="box-body">
                       <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Product Name<span class="text-red">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="product_name"  name="product_name" placeholder="Enter Your Product Name" value="<?php echo edit_display('product_name', @$info); ?>">
                                    </div>
                        </div>
                        
                        <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Category<span
                                            class="text-red">*</span></label>

                                    <div class="col-sm-6">
                                        <select class="form-control select2" style="width: 100%;" name="category_id"
                                                id="category_id">
                                            <option value="">Choose Category</option>
                                            <?php echo select_display(@$list_category, 'category_id', 'category_name', edit_display('category_id', @$info, 'category_id')); ?>
                                        </select>
                                    </div>
                        </div>
                        <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Product Short Description<span class="text-red">*</span></label>
                                    <div class="col-sm-6">
                                        <textarea class="form-control" id="product_short_desc"  name="product_short_desc" placeholder="Enter your product short description" value=""><?php echo edit_display('product_short_desc', @$info); ?></textarea>
                                    </div>
                        </div>
                        <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Product Description<span class="text-red">*</span></label>
                                    <div class="col-sm-6">
                                        <textarea class="form-control" id="product_desc"  name="product_desc" placeholder="Enter your product description" value=""><?php echo edit_display('product_desc', @$info); ?></textarea>
                                    </div>
                        </div>
                        <div class="form-group">
                                <label class="col-sm-2 control-label">Product product_image</label>
                                <?php  
                                //if (@$info->product_image == "") { ?>
                                <div class="col-sm-6">
                                     <input type="file" id="product_image" name="product_image" class="form-control form-control-flat" >
                                </div>
                                <?php //} ?>
                        </div>
                        <div class="form-group">
                                <?php  
                               # _pre($files);
                                         if (@$info->product_image != "") { ?>
                                        <p style="margin-left:20%;" id="idrow_<?= @$info->product_id ?>">
                                            <img src="<?= DIR_PRODUCT_URL ?><?= @$info->product_image; ?>" width="150" height="150" />
                                            <button type="button" onclick="deletproduct(<?= @$info->product_id; ?>)"
                                               data-toggle="tooltip" title="" value="<?= @$info->product_id; ?>" class="MultiFile-remove"
                                               data-original-title="remove"> <i class="fa fa-trash fa-o"></i>
                                            </button>
                                        </p>
                                        <?php }  ?>
                        </div>
                        <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Product Price<span class="text-red">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="product_prices"  name="product_prices" placeholder="Enter your product price" value="<?php echo edit_display('product_prices', @$info); ?>">
                                    </div>
                        </div>
                        <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Product Special Price<span class="text-red">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="special_product_prices"  name="special_product_prices" placeholder="Enter your product special price" value="<?php echo edit_display('special_product_prices', @$info); ?>">
                                    </div>
                        </div>
                        <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Shop Name<span
                                            class="text-red">*</span></label>

                                    <div class="col-sm-6">
                                        <select class="form-control select2" style="width: 100%;" name="shop_id"
                                                id="shop_id">
                                            <option value="">Choose a Shop</option>
                                            <?php echo select_display(@$list_shop, 'shop_id', 'shop_name', edit_display('shop_id', @$info, 'shop_id')); ?>
                                        </select>
                                    </div>
                        </div>
                        <div class="form-group">
                            <label for="inputlbl" class="col-sm-2 control-label"></label>

                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success ">Save</button>
                                <a href="<?php echo $list_url; ?>">
                                    <button type="button" class="btn btn-default">Cancel</button>
                                </a>
                            </div>
                        </div>

                    </div>
                    <?php echo form_close(); ?>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                </div>
                <!-- /.box-footer -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
<?php include_once("_script_crud.php"); ?>
<script>
    $(document).ready(function () {
        $("#crud_form").validate({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                error.insertAfter(element);
                // for other inputs, just perform default behavior
            },
            ignore: "",
            rules: {
                product_name: {
                    required: true
                },
                category_id: {
                    required: true
                },
                product_short_desc: {
                    required: true
                },
                product_desc: {
                    required: true
                },
                // product_image: {
                //     required: true
                // },
                product_prices: {
                    required: true,
                    number: true
                },
                shop_id: {
                    required: true
                },
                special_product_prices: {
                    required: true,
                    number: true
                },
                
                
            },
            messages: {
                product_name: {
                    required: 'Please Enter Product Name.'
                },
                category_id: {
                    required: 'Please Choose a Category.'
                },
                product_short_desc: {
                    required: 'Please Enter a Product Short Description.'
                },
                product_desc: {
                    required: 'Please Enter a Product Description.'
                },
                // product_image: {
                //     required: 'Please Select Product Image.'
                // },
                product_prices: {
                    required: 'Please Enter a Product Price.',
                    number:'Please Enter a Number Only.'
                },
                special_product_prices: {
                    required: 'Please Enter a Product Special Price.',
                    number:'Please Enter a Number Only.'
                },
                shop_id: {
                    required: 'Please Choose a Shop.'
                },
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                //successHandler1.hide();
                //errorHandler1.show();
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            },
            submitHandler: function (frmadd) {
                successHandler1.show();
                errorHandler1.hide();
            }
        });
    });
</script>