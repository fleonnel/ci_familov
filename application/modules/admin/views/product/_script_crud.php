<?php
if ($method == 'lists') { ?>
    <script type="text/javascript">
        var list_table_one;
        var list_url = "<?php echo $list_url; ?>";
        $(document).ready(function () {
            var name_of_file = "<?php echo string_prefix($export_prefix,_str_repalace("-","_",get_current_date()));?>";
            var export_coulmn = "0,1";
            <?php //TODO Change Name for export file here ?>
            list_table_one = $('#list_table_one').DataTable({
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [[1, 'asc']], //Initial no order.
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": list_url,
                    "type": "POST"
                },
                //Set column definition initialisation properties.
                "columnDefs": [
                    {
                        <?php //TODO Set Column number here for disable sorting ?>
                        "targets": [0, 1], //first column / numbering column
                        "orderable": false, //set not orderable
                    },
                ],
                pageLength: 10,
                responsive: true,

            });
        });
        function reload_table() {
            list_table_one.ajax.reload(null, false); //reload datatable ajax
        }
    </script>
<?php } else if ($method == 'add' || $method == 'update') { ?>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2();
        });
    </script>

<?php } ?>
<script>
function deletlogo(id){
    var base_url="<?= base_url(); ?>";
  $.ajax({
    url: base_url + 'admin/shop/removelogo/'+ id, //maintains the (controller/function/argument) logic in the MVC pattern
    type: 'post',
    success: function(data){
        $('#idrow_'+id).remove();
        location.reload();
        console.log(data);
    },
    error: function(a,b,c){
        console.log(a,b,c);
    }
});
}
</script>
<script>
function deletproduct(id){
    var base_url="<?= base_url(); ?>";
  $.ajax({
    url: base_url + 'admin/product/remove/'+ id, //maintains the (controller/function/argument) logic in the MVC pattern
    type: 'post',
    success: function(data){
        $('#idrow_'+id).remove();
        location.reload();
        console.log(data);
    },
    error: function(a,b,c){
        console.log(a,b,c);
    }
});
}
</script>
<script>
  $(function() {
    $('#product_desc').froalaEditor({
        toolbarButtons: ['undo', 'redo' , '|', 'bold', 'italic','align', 'formatOL', 'formatUL', 'insertLink', 'insertImage','fullscreen' ,'underline', 'strikeThrough', 'subscript', 'superscript', 'outdent', 'indent', 'clearFormatting', 'insertTable', 'html'],
        toolbarButtonsXS: ['undo', 'redo' , '-', 'bold', 'italic', 'underline'],
      pluginsEnabled: null,
      height:300
    })
  });
</script>