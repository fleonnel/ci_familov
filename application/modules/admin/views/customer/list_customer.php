<?php
#TODO : Change Controller Name, Add Button Text, Export Prefix name, Column Order in Datatable js.
$list_url = admin_url(CUSTOMER_C . "data_list");
$add_update_url = admin_url(CUSTOMER_C . "add");
$update_status_url = admin_url(CUSTOMER_C . "update_status") . '/';
$btn_add_text = 'Add Product';
$export_prefix = 'customer_';
?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><strong><?= @$level_two ?></strong></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php _notify(); ?>
                    <table id="list_table_one"
                           class="table table-striped table-bordered table-hover dataTables-example newTab">
                        <thead>
                        <tr>
                            <th>No#</th>
                            <th>Name</th>
                            <th>Email Address</th>
                            <th>Address</th>
                           <!--  <th>Postal Code</th> -->
                            <th>Phone Number</th>
                            <th>SignUp Date</th>
                            <th>DOB</th>
                            <th>Country</th>
                            <th>City</th>
                            <th>Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
</section>
<!-- /.content -->

<script type="text/javascript">
    var list_table_one;
    var list_url = "<?php echo $list_url; ?>";
    $(document).ready(function () {
        var name_of_file = "<?php echo string_prefix($export_prefix,_str_repalace("-","_",get_current_date()));?>";
        var export_coulmn = "0";
        <?php //TODO Change Name for export file here ?>
        list_table_one = $('#list_table_one').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": list_url,
                "type": "POST"
            },
            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    <?php //TODO Set Column number here for disable sorting ?>
                    "targets": [0, 7, 8,9,10], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],
            pageLength: 10,
            responsive: true,
            "autoWidth": false

        });
    });
    function reload_table() {
        list_table_one.ajax.reload(null, false); //reload datatable ajax
    }
    function status_update(id, status) {
        var url = "<?php echo $update_status_url ;?>";
        swal({
                title: "<?=CONFIRMATION_MESSAGE_FOR_STATUS_TITLE?>",
                text: "<?=CONFIRMATION_MESSAGE_FOR_STATUS_TEXT?>",
                type: "<?=SWEET_ALERT_INFO?>",
                showCancelButton: true,
                confirmButtonColor: "<?=SWEET_ALERT_CONFIRMATION_COLOR?>",
                confirmButtonText: "<?=SWEET_ALERT_CONFIRMATION_BUTTON_YES_TEXT?>",
                cancelButtonText: "<?=SWEET_ALERT_CONFIRMATION_BUTTON_CANCEL_TEXT?>",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: {id: id, status: status},
                        dataType: "JSON",
                        success: function (data) {
                            if (data.FLASH_STATUS == '<?=FLASH_STATUS_SUCCESS?>') //if success close modal and reload ajax table
                            {
                                reload_table();
                                var STATUS_TYPE = '';
                                STATUS_TYPE = '<?=MESSAGE_DELETE_SUCCESS?>';
                                toastr.success(data.FLASH_MESSAGE, '<?=SUCCESS?>');
                            }
                            else if (data.FLASH_STATUS == '<?=FLASH_STATUS_ERROR?>') {
                                toastr.error(data.FLASH_MESSAGE, '<?=ERROR?>');
                            } else {
                                toastr.error('<?=MESSAGE_CHANGE_STATUS_ERROR?>', '<?=ERROR?>');
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            toastr.error('<?=DATA_TABLE_ERROR?>', '<?=ERROR?>');
                        }
                    });
                } else {
                    toastr.info('<?=MESSAGE_LAST_OPERATION_DISCARD?>', '<?=INFO?>');
                }
            });
    }
</script>
<?php #include_once("_script_crud.php"); ?>
<script language="javascript">
    function action_details(id) {
        var url = "<?php echo base_url(ROOT_DIR_ADMIN.CUSTOMER_C."view_profile") ;?>";
        var userdata = "id=" + id;
        $.ajax({
            type: "POST",
            url: url,
            data: userdata,
            dataType: "JSON",
            cache: false,
            success: function (data) {
                var resdata = data;
                document.getElementById('modal-body').innerHTML = data.html;
                if (data.FLASH_STATUS == '<?=FLASH_STATUS_SUCCESS?>') //if success close modal and reload ajax table
                {
                    $('#myModal').modal('show');
                    //toastr.success(data.FLASH_MESSAGE, '<?=SUCCESS?>');
                }
                else if (data.FLASH_STATUS == '<?=FLASH_STATUS_ERROR?>') {
                    toastr.error(data.FLASH_MESSAGE, '<?=ERROR?>');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error('<?=DATA_TABLE_ERROR?>', '<?=ERROR?>');
            }
        });
    }
    // var size=document.getElementById('mysize').value;

</script>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><strong>Customer Detail</strong></h4>
            </div>
            <div class="modal-body" id="modal-body">
                No Data Found
            </div>
            <div class="modal-footer" id="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<style>.page-header{padding-bottom: 26px;}</style>
