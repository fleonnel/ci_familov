<section class="">
    <div id="printableArea">
        <!-- <div class="row">
            <div class="col-xs-12"><h3><strong class="text-center">
                        <img src="<?php /*echo(ASSETS_URL_FRONT . 'img/images/familov-logo.png'); */ ?>" class="text-center"
                             alt="<? /*=SITE_NAME_COM*/ ?>" style="margin-top: 10px;margin-left: 30%; margin-bottom:30px" width="250px"></strong></h3>
            </div>
        </div>-->
        <div class="row">
            <?php
            $address = '';
            if (@$customer_info->home_address != '')
                $address .= @$customer_info->home_address;
            if ($address == '')
                $address .= @$customer_info->city_id;
            else
                $address .= ", " . @$customer_info->city_id;

            if ($address == '')
                $address .= @$customer_info->country_id;
            else
                $address .= ",<br>" . @$customer_info->country_id;

            if ($address == '')
                $address .= @$customer_info->postal_code;
            else
                $address .= ", " . @$customer_info->postal_code;


            $status_view = '';
            if (ucfirst(trim(@$customer_info->status)) != '') {
                if (CUSTOMER_STATUS_ACTIVE == @$customer_info->status) {
                    $status_view = status_tag(CUSTOMER_STATUS_ACTIVE, COLOR_S);
                } else if (CUSTOMER_STATUS_IN_ACTIVE == @$customer_info->status) {
                    $status_view = status_tag(CUSTOMER_STATUS_IN_ACTIVE, COLOR_W);
                }
            }
            $email = '<i class="fa fa-envelope-o margin-r-5"></i>';
            $text = '<i class="fa fa-mobile margin-r-5"></i>';
            $notification_email = $email . ' No';
            $notification_text = $text . ' No';
            if (@$customer_info->notification_email == 1) {
                $notification_email = $email . ' Yes (Let me know about news and offers by email)';
            }
            if (@$customer_info->notification_text == 1) {
                $notification_text = $text . ' Yes (Let me know about news and offers by text)';
            }
            ?>
            <div class="col-sm-12">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title pull-left">
                                <strong><?= @$customer_info->username . " " . @$customer_info->lastname ?></strong></h3>
                            <strong class="pull-right"><?= $status_view ?></strong>
                        </div>
                        <div class="box-body">
                            <strong><i class="fa fa-envelope-o margin-r-5"></i> E-mail</strong>

                            <p class="text-muted">
                                <?= _set_dash(@$customer_info->email_address) ?>
                            </p>
                            <hr>
                            <strong><i class="fa fa-book margin-r-5"></i> Contact Number</strong>

                            <p class="text-muted">
                                <?= (@$customer_info->sender_phone != '') ? @$customer_info->phone_code . " " . @$customer_info->sender_phone : '-' ?>
                            </p>
                            <hr>
                            <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

                            <p class="text-muted"> <?= @$address ?></p>
                            <hr>
                            <!--<strong><i class="fa fa-pencil margin-r-5"></i> Status</strong>

                            <p class="text-muted"> <? /*= @$status_view */ ?></p>
                            <hr>-->
                            <strong><i class="fa fa-bell-o margin-r-5"></i> Notification Preferences</strong>

                            <p>
                                <?= $notification_email ?>
                            </p>

                            <p>
                                <?= $notification_text ?>
                            </p>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </div>
    </div>
</section>