<?php
$action_url = '';
$list_url = base_url(ROOT_DIR_ADMIN . EMAIL_TEMPLATE_C . 'lists');
?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><strong><?=@$level_two?></strong></h3>
                    <span class="pull-right text-muted">Mandatory information is marked with an asterisk <span class="text-red">*</span></span>
                </div>
                <!-- /.box-header -->
                <?php _notify(); ?>
                <div class="box-body">
                    <form class="form-horizontal" name="crud_form" id="crud_form" method="post">
                        <div class="box-body">
                            <?php if(edit_display('email_id', @$info) != ''){?>
                             <div class="form-group">
                                 <label for="inputlbl" class="col-sm-2 control-label">Tags</label>
                                <div class="col-sm-6">
                                    <?php $myArray = explode(',', edit_display('vTags', @$info)); ?>
                                    <?php foreach ($myArray as $tags) { ?>
                                        <?php echo $tags; ?></br>
                                    <?php } ?>
                                </div>
                            </div>
                             <?php } ?>
                            <div class="form-group <?php if((_form_validaiton('vEmailFormatType',$form_errors) == 1)) {echo 'has-error2' ;}?>">
                                <label for="inputlbl" class="col-sm-2 control-label">Email Key <span class="text-red">*</span></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="vEmailFormatType"  name="vEmailFormatType" placeholder="Enter Email Key" value="<?php echo edit_display('vEmailFormatType', @$info); ?>" <?php if(edit_display('vEmailFormatType', @$info) != ''){?>disabled="disabled" <?php } ?>>
                                     <?php if(edit_display('vEmailFormatType', @$info) != ''){?>
                                     <input type="hidden" class="form-control" id="vEmailFormatType"  name="vEmailFormatType" value="<?php echo edit_display('vEmailFormatType', @$info); ?>">
                                     
                                     <?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputlbl" class="col-sm-2 control-label">Email Title(English) <span class="text-red">*</span></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control"  id="vEmailFormatTitle_EN"  name="vEmailFormatTitle_EN" placeholder="Enter Email Title"  value="<?php echo edit_display('vEmailFormatTitle_EN', @$info); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputlbl" class="col-sm-2 control-label">Email Subject (English)<span class="text-red">*</span></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control"  id="vEmailFormatSubject_EN"  name="vEmailFormatSubject_EN" placeholder="Enter Email Subject" value="<?php echo edit_display('vEmailFormatSubject_EN', @$info); ?>">
                                </div>
                            </div>
                             
                             <div class="form-group">
                                <label for="inputlbl" class="col-sm-2 control-label">Email Description (English)<span class="text-red">*</span></label>
                                <div class="col-sm-6">
                                    <textarea class="form-control" id="tEmailFormatDesc_EN" name="tEmailFormatDesc_EN"
                                              placeholder="Enter Email Html"><?= @$info->tEmailFormatDesc_EN ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputlbl" class="col-sm-2 control-label">Email Title(French) <span class="text-red">*</span></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control"  id="vEmailFormatTitle_FR"  name="vEmailFormatTitle_FR" placeholder="Enter Email Title"  value="<?php echo edit_display('vEmailFormatTitle_FR', @$info); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputlbl" class="col-sm-2 control-label">Email Subject (French)<span class="text-red">*</span></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control"  id="vEmailFormatSubject_FR"  name="vEmailFormatSubject_FR" placeholder="Enter Email Subject" value="<?php echo edit_display('vEmailFormatSubject_FR', @$info); ?>">
                                </div>
                            </div>
                             <?php if(edit_display('tags', @$info) != ''){?>
                             <div class="form-group">
                                 <label for="inputlbl" class="col-sm-2 control-label">Template Variable</label>
                                <div class="col-sm-6">
                                    <?php $myArray = explode(',', edit_display('tags', @$info)); ?>
                                    <?php foreach ($myArray as $tags) { ?>
                                        <?php echo $tags; ?></br>
                                    <?php } ?>
                                </div>
                            </div>
                             <?php } ?>
                             <div class="form-group">
                                <label for="inputlbl" class="col-sm-2 control-label">Email Description (French)<span class="text-red">*</span></label>
                                <div class="col-sm-6">
                                    <textarea class="form-control" id="tEmailFormatDesc_FR" name="tEmailFormatDesc_FR"
                                              placeholder="Enter Email Html"><?= @$info->tEmailFormatDesc_FR ?></textarea>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="inputlbl" class="col-sm-2 control-label"></label>
                                <div class="col-sm-6">
                                    <button type="submit" class="btn btn-success ">Save</button>
                                    <a href="<?php echo $list_url; ?>">
                                    <button type="button" class="btn btn-default">Cancel</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                </div>
                <!-- /.box-footer -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->

<script>
    $(document).ready(function () {
        $("#crud_form").validate({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                error.insertAfter(element);
                // for other inputs, just perform default behavior
            },
            ignore: "",
            rules: {
                
                email_title: {
                    required: true
                },
                email_subject: {
                    required: true,
                },
                email_html: {
                    required: true
                },
               
            },
            messages: {
                
                email_title: {
                    required: 'Please Enter Email Title.'
                },
                email_subject: {
                    required: 'Please Enter Email Subject.',
                },
                email_html: {
                    required: 'Please Add Email Html.'
                },
               
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                //successHandler1.hide();
                //errorHandler1.show();
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            },
            submitHandler: function (frmadd) {
                successHandler1.show();
                errorHandler1.hide();
            }
        });
    });
</script>
<script>
  $(function () {
    CKEDITOR.replace('tEmailFormatDesc_EN')
    //bootstrap WYSIHTML5 - text editor
    $('.tEmailFormatDesc_EN').wysihtml5();
    CKEDITOR.replace('tEmailFormatDesc_FR')
    //bootstrap WYSIHTML5 - text editor
    $('.tEmailFormatDesc_FR').wysihtml5();
  })
</script>
