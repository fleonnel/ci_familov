<!-- Main content -->
<section class="content">
    <div class="row">
        <?php _notify(); ?>
    </div>

    <div class="row">
        <?php
        if($this->roles_id == SUPER_ADMIN_ROLE_ID || $this->roles_id == ADMIN_ROLE_ID)
        {
        ?>
            <div class="col-lg-2 col-xs-6">
                <div class="small-box bg-yellow-light" style="background-color: #eaff00; ">
                    <div class="inner">
                        <h3><?= @$total_order_wait ?></h3>

                        <p>New Orders</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="<?php echo base_url(ROOT_DIR_ADMIN.ORDER."lists") . '/' ;?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        <?php
        }
        ?>
        

        <div class="col-lg-2 col-xs-6">
            <div class="small-box bg-pink-light" style=" background-color: #ff3fd7; ">
                <div class="inner">
                    <h3><?= @$total_order_in_process ?></h3>
                    <p>Pending Orders</p>
                </div>
                <div class="icon">
                    <i class="fa fa-hourglass-half"></i>
                </div>
                <a href="<?php echo base_url(ROOT_DIR_ADMIN.ORDER."lists") . '/' ;?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <?php
        if($this->roles_id == SUPER_ADMIN_ROLE_ID || $this->roles_id == ADMIN_ROLE_ID)
        {
        ?>
        <div class="col-lg-2 col-xs-6">
            <div class="small-box bg-gray-light">
                <div class="inner">
                    <h3><?= @$total_order_delivered ?></h3>

                    <p>Delivered Orders</p>
                </div>
                <div class="icon">
                    <i class="fa fa-check-square-o"></i>
                </div>
                <a href="<?php echo base_url(ROOT_DIR_ADMIN.ORDER."lists") . '/' ;?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <?php
        }
        ?>
        <?php
        if($this->roles_id == SUPER_ADMIN_ROLE_ID || $this->roles_id == ADMIN_ROLE_ID)
        {
        ?>
        <?php
        }
        else
        {
        ?>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-gray-light">
                <div class="inner">
                    <h3><?= @$total_order_buy ?></h3>

                    <p>Buy Orders</p>
                </div>
                <div class="icon">
                    <i class="fa fa-check-square-o"></i>
                </div>
                <a href="<?php echo base_url(ROOT_DIR_ADMIN.ORDER."lists") . '/' ;?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <?php
        }
        ?>
        <?php
        if($this->roles_id == SUPER_ADMIN_ROLE_ID || $this->roles_id == ADMIN_ROLE_ID)
        {
        ?>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-gray-light">
                <div class="inner">
                    <h3><?= @$total_order ?></h3>
                    <p>Total Orders</p>
                </div>
                <div class="icon">
                    <i class="fa  fa-opencart"></i>
                </div>
                <a href="<?php echo base_url(ROOT_DIR_ADMIN.ORDER."lists") . '/' ;?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <?php
        }
        ?>
        <?php
        if($this->roles_id == SUPER_ADMIN_ROLE_ID || $this->roles_id == ADMIN_ROLE_ID)
        {
        ?>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-gray-light">
                <div class="inner">
                    <h3><?= @$total_customer ?></h3>
                    <p>Total Customer</p>
                </div>
                <div class="icon">
                    <i class="ion ion-ios-people-outline"></i>
                </div>
                <a href="<?php echo base_url(ROOT_DIR_ADMIN.CUSTOMER."lists") . '/' ;?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-gray-light">
                <div class="inner">
                    <h3><?= number_format(@$order_total_eur[0]->total_prize, 2); ?></h3>
                    <p>Sales in €</p>
                </div>
                <div class="icon">
                    <i class="fa fa-euro"></i>
                </div>
                <a href="<?= A_TAG_DISABLE ?>" class="small-box-footer">&nbsp;</a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-gray-light">
                <div class="inner">
                    <h3><?= number_format(@$order_total_dollor[0]->total_prize, 2); ?></h3>
                    <p>Sales in $</p>
                </div>
                <div class="icon">
                    <i class="ion-social-usd-outline"></i>
                </div>
                <a href="<?= A_TAG_DISABLE ?>" class="small-box-footer">&nbsp;</a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-gray-light">
                <div class="inner">
                    <h3><?= number_format(@$cart_value_euro, 2); ?></h3>
                    <p>Cart Value in €</p>
                </div>
                <div class="icon">
                    <i class="fa fa-euro"></i>
                </div>
                <a href="<?= A_TAG_DISABLE ?>" class="small-box-footer">&nbsp;</a>
            </div>
        </div>
        <?php
        }
        ?>        
        <?php if ($this->roles_id == SUPER_ADMIN_ROLE_ID) { ?>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-gray-light">
                <div class="inner">
                    <h3><?= number_format(@$cart_value_dollor, 2); ?></h3>
                    <p>Cart Value in $</p>
                </div>
                <div class="icon">
                    <i class="ion-social-usd-outline"></i>
                </div>
                <a href="<?= A_TAG_DISABLE ?>" class="small-box-footer">&nbsp;</a>
            </div>
        </div>
       <!-- <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-green-gradient">
                <div class="inner">
                    <h3><?= number_format(@$net_profit_euro, 2); ?></h3>
                    <p>Profit in €</p>
                </div>
                <div class="icon">
                    <i class="fa fa-euro"></i>
                </div>
                <a href="<?= A_TAG_DISABLE ?>" class="small-box-footer">&nbsp;</a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?= number_format(@$net_profit_dollor, 2); ?></h3>
                    <p>Profit in $</p>
                </div>
                <div class="icon">
                    <i class="ion-social-usd-outline"></i>
                </div>
                <a href="<?= A_TAG_DISABLE ?>" class="small-box-footer">&nbsp;</a>
            </div>
        </div>-->
        <?php } ?>
    </div>
</section>
<?php
if($this->roles_id == SUPER_ADMIN_ROLE_ID || $this->roles_id == ADMIN_ROLE_ID)
{
?>
<div class="row" style="margin-left:0px;margin-right:0px;">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Yearly Recap Report</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button>-->
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-center">
                            
                            <strong>Sales: 1 Jan, <?=date('Y')?> - 31 Dec, <?=date('Y')?></strong>
                        </p>

                        <div class="chart">
                            <!-- Sales Chart Canvas -->
                            <canvas id="salesChart" style="height: 180px; width: 703px;" width="703"
                                    height="180"></canvas>
                        </div>
                        <!-- /.chart-responsive -->
                    </div>
                    <!-- /.col -->

                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- ./box-body -->

            <!-- /.box-footer -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<?php
}
?>
<div class="row" style="margin-left:0px;margin-right:0px;">
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Latest Orders</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin">
                        <thead>
                        <tr>
                            <th>Code</th>
                            <th>Shop Name</th>
                            <th>Sender Name</th>
                            <th>Recipient Name</th>
                            <th>Date Time</th>
                            <th>Order Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(count(@$order) > 0) {
                        foreach (@$order as $list_value) {
                            $tr_color = "";
                            if($list_value->order_status == ORDER_STATUS_IN_PROCESS || $list_value->order_status == ORDER_STATUS_BUY)
                            {
                                $tr_color = total_days($list_value->date_time,$list_value->order_id);
                            }
                            ?>
                            <?php
                                if($tr_color == "light_pink")
                                {
                            ?>
                                    <tr class="light-pink">
                            <?php
                                }
                                else if($tr_color == "light_yellow")
                                {
                            ?>
                                    <tr class="light-yellow">
                            <?php
                                }
                                else
                                {
                            ?>
                                    <tr>
                            <?php
                                }
                            ?>
                            
                                <td><?= ucfirst(trim($list_value->generate_code)) ?></td>
                                <td><?= stripslashes(ucfirst(trim($list_value->shop_name))) ?></td>
                                <td><?= stripslashes(ucfirst(trim($list_value->username))) . ' ' . stripslashes(ucfirst(trim($list_value->lastname))); ?></td>
                                <td><?= _set_dash(stripslashes(ucfirst(trim($list_value->receiver_name)))) ?></td>
                                <td><?= date_time($list_value->date_time, 1) ?></td>
                                <?php $status = '';//date_time($v->date_time, 7)
                                if ($list_value->order_status == ORDER_STATUS_WAIT) {
                                    $status .= status_tag(ucfirst(trim($list_value->order_status)), COLOR_W);
                                } else if ($list_value->order_status == ORDER_STATUS_IN_PROCESS) {
                                    $status .= status_tag(ucfirst(trim($list_value->order_status)), COLOR_P);
                                } else if ($list_value->order_status == ORDER_STATUS_DELIVERED) {
                                    $status .= status_tag(ucfirst(trim($list_value->order_status)), COLOR_S);
                                } else if ($list_value->order_status == ORDER_STATUS_BUY) {
                                    $status .= status_tag(ucfirst(trim($list_value->order_status)), COLOR_I);
                                } else if ($list_value->order_status == ORDER_STATUS_CANCEL) {
                                    $status .= status_tag(ucfirst(trim($list_value->order_status)), COLOR_E);
                                }
                                ?>
                                <td><span><?= $status ?></span></td>
                                <td>
                                    <div class="sparkbar" data-color="#00a65a" data-height="20">
                                        <canvas width="34" height="20"
                                                style="display: inline-block; width: 34px; height: 20px; vertical-align: top;"></canvas>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                        <?php } ?>
                        </tbody>
                    </table>                    
                </div>
                <!-- /.table-responsive -->
                <?php
                if(count(@$order) < 1) {
                ?>
                <div style="text-align: center;">No data available in table</div>
                <?php
                }
                ?>
            </div>

            <!-- /.box-body -->
            <div class="box-footer clearfix">
                <!--  <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a> -->
                <?php
                if(count(@$order) > 0) {
                ?>
                    <a href="<?php echo base_url(ROOT_DIR_ADMIN . ORDER . "lists") . '/'; ?>" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
                <?php
                }
                ?>                
            </div>
            <!-- /.box-footer -->
        </div>
    </div>
</div>
<!-- /.content -->
<script>
    $(function () {

        'use strict';

        /* ChartJS
         * -------
         * Here we will create a few charts using ChartJS
         */

        // -----------------------
        // - MONTHLY SALES CHART -
        // -----------------------

        // Get context with jQuery - using jQuery's .get() method.
        var salesChartCanvas = $('#salesChart').get(0).getContext('2d');
        // This will get the first returned node in the jQuery collection.
        var salesChart = new Chart(salesChartCanvas);
        var count_array = [];    
        var month_array = [];    
        <?php 
        $ctr=0;
            foreach (@$order_month_total[0] as $key => $value) { ?>
              count_array['<?=$ctr?>'] = '<?=$value?>';   
              month_array['<?=$ctr?>'] = '<?=$key?>';   
            <?php 
            $ctr++;
            }
        ?>
            
        var salesChartData = {
            labels: month_array,//['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov','Dec'],
            datasets: [
                {
                    label: 'Orders',
                    fillColor: 'rgba(44,251,147,0.1)',
                    strokeColor: 'rgba(44,251,147,1)',
                    pointColor: '#222d32',
                    pointStrokeColor: 'rgba(0,0,0,1)',
                    pointHighlightFill: '#000',
                    pointHighlightStroke: 'rgba(60,141,188,1)',
                    data: count_array//[65, 59, 80, 81, 56, 55, 40]
                }
            ]
        };

        var salesChartOptions = {
            // Boolean - If we should show the scale at all
            showScale: true,
            // Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines: true,
            // String - Colour of the grid lines
            scaleGridLineColor: 'rgba(0,0,0,.05)',
            // Number - Width of the grid lines
            scaleGridLineWidth: 1,
            // Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,
            // Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,
            // Boolean - Whether the line is curved between points
            bezierCurve: true,
            // Number - Tension of the bezier curve between points
            bezierCurveTension: 0.3,
            // Boolean - Whether to show a dot for each point
            pointDot: true,
            // Number - Radius of each point dot in pixels
            pointDotRadius: 3,
            // Number - Pixel width of point dot stroke
            pointDotStrokeWidth: 1,
            // Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius: 20,
            // Boolean - Whether to show a stroke for datasets
            datasetStroke: true,
            // Number - Pixel width of dataset stroke
            datasetStrokeWidth: 2,
            // Boolean - Whether to fill the dataset with a color
            datasetFill: true,
            // String - A legend template
            legendTemplate: '<ul class=\'<%=name.toLowerCase()%>-legend\'><% for (var i=0; i<datasets.length; i++){%><li><span style=\'background-color:<%=datasets[i].lineColor%>\'></span><%=datasets[i].label%></li><%}%></ul>',
            // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: true,
            // Boolean - whether to make the chart responsive to window resizing
            responsive: true
        };

        // Create the line chart
        salesChart.Line(salesChartData, salesChartOptions);
    });
</script>