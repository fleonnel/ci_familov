<!-- Main content -->
<section class="content">
    <div class="row">
        <?php _notify(); ?>
    </div>
    <div class="row">
        <div class="container">
            <div class="row text-center features-block c3">
                <div class="col-md-5">
                    <div class="gt_main_services bg_3">
                        <i class="fa fa-money"></i>

                        <h1><span class="number" style="font-size: 40px;"><?= @$order_total['total_prize'] ?> €</span>
                        </h1>
                        <h5>sales</h5>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="gt_main_services bg_3">
                        <i class="fa fa-smile-o"></i>

                        <h1><span class="number" style="font-size: 40px;"> <?= @$order_delivered ?> </span></h1>
                        <h5>Delivered </h5>
                    </div>
                </div>
                <div class="col-md-3" style="background: #f44336;">
                    <div class="gt_main_services bg_3" style="background: #f44336;">
                        <i class="fa fa-frown-o"></i>

                        <h1><span class="number"
                                  style="font-size: 40px;background: #f44336;"> <?= @$order_to_delivered ?> </span></h1>
                        <h5> to delivered</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="container">
            <div class="row text-center features-block c3">
                <div class="col-md-4">
                    <div class="gt_main_services bg_3">
                        <i class="fa fa-trophy"></i>

                        <h2><span class="number" style="font-size: 40px;"> <?= @$total_order ?> </span></h2>
                        <h5>orders</h5>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="gt_main_services bg_3" style="background: orange;">
                        <i class="fa fa-clock-o"></i>

                        <h2><span class="number"
                                  style="font-size: 40px;background: orange;"> <?= @$order_wait ?> </span></h2>
                        <h5>wait </h5>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="gt_main_services bg_3">
                        <i class="fa fa-user"></i>

                        <h2><span class="number" style="font-size: 40px;"> <?= @$total_customer ?></span></h2>
                        <h5> user </h5>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="gt_main_services bg_3">
                        <i class="fa fa-calculator"></i>

                        <h2><span class="number" style="font-size: 40px;">42.86 €</span></h2>
                        <h5> cart Value </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--<div class="col-lg-4 col-xs-6">
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3><? /*= @$total_request */ ?></h3>
                <p>Request</p>
            </div>
            <div class="icon">
                <i class="fa  fa-list"></i>
            </div>
            <a href="<? /*= admin_url(REQUEST_SECTION_C) */ ?>" class="small-box-footer">More info <i
                    class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-lg-4 col-xs-6">
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3><? /*= @$total_ticket */ ?></h3>

                <p>Ticket</p>
            </div>
            <div class="icon">
                <i class="fa fa-link"></i>
            </div>
            <a href="<? /*= admin_url(TICKET_SECTION_C) */ ?>" class="small-box-footer">More info <i
                    class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>-->

</section>
<style type="text/css">.bg_3 {
        background: #36cd75;
        border-radius: 3px;
    }

    .gt_main_services {
        float: left;
        width: 100%;
        padding: 40px 30px 50px;
        position: relative;
    }

    .gt_main_services h5 {
        margin-bottom: 15px;
        font-weight: 600;
        margin-top: 25px;
        color: #fff;
        font-size: 20px;
    }

    .gt_main_services > i {
        font-size: 60px;
        margin-bottom: 23px;
        display: block;
        color: #fff;
    }

    i {
        font-family: 'iconfont';
        speak: none;
        font-style: normal;
        font-weight: normal;
        font-variant: normal;
        text-transform: none;
        line-height: 1;
        -webkit-font-smoothing: antialiased;
    }
</style>

<!-- /.content -->
