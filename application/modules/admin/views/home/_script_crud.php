    <script>
        $(document).ready(function () {
            $("#crud_form").validate({
                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'help-block',
                errorPlacement: function (error, element) { // render error placement for each input type
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                },
                ignore: "",
                rules: {
                    day_out: {
                        required: true
                    }
                },
                messages: {
                    day_out: {
                        required: 'Please Enter Day Out Number.'
                    }
                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    //successHandler1.hide();
                    //errorHandler1.show();
                },
                highlight: function (element) {
                    $(element).closest('.help-block').removeClass('valid');
                    // display OK icon
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                    // add the Bootstrap error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error');
                    // set error class to the control group
                },
                success: function (label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function (frmadd) {
                    successHandler1.show();
                    errorHandler1.hide();
                }
            });
        });
    </script>
<script type="text/javascript">
        var list_table_one;
        var list_url = "<?php echo $list_url; ?>";
        $(document).ready(function () {
            var name_of_file = "<?php echo string_prefix($export_prefix,_str_repalace("-","_",get_current_date()));?>";
            var export_coulmn = "0,1";
            <?php //TODO Change Name for export file here ?>
            list_table_one = $('#list_table_one').DataTable({
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [[1, 'asc']], //Initial no order.
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": list_url,
                    "type": "POST"
                },
                //Set column definition initialisation properties.
                "columnDefs": [
                    {
                        <?php //TODO Set Column number here for disable sorting ?>
                        "targets": [0], //first column / numbering column
                        "orderable": false, //set not orderable
                    },
                ],
                pageLength: 10,
                responsive: true,

            });
        });
        function reload_table() {
            list_table_one.ajax.reload(null, false); //reload datatable ajax
        }
</script>
