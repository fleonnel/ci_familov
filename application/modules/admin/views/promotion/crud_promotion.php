<!-- Select2 -->
<script src="<?php echo base_url(ASSETS_ADMIN . 'plugins/select2/select2.full.min.js'); ?>"></script>
<?php
$action_url = '';
$list_url = admin_url(PROMOTION . 'lists');
?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><strong><?= @$level_two ?></strong></h3>
                        <span class="pull-right text-muted">Mandatory information is marked with an asterisk <span
                                class="text-red">*</span></span>
                </div>
                <!-- /.box-header -->
                <?php _notify(); ?>
                <div class="box-body">
                    <?php echo form_open('', 'class="form-horizontal" id="crud_form"  name="crud_form" enctype="multipart/form-data"'); ?>
                    <div class="box-body">
                       <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Promo code<span class="text-red">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="vCode"  name="vCode" placeholder="Enter Code" value="<?php echo edit_display('vCode', @$info); ?>">
                                    </div>
                        </div>
                        <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Amount<span class="text-red">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="iAmount"  name="iAmount" placeholder="Enter an Amount" value="<?php echo edit_display('iAmount', @$info); ?>">
                                    </div>
                        </div>
                        <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Description<span class="text-red">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="vDescription"  name="vDescription" placeholder="Enter an Description" value="<?php echo edit_display('vDescription', @$info); ?>">
                                    </div>
                        </div>
                        <div class="form-group">
                                <label class="col-sm-2 control-label">Status<span class="text-red">*</span></label>
                                <div class="col-sm-6">
                                    <select class="form-control select2" name="eStatus" id="eStatus" data-placeholder="Choose status" style="display: none;">
                                        <option value="Active" <?php if(@$info->eStatus=="Active") { echo "selected='selected'"; } ?>>
                                            Active
                                        </option>
                                        <option value="Inactive" <?php if(@$info->eStatus=="Inactive") { echo "selected='selected'"; } ?>>
                                            Inactive
                                        </option>
                                    </select>
                                </div>
                            </div>
                        <div class="form-group">
                                    <label class="col-sm-2 control-label">Start Date</label>
                                    <div class="col-sm-6">
                                        
                                        <input class="form-control form-control-flat" id="start_date" name="start_date" placeholder="Enter an Start Date" value="<?php echo edit_display('start_date', @$info); ?>" >
                                    </div>
                        </div>
                         <div class="form-group">
                                    <label class="col-sm-2 control-label">End Date</label>
                                    <div class="col-sm-6">
                                        
                                        <input class="form-control form-control-flat" id="end_date" name="end_date" placeholder="Enter an End Date" value="<?php echo edit_display('end_date', @$info); ?>" >
                                    </div>
                        </div>
                        <div class="form-group">
                            <label for="inputlbl" class="col-sm-2 control-label"></label>

                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success ">Save</button>
                                <a href="<?php echo $list_url; ?>">
                                    <button type="button" class="btn btn-default">Cancel</button>
                                </a>
                            </div>
                        </div>

                    </div>
                    <?php echo form_close(); ?>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                </div>
                <!-- /.box-footer -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
<?php include_once("_script_crud.php"); ?>
<script>
    $(document).ready(function () {
        $("#crud_form").validate({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                error.insertAfter(element);
                // for other inputs, just perform default behavior
            },
            ignore: "",
            rules: {
                vCode: {
                    required: true
                },
                vDescription: {
                    required: true
                },
                iAmount: {
                    required: true,
                    number: true
                },
                eStatus: {
                    required: true
                },
                start_date: {
                    required: true,
                },
                end_date: {
                    required: true,
                },
                
                
            },
            messages: {
                vCode: {
                    required: 'Please Enter Promo Code.'
                },
                vDescription: {
                    required: 'Please Code Description.'
                },
                iAmount: {
                    required: 'Please Enter a Amount.',
                    number:'Please Enter a Number Only.'
                },
                eStatus: {
                    required: 'Please Select a Status.'
                },
                start_date: {
                    required: 'Please Enter a Start Date.',
                },
                end_date: {
                    required: 'Please Enter a End Date.',
                },
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                //successHandler1.hide();
                //errorHandler1.show();
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            },
            submitHandler: function (frmadd) {
                successHandler1.show();
                errorHandler1.hide();
            }
        });
    });
</script>