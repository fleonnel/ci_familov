<?php
$action_url = '';
$list_url = admin_url(PERMISSION_C . 'lists');
?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-3">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Roles</h3>

                    <div class="box-tools">
                        <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                 class="fa fa-minus"></i>
                         </button>-->
                    </div>
                </div>
                <div class="box-body no-padding">
                    <ul class="nav nav-pills nav-stacked">
                        <?php
                        foreach (@$list_roles as $k => $v) { ?>
                            <li class="<?= (@$roles_id == $v->roles_id) ? "active" : "" ?>">
                                <a href="<?= admin_url(PERMISSION_C . 'add_update/' . $v->roles_id) ?>"><i
                                        class="fa fa-circle text-green"></i> <?= $v->name ?>
                                </a>
                            </li>
                        <?php
                        }
                        ?>
                        <!--<li class="active"><a href="#"><i class="fa fa-inbox"></i> Admin
                                <span class="label label-primary pull-right">12</span></a></li>-->
                    </ul>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /. box -->
            <!--<div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Labels</h3>

                    <div class="box-tools">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body no-padding">
                    <ul class="nav nav-pills nav-stacked">
                        <li><a href="#"><i class="fa fa-circle-o text-red"></i> Important</a></li>
                        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> Promotions</a></li>
                        <li><a href="#"><i class="fa fa-circle-o text-light-blue"></i> Social</a></li>
                    </ul>
                </div>
            </div>-->
            <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-unlock-alt text-black"></i> <?= @$role_name ?></h3>

                    <div class="box-tools pull-right">
                        <!--<div class="has-feedback">
                            <input type="text" class="form-control input-sm" placeholder="Search Mail">
                            <span class="glyphicon glyphicon-search form-control-feedback"></span>
                        </div>-->
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <?php
                    _notify();
                    if (count(@$list_permission) == 0) { ?>
                        <div class="callout callout-info">
                            <h4><?= MESSAGE_DATA_AVAILABLE ?></h4>
                        </div>
                    <?php } else { ?>
                        <!--<div class="mailbox-controls">
                            <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
                        </div>-->
                        <?php #_pre(@$list_permission,0)?>
                        <form class="form-horizontal" name="crud_form" id="crud_form" method="post">
                            <div class="table-responsive mailbox-messages">
                                <table class="table table-hover table-striped">
                                    <thead>
                                    <tr>
                                        <th>Module Name</th>
                                        <th>View</th>
                                        <th>Add</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach (@$list_permission as $pk => $pv) {
                                        if (count(@$list_roles_permission) > 0) {
                                            $permission_roles_id = @$list_roles_permission->roles_id;
                                            $permission_access_data = json_decode(@$list_roles_permission->access_data);
                                            $permission_access_data = @$permission_access_data->$pk;
                                        }
                                        ?>
                                        <tr>
                                            <td><strong> <i class="<?= @$pv['icon'] ?>"></i> <?= $pv['title'] ?>
                                                </strong></td>
                                            <?php
                                            if (count($pv['actions']) >= 0) {
                                                foreach ($pv['actions'] as $ak => $av) {
                                                    #_pre($ak,0);
                                                    if ($ak != 'close' && $ak != 'complete') {
                                                    ?>
                                                    <td><input type="checkbox"
                                                               name="action[<?= $roles_id ?>][<?= $pk ?>][]"
                                                               id="<?= $pk ?>"
                                                               value="<?= $ak ?>" <?php if (count(@$permission_access_data) && in_array($ak, @$permission_access_data)) echo 'checked="checked"' ?> >
                                                    </td>
                                                    <?php }
                                                } ?>
                                                <!--
                                                      <td><input type="checkbox" name="<? /*=$pk*/
                                                ?>" id="" value=""></td>
                                                      <td><input type="checkbox" name="<? /*=$pk*/
                                                ?>" id="" value=""></td>
                                                      <td><input type="checkbox" name="<? /*=$pk*/
                                                ?>" id="" value=""></td>
                                                -->
                                            <?php } else { ?>
                                                <td><span class="label btn-warning text-center"><?= N_A ?></span></td>
                                                <td><span class="label btn-warning text-center"><?= N_A ?></span></td>
                                                <td><span class="label btn-warning text-center"><?= N_A ?></span></td>
                                                <td><span class="label btn-warning text-center"><?= N_A ?></span></td>
                                            <?php } ?>

                                        </tr>
                                    <?php }?>

                                    </tbody>
                                </table>
                                <div class="mailbox-controls">
                                    <div class="col-md-8 col-md-offset-3">
                                        <button type="submit" class="btn btn-success ">Save</button>
                                        <a href="<?php /*echo $list_url; */
                                        ?>">
                                            <button type="button" class="btn btn-default">Cancel</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    <?php } ?>

                </div>
                <!-- /.box-body -->
                <div class="box-footer no-padding">
                </div>
            </div>
            <!-- /. box -->
        </div>
        <!-- /.col -->
    </div>
</section>
<!-- /.content -->
<?php include_once("_script_crud.php"); ?>
