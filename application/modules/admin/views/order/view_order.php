<?php
$list_url = admin_url(ORDER);
        //     echo "<pre>";
        //     print_r($order_details);
        // echo "</pre>";exit;
?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header" style="text-align:center;">
                    <h3 class="box-title"><strong>Order Details </strong></h3>
                    <div class="rom-md-12">
                        <a href="<?php echo $list_url; ?>">
                            <button type="button" class="btn btn-default pull-right">Back</button>
                        </a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php _notify(); ?>
                    <div id="detail_section">

            <div id="printableArea">
                        <img src="<?php echo base_url() ?>/uploads/images/familov-logo.png" class="text-center" alt="..." style="margin-top: 10px;margin-left: 41%; margin-bottom:30px" width="250px">
                        <p id="sender_name"> <b>Magasin : </b><span id="sendername"><?= @$shop_name->shop_name ?></span></p> <br>
                        <p id="generate_code" style=" font-size: 20px; font-weight: bold;">Code : <span id="gcode"><?= @$info->generate_code ?></span></p>
                        <p id="user_name">De : <span id="username"><?= @$customer_name->username ?> <?= @$customer_name->lastname ?></span></p>
                        <p id="receiver_name">Pour : <span id="gname"><?= @$info->receiver_name ?> </span></p>
                        <p id="receiver_phone">contact : <span id="gphone"><?= @$info->receiver_phone ?></span></p>
                        <p id="sender_name">Mode de livraison : <span class="delivery_type">Pick up from the shop</span></p> <br>
                        <p id="receiver_msg"><b>Message pour le bénéficiaire :</b> <span id="greeting_msg">-</span></p>
                    <hr>
                    <table width="600" class="table">
                        <thead>
                        <tr>
                            <th>Produits</th>
                            <th>Quantités</th>
                        </tr>
                        </thead>
                        <tbody id="order_body">
                            <?php foreach ($order_details as $order) { ?>
                            <tr>
                                <td><b><?php echo $order->product_name;?></b></td>
                                <td> <?php echo $order->quantity;?>x</td>
                            </tr>
                            <?php  } ?>
                        </tbody>
                        <tbody>
                        </tbody>
                    </table>
                    <br>
                        <p id="sender_name"> <b>NB : Merci de bien vérifier  la commande avant toute livraison :) !!!</b></p> 
                </div>
            </div>
            <div class="modal-footer">
                    <a href="#" class="btn btn-success pull-right" aria-hidden="true" onclick="printDiv('printableArea')"><i class="icon-white icon-print"></i> IMPRIMER la commande </a>&nbsp;<!--<button data-dismiss="modal" aria-hidden="true" class="btn btn-inverse btn-small"><i class="icon-white icon-remove"></i> Close</button>-->
                    
                    
                </div>
                    <?php //echo _header_footer(@$info->email_html); ?>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
<?php include_once("_script_crud.php"); ?>