<?php
#TODO : Change Controller Name, Add Button Text, Export Prefix name, Column Order in Datatable js.
$list_url = admin_url(ORDER. "data_list");
$add_update_url = admin_url(ORDER . "add");
$export_prefix = 'order_';
?>
<style type="text/css">
    @media screen {
    #printSection {
        display: none;
    }
}
@media print {
    body * {
        visibility:hidden;
    }
    #printSection, #printSection * {
        visibility:visible;
    }
    #printSection {
        position:absolute;
        left:0;
        top:0;
    }
}
</style>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                <?php
                if($this->roles_id == SUPER_ADMIN_ROLE_ID || $this->roles_id == ADMIN_ROLE_ID)
                {}
                else
                {
                ?>                
<!--<div class="warper container-fluid">
    <div>PENSÉE DE LA SEMAINE :  La vie est comme une bicyclette, il faut avancer pour ne pas perdre l’équilibre.
    </div>
    <br>
    <div class="page-header">
        <h4 style="background:yellow ; padding:15px;">
            <b><u>NOTE IMPORTANTE</u> </b>:
            <br> 
            Hello chers Managers, 
            <br>les clients apprécient vraiment lorsque leurs proches qui viennent rétirer le paquet recoivent un acceuil chaleureux  et n´attendent pas longtemps . Merci également de passer aussitôt  les commandes déjà livrées  au status "Delivered" afin que le client soit immédiatement  notifié que le paquet a été bien livré  à sa famille!<br> Merci et  Belle semaine :) 
        </h4> 
        <div> Il ya actuellement : <?= @$total_order_in_process; ?> commandes en attente de livraison ! </div>
        
        <div class="row">
            <div class="col-lg-6 col-xs-6">
                <div class="small-box bg-red">
                    <div class="inner" style="text-align: center;">
                        <p style="font-size: 70px;"><i class="fa fa-frown-o"></i></p>
                        <h1><?= @$total_order_in_process; ?></h1>
                        <p><h2>En attente <br> de livraison</h2></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-xs-6">
                <div class="small-box bg-green">
                    <div class="inner" style="text-align: center;">
                        <p style="font-size: 70px;"><i class="fa fa-smile-o"></i></p>
                        <h1><?= @$total_order_buy; ?></h1>
                        <p><h2>déjà livrées <br> aux clients</h2></p>
                    </div>
                </div>
            </div>                                
        </div>
    </div>
   
</div>
 <!-- Warper Ends Here (working area) -->
                <?php
                }
                ?>
                    <?php _notify(); ?>
                    
                    <div style="height:100%;overflow:auto; width:100%">
                    <table id="list_table_one"
                           class="table table-striped1 table-bordered table-hover1 dataTables-example newTab" style="font-weight: 600;">
                        <thead>
                        <tr>
                            <th></th>
                            <th style="width:95px;">Code</th>
                            <th>Shop </th>
                            <th>From </th>
                            <th>To </th>
                            <th style="width:123px;">Phone</th>
                            <?php
                            if($this->roles_id != 3){
                            ?>
                                <th>Total</th>
                            <?php
                            }
                            ?>
                            <?php if ($this->roles_id == SUPER_ADMIN_ROLE_ID ) { ?>
                            <th>Paid </th>
                            <?php } ?>
                            <th>Date</th>
                            <th>Mode</th>
                            <?php if ($this->roles_id == SUPER_ADMIN_ROLE_ID ) { ?>
                            <th>Net </th>
                            <?php } ?>
                            <th>Detail</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                
                </div>
                 <div class="col-md-12 margin-bottom pull-right">
                            <div class="col-md-9">
                                
                            </div>
                            
                            <?php
                                if($this->roles_id == SUPER_ADMIN_ROLE_ID || $this->roles_id == ADMIN_ROLE_ID)
                                {
                            ?>
                            <div class="col-md-3">
                                <select class="form-control request-filter-option" style="width: 100%;"
                                        name="order_status" id="order_status">
                                    <option value="">All Status</option>
                                    <option value="<?= ORDER_STATUS_WAIT ?>">Awaiting for payment</option>
                                    <option value="<?= ORDER_STATUS_BUY ?>">Payment accepted</option>
                                    <option value="<?= ORDER_STATUS_IN_PROCESS ?>">In Process</option>
                                    <option value="<?= ORDER_STATUS_DELIVERED ?>">Delivered</option>
                                    <option value="<?= ORDER_STATUS_CANCEL ?>">Cancel</option>
                                </select>
                            </div>
                            <?php
                                }
                            ?>
                    </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
</section>
<!-- /.content -->

<script type="text/javascript">
    var list_table_one;
    var list_url = "<?php echo $list_url; ?>";
    $(document).ready(function () {
        var name_of_file = "<?php echo string_prefix($export_prefix,_str_repalace("-","_",get_current_date()));?>";
        var export_coulmn = "0,1";
        <?php //TODO Change Name for export file here ?>
        list_table_one = $('#list_table_one').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": list_url,
                "type": "POST",
                "data": function (data) {
                        data.order_status = $('#order_status').val();
                    }
            },
            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    <?php //TODO Set Column number here for disable sorting ?>
                    <?php if ( $this->roles_id == SUPER_ADMIN_ROLE_ID ) { ?>
                    "targets": [0, 1, 5, 6, 7,8,9,10,11,12,13], //first column / numbering column
                    <?php }else{?>
                        <?php
                        if( $this->roles_id == 3 )
                        {
                        ?>
                            "targets": [0, 1, 5, 6, 8,9,10],
                        <?php
                        }
                        else
                        {
                        ?>
                            "targets": [0, 1, 5, 6, 7,8,9,10,11,12],
                        <?php
                        }
                        ?>                        
                    <?php } ?>
                    "orderable": false, //set not orderable
                },
            ],
            pageLength: 10,
            responsive: true,
            "autoWidth": false,
            "createdRow": function ( row, data, index ) {
                if(row.firstChild.firstChild.hasAttribute && row.firstChild.firstChild.hasAttribute('class'))
                {
                    if(row.firstChild.firstChild.getAttribute('class') == "light_yellow")
                        $(row).addClass('light-yellow');
                    if(row.firstChild.firstChild.getAttribute('class') == "light_pink")
                        $(row).addClass('light-pink');
                }
            }
        });
    });
    function reload_table() {
        list_table_one.ajax.reload(null, false);
    } //reload datatable ajax
        $(".request-filter-option").change(function () {
            list_table_one.ajax.reload(null, false);
        });
        $(".request-filter-option").keyup(function () {
            list_table_one.ajax.reload(null, false);
        });
</script>
<?php #include_once("_script_crud.php"); ?>
<script>
    function printDiv() {
        /*var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;*/

        /*document.body.innerHTML = printContents;
         window.print();
         document.body.innerHTML = originalContents;
         */

        /*var myWindow = window.open('', '', '');        
        myWindow.document.write($('#' + divName).html());
        myWindow.document.close();
        myWindow.focus();
        myWindow.print();
        myWindow.close();*/

        printElement(document.getElementById("myModal"));
    }
    function printElement(elem) {
    var domClone = elem.cloneNode(true);

    var $printSection = document.getElementById("printSection");

    if (!$printSection) {
        var $printSection = document.createElement("div");
        $printSection.id = "printSection";
        document.body.appendChild($printSection);
    }

    $printSection.innerHTML = "";
    $printSection.appendChild(domClone);
    window.print();
}
</script>
<script>
    function action_change(value, id,code,old_status) {
        var url = "<?php echo base_url(ROOT_DIR_ADMIN.ORDER."change_status") . '/' ;?>";
        var userdata = "id=" + id + "&order_status=" + value;
        var p_name = $("#o_" + id).data('number');
        var popup_title = "<?=CONFIRMATION_MESSAGE_FOR_STATUS_UPDATE_TITLE?>";
        popup_title = popup_title.replace("#ORDER_NO#", code);
        popup_title = popup_title.replace("#OLD_STATUS#",old_status);
        popup_title = popup_title.replace("#NEW_STATUS#",value);

        swal({
                title: popup_title,
                text: "<?=CONFIRMATION_MESSAGE_FOR_STATUS_TEXT?>",
                type: "<?=SWEET_ALERT_INFO?>",
                showCancelButton: true,
                confirmButtonColor: "<?=SWEET_ALERT_CONFIRMATION_COLOR?>",
                confirmButtonText: "<?=SWEET_ALERT_CONFIRMATION_BUTTON_YES_TEXT?>",
                cancelButtonText: "<?=SWEET_ALERT_CONFIRMATION_BUTTON_CANCEL_TEXT?>",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: userdata,
                        dataType: "JSON",
                        cache: false,
                        success: function (data) {
                            if (data.FLASH_STATUS == '<?=FLASH_STATUS_SUCCESS?>') //if success close modal and reload ajax table
                            {
                                reload_table();
                                var STATUS_TYPE = '';
                                STATUS_TYPE = '<?=MESSAGE_CHANGE_STATUS_SUCCESS?>';
                                toastr.success(data.FLASH_MESSAGE, '<?=SUCCESS?>');
                            }
                            else if (data.FLASH_STATUS == '<?=FLASH_STATUS_ERROR?>') {
                                toastr.error(data.FLASH_MESSAGE, '<?=ERROR?>');
                            } else {
                                toastr.error('<?=MESSAGE_CHANGE_STATUS_ERROR?>', '<?=ERROR?>');
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            toastr.error('<?=DATA_TABLE_ERROR?>', '<?=ERROR?>');
                        }
                    });
                } else {
                    toastr.info('<?=MESSAGE_LAST_OPERATION_DISCARD?>', '<?=INFO?>');
                }
            });
        //});
    }
</script>
<script language="javascript">
    function action_details(id) {
        <?php 
        if ($this->roles_id == SUPER_ADMIN_ROLE_ID ) { 
            ?>
             var url = "<?php echo base_url(ROOT_DIR_ADMIN.ORDER."view_order") . '/' ;?>";
              <?php  } else { ?>
                  var url = "<?php echo base_url(ROOT_DIR_ADMIN.ORDER."view_order_manager") . '/' ;?>";
             <?php } ?>

       
        var order_status = $('#order_status').val();
        var userdata = "id=" + id + "&order_status=" + order_status;
        //alert(userdata);
        $.ajax({
            type: "POST",
            url: url,
            data: userdata,
            dataType: "JSON",
            cache: false,
            success: function (data) {
                var resdata = data;
                document.getElementById('modal-body').innerHTML = data.html;
                document.getElementById('myModalLabel').innerHTML = data.order_title;

                if (data.FLASH_STATUS == '<?=FLASH_STATUS_SUCCESS?>') //if success close modal and reload ajax table
                {
                    $('#myModal').modal('show');
                    //toastr.success(data.FLASH_MESSAGE, '<?=SUCCESS?>');
                }
                else if (data.FLASH_STATUS == '<?=FLASH_STATUS_ERROR?>') {
                    toastr.error(data.FLASH_MESSAGE, '<?=ERROR?>');
                } else {
                    toastr.error('<?=MESSAGE_VIEW_ERROR?>', '<?=ERROR?>');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error('<?=DATA_TABLE_ERROR?>', '<?=ERROR?>');
            }
        });
    }
    // var size=document.getElementById('mysize').value;
</script>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body" id="modal-body">
            </div>
            <div class="modal-footer" id="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <?php 
        if ($this->roles_id != SUPER_ADMIN_ROLE_ID ) { 
            ?>
            <!--<button type="button" class="btn btn-danger" onClick="opendialogbox();" >Upload Billing</button>-->
          
            <?php }else{ ?> 
                                
                                 <!-- <div class="form-group">
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="order_bill_price"  name="order_bill_price" placeholder="Enter Price" value="<?php echo edit_display('order_bill_price', @$info); ?>"  required>
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-success " onclick="savedata(<?= @$data->order_id ?>);">Save</button>
                                    </div>
                                </div> -->
                            
                <button type="button" class="btn btn-success" onClick="opendialogbox1();" >View Billing</button>
            <?php } ?>
            </div>
        </div>
    </div>
</div>

<!-- // order id
// order number
// Recipient name
// Recipient Phone editable -->
<div class="modal fade" id="edit_recipient_phone">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit Recipient Phone</h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-danger" id="editImgJobMsg" style="display: none; margin: 5vh; margin-bottom: auto;">
            <button class="close" data-close="alert"></button>
            <span></span>
        </div>        
            <input type="hidden" name="order_id" id="order_id">
            <p><div class="form-group">
                <label>Order Number: &nbsp;</label><span id="generate_code"></span>
            </div></p>
            <p><div class="form-group">
                <label>Recipient Name: &nbsp;</label><span id="receiver_name"></span>
            </div></p>
            <p><div class="form-group">
                <label>Recipient Phone</label>
                <input type="text" class="form-control" placeholder="Enter Recipient Phone" name="receiver_phone" id="receiver_phone">
                <span class="error recipientPhoneError" style="color: red;display: none;" id="err_recipient_phone">Enter Recipient Phone</span>
            </div></p>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left no-print" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" onclick='submit_form_edit_recipient_phone()'>Save changes</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<script type="text/javascript">
function opendialogbox(){
$('#myModal1').modal('show');;
}
</script>
<script type="text/javascript">
function opendialogbox1(inputid){
$('#myModal2').modal('show');;
}
</script>
<script>
function deletbill(id){
    var base_url="<?= base_url(); ?>";
  $.ajax({
    url: base_url + 'admin/order/remove/'+ id, //maintains the (controller/function/argument) logic in the MVC pattern
    type: 'post',
    success: function(data){
        $('#idrow_'+id).remove();
        location.reload();
        console.log(data);
    },
    error: function(a,b,c){
        console.log(a,b,c);
    }
});
}
</script>
<script type="text/javascript">
function savedata(id)
{
 var base_url="<?= base_url(); ?>";
 var url= base_url + 'admin/order/savebilldata';
 var price=document.getElementById( "order_bill_price" ).value;
  $.ajax({
  type: 'post',
  url: url,
  data: {
   id: id, price: price,
  },
  success: function (data) {
    toastr.success(data.FLASH_MESSAGE, '<?=SUCCESS?>');
  }
  });
 
}
function editRecipientPhone(order_id) {
    // order id
    // order number
    // Recipient name
    // Recipient Phone editable

    $("#order_id").val();
    $("#generate_code").text();
    $("#receiver_name").text();
    $("#receiver_phone").val('');

    var admin_url = "<?= admin_url(); ?>";
    admin_url = admin_url + "order/getOrderInfo/" + order_id;    
    $.ajax({
        url: admin_url, //maintains the (controller/function/argument) logic in the MVC pattern
        type: 'get',
        success: function(data){
            data = JSON.parse(data);
            if(data.status == "true") {
                $("#order_id").val(data.order_id);
                $("#generate_code").text(data.generate_code);
                $("#receiver_name").text(data.receiver_name);
                $("#receiver_phone").val(data.receiver_phone);                
                $("#edit_recipient_phone").modal("show");
            } else {

            }
        },
        error: function(a,b,c){
            console.log(a,b,c);
        }
    });
}
function submit_form_edit_recipient_phone() {
    
    $(".recipientPhoneError").css('display','none');
    var receiver_phone = $("#receiver_phone").val();
    var order_id = $("#order_id").val();
    if(receiver_phone == null || receiver_phone == undefined || receiver_phone == '') {
        $("#err_recipient_phone").css('display','');
    } else {
        var admin_url = "<?= admin_url(); ?>";
        admin_url = admin_url + "order/update_Recipient_Phone/";
        var data = {
            receiver_phone: receiver_phone,
            order_id: order_id
        }
        $.ajax({
            url: admin_url, //maintains the (controller/function/argument) logic in the MVC pattern
            type: 'post',
            data: data,
            success: function(data){
                var response = JSON.parse(data);
                if(response.status == "true" || response.status == true)
                {
                    $("#order_id").val();
                    $("#generate_code").text();
                    $("#receiver_name").text();
                    $("#receiver_phone").val('');
                    $("#edit_recipient_phone").modal('hide');                
                    toastr.success('Success', 'Recipient phone is updated.', {positionClass: 'toast-top-right'});
                    reload_table();
                }
                else
                {
                    toastr.error('Error', 'Recipient phone is not updated.', {positionClass: 'toast-top-right'});
                    $("#editImgJobMsg").find('span').html(response.message);
                    $("#editImgJobMsg").css('display','');
                }
            },
            error: function(a,b,c){
                toastr.error('Error', 'Recipient phone is not updated.', {positionClass: 'toast-top-right'});
                $("#editImgJobMsg").find('span').html(response.message);
                $("#editImgJobMsg").css('display','');
            }
        });
    }

}
</script>


<style>
    .sa-button-container > .cancel {
        background-color: #dc3545 !important;
    }

    .sa-button-container > .confirm {
        background-color: #3bb33b !important
    }
    
  @media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}
</style>