<?php
$action_url = '';
$list_url = admin_url(ADMIN_C . 'lists');
?>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"><strong><?=@$level_two?></strong></h3>
                        <span class="pull-right text-muted">Mandatory information is marked with an asterisk <span class="text-red">*</span></span>
                    </div>
                    <!-- /.box-header -->
                    <?php _notify(); ?>
                    <div class="box-body">
                        <form class="form-horizontal" name="crud_form" id="crud_form" method="post">
                            <div class="box-body">
                                <div class="form-group ">
                                    <label for="inputlbl" class="col-sm-2 control-label">First Name <span class="text-red">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="first_name"  name="first_name" placeholder="Enter First Name" value="<?php echo edit_display('first_name', @$info); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Last Name <span class="text-red">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control"  id="last_name"  name="last_name" placeholder="Enter Last Name"  value="<?php echo edit_display('last_name', @$info); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Email <span class="text-red">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="email" class="form-control"  id="email"  name="email" placeholder="Enter Email Id" value="<?php echo edit_display('email', @$info); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Admin Type<span class="text-red">*</span></label>
                                    <div class="col-sm-6">
                                        <select class="form-control select2" style="width: 100%;" name="admin_type_id" id="admin_type_id">
                                            <?php foreach ($list_roles as $key_at => $value_at) { ?>
                                                <option
                                                    value="<?= $value_at->roles_id ?>" <?php echo (edit_display('roles_id', @$info) == $value_at->roles_id) ? "selected" : '' ?>><?= $value_at->name ?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Admin Status<span class="text-red">*</span></label>
                                    <div class="col-sm-6">
                                        <select class="form-control select2" style="width: 100%;" name="admin_status" id="admin_status">
                                            <?php foreach ($list_admin_status as $key_status => $value_status) { ?>
                                                <option
                                                    value="<?= $key_status ?>"  <?php echo (edit_display('admin_status', @$info) == $key_status) ? "selected" : '' ?>><?= $value_status ?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label"></label>
                                    <div class="col-sm-6">
                                        <button type="submit" class="btn btn-success ">Save</button>
                                        <a href="<?php echo $list_url; ?>">
                                            <button type="button" class="btn btn-default">Cancel</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

<?php include_once("_script_crud.php"); ?>