<?php
#TODO : Change Controller Name, Add Button Text, Export Prefix name, Column Order in Datatable js.
$list_url = admin_url(ADMIN_C  . "data_list");
$add_update_url = admin_url(ADMIN_C . "add");
$change_admin_manager_password_url = admin_url(ADMIN_C . "change_admin_manager_password");
$update_status_url = admin_url(ADMIN_C . "update_status") . '/';
$delete_url = admin_url(ADMIN_C . "action_delete") . '/';
$btn_add_text = 'Add Admin';
$export_prefix = 'Admin_';
?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><strong><?=@$level_two?></strong></h3>
                    <div class="rom-md-12">

                        <?php /*if($this->acl_model->isAccess($this->class, $this->method)) { */ ?>
                        <a href="<?php echo $add_update_url; ?>">
                            <button type="button" class="btn btn-success pull-right"><?= $btn_add_text ?></button>
                        </a>
                        <?php /*} */ ?>
                        <?php
                            if ($this->roles_id == SUPER_ADMIN_ROLE_ID) {
                        ?>
                                <a href="<?php echo $change_admin_manager_password_url; ?>">
                                    <button type="button" class="btn btn-success pull-right" style="margin-right: 25px;">Update Admin/Manager Password</button>
                                </a>
                        <?php
                            }
                        ?>
                        
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php _notify(); ?>
                    <table id="list_table_one" class="table table-striped table-bordered table-hover dataTables-example newTab">
                        <thead>
                        <tr>
                            <th>No#</th>
                            <th>Full Name</th>
                            <th>Email</th>
                            <th>Type</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
<?php include_once("_script_crud.php"); ?>