<script src="<?php echo base_url(ASSETS_ADMIN . 'plugins/select2/select2.full.min.js'); ?>"></script>
<?php
$list_url = admin_url(ADMIN_C);
?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><strong><?= @$info->name ?></strong></h3>

                    <div class="rom-md-12">
                        <a href="<?php echo $list_url; ?>">
                            <button type="button" class="btn btn-default pull-right">Back</button>
                        </a>
                    </div>
                </div>
    <section class="">
        <div class="row">
            <div class="col-sm-1">
            </div>
            <div class="col-sm-5">
            <table class="table table-hover">
                <tbody>
                    <tr>
                      <td class="text-left" width="20%">Name</td>
                      <td class="text-left" width="80%"><?= @$info->first_name .' '. @$info->last_name ?></td>
                        </tr>
                      <tr>
                          <td class="text-left" width="20%">Role</td>
                          <td class="text-left" width="80%">Manager</td>
                      </tr>
                      
                      <tr>
                          <td class="text-left" width="20%">Email</td>
                          <td class="text-left" width="80%"><?= @$info->email ?></td>
                      </tr>
                      <tr>
                          <td class="text-left" width="20%">Status</td>
                          <td class="text-left" width="80%"><?= @$info->admin_status ?></td>
                      </tr>
                </tbody>
            </table> 
            </div>  
        </div>
    </section>
            <div class="box-body">
                <form class="form-horizontal" id="crud_form" role="form" method="post" name="crud_form"
                                  action="<?= base_url(ROOT_DIR_ADMIN . ADMIN_C .'admin_update/'.@$info->admin_id) ?>">
                    <!-- /.box-header -->
                    <!-- <div class="box-body"> -->
                        <div class="form-group">
                                <?php
                                $ids = array();
                                if (@$info->shop_id != '') {
                                    $ids = explode(",", @$info->shop_id);
                                }
                                ?>
                                <?php echo form_label('Shops' . span_asterisk(), 'shop_id', "class='col-sm-2 control-label'"); ?>
                                <div class="col-sm-6">
                                    <?php echo form_multiselect('shop_ids[]', @$list_shop, set_value('shop_id', @$ids), array('class' => 'form-control select2', 'id' => 'shop_id', 'data-placeholder' => "Select a Shops")); ?>
                                </div>
                                <div class="col-sm-2">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="select_all_shops" id="select_all_shops">
                                            All Shops
                                        </label>
                                    </div>
                                </div>
                        </div>
                        <div class="form-group">
                                <label for="inputlbl" class="col-sm-2 control-label"></label>
                                <div class="col-sm-6">
                                    <button type="submit" class="btn btn-success">Save</button>
                                    <a href="<?php echo $list_url; ?>">
                                        <button type="button" class="btn btn-default">Cancel</button>
                                    </a>
                                </div>
                        </div>
                    <!-- /.box-body -->
                     <!-- </div> -->
                </form>
            <!-- /.box -->
            </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<script>
        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2();
        });
</script>
<script>
    $(document).ready(function () {
        $("#crud_form").validate({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                error.insertAfter(element);
                // for other inputs, just perform default behavior
            },
            ignore: "",
            rules: {
                'shop_ids[]': {
                    required: true
                }
            },
            messages: {
                'shop_ids[]': {
                    required: 'Please Select Shops.'
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                //successHandler1.hide();
                //errorHandler1.show();
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            },
            submitHandler: function (frmadd) {
                successHandler1.show();
                errorHandler1.hide();
            }
        });
    });
    
    $('#select_all_shops').click(function() {
        if($("#select_all_shops").is(':checked') ){
            $("#shop_id > option").prop("selected","selected");
            $("#shop_id").trigger("change");
        }else{
            $("#shop_id > option").removeAttr("selected");
            $("#shop_id").trigger("change");
        }
    });

</script>
