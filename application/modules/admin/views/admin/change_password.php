<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><strong><?=@$level_two?></strong></h3>
                    <span class="pull-right text-muted">Mandatory information is marked with an asterisk <span class="text-red">*</span></span>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php _notify(); ?>
                    <form class="form-horizontal" name="crud_form" id="crud_form" method="post" >
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputlbl" class="col-sm-2 control-label">Current Password <span class="text-red">*</span></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="current_password"  name="current_password" placeholder="Enter Current Password" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputlbl" class="col-sm-2 control-label">New Password<span class="text-red">*</span></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control"  id="new_password"  name="new_password" placeholder="Enter New Password"  value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputlbl" class="col-sm-2 control-label">Confirm New Password<span class="text-red">*</span></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control"  id="new_confirm_password"  name="new_confirm_password" placeholder="Repeat Password" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputlbl" class="col-sm-2 control-label"></label>
                                <div class="col-sm-6">
                                    <button type="submit" class="btn btn-success ">Change Password</button>
                                    <a href="<?php echo base_url(ROOT_DIR_ADMIN.DASHBOARD_C); ?>">
                                    <button type="button" class="btn btn-default">Cancel</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                </div>
                <!-- /.box-footer -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->

<script>
    $(document).ready(function () {
        $("#crud_form").validate({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                error.insertAfter(element);
                // for other inputs, just perform default behavior
            },
            ignore: "",
            rules: {
                current_password: {
                    required: true
                },
                new_password: {
                    required: true
                },
                new_confirm_password: {
                    required: true,
                    equalTo: new_password
                }
            },
            messages: {
                current_password: {
                    required: 'Please Enter Current Password.',
                    remote:"This email address is already registered."
                },
                new_password: {
                    required: 'Please Enter New Password.'
                },
                new_confirm_password: {
                    required: 'Please Enter Confirm Password.',
                    equalTo: "Password Do Not Match."
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                //successHandler1.hide();
                //errorHandler1.show();
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            },
            submitHandler: function (frmadd) {
                successHandler1.show();
                errorHandler1.hide();
            }
        });
    });
</script>