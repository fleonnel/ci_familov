<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*  * CodeIgniter Admin Librabry
 *  * @author Backend Brains
 *  Vikrant : 8000255245
 *  Vishal : 9033966266
 */
class Admin {

	protected $CI;
	private $adminData = null;

    /**
     * Class constructor
     */
    public function __construct() {

    	$this->CI = &get_instance();
        $this->adminData = $this->CI->session->userdata(SESSION_NAME);
    }

    /**
     * Check Admin Session
     * @return boolean
     */
    public function isOnline() {
        if ($this->getData()) {
            if ($this->getData()->admin_id > 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Getting Staff Data
     * @return object
     */
    public function getData() {
        return $this->adminData;
    }

    /**
     * Set Admin Model Resource
     * @param $adminData
     */
    public function setData($adminData) {
        $this->adminData = $adminData;
    }

    /**
     * Set Staff Model Resource to Session
     */
    public function login() {
        $session[SESSION_NAME] = $this->adminData;
	    $session[SESSION_NAME]->is_master_admin_login = TRUE;
	    $session[SESSION_NAME]->is_admin_login = TRUE;
        $this->session->set_userdata($session);
    }

    /**
     * Remove Staff Model Resource From Session
     */
    public function logout() {
        $this->session->unset_userdata(SESSION_NAME);
        $this->response->redirect('/admin/');
    }

}