<?php

/**
 * Nav Elements Plugins
 * Helps to build navigation for the Admin section
 * @author Backend Brains
 *  Vikrant : 8000255245
 *  Vishal : 9033966266
 */


class Nav {

    protected $CI;
    /**
     * Top Account Menu
     * @var type array
     */
    private $accountMenu = array();
    private $accountMenuKey = array();

    /**
     * Class constructor
     */
    public function __construct() {

        $this->CI = &get_instance();
        $this->CI->load->model('admin/acl_model');
        $this->setAccountMenu();
    }

    private function setAccountMenu() {
         $this->accountMenu = array(
            'dashboard' => array(
                'title' => 'Dashboard',
                'link' => base_url('admin/dashboard/home'),
                'controller' => 'dashboard',
                'icon' => '<i class="fa fa-dashboard text-yellow"></i>',
            ),
            'admin' => array(
                'title' => 'Admin/Manager',
                'link' => base_url('admin/admin/lists'),
                'controller' => 'admin',
                'icon' => '<i class="fa fa-user-secret text-red"></i>',
            ),
            // 'department' => array(
            //     'title' => 'Department',
            //     'link' => base_url('admin/department/lists'),
            //     'controller' => 'department',
            //     'icon' => '<i class="fa fa-circle-o text-green"></i>',
            // ),
            // 'roles' => array(
            //     'title' => 'Designation (Roles)',
            //     'link' => base_url('admin/designations/lists'),
            //     'controller' => 'designations',
            //     'icon' => '<i class="fa fa-circle-o text-green"></i>',
            // ),
            
            'country' => array(
                'title' => 'Country',
                'link' => base_url('admin/country/lists'),
                'controller' => 'country',
                'icon' => '<i class="fa fa-globe"></i>',
            ),
            'city' => array(
                'title' => 'City',
                'link' => base_url('admin/city/lists'),
                'controller' => 'city',
                'icon' => '<i class="fa fa-map-marker"></i>',
            ),
            'shop' => array(
                'title' => 'Shops',
                'link' => base_url('admin/shop/lists'),
                'controller' => 'shop',
                'icon' => '<i class="fa fa-building-o"></i>',
            ),
            'categories' => array(
                'title' => 'Categories',
                'link' => base_url('admin/category/lists'),
                'controller' => 'category',
                'icon' => '<i class="fa  fa-bars"></i>',
            ),
            'product' => array(
                'title' => 'Products',
                'link' => base_url('admin/product/lists'),
                'controller' => 'product',
                'icon' => '<i class="fa fa-database"></i>',
            ),
            'customer' => array(
                'title' => 'Customer',
                'link' => base_url('admin/customer/lists'),
                'controller' => 'customer',
                'icon' => '<i class="fa fa-users"></i>',
            ),
            'order' => array(
                'title' => 'Orders',
                'link' => base_url('admin/order/lists'),
                'controller' => 'order',
                'icon' => '<i class="fa fa-shopping-cart"></i>',
            ),
            'promotions' => array(
                'title' => 'Promotions',
                'link' => base_url('admin/promotion/lists'),
                'controller' => 'promotion',
                'icon' => '<i class="fa fa-bullhorn"></i>'
            ),
            'currency' => array(
                'title' => 'Currency',
                'link' => base_url('admin/currency/lists'),
                'controller' => 'currency',
                'icon' => '<i class="fa fa-euro"></i>'
            ),
            // 'label' => array(
            //     'title' => 'Label',
            //     'link' => base_url('admin/label/lists'),
            //     'controller' => 'label',
            //     'icon' => '<i class="fa fa-language"></i>'
            // ),
            'banner' => array(
                'title' => 'Banner',
                'link' => base_url('admin/banner/lists'),
                'controller' => 'banner',
                'icon' => '<i class="fa fa-image"></i>'
            ),
            'email_template' => array(
                'title' => 'Email Templates',
                'link' => base_url('admin/email_template/lists'),
                'controller' => 'email_template',
                'icon' => '<i class="fa fa-envelope-o"></i>'
            ),
           /*  'request_section' => array(
                 'title' => 'Request Management',
                 'link' => base_url('admin/request_section/lists'),
                 'controller' => 'request_section',
                 'icon' => '<i class="fa fa-circle text-yellow"></i>',
                 'child' => array(
                     'request_section_list' => array(
                         'title' => 'Request Listing',
                         'link' => base_url('admin/request_section/lists'),
                         'controller' => 'request_section',
                         'method' => 'lists',
                         'icon' => '<i class="fa fa-circle-o text-yellow"></i>'
                     ),
                     'request_section_add' => array(
                         'title' => 'Request Add',
                         'link' => base_url('admin/request_section/add'),
                         'controller' => 'request_section',
                         'method' => 'add',
                         'icon' => '<i class="fa fa-circle-o text-yellow"></i>'
                     ),
                     'request_section_close' => array(
                         'title' => 'Request Close',
                         'link' => base_url('admin/request_section/close_lists'),
                         'controller' => 'request_section',
                         'method' => 'close_lists',
                         'icon' => '<i class="fa fa-circle-o text-success"></i>'
                     ),
                     'request_section_complete' => array(
                         'title' => 'Request Complete',
                         'link' => base_url('admin/request_section/complete_lists'),
                         'controller' => 'request_section',
                         'method' => 'complete_lists',
                         'icon' => '<i class="fa fa-circle-o text-green"></i>'
                     )
                 )
             ),*/
            'access_control' => array(
                'title' => 'Access Control',
                'link' => base_url('admin/permission/add_update'),
                'controller' => 'permission',
                'icon' => '<i class="fa fa-unlock-alt text-red"></i>',
            ),
             'control_management' => array(
                 'title' => 'Control Management',
                 'link' => '#',
                 'controller' => 'setting',
                 'icon' => '<i class="fa fa-gears text-gray"></i>',
                 'child' => array(

                     'setting' => array(
                         'title' => 'Setting',
                         'link' => base_url('admin/setting/lists'),
                         'controller' => 'setting',
                         'icon' => '<i class="fa fa-cog text-grey"></i>'
                     )
                 )
             ),
        );
    }

    /**
     * Account Menu
     * @return string
     */
    public function getAccountMenu()
    {
        global $menuHtml;
        $this->accountMenuKey = array_keys($this->accountMenu);
        $this->getMenuRecursion($this->accountMenu);
        return $menuHtml;
    }

    /**
     * Callback recursive menu function
     */
    private function getMenuRecursion($mainMenu) {
        global $menuHtml;
        $menuHtml .= '<ul class="sidebar-menu">';
        foreach ($mainMenu as $menuKey => $menu) {
            if ($this->CI->acl_model->isAccess($menu['controller'], 'view') || $menu['controller'] == '#') {
                $main_li_class = isset($menu['child']) && count($menu['child']) > 0 ?'treeview':'';
                $currentControllerName = $this->CI->router->fetch_class();
                $currentMethodName = $this->CI->router->fetch_method();
                $active_class = $currentControllerName == $menu['controller'] && $this->CI->acl_model->isAccess($currentControllerName, 'view')?'active':'';
                $active_method = $currentMethodName == @$menu['method'] && $this->CI->acl_model->isAccess($currentControllerName, 'view') ? 'active' : '';
                $submenuHtml = "";
                $submenu_open_icon = "";
                if(isset($menu['child']) && count($menu['child'])) {
                    $submenu_open_icon = '<span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span>';
                    $submenuHtml .= '<ul class="treeview-menu">';
                    foreach ($menu['child'] as $submenu) {
                        $sub_active_class = "";
                        if ($this->CI->acl_model->isAccess($submenu['controller'], 'view')) {
                            if ($currentControllerName == $submenu['controller'] && $currentMethodName == $submenu['method'] && $this->CI->acl_model->isAccess($this->CI->router->fetch_class(), 'view')) {
                                $sub_active_class = "active";
                                $active_class = "active";
                            }
                            $submenuHtml .= '<li class="'.$sub_active_class.'">';
                            $submenuHtml .= '<a href="'.$submenu['link'].'">'.$submenu["icon"].$submenu["title"].'</a>';
                            $submenuHtml .= '</li>';
                        }
                    }
                    $submenuHtml .= '</ul>';
                }
                $menuHtml .= '<li class="'.$active_class.'"> <a href="'.$menu["link"].'">'.$menu["icon"].' <span>'.$menu["title"].'</span>'.$submenu_open_icon.' </a>'.$submenuHtml.' </li>';
            }
        }
        $menuHtml .= '</ul>';
    }

}
