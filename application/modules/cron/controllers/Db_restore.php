<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Db_restore extends Cron_Controller
{
    var $tb_customer_old = 'customer_old';
    var $tb_customer_new = 'customers';
    var $tb_order = 'order_leo';
    var $tb_order_item = 'order_items_leo';
    var $primary_key = '';
    var $dir = '';

    public function __construct()
    {
        parent::__construct();
        $this->dir = 'cron/';
    }

    public function index()
    {
        _e();
        #Customer - Done
        #Product - Done
        #Order
        #Order Item
        #todo : for order check online Order, orders and order_item


        # Admin Table.
        # categories    - Live Table
        # city          - Live Table
        # country       - Live Table
        # currency      - Live Table
        # customers     - Live Data and new table structure
        # email_template - New Table
        # language      #TODO Remaining
        # product       - New Table schema and old data. [ALTER TABLE `products_leo` ADD `product_availability` ENUM('AVAILABLE','NOT AVAILABLE') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'AVAILABLE' AFTER `product_status`;]
        # product_price_currency    - Not used
        # promocode     - Live Table
        # referall_log  - Live Table
        # referrels     - Live Table
        # shops         - New Table Schema and Olda data. [ALTER TABLE `shops_leo` ADD `shop_status` ENUM('PUBLISHED','UNPUBLISHED') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'PUBLISHED' AFTER `home_delivery_status`;]
    }

    public function admin_restore()
    {

        $admin_list = $this->dbcommon->getInfo_('select * from admin_leo', 1);
        #_pre($admin_list);
        foreach ($admin_list as $k => $v) {
            $roles_id = 3;
            if ($v->admin_type == 'ADMIN') {
                if ($k == 0) {
                    $roles_id = 1;
                } else {
                    $roles_id = 2;
                }
            }
            $email = $v->uname . '@' . $v->uname . '.com';
            $crud_data = array(
                'email' => $email,
                'password' => md5($v->password),
                'user_name' => $v->uname,
                'first_name' => $v->uname,
                'last_name' => $v->uname,
                'admin_type_id' => $roles_id,
                'roles_id' => $roles_id,
                'admin_status' => 'Active',
            );
            $insert_response = $this->dbcommon->insert('admin', $crud_data);
            $insert_id = $this->db->insert_id();

        }
    }
    public function customer_restore()
    {

        #$customer_list = $this->dbcommon->getInfo_('select * from ' . $this->tb_customer_new . " WHERE customer_id = '2276' ORDER BY customer_id DESC", 1);
        $customer_list = $this->dbcommon->getInfo_('select * from ' . $this->tb_customer_new . "  ORDER BY customer_id DESC", 1);

        foreach ($customer_list as $k => $v) {
            if ($v->referral == '') {
                $v->referral = uniqid("referral", true);
                _e("myReferral =>" . $v->referral, 0);
            }
            $phone_no = $v->sender_phone;
            $update_data = array(
                'sender_phone' => $phone_no,//$v->phone_number,
                'if_vendor' => $v->if_vendor,
                'username' => $v->username,
                'lastname' => $v->lastname,
                'recp_name' => $v->recp_name,
                'password' => md5($v->password),
                'email_address' => $v->email_address,
                'home_address' => $v->home_address,
                'phone_number' => $phone_no,//$v->phone_number,
                'city_id' => $v->city_id,
                'country_id' => $v->country_id,
                'shop_name' => $v->shop_name,
                'shop_address' => $v->shop_address,
                'shop_logo' => $v->shop_logo,
                'shop_banner' => $v->shop_banner,
                'status' => $v->status,
                'referral' => $v->referral,
                'maturity_status' => $v->maturity_status,
                'iGift' => $v->iGift,
                'eUsed' => $v->eUsed,
                'myReferral' => $v->myReferral,
                'totalReferral' => $v->totalReferral,
                'dob' => $v->dob,
                'notification_email' => $v->notification_email,
                'notification_text' => $v->notification_text,
                'country_code' => $v->country_code,
                'home_gender' => $v->home_gender,
                'postal_code' => $v->postal_code,
                'phone_code' => $v->phone_code,
                'stripe_id' => $v->stripe_id,
                'pwd_token' => '',
            );
            $customer_id = $v->customer_id;
            $where = array('customer_id' => $customer_id);
            _e('Customer_id => ' . $customer_id, 0);
            $update_response = $this->dbcommon->update($this->tb_customer_new, $where, $update_data);
            _e('Update Response => ' . $update_response, 0);
        }
        # _pre($customer_list);
    }

    /****
     * Order Table:
     * 1. Get Order Id From the Order Table
     * 2. By this order id fetch order records from the order items. (2 decimal Point)
     * 3. Calculate Sub total based on Order Items total (2 decimal Point)
     * 4. Service Fees, delivery fees (2 decimal Point)
     * 5. Find Promo code amount and promo code using promo code Id (2 decimal Point)
     * 6. Calculate the grand total. (2 decimal Point)
     * #7. find payment currency code
     * 8.  Total_order and sub_total should be same
     * 9. Greeting msg and receiver name with add_slshes
     * 10. generate_code Follow the pattern
     * Ex. Existing one : F-XXX   (XXX should be uppercase, may be contain digit also.)

     */

    public function migrate_orders($limit = 10, $start)
    {
        $list_orders = $this->dbcommon->getInfo_('select * from ' . $this->tb_order . "  ORDER BY order_id  DESC LIMIT " . $start . ", " . $limit, 1);
#_pre($list_orders);
        foreach ($list_orders as $k => $v) {
            $list_order_item = $this->dbcommon->getInfo_("select * from " . $this->tb_order_item . " WHERE order_id = " . $v->order_id . "  ORDER BY order_id  DESC  ", 1);
            $cart_amount_total = 0;
            if (count($list_order_item) > 0) {
                foreach ($list_order_item as $ok => $ov) {
                    #$tot = $ov->product_prize * quantity;
                    $tot = $ov->total_prize;
                    $cart_amount_total += $tot;
                }
            } else {
                $cart_amount_total = $v->total_order;
            }
            $promo_code = '';
            $promo_discount_amount = 0;
            $temp_dis_amount = 0;
            if ($v->promo_id > 0) {
                $promo_info = $this->dbcommon->getInfo_("select * from promocode WHERE  promo_id = $v->promo_id LIMIT 1");
                if (count($promo_info) > 0) {
                    $promo_code = $promo_info->vCode;
                    $temp_dis_amount = $promo_info->iAmount;
                }
                #_pre($promo_info, 0);
            }
            #_pre($promo_info);
            $grand_total = 0;
            if ($v->discount > 0) {
                $promo_discount_amount = $v->discount;
            } else {
                $promo_discount_amount = $temp_dis_amount;
            }
            $promo_discount_amount = convert_price_restore($promo_discount_amount, $v->payment_curr);
            #64.39
            #@$v->delivery_charges= 0;
            $grand_total = ($cart_amount_total + convert_price_restore($v->service_tax, $v->payment_curr) + convert_price_restore(@$v->delivery_price, $v->payment_curr)) - ($promo_discount_amount);
            #_pre($cart_amount_total,0 );
            #_pre(" ST=>".$v->service_tax,0 );
            #_pre("Dcharge => ".@$v->delivery_price,0 );
            #_pre("Disc => ".@$promo_discount_amount,0 );
            #_pre($grand_total,0 );
            #_pre($list_order_item );
            $order_array = '';
            $curr_code = '$';
            if ($v->payment_curr == 'EUR') {
                $curr_code = '&euro;';
            }

            $order_array = array(
                'generate_code' => ORDER_TEXT . strtoupper($v->generate_code),
                #'receiver_name' => addslashes($this->input->post('recp_name')),
                #'greeting_msg' => addslashes($this->input->post('greeting_msg')),

                #'payment_curr' => $this->currency_code,
                'payment_curr_code' => $curr_code,
                'total_order' => $cart_amount_total,
                'sub_total' => ($cart_amount_total),
                #'delivery_charges' => convert_price(@$delivery_info->delivery_price),
                'promo_discount_amount' => $promo_discount_amount,//convert_price(@$promo_code->promo_code_amount),
                'grand_total' => ($grand_total),
                'promo_code' => $promo_code //@$promo_code->promo_code,
            );
            _e("Order ID => " . $v->order_id, 0);
            $where = array('order_id' => $v->order_id);
            $update_response = $this->dbcommon->update($this->tb_order, $where, $order_array);
            #_pre($order_array, 0);
        }

    }
}


