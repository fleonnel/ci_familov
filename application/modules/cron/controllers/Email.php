<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Email extends Cron_Controller
{
    var $table = '';
    var $primary_key = '';
    var $dir = '';
    public function __construct()
    {
        parent::__construct();
        $this->dir = 'cron/';
    }

    public function index()
    {

    }

    public function order_mail_send()
    {
        $mail_list = $this->dbcommon->getInfo_("select * from  mail_list WHERE  send_status =0 ORDER BY id ASC LIMIT 1", 1);
        #_pre($mail_list);
        $this->load->library('mail');
        foreach ($mail_list as $k => $v) {
            $to = $v->to_id;
            $name = $v->name;

            $this->mail->setTo($to, $name);
            #$this->mail->setCc(ADMIN_EMAIL, 'Familov');
            $this->mail->setSubject($v->subject);
            $this->mail->setBody(base64_decode($v->body));
            $status = $this->mail->send();
            #_e($status." =>".$to,0);
            $mail_array1 = array(
                'send_status' => 1,
            );
            $where = array('id' => $v->id);
            $update_response = $this->dbcommon->update('mail_list', $where, $mail_array1);
        }
    }


}


