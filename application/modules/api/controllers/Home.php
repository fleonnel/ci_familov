<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends API_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->decodeJSONData();
        $this->load->model("country_model");
        $this->load->model("city_model");
        $this->load->model("shop_model");
        $this->load->model("banner_model");
    }

    public function home_get()
    {
        $response_country = $this->country_model->get_country_list();
        $response_banner = $this->banner_model->get_banner_list();
        $data['country_list'] = (!empty($response_country)) ? $response_country : _array_to_obj(array());
        $data['city_list'] = _array_to_obj(array());
        $data['shop_list'] = _array_to_obj(array());
        $banner = _array_to_obj(array());
        if (!empty($response_banner)) {
            foreach ($response_banner as $k => $v) {
                $banner->$k = DIR_BANNER_URL . @$v->banner_image;
            }
        }
        $data['banner'] = (!empty($banner)) ? $banner : _array_to_obj(array());
        $data = [
            'status' => TRUE,
            'data' => $data,
        ];
        $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
    }

    public function city_get()
    {
        $country_id = $this->db->escape_str(trim($this->input->get('country_id')));
        if ($country_id == '' || $country_id <= 0) {
            $this->set_response([
                'status' => FALSE,
                'message' => $this->lang->line('MSG_BAD_REQUEST'),
            ], REST_Controller::HTTP_OK);
        } else {
            $response_country = $this->city_model->get_city_list_by_country($country_id);
            $data['city_list'] = (!empty($response_country)) ? $response_country : _array_to_obj(array());
            $data = [
                'status' => TRUE,
                'data' => $data,
            ];
            $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
    }

    public function shop_get()
    {
        $city_id = $this->db->escape_str(trim($this->input->get('city_id')));
        if ($city_id == '' || $city_id <= 0) {
            $this->set_response([
                'status' => FALSE,
                'message' => $this->lang->line('MSG_BAD_REQUEST'),
            ], REST_Controller::HTTP_OK);
        } else {
            $response_shop = $this->shop_model->get_shop_list_by_city($city_id);
            // $shop_list = _array_to_obj(array());
            $shop_list = array();
            if (!empty($response_shop)) {
                foreach ($response_shop as $k => $v) {
                    // $shop_list->$k->shop_id = $v->shop_id;
                    // $shop_list->$k->shop_name = $v->shop_name;
                    $list['shop_id'] = $v->shop_id;
                    $list['shop_name'] = $v->shop_name;
                    $shop_list[] = $list;
                }
            }
            $data['shop_list'] = (!empty($response_shop)) ? $shop_list : _array_to_obj(array());
            $data = [
                'status' => TRUE,
                'data' => $data,
            ];
            $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
    }

    public function get_currency_get()
    {
        $str = "select * from currency where eDefault='Yes'";
        $response_currency = $this->db->query($str)->row();
        $data['currency_data'] = (!empty($response_currency)) ? $response_currency : _array_to_obj(array());
        $data = [
            'status' => TRUE,
            'data' => $data,
        ];
        $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
    }

}

?>