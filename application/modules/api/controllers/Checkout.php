<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Checkout extends API_Controller
{
    private $perPage = 12;
    private $perPageimage = 1;
    private $start_page = 1;

    public function __construct()
    {
        parent::__construct();
        $this->decodeJSONData();
        $this->load->model("category_model");
        $this->load->model("order_model");
        $this->load->model("promotion_model");
    }

    public function shop_delivery_option_get()
    {
        $shop_id = $this->db->escape_str(trim($this->input->get('shop_id')));
        $currency = $this->db->escape_str(trim($this->input->get('currency')));
        if ($shop_id == '' || $shop_id <= 0 || $currency == '' || $currency == null) {
            $this->set_response([
                'status' => FALSE,
                'message' => $this->lang->line('MSG_BAD_REQUEST'),
            ], REST_Controller::HTTP_BAD_REQUEST);
        } else {
            $response_shop = $this->shop_model->get_info_by_id($shop_id);
            if (!empty($response_shop)) {
                $response_shop->pickup_from_shop_price = convert_price_app($response_shop->pickup_from_shop_price, $currency);
                $response_shop->home_delivery_price = convert_price_app($response_shop->home_delivery_price, $currency);
            }
            $data['shop_info'] = (!empty($response_shop)) ? $response_shop : _array_to_obj(array());
            $data['shop_obj'] = _array_to_obj(array());
            $data['shop_array'] = (array()); #TODO check object and array.
            $data['currency'] = $currency; #TODO check object and array.
            $data = [
                'status' => TRUE,
                'data' => $data,
            ];
            $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
    }

    public function order_promo_code_post()
    {
        $this->form_validation->set_rules('promo_code', 'promo_code', 'trim|required');
        $this->form_validation->set_rules('cart_amount_total', 'cart_amount_total', 'trim|required');
        $this->form_validation->set_rules('currency', 'currency', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $form_errors = _array_to_obj($this->form_validation->error_array());
            $this->set_response([
                'status' => FALSE,
                'message' => $form_errors,
            ], REST_Controller::HTTP_OK);
        } else {
            $promo_code = $this->input->post('promo_code');
            $cart_amount_total = $this->input->post('cart_amount_total');
            $currency = $this->db->escape_str(trim($this->input->post('currency')));
            $promo_response = $this->promotion_model->get_info_by_code($promo_code);
            if (empty($promo_response)) {
                $data = [
                    'status' => FALSE,
                    'message' => $this->lang->line('MSG_PRODUCT_CHECKOUT_11')
                ];
                $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            } else {
                #$cart_amount_total = $cart_amount_total + SERVICE_FEES + @$delivery_info->delivery_price; #TODO add all charges
                #$cart_amount_total =  convert_price_app($cart_amount_total, $currency);//convert_price($cart_amount_total);
                $amount = $promo_response->iAmount;
                $amount = convert_price_app($amount, $currency);
                if ($cart_amount_total < $amount) {
                    $data['promo_info']['code'] = $promo_code;
                    $data['promo_info']['promo_id'] = '';
                    $data['promo_info']['promo_code_amount'] = 0;
                    $data = [
                        'status' => FALSE,
                        'data' => $data,
                        'message' => $this->lang->line('MSG_PRODUCT_CHECKOUT_13')
                    ];
                    $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                } else {
                    $data['promo_info']['code'] = $promo_response->vCode;
                    $data['promo_info']['promo_id'] = $promo_response->promo_id;
                    $data['promo_info']['promo_code_amount'] = $promo_response->iAmount;
                    $data = [
                        'status' => true,
                        'data' => $data,
                        'message' => $this->lang->line('MSG_PRODUCT_CHECKOUT_12')

                    ];
                    $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                }
            }
            #_pre($promo_response);
        }
    }

    public function checkout_proceed_post()
    {
        _pre($_REQUEST);
        $shop_id = $this->db->escape_str(trim($this->input->get('shop_id')));
        $currency = $this->db->escape_str(trim($this->input->get('currency')));
        if ($shop_id == '' || $shop_id <= 0 || $currency == '' || $currency == null) {
            $this->set_response([
                'status' => FALSE,
                'message' => $this->lang->line('MSG_BAD_REQUEST'),
            ], REST_Controller::HTTP_BAD_REQUEST);
        } else {
            _e();
            /*$response_shop = $this->shop_model->get_info_by_id($shop_id);
            if (!empty($response_shop)) {
                $response_shop->pickup_from_shop_price = convert_price_app($response_shop->pickup_from_shop_price, $currency);
                $response_shop->home_delivery_price = convert_price_app($response_shop->home_delivery_price, $currency);
            }
            $data['shop_info'] = (!empty($response_shop)) ? $response_shop : _array_to_obj(array());
            $data['shop_obj'] = _array_to_obj(array());
            $data['shop_array'] = (array()); #TODO check object and array.
            $data['currency'] = $currency; #TODO check object and array.*/
            $data = [
                'status' => TRUE,
                'data' => $data,
            ];
            $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
    }
    /*public function checkout_proceed_post()
    {
        $this->form_validation->set_rules('promo_code', 'promo_code', 'trim|required');
        $this->form_validation->set_rules('cart_amount_total', 'cart_amount_total', 'trim|required');
        $this->form_validation->set_rules('currency', 'currency', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $form_errors = _array_to_obj($this->form_validation->error_array());
            $this->set_response([
                'status' => FALSE,
                'message' => $form_errors,
            ], REST_Controller::HTTP_OK);
        } else {
            #$cart_amount_total = $this->input->post('cart_amount_total');
            #$currency = $this->db->escape_str(trim($this->input->post('currency')));
        }
    }*/
}

?>