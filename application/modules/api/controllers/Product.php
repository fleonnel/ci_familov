<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Product extends API_Controller
{
    private $perPage = 12;
    private $perPageimage = 1;
    private $start_page = 1;

    public function __construct()
    {
        parent::__construct();
        $this->decodeJSONData();
        $this->load->model("category_model");
        $this->load->model("order_model");
    }

    public function product_list_get()
    {
        $country_id = $this->db->escape_str(trim($this->input->get('country_id')));
        $city_id = $this->db->escape_str(trim($this->input->get('city_id')));
        $shop_id = $this->db->escape_str(trim($this->input->get('shop_id')));
        $currency = $this->db->escape_str(trim($this->input->get('currency')));
        if ($country_id == '' || $city_id == '' || $shop_id == '' || $currency == '' || $country_id <= 0 || $city_id <= 0 || $shop_id <= 0) {
            $this->set_response([
                'status' => FALSE,
                'message' => $this->lang->line('MSG_BAD_REQUEST'),
            ], REST_Controller::HTTP_BAD_REQUEST);
        } else {
            $search_keyword = $this->db->escape_str(trim($this->input->get('search_keyword')));
            $category_id = $this->db->escape_str(trim($this->input->get('category_id')));
            $page = $this->db->escape_str(trim($this->input->get('page')));
            $currency = $this->db->escape_str(trim($this->input->get('currency')));
            $product_list_array = new stdClass();// array() ;//_array_to_obj(array());
            if ($page != '') {
                if ($page > 1) {
                    $start = ceil($page * $this->perPage);
                } else {
                    $start = 0;
                }
                $product_list = $this->product_model->product_list_shop_wise($shop_id, $category_id, $search_keyword, $this->perPage, $start);
            } else {
                $product_list = $this->product_model->product_list_shop_wise($shop_id, $category_id, $search_keyword);
            }
            $all_total_product = count($this->product_model->product_list_shop_wise($shop_id, $category_id, $search_keyword));
            if (!empty($product_list)) {
                foreach ($product_list as $k => $v) {
                    $discount = 0.0;
                    if (@$v->special_product_prices > 0) {
                        $discount = get_percentage(convert_price_app(@$v->product_prices, $currency), convert_price_app(@$v->special_product_prices, $currency)) . "%";
                        $v->discount = $discount;
                    }
                    @$v->discount = $discount;
                    #  @$v->product_desc = strip_tags($v->product_desc);
                    @$v->convert_product_prices = convert_price_app(@$v->product_prices, $currency);
                    @$v->convert_special_product_prices = convert_price_app(@$v->special_product_prices, $currency);
                    @$v->product_image = DIR_PRODUCT_URL . $v->product_image;
                    $product_list_array->$k = @$v;
                }
            }
            $response_shop = $this->shop_model->get_info_by_id($shop_id);
            $category_response = $this->category_model->get_category_list_shop_wise($shop_id);
            foreach ($category_response as $kc => $vc) {
                @$vc->category_image = DIR_CATEGORY_URL . $vc->category_image;
                $category_response[$kc] = @$vc;
            }
            foreach ($response_shop as $ks => $vs) {
                $response_shop->shop_logo = DIR_SHOP_URL . @$vs->shop_logo;
                #$response_shop->shop_banner = DIR_SHOP_URL.$vs->shop_banner;
            }
            $data['currency'] = $currency;
            $data['total_products'] = count($product_list);
            $data['all_total_products'] = $all_total_product;
            $data['search_keyword'] = $search_keyword;
            $data['category_id'] = $category_id;
            $data['shop_info'] = (!empty($response_shop)) ? $response_shop : _array_to_obj(array());
            $data['category_list'] = (!empty($category_response)) ? $category_response : _array_to_obj(array());
            $data['product_list'] = (!empty($product_list)) ? $product_list : _array_to_obj(array());
            $data = [
                'status' => TRUE,
                'data' => $data,
            ];
            $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
    }

    public function order_list_get()
    {
        $customer_id = $this->db->escape_str(trim($this->input->get('customer_id')));
        if ($customer_id == '' || $customer_id <= 0) {
            $this->set_response([
                'status' => FALSE,
                'message' => $this->lang->line('MSG_BAD_REQUEST'),
            ], REST_Controller::HTTP_BAD_REQUEST);
        } else {
            $customer_response = $this->customer_model->get_info_by_id($customer_id);
            if (!empty($customer_response)) {
                $order_list = $this->order_model->get_order_list($customer_id);
                if (!empty($customer_response)) {
                    foreach ($order_list as $k => $v) {
                        $v->amount = $v->payment_curr_code . ' ' . $v->grand_total;
                        $v->date_time = date_time($v->date_time, 7);
                        $order_list[$k] = @$v;
                    }
                    unset($customer_response->password);
                    $data['customer_info'] = $customer_response;
                    $data['total_orders'] = count($order_list);
                    $data['order_list'] = $order_list;
                    $data = [
                        'status' => TRUE,
                        'data' => $data,
                    ];
                    $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                } else {
                    $data = [
                        'status' => FALSE,
                        'message' => $this->lang->line('MSG_ORDER_NOT_FOUND')
                    ];
                    $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                }
            } else {
                $data = [
                    'status' => FALSE,
                    'message' => $this->lang->line('MSG_USER_NOT_FOUND')
                ];
                $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }

        }
    }

    public function order_detail_by_id_get()
    {

        $customer_id = $this->db->escape_str(trim($this->input->get('customer_id')));
        $order_id = $this->db->escape_str(trim($this->input->get('order_id')));
        if ($customer_id == '' || $customer_id <= 0 || $order_id == '' || $order_id <= 0) {
            $this->set_response([
                'status' => FALSE,
                'message' => $this->lang->line('MSG_BAD_REQUEST'),
            ], REST_Controller::HTTP_BAD_REQUEST);
        } else {
            $order_response = $this->order_model->get_order_by_id($customer_id, $order_id);
            $order_detail_array = array();
            if (!empty($order_response)) {
                $order_detail_response = $this->order_model->get_order_detail_by_id($customer_id, $order_id);
                if (!empty($order_detail_response)) {
                    $order_response->amount = $order_response->payment_curr_code . ' ' . $order_response->grand_total;
                    $order_response->date_time = date_time($order_response->date_time, 7);

                    foreach ($order_detail_response as $k => $v) {
                        $order_detail_response[$k]->product_image = DIR_PRODUCT_URL . $v->product_image;
                    }
                    $data['order_info'] = $order_response;
                    $data['order_items'] = $order_detail_response;
                    $data = [
                        'status' => TRUE,
                        'data' => $data,
                    ];
                    $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                } else {
                    $data = [
                        'status' => FALSE,
                        'message' => $this->lang->line('MSG_ORDER_DETAIL_NOT_FOUND')
                    ];
                    $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                }

            } else {
                $data = [
                    'status' => FALSE,
                    'message' => $this->lang->line('MSG_ORDER_NOT_FOUND')
                ];
                $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
        }
    }

}

?>