<?php

// Test Api Controller
class Token extends API_Controller
{

    function __construct()
    {
        parent::__construct();

    }

    function token_get()
    {
        #$data['id'] = $this->get('id');
        $Data['key'] = $this->encodeJSONData();
        $Data['status'] = STATUS_TRUE;
        $this->response($Data);
    }

    function key_get()
    {
        #$data['id'] = $this->get('id');
        $Data['key'] = $this->encodeJSONData();
        $Data['status'] = STATUS_TRUE;
        $this->response($Data);
    }

    function token_post()
    {
        $this->decodeJSONData();
        $Data['tokenData'] = $this->getTokenData();
        $Data['status'] = STATUS_TRUE;
        $this->response($Data);
    }
}

?>