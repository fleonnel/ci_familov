$(document).ready(function(){

$('#currentpass,#newpass,#retypepass').bind("cut copy paste",function(e) {
                e.preventDefault();
});

$("#currentpass").change(function(){

        $('.submitbtn').attr('disabled', true);
    var old_pass = $(this).val();

                    $.ajax({
                        url: 'getoldpasscheck',
                        data: { old_pass:old_pass },
                        type: 'POST',
                        success: function(data)
                        {   

                            var data = $.parseJSON(data);

                            if(data.response>0)
                            {   
                                $('.submitbtn').attr('disabled', false);
                                $("#currentpass").removeClass("emailtxt"); 
                            }
                            else
                            {   
                                $("#currentpass").addClass("emailtxt");
                                $('.submitbtn').attr('disabled', true);
                            }

                        }
                    });

    });

 /* $("#email").change(function(){

        $('.submitbtn').attr('disabled', true);
    var email = $(this).val();


                    $.ajax({
                        url: 'getcheckemailadd',
                        data: { email:email },
                        type: 'POST',
                        success: function(data)
                        {   

                            var data = $.parseJSON(data);


                            if(data.response>0)
                            {   
                                $('.submitbtn').attr('disabled', true);
                            }
                            else
                            {   
                                $('.submitbtn').attr('disabled', false); 
                                
                            }

                        }
                    });

    });*/


$('#changepassword').bootstrapValidator({
        message:'This value is not valid',
        feedbackIcons: {
          valid: 'glyphicon glyphicon-ok',
         // invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
      },
fields: {
    currentpass: {
          validators: {
              notEmpty: {
                  message: 'The current password is required'
              }
          }
    },
    newpass: {
          validators: {
              notEmpty: {
                  message: 'The password is required'
              }
          }
    },
    retypepass: {
          validators: {
              identical: {
                field: 'newpass',
                message: 'The password and its confirm are not the same'
              },
              notEmpty: {
                message: 'Confirm Password id is required'
              }
          }
    }
}

});



$('#respasswordForm').bootstrapValidator({
        message:'This value is not valid',
        feedbackIcons: {
          valid: 'glyphicon glyphicon-ok',
        //  invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
      },
fields: {

    newpass: {
          validators: {
              notEmpty: {
                  message: 'The password is required'
              }
          }
    },
    retypepass: {
          validators: {
              identical: {
                field: 'newpass',
                message: 'The password and its confirm are not the same'
              },
              notEmpty: {
                message: 'Confirm Password id is required'
              }
          }
    }
}

});

$("#forgotpassform").bootstrapValidator({
        message:'This value is not valid',
        feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        validating: 'glyphicon glyphicon-refresh'
      },
fields: {

    email: {
          validators: {
              notEmpty: {
                  message: 'Email id is required'
              },
            emailAddress: {
                        message: 'The value is not a valid email address'
                    }
          }
    }
}

});

$("#userForm").bootstrapValidator({
        message:'This value is not valid',
        feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        validating: 'glyphicon glyphicon-refresh'
      },
fields: {

    username: {
          validators: {
              notEmpty: {
                  message: 'Email id is required'
              },
            emailAddress: {
                        message: 'The value is not a valid email address'
                    }
          }
    },
    password: {
          validators: {
              notEmpty: {
                message: 'Password id is required'
              }
          }
    }
}

});


/* Agent Add validation start

$("#addagents").bootstrapValidator({
        message:'This value is not valid',
        feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        validating: 'glyphicon glyphicon-refresh'
      },
fields: {
    
    first_name: {
          validators: {
              notEmpty: {
                  message: 'First name is required'
              }
          }
    }, email: {
          validators: {
              notEmpty: {
                  message: 'Email id is required'
              },emailAddress: {
                        message: 'The value is not a valid email address'
                    }
          }
    },
    confirm_email: {
          validators: {
              identical: {
                field: 'email',
                message: 'The password and its confirm are not the same'
              },
              notEmpty: {
                message: 'Confirm Email id is required'
              },emailAddress: {
                        message: 'The value is not a valid email address'
                    }
          }
    },phone: {
        validators: {
        notEmpty: {
                message: 'Please enter your phone no.'
            },
        digits: {
                message: 'The phone number is not valid'
            },
            stringLength: {
                min: 10,
                max: 12,
                message: 'The phone no. must be 10 digits'
            }
        }
    },business_phone: {
        validators: {
       
        digits: {
                message: 'The business phone number is not valid'
            },
            stringLength: {
                min: 10,
                max: 12,
                message: 'The business phone no. must be 10 digits'
            }
        }
    },twitter_url: {
        validators: {
       
        uri:{
                message: 'Please Enter Valid Url.'
            }
        }
    },facebook_url: {
        validators: {
       
        uri:{
                message: 'Please Enter Valid Url.'
            }
        }
    },linked_url: {
        validators: {
       
        uri:{
                message: 'Please Enter Valid Url.'
            }
        }
    },video_url1: {
        validators: {
       
        uri:{
                message: 'Please Enter Valid Url.'
            }
        }
    },video_url2: {
        validators: {
       
        uri:{
                message: 'Please Enter Valid Url.'
            }
        }
    },video_url3: {
        validators: {
       
        uri:{
                message: 'Please Enter Valid Url.'
            }
        }
    },jud: {
        validators: {
        notEmpty: {
                message: 'Please enter your phone no.'
            }
        }
    }
}

});

/* Agent Add validation ends */

/* Agent Edit Validation start  

$("#editagents").bootstrapValidator({
        message:'This value is not valid',
        feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        validating: 'glyphicon glyphicon-refresh'
      },
fields: {
    
    first_name: {
          validators: {
              notEmpty: {
                  message: 'First name is required'
              }
          }
    }, email: {
          validators: {
              notEmpty: {
                  message: 'Email id is required'
              },emailAddress: {
                        message: 'The value is not a valid email address'
                    }
          }
    },
    confirm_email: {
          validators: {
              identical: {
                field: 'email',
                message: 'The password and its confirm are not the same'
              },
              notEmpty: {
                message: 'Confirm Email id is required'
              },emailAddress: {
                        message: 'The value is not a valid email address'
                    }
          }
    },phone: {
        validators: {
        notEmpty: {
                message: 'Please enter your phone no.'
            },
        digits: {
                message: 'The phone number is not valid'
            },
            stringLength: {
                min: 10,
                max: 12,
                message: 'The phone no. must be 10 digits'
            }
        }
    },business_phone: {
        validators: {
       
        digits: {
                message: 'The business phone number is not valid'
            },
            stringLength: {
                min: 10,
                max: 12,
                message: 'The business phone no. must be 10 digits'
            }
        }
    },twitter_url: {
        validators: {
       
        uri:{
                message: 'Please Enter Valid Url.'
            }
        }
    },facebook_url: {
        validators: {
       
        uri:{
                message: 'Please Enter Valid Url.'
            }
        }
    },linked_url: {
        validators: {
       
        uri:{
                message: 'Please Enter Valid Url.'
            }
        }
    },video_url1: {
        validators: {
       
        uri:{
                message: 'Please Enter Valid Url.'
            }
        }
    },video_url2: {
        validators: {
       
        uri:{
                message: 'Please Enter Valid Url.'
            }
        }
    },video_url3: {
        validators: {
       
        uri:{
                message: 'Please Enter Valid Url.'
            }
        }
    },jud: {
        validators: {
        notEmpty: {
                message: 'Please enter your phone no.'
            }
        }
    }
}

});

/* Agent Edit Validation Ends */


});
